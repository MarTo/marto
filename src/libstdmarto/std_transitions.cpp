#include "std_transitions.h"

#define DEBUG_REGISTER

#ifdef DEBUG_REGISTER
#include <iostream>
#endif

namespace marto::autoregister::transitions {

extern "C" {
void initTransitionLibrary(Simulation *config) {
#ifdef DEBUG_REGISTER
    std::cerr << "Main registration" << std::endl;
#endif
    marto::Registry<marto::stdlib::TransitionRegistrationInfo>::apply(config);
}
}
}; // namespace marto::autoregister::transitions
