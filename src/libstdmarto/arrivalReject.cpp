#include "std_transitions.h"

namespace marto::stdlib {

namespace {                             // do not pollute marto namespace
template <class Kind, class Parameters> //
class TransitionParams {
  public:
    Parameter<Kind, TransitionParams, marto::queue_id_t> to;
    TransitionParams(Parameters *params) : to(params, "to" /* TODO 2, 2 */){};
};
}; // namespace

class ArrivalReject
    : public is_std_transition<ArrivalReject, transition::is_monotonic,
                               transition::with_parameters<TransitionParams>> {
    marto_stdlib_transition_setup(ArrivalReject);

  public:
    Point *apply(Point *p, const EventState *ev) const {
        // We only get vectors of values as defined in the
        // configuration file
        auto param = ev->effectiveTransitionParameters(this);
        auto to = param->to[0]; // the only random destination queue

        if (!p->at(to)->isFull()) {
            p->at(to)->addClient();
        }
        return p;
    }
};

}; // namespace marto::stdlib
