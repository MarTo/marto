/* -*-c++-*- C++ mode for emacs */
/* header for adding transitions in the default MarTo transition library */
#ifndef MARTO_STD_TRANSITIONS_H
#define MARTO_STD_TRANSITIONS_H

#include <list>
#include <marto.h>
#include <marto/autoregister.h>
#include <string>

namespace marto::stdlib {

class TransitionRegistrationInfo {
  public:
    typedef void register_function(std::string);
    typedef void apply_function(Simulation *);
    template <class TRegistered> struct Register {
        void operator()(std::string name, Simulation *config) {
            std::cerr << "Registering " << name << std::endl;
            marto::new_shared<TRegistered>(std::move(name), *config);
        }
    };
};

template <class Self>
class TransitionAutoRegistration
    : public AutoRegister<TransitionRegistrationInfo, Self> {
/** Allows one to easily force the autoregistration */
#define marto_stdlib_transition_autoregister(name) marto_autoregister(name)
};

namespace transition {

// namespace import, for better readability when defining transitions
using collect::is_monotonic;
using collect::with_parameters;

} // namespace transition

template <typename T, typename... TCol>
using is_std_transition =
    typename marto::transition::bits::is_transition_helper<
        T, TransitionAutoRegistration<T>, TCol...>::type;

/** Allows one to easily inherit Transition constructor(s) */
#define marto_stdlib_transition_setup(Self)                                    \
    using transition_class::transition_class;                                  \
    using transition_class::apply;                                             \
    marto_stdlib_transition_autoregister(#Self)

}; // namespace marto::stdlib

#endif
