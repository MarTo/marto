#include "std_transitions.h"

namespace marto::stdlib {

namespace {                             // do not pollute marto namespace
template <class Kind, class Parameters> //
class TransitionParams {
  public:
    Parameter<Kind, TransitionParams, marto::queue_id_t> from;
    Parameter<Kind, TransitionParams, marto::queue_id_t> to;
    TransitionParams(Parameters *params)
        : from(params, "from" /* TODO 2, 2 */), to(params, "to"){};
};
}; // namespace

class JSQ2
    : public is_std_transition<JSQ2, transition::is_monotonic,
                               transition::with_parameters<TransitionParams>> {
    marto_stdlib_transition_setup(JSQ2);

    Point *apply(Point *p,
                 const EventState *ev) const { // Event ev contains transition
                                               // specification (parameters)
        // fromList is a random sequence of Queues (specified in ev)
        // to prevent access to random generation and protect
        // monotonicity marto::ParameterValues<marto::Queue> as
        // defined in the configuration file auto *toList =
        // ev->getParameter("to");

        // auto from = fromList->get<queue_id_t>(0); // the only source queue
        // parameters.getFrom(ev);
        auto param = ev->effectiveTransitionParameters(this);
        auto from = param->from[0];
        auto &to = param->to;
        auto to0 = to[0];
        auto to1 = to[1];
        // auto to0 =
        //    toList->get<queue_id_t>(0); // the first random destination queue
        // auto to1 =
        //    toList->get<queue_id_t>(1); // second random destination queue

        if (!p->at(from)->isEmpty()) {
            if (p->at(to0)->compareTo(p->at(to1)) > 0)
                p->at(to1)->addClient();
            else
                p->at(to0)->addClient();
            p->at(from)->removeClient();
        }
        return p;
    }
};

}; // namespace marto::stdlib
