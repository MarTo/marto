#include "std_transitions.h"

namespace marto::stdlib {

namespace {                             // do not pollute marto namespace
template <class Kind, class Parameters> //
class TransitionParams {
  public:
    Parameter<Kind, TransitionParams, marto::queue_id_t> from;
    TransitionParams(Parameters *params)
        : from(params, "from" /* TODO 2, 2 */){};
};
}; // namespace

/** Departure of a single client
 *  from a single queue
 *  Client can be dropped or equivalently sent to "outside queue"
 */
class Departure
    : public is_std_transition<Departure, transition::is_monotonic,
                               transition::with_parameters<TransitionParams>> {
    marto_stdlib_transition_setup(Departure);

    Point *apply(Point *p, const EventState *ev) const {
        auto param = ev->effectiveTransitionParameters(this);
        auto from = param->from[0]; // the only (?) source queue

        if (!p->at(from)->isEmpty()) {
            p->at(from)->removeClient();
        }
        return p;
    }
};

}; // namespace marto::stdlib
