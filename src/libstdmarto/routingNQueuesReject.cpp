#include "std_transitions.h"

namespace marto::stdlib {

namespace {                             // do not pollute marto namespace
template <class Kind, class Parameters> //
class TransitionParams {
  public:
    Parameter<Kind, TransitionParams, marto::queue_id_t> from;
    Parameter<Kind, TransitionParams, marto::queue_id_t> to;
    TransitionParams(Parameters *params)
        : from(params, "from" /* TODO 2, 2 */), to(params, "to"){};
};
}; // namespace

/**
 * \brief      Routing n queues from origin queue to destination queues
 * \details    Remove 1 packet from each origin queue and send to destination
 *             queues. Reject if destination queues are full.
 */

class routingNQueuesReject
    : public is_std_transition<routingNQueuesReject, transition::is_monotonic,
                               transition::with_parameters<TransitionParams>> {
    marto_stdlib_transition_setup(routingNQueuesReject);

    Point *apply(Point *p, const EventState *ev) const {
        auto param = ev->effectiveTransitionParameters(this);
        auto &from = param->from;
        auto &to = param->to;

        int count = 0;
        for (unsigned int i = 0; i < from.count(); ++i) {
            auto index = from[i];
            if (!p->at(index)->isEmpty()) {
                p->at(index)->removeClient();
                count++;
            }
        }

        for (unsigned int i = 0; i < to.count(); ++i) {
            auto index = to[i];
            while (!p->at(index)->isFull() && count > 0) {
                p->at(index)->addClient();
                count--;
            }
        }

        return p;
    }
};

}; // namespace marto::stdlib
