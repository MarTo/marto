%module marto
/* import all needed SWIG module */
%include <std_shared_ptr.i>
%include <std_string.i>
%include <std_vector.i>
%include <stdint.i>
%include <stl.i>

/* code added in the generated C++ wrapper */
%{
#include <marto.h>
%}

/* make swig itself (but not the C/C++ compiler) ignore attributes */
#define __attribute__(x)

/* features in order to avoid unwanted calls of C++ destructors */
%feature("ref") marto::Point ""
%feature("unref") marto::Point ""

/* create a template in order to handle std::vector<std::string> datatype */
%template(VectorString) std::vector<std::string>;
/* required to correctly handle marto::Point::value_type */
%template(VectorQueue) std::vector<std::shared_ptr<marto::QueueState>>;


/* a parameter is something that is not ... (better way to define this?) */
%define %$isparameter  %$not %$isvariable, %$not %$isfunction, %$not %$isclass, %$not %$isconstructor, %$not %$isunion, %$not %$istemplate   %enddef

/* use snake_case naming style */
%rename("%(command:perl -e 'foreach (@ARGV) { s/.*::// ; s/([A-Z])/_\\l\\1/g ; print  $_, \"\\n\" }' )s", regextarget=1, fullname=1, %$isfunction) "marto::Event(|Type|State)::.*";
%rename("%(command:perl -e 'foreach (@ARGV) { s/.*::// ; s/([A-Z])/_\\l\\1/g ; print  $_, \"\\n\" }' )s", regextarget=1, fullname=1, %$isfunction) "marto::(StateLess|Standard|)Queue::.*";
%rename("%(command:perl -e 'foreach (@ARGV) { s/.*::// ; s/([A-Z])/_\\l\\1/g ; print  $_, \"\\n\" }' )s", regextarget=1, fullname=1, %$isfunction) "marto::QueueState::.*";
%rename("%(command:perl -e 'foreach (@ARGV) { s/.*::// ; s/([A-Z])/_\\l\\1/g ; print  $_, \"\\n\" }' )s", regextarget=1, fullname=1, %$isfunction) "marto::Simulation::.*";
%rename("%(command:perl -e 'foreach (@ARGV) { s/.*::// ; s/([A-Z])/_\\l\\1/g ; print  $_, \"\\n\" }' )s", regextarget=1, fullname=1, %$isfunction) "marto::RefCounted::.*";

%rename("param_%s", regextarget=1, %$isparameter) "^..?$";

/* rename paramaters whose name conflicts with built-in names in python */
%rename("_%s", %$isparameter) type;
%rename("_%s", %$isparameter) id;
/* rename paramaters to snake case */
%rename("parameter_name", %$isparameter) parameterName;
%rename("actual_values", %$isparameter) actualValues;
%rename("event_name", %$isparameter) eventName;
%rename("event_rate", %$isparameter) evtRate;
%rename("transition_name", %$isparameter) trName;
/* rename methods that conflicts with built-in names in python */
%rename("get_%s", %$isfunction) type;
%rename("get_%s", %$isfunction) min;
%rename("get_%s", %$isfunction) max;
%rename("get_%s", %$isfunction) id;
%rename("do_%s", %$isfunction) apply;
%rename("do_%s", %$isfunction) compile;
/* rename methods for homogeneity */
%rename("get_%s", %$isfunction) capacity;
%rename("get_%s", %$isfunction) name;
%rename("get_%s", %$isfunction) rate;
%rename("get_%(undercase)s", %$isfunction) transitionName;
/* rename class for the special case queue_id_t */
%rename("QueueID", %$isclass) queue_id_t;


/* all needed headers for the SWIG module */
%import "marto/forward-decl.h"
%import "marto/macros.h"
%include "marto/memory.h"
%shared_ptr(marto::EventType)
%shared_ptr(marto::EventState)
%shared_ptr(marto::Event)
%shared_ptr(marto::Queue)
%shared_ptr(marto::StateLessQueue)
%shared_ptr(marto::OutsideQueue)
%shared_ptr(marto::Transition)
%shared_ptr(marto::Simulation)
%shared_ptr(marto::StandardQueue)
%shared_ptr(marto::SymbolicParameter)
%shared_ptr(marto::SymbolicRealParameter)
%shared_ptr(marto::SymbolicConstantParameter< marto::queue_id_t >)

 //%import "marto/collections.h"
%import "marto/types.h"
/* fake class to workaround for SWIG the real class defined with a BOOST macro */
class marto::queue_id_t {};

/************************************************/
/* support to create objects with marto::new_shared<>(...) mecanism */
%define new_shared(classname, params, call)
%newobject classname::new;
%extend classname {
    static std::shared_ptr<classname> new params {
        return marto::new_shared<classname> call;
    }
}
%enddef

/* must be before new_shared() */
%pythonappend marto::Simulation::new() %{
    val.load_transition_library()
%}
new_shared(marto::Simulation, (), ())
new_shared(marto::StandardQueue, (std::string name, std::shared_ptr<marto::Simulation> s, int v), (std::move(name), *s, v))
new_shared(marto::OutsideQueue, (std::string name, std::shared_ptr<marto::Simulation> s), (std::move(name), *s))
new_shared(marto::SymbolicConstantParameter<marto::queue_id_t>, (std::vector<marto::queue_id_t> vect), (std::move(vect)))
/************************************************/

%include "marto/simulation.h"
%include "marto/symbolic-parameters.h"
%include "marto/event.h"
%include "marto/queue.h"
/* ignore protected inheritence of marto::Point */
%warnfilter(309) marto::Point; /* does not work, global suppression is the only way :-( */
#pragma SWIG nowarn=309
%include "marto/state.h"
%include "marto/symbolic-parameters-library.h"

/* create a template in order to use SymbolicConstantParameter  */
namespace std {
    %template(VectorQueueId) vector<marto::queue_id_t>;
}
/* template for SymbolicConstantParameter */
%template(SymbolicConstantParameterMartoQueueId) marto::SymbolicConstantParameter<marto::queue_id_t>;

/* add some python code in order to no longer depend on Event.py */
%extend marto::EventType{
%pythoncode %{
    #Method that will allow to add parameters in the current Event object.
    def add_constant_parameter(self, parameter_name, *queues):

        tab_id_queue = []
        for queue in queues:
            idx=None
            if isinstance(queue, Queue):
                print("Queue")
                idx = queue.get_id()
            if isinstance(queue, QueueID):
                print("QueueID")
                idx = queue
            elif isinstance(queue, int):
                print("int")
                # TODO: check bounds
                idx = QueueID(queue)
            elif isinstance(queue, str):
                print("str")
                idx = self.config().get_queue(queue).get_id()

            if idx is not None:
                tab_id_queue.append(idx)
            else:
                raise Exception("Cannot convert "+queue+" to a Queue")

        #print(tab_id_queue)
        self.register_parameter(parameter_name, SymbolicConstantParameterMartoQueueId.new(tab_id_queue))
    %}
}

/* queue_id_t is defined by BOOST_STRONG_TYPEDEF(uint32_t, queue_id_t) in C++
 *
 * Here, we create explicit converters
 */
/* overload name for python */
%rename(__int__) marto::queue_id_t::operator uint32_t;
/* add the C functions to do the conversion */
%{
uint32_t marto_queue_id_t_operator_SS_uint32_t(marto::queue_id_t *qid) {
    return (long)(*qid);
}
marto::queue_id_t *new_marto_queue_id_t(uint32_t val) {
    marto::queue_id_t *res = new(marto::queue_id_t);
    *res=val;
    return res;
}
%}
/* add a fake method */
%extend marto::queue_id_t{
    queue_id_t(uint32_t);
    operator uint32_t();
}
/* queue_id_t done */

%extend marto::Simulation{
    %pythoncode %{
    #Configuration side
    #add_queue must return the created Event object, so that we can manipulate it later
    def add_queue(self, capacity, name=None):
        #Allows to add a queue to the queue collector.
        if name is None:
            #We do an automatic naming of the object like : Q0,Q1,Q2...
            queue = StandardQueue.new("Q" + str(self.get_queue_size()+1), self, capacity)
        else:
            queue = StandardQueue.new(name, self, capacity)
        return queue

    #addEvent must return the created Event object, so that we can add parameters to it.
    def add_event_type(self, name, rate, ev_type):
        #Allows to add a event to the event collector.
        event = EventType.make(self, name, rate, ev_type)
        return event

     #Method that will create an object Point from the Configuration.
    def create_point(self):
        # FIXME: allows one to create point with specific state (i.e. use callbacks)
        return Point(self)

    #Method that will create a HyperRectangle object from two Point object.
    @classmethod
    def create_hyper_rectangle(cls, p_min, p_max):
        return HyperRectangle(p_min, p_max)

    #Experience Side
    #Method that will allow to choose the parameter with which the experiment will be launched.
    #def set_parameter(self, seed, print_model=1, print_param=1, print_simulation=1, time_output_filename=None):
    #    self.__seed                 = seed
    #    self.__print_model          = print_model
    #    self.__print_param          = print_param
    #    self.__print_simulation     = print_simulation
    #    self.__time_output_filename = time_output_filename

    #Method that will allow to choose the method with which the experiment will be launched.
    #def set_method(self, method, sample_number, antithetic, doubling, trajectory_max, split_threshold,
    #               init_memory_length=None, initial_state=None, num_threads=None, num_trajectory=None):
    #    self.__method             = method
    #    self.__sample_number      = sample_number
    #    self.__antithetic         = antithetic
    #    self.__doubling           = doubling
    #    self.__trajectory_max     = trajectory_max
    #    self.__split_threshold    = split_threshold
    #    self.__init_memory_length = init_memory_length
    #    self.__initial_state      = initial_state
    #    self.__num_threads        = num_threads
    #    self.__num_trajectory     = num_trajectory
    %}
}

