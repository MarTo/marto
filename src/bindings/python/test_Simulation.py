from marto import Simulation
from nose.tools import assert_true, assert_equal

#Testing for the creation of a Simulation object.
c=Simulation.new()

#Testing to retrieve the list of all transition,event and queue.
q1 = c.add_queue(10,"queue1")
q2 = c.add_queue(2,"queue2")
ev1 = c.add_event_type("Arrival MM1",0.5,"ArrivalReject")
ev2 = c.add_event_type("Departure MM1",1,"Departure")

def test_object_creations():
    '''Testing for the creation of objects
    '''
    assert_true(c)
    assert_true(q1)
    assert_true(q2)
    assert_true(ev1)
    assert_true(ev2)

def test_collections():
    '''Testing registered collections
    '''
    transition_names = c.get_transition_names()
    assert_equal(len(transition_names), 4)
    event_names = c.get_event_names()
    assert_equal(event_names, ("Arrival MM1", "Departure MM1"))
    queue_names = c.get_queue_names()
    assert_equal(queue_names, ("queue1", "queue2"))

def test_points():
    '''Testing the creation of two Point objects and one HyperRectangle object.
    '''
    point1 = c.create_point()
    point2 = c.create_point()
    #hr1 = c.createHyperRectangle(p1,p2)
    assert_true(point1)
    assert_true(point2)

if __name__ == "__main__":
    import nose
    nose.runmodule()
