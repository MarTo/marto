from marto import Simulation
from nose.tools import assert_true, assert_equal#, assert_raises

c=Simulation.new()
q1 = c.add_queue(5)
q2 = c.add_queue(10,"queue2")
q2b = c.get_queue("queue2")
def test_object_creations():
    ''' Testing for the creation of objects
    '''
    assert_true(c)
    assert_true(q1)
    assert_true(q2)

def test_bad_creation():
    '''Testing failing creation
    '''
    #assert_raises(TypeError, lambda: c.add_queue(7, "queue2"))

def test_queue_getters():
    '''Testing the getters for the Queue object.
    '''
    assert_equal(q1.get_min(),0)
    assert_equal(q1.get_max(), 5)
    assert_equal(q1.get_capacity(), 5)
    assert_equal(q1.get_name(), "Q1")
    assert_equal(int(q1.get_id()), 0)
    #Testing the naming of a Queue object.
    assert_equal(q2.get_name(),"queue2")

def test_queue_retrieval():
    '''Testing the getQueue method from simulation object.
    '''
    q2b.get_id()
    assert_equal(q2b.get_min(),0)
    assert_equal(q2b.get_max(), 10)

def test_queue_collection():
    ''' Checking all queue names
    '''
    assert_equal(c.get_queue_names(), ("Q1", "queue2"))

def test_queue_derivated():
    ''' Checking we correctly get derivated type
    '''
    assert_equal(q2.get_capacity(), 10)

#Not to fix, but must find a way to make it true
#assert_equal(q2,q2b)
def test_queue_names():
    ''' Checking various queue names
    '''
    assert_equal(q2.get_name(), q2b.get_name())
    assert_true(q1.get_name() != q2.get_name())

if __name__ == "__main__":
    import nose
    nose.runmodule()
