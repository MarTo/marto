from marto import Simulation, QueueID
from nose.tools import assert_true, assert_equal

c=Simulation.new()
e1 = c.add_event_type("Arrival MM1", 1, "ArrivalReject")
e2 = c.get_event_type("Arrival MM1")

def test_object_creations():
    ''' Testing for the creation of objects
    '''
    assert_true(c)
    assert_true(e1)
    assert_true(e2)

def test_event_getters():
    '''Testing the Getters for the Event object.
    '''
    assert_equal(e1.get_name(), "Arrival MM1")
    assert_equal(e1.get_rate(), 1)
    assert_equal(e1.get_transition_name(), "ArrivalReject")

def test_get_event_type():
    '''Testing the getEvent method from Simulation object.
    '''
    assert_equal(e2.get_name(), "Arrival MM1")
    assert_equal(e2.get_rate(), 1)
    assert_equal(e2.get_transition_name(), "ArrivalReject")
    assert_equal(e2.get_name(), e1.get_name())
    #Not to fix, but must find a way to make it true
    #assert_equal(e1,e2)

q1 = c.add_queue(20, name="queue1")
q2 = c.add_queue(10)
def test_queue_names():
    '''Testing queue avaibility
    '''
    assert_equal(c.get_queue_names(), ("Q2", "queue1"))

def test_add_constant_parameter():
    '''Testing the addConstantParameter method, the simple way.
    '''
    #Testing adding a parameter from e1...
    e1.add_constant_parameter("from", q1, q2)
    #...and from e2.
    e2.add_constant_parameter("to", QueueID(0), QueueID(1))
    #Testing various correct parameters
    e2.add_constant_parameter("test_param", 0, q1, "queue1", 1, q2, "Q2")
    # Both should fail ("marto::ExistingName") but exceptions are not handled yet
    #e2.add_constant_parameter("from", q1, q2)
    #e1.add_constant_parameter("to", 0, 1)
    #Testing the addConstantParameter method, with the name of queue.
    c.add_queue(2, "queue3")
    e1.add_constant_parameter("bypass", "queue3")

if __name__ == "__main__":
    import nose
    nose.runmodule()
