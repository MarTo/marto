#include "test_transition.h"
#include "gtest/gtest.h"
#include <marto.h>
#include <stdexcept>

namespace {

using namespace marto;

TEST(Transitions, RegisterTransition) {
    auto c = new_shared<Simulation>();
    Transition::pointer tr =
        new_shared<TransitionTest>("TransitionTestDupName", *c);
    ASSERT_NE(tr, nullptr);
    ASSERT_THROW(new_shared<TransitionTest>("TransitionTestDupName", *c),
                 ExistingName);
}

TEST(Transitions, LibTransition) {
    auto c1 = new_shared<Simulation>();
    auto c2 = new_shared<Simulation>();
    /* loading the default library in c1 */
    c1->loadTransitionLibrary();
    /* Checking a transition is really here */
    auto t1 = c1->getTransition("ArrivalReject");
    ASSERT_NE(t1, nullptr);
    ASSERT_TRUE(t1->name() == "ArrivalReject");
    /* Checking that we cannot load the same library twice... */
    ASSERT_THROW(c1->loadTransitionLibrary(), ExistingName);
    /* ...unless in another configuration */
    c2->loadTransitionLibrary();
    /* Checking the transition is really here... */
    auto t2 = c2->getTransition("ArrivalReject");
    ASSERT_NE(t2, nullptr);
    ASSERT_TRUE(t2->name() == "ArrivalReject");
    /* ...but not the same as the one from the first config */
    ASSERT_NE(t1, t2);
}

TEST(EventTypes, RegisterEventTypeWithUnknownTransition) {
    auto c = new_shared<Simulation>();
    new_shared<TransitionTest>("TransitionTest", *c);
    try {
        // we should generate a UnknownName exception
        EventType::make(c.get(), "My super event", 42.0,
                        "UnknownTransitionForTest");
        // if not, we check for the behavior of getTransition...
        ASSERT_THROW(c->getTransition("UnknownTransitionForTest"), UnknownName)
            << "Transition 'UnknownTransitionForTest' should not exist";
        // ..before reporting the failure
        FAIL() << "EventType successfully created with an unknown transition";
    } catch (const UnknownName &e) {
        // expected path
        SUCCEED();
    } catch (...) {
        // unexpected exception type
        FAIL() << "EventType creation generated an unknown exception";
        throw;
    }
}

TEST(EventTypes, RegisterEventTypeTwice) {
    auto c = new_shared<Simulation>();
    new_shared<TransitionTest>("TransitionTest", *c);
    ASSERT_TRUE(
        EventType::make(c.get(), "My super event", 42.0, "TransitionTest"));
    // we cannot create a new EventType with the same name
    ASSERT_THROW(
        EventType::make(c.get(), "My super event", 42.0, "TransitionTest"),
        ExistingName);
    // but we can create an EventType with another name and the same transition
    ASSERT_TRUE(
        EventType::make(c.get(), "My new super event", 42.0, "TransitionTest"));
}

TEST(Queues, CreateQueueSimulations) {
    auto c = new_shared<Simulation>();
    int capacity = 10;
    auto q = new_shared<StandardQueue>("Q1", *c, capacity);
    ASSERT_EQ(q, c->getQueue("Q1"));
    ASSERT_EQ(false, q->hasNoState());
    /* we cannot create another queue in the same config with the same name */
    ASSERT_THROW(new_shared<StandardQueue>("Q1", *c, capacity), ExistingName);
    // but we can create a Queue with another name and the same capacity
    ASSERT_TRUE(new_shared<StandardQueue>("my new queue", *c, capacity));
    // and we can create a Queue with another name and another capacity
    ASSERT_TRUE(new_shared<StandardQueue>("Q3", *c, 2 * capacity + 1));
    /* we can create a dummy queue for the external world */
    auto qout = new_shared<OutsideQueue>("Outside", *c);
    ASSERT_EQ(true, qout->hasNoState());
    /* we can create another dummy queue with another name */
    new_shared<OutsideQueue>("Other Outside", *c);
    /* we cannot create another dummy queue with existing name */
    ASSERT_THROW(new_shared<OutsideQueue>("Outside", *c), ExistingName);
    ASSERT_THROW(new_shared<StandardQueue>("Q4", *c, -1),
                 std::invalid_argument);
    auto queuesNames = c->getQueueNames();
    size_t i = 0;
    for (auto it : queuesNames) {
        std::cout << i++ << ": " << it << std::endl;
    }
    ASSERT_EQ(queuesNames.size(), 5);
    /* names are sorted */
    ASSERT_TRUE(queuesNames[0] == "Other Outside");
    ASSERT_TRUE(queuesNames[1] == "Outside");
    ASSERT_TRUE(queuesNames[2] == "Q1");
    ASSERT_TRUE(queuesNames[3] == "Q3");
    ASSERT_TRUE(queuesNames[4] == "my new queue");
}

} // namespace
