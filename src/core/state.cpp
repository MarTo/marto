#include <marto.h>
#include <vector>

namespace marto {

std::ostream &operator<<(std::ostream &out, const marto::Point &p) {
    out << "Point(";
    bool first = true;
    for (const auto &q : p) {
        if (first) {
            first = false;
        } else {
            out << ", ";
        }
        out << q->value();
    }
    out << ")";
    return out;
}

Point::Point(Simulation *const config) : WithSimulation(config) {
    resize(config->map_of<Queue>::size());
    size_t i = 0;
    auto &queuesVect = config->map_of<Queue>::as_vector();
    for (auto qc = queuesVect.begin(); qc < queuesVect.end(); qc++, i++) {
        std::vector<value_type>::at(i) = (*qc)->newQueueState();
    }
}

Point::Point(Simulation *const config, queue_value_t value)
    : Point(config, [value](queue_id_t __marto_unused(id),
                            shared_ptr<Queue> __marto_unused(qc),
                            Point *__marto_unused(p)) { return value; }) {}

Point::Point(Simulation *const config, Point::initCallback_t *f, void *arg)
    : Point(config, [arg, f](queue_id_t id, shared_ptr<Queue> qc, Point *p) {
          return f(id, qc, p, arg);
      }) {}

Point::Point(Simulation *const config, const Point::initLambdaCallback_t &f)
    : WithSimulation(config) {
    resize(config->map_of<Queue>::size());
    size_t i = 0;
    auto &queuesVect = config->map_of<Queue>::as_vector();
    for (auto qc = queuesVect.begin(); qc < queuesVect.end(); qc++, i++) {
        if ((*qc)->hasNoState()) {
            // No callback for state-less queue
            // TODO: should probably even not stored into the Point object...
            std::vector<std::shared_ptr<QueueState>>::at(i) =
                (*qc)->newQueueState();
        } else {
            std::vector<std::shared_ptr<QueueState>>::at(i) =
                (*qc)->newQueueState(f((queue_id_t)i, *qc, this));
        }
    }
}

// Specialisation: a Point always gives a Point
Point *Point::accept(const Transition *t, const EventState *ev) {
    return t->apply(this, ev);
}

SetImpl *HyperRectangle::accept(const Transition *t, const EventState *ev) {
    return t->apply(this, ev);
}

SetImpl *Union::accept(const Transition *t, const EventState *ev) {
    return t->apply(this, ev);
}
} // namespace marto
