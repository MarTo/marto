/* -*-c++-*- C++ mode for emacs */
/* Transition used in various tests */
#ifndef MARTO_TRANSITION_TEST_H
#define MARTO_TRANSITION_TEST_H

#include <marto/state.h>
#include <marto/transition.h>

template <class Kind, class Parameters> //
class TransitionTestParameters {
  public:
    marto::Parameter<Kind, TransitionTestParameters, marto::queue_id_t> from;
    marto::Parameter<Kind, TransitionTestParameters, marto::queue_id_t,
                     marto::MartoTypeStandard>
        to;
    // ParamList<Kind, queue_id_t, 2, 2> to;
    TransitionTestParameters(Parameters *params)
        : from(params, "from"), to(params, "to" /* TODO 2, 2 */){};
};

class TransitionTest
    : public marto::is_transition<
          TransitionTest, marto::collect::is_monotonic,
          marto::collect::with_parameters<TransitionTestParameters>> {

    // using transition_class::transition_class;
    using transition_class::apply;
    marto::Point *apply(marto::Point *p, __attribute__((unused))
                                         const marto::EventState *ev) const {
        for (auto q = p->begin(); q < p->end(); q++)
            (*q)->addClient(1);
        return p;
    }
};

#endif
