/* -*-c++-*- C++ mode for emacs */
#ifndef MARTO_H
#define MARTO_H

/* do not include implementations when including headers until the end */
#define MARTO_HEADER_NEED_IMPL

#include <marto/collections.h>
#include <marto/event.h>
#include <marto/except.h>
#include <marto/history-stream.h>
#include <marto/history.h>
#include <marto/macros.h>
#include <marto/memory.h>
#include <marto/queue.h>
#include <marto/random.h>
#include <marto/simulation.h>
#include <marto/state.h>
#include <marto/symbolic-parameters-library.h>
#include <marto/transition.h>
#include <marto/types.h>

/* include all implementation headers */
#include <marto/headers-impl.h>

#endif
