#include <marto.h>

namespace marto {

class StandardQueueState : public TypedQueueState<StandardQueue> {
  protected:
    queue_value_t _value;

  private:
    friend StandardQueue;
    StandardQueueState(StandardQueue *c)
        : TypedQueueState<StandardQueue>(c), _value(0) {
    } // queue empty at creation by default
    StandardQueueState(StandardQueue *c, queue_value_t v)
        : TypedQueueState<StandardQueue>(c), _value(0) {
        if (v < 0 || v > capacity()) {
            throw std::out_of_range("queue state");
        }
        _value = v;
    }

  public:
    queue_value_t capacity() const { return realQueue()->capacity(); }
    virtual queue_value_t value() const { return _value; }
    virtual bool isEmpty() { return _value == 0; }
    virtual bool isFull() { return _value == capacity(); }
    virtual queue_value_t addClient(queue_value_t nb = 1) {
        queue_value_t prev = _value;
        _value += nb;
        if (marto_unlikely(_value > capacity())) {
            _value = capacity();
        }
        return _value - prev;
    }
    virtual queue_value_t removeClient(queue_value_t nb = 1) {
        queue_value_t prev = _value;
        // TODO: assuming that value is less than 2^31
        if (marto_unlikely((queue_value_t)_value < nb)) {
            _value = 0;
        } else {
            _value -= nb;
        }
        return prev - _value;
    }
    using TypedQueueState<StandardQueue>::compareTo;
    virtual int compareTo(QueueState *q) {
        StandardQueueState *other = dynamic_cast<StandardQueueState *>(
            q); // TODO: handle bad conversion
        return _value - other->_value;
    };
};

std::shared_ptr<QueueState> StandardQueue::allocateQueueState() {
    return std::shared_ptr<QueueState>(new StandardQueueState(this));
}
std::shared_ptr<QueueState> StandardQueue::allocateQueueState(queue_value_t v) {
    return std::shared_ptr<QueueState>(new StandardQueueState(this, v));
}

class OutsideQueueState : public TypedQueueState<OutsideQueue> {
  private:
    friend OutsideQueue;
    OutsideQueueState(OutsideQueue *c) : TypedQueueState<OutsideQueue>(c) {}

  public:
    virtual queue_value_t value() const { return 0; } // FIXME: assert(false) ?
    virtual bool isEmpty() { return false; }
    virtual bool isFull() { return false; }
    virtual queue_value_t addClient(queue_value_t nb = 1) { return nb; }
    virtual queue_value_t removeClient(queue_value_t nb = 1) { return nb; }
    virtual int compareTo(QueueState *__marto_unused(q)) {
        throw std::invalid_argument(
            "Trying to compare queue state with outside queue");
    };
};

std::shared_ptr<QueueState> OutsideQueue::allocateQueueState() {
    return std::shared_ptr<QueueState>(new OutsideQueueState(this));
}
} // namespace marto
