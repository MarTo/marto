#pragma once

#include "gtest/gtest.h"
#include <iostream>
#include <string>

class TestingWithCOUT : public ::testing::Test {
  protected:
    TestingWithCOUT() : sbuf{nullptr} {
        // intentionally empty
    }

    ~TestingWithCOUT() override = default;

    // Called before each unit test
    void SetUp() override {
        // Save cout's buffer...
        sbuf = std::cout.rdbuf();
        // Redirect cout to our stringstream buffer or any other ostream
        std::cout.rdbuf(buffer.rdbuf());
    }

    // Called after each unit test
    void TearDown() override {
        std::string expected{};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);

        // When done redirect cout to its old self
        std::cout.rdbuf(sbuf);
        sbuf = nullptr;
    }
    std::string get_buffer() {
        std::string captured = buffer.str();
        buffer = std::stringstream{};
        return captured;
    }

  private:
    std::stringstream buffer{};
    std::streambuf *sbuf;
};

template <template <typename...> class T, typename... Args>
struct test_template {
  private:
    struct dummy { /* something */
    };
    template <template <typename...> class C, typename P, typename... CArgs>
    static auto test(P *p)
        -> decltype(std::declval<C<CArgs...>>(), std::true_type());

    template <template <typename...> class C, typename, typename...>
    static std::false_type test(...);

  public:
    typedef decltype(test<T, dummy, Args...>(nullptr)) type;
    static constexpr bool value = !std::is_same<std::false_type, type>::value;
};

template <template <typename...> class T, typename... Args>
constexpr inline bool test_template_v = test_template<T, Args...>::value;
