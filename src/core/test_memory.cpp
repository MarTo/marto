#include "gtest-extensions.h"
#include <marto/memory.h>

#include <iostream>

#include <marto/debug.h>

template <typename T> struct RC {
    static int nb;
    RC() { nb++; }
    ~RC() { nb--; }
};
template <typename T> int RC<T>::nb = 0;

#define template_struct_trace(X, XT) struct X : marto::trace<XT>, RC<XT>

#define struct_trace(X) template_struct_trace(X, X)

#define template_trace(X, XT)                                                  \
    X:                                                                         \
    marto::trace<XT>, RC<XT>

#define trace(X) template_trace(X, X)

namespace diamon {

struct trace(A), marto::virtual_enable_shared_from_this<A>,
    marto::enable_make_shared<
        A, marto::as_other<marto::virtual_enable_shared_from_this<A>>> {
    using make_shared_class::pointer;
    A() { std::cout << "Building A()\n"; }
};
struct trace(B), // marto::virtual_enable_shared_from_this<B>,
    marto::enable_make_shared<B, marto::collect::add_virtual_shared_from_this> {
    using marto::virtual_enable_shared_from_this<B>::pointer;
    B() { std::cout << "Building B()\n"; }
};
struct trace(Z), marto::enable_make_shared<Z, marto::as_public<A, B>>{
                     Z(){std::cout << "Building Z()\n";
} // namespace diamon
~Z() { std::cout << "Destroying Z()\n"; }

protected:
Z(int z1) { std::cout << "Building Z(int=" << z1 << ")\n"; }
}
;

struct_trace(Z2), marto::enable_make_shared<Z2, marto::as_public<A, B>> {
    int for_indent;
    Z2() { std::cout << "Building Z2()\n"; }
    ~Z2() { std::cout << "Destroying Z2()\n"; }

  protected:
    Z2(int z1) { std::cout << "Building Z2(int=" << z1 << ")\n"; }
};

struct_trace(W),
    marto::enable_make_shared<W, marto::as_public<marto::inherit_ctor<Z2>>> {
    using make_shared_class::make_shared_class;

    ~W() { std::cout << "Destroying W()\n"; }

  protected:
    W(int w1, int w2) : make_shared_class(w1 + w2) {
        std::cout << "Building W(int=" << w1 << ",int=" << w2 << ")\n";
    }
};

struct trace(W2), Z2 {
    int for_indent;
    W2(int w1, int w2) {
        std::cout << "Building W2(int=" << w1 << ",int=" << w2 << ")\n";
    }
    ~W2() { std::cout << "Destroying W2()\n"; }
};

class HasEvaluationTiming {
    virtual explicit operator int() const = 0;
};

class WithData2 : public virtual HasEvaluationTiming {
  public:
    virtual operator int() const final override { return 42; }
};

class SymbolicParameter :
    // public virtual HasEvaluationTiming,
    public marto::virtual_enable_shared_from_this<SymbolicParameter> {
  public:
    class WithData : public virtual HasEvaluationTiming {
      public:
        virtual operator int() const final override { return 42; }
    };

    virtual ~SymbolicParameter() {}
};

class SymbolicRealParameter : public SymbolicParameter {};

class SymbolicNameLookup
    : public marto::enable_make_shared<SymbolicNameLookup,
                                       SymbolicRealParameter> {};

class SymbolicBindings
    : public marto::virtual_enable_shared_from_this<SymbolicBindings> {};

class SymbolicFunctionCall
    : public marto::enable_make_shared<SymbolicFunctionCall, SymbolicNameLookup,
                                       SymbolicParameter::WithData,
                                       SymbolicBindings> {
#if 1
    using IT = marto::bits::has_shared_from_this_tuple<
        std::tuple<diamon::SymbolicNameLookup>>;
    using IO = marto::bits::has_shared_from_this_tuple<std::tuple<
        diamon::SymbolicParameter::WithData, diamon::SymbolicBindings>>;
#else
    using IT = marto::has_shared_from_this_tuple<
        std::tuple<diamon::SymbolicParameter::WithData>>;
    using IO =
        marto::has_shared_from_this_tuple<std::tuple<diamon::SymbolicBindings>>;
#endif
    static constexpr bool value = IT::value || IO::value;
    using type =
        std::conditional_t<IT::value, typename IT::type, typename IO::type>;
    using virtual_base_tuple = std::conditional_t<
        IT::value && IO::value,
        marto::tuple_intersect_t<typename IT::virtual_base_tuple,
                                 typename IO::virtual_base_tuple>,
        marto::tuple_cat_t<typename IT::virtual_base_tuple,
                           typename IO::virtual_base_tuple>>;

    ~SymbolicFunctionCall() {
        marto::static_assert_show_types<
            true, typename IT::virtual_base_tuple,
            typename IO::virtual_base_tuple, virtual_base_tuple,
            typename marto::bits::has_shared_from_this_method<
                SymbolicNameLookup>::type,
            typename marto::bits::has_shared_from_this_method<
                SymbolicBindings>::type,
            typename marto::bits::has_shared_from_this_method<
                SymbolicParameter::WithData>::type>("yes");
    }
};

} // namespace diamon

namespace diamon2 {

struct_trace(O), marto::enable_shared_from_this<O> {
    int for_indent;
    O() { std::cout << "Building O()\n"; }
};

struct_trace(A),
    marto::enable_make_shared<A, marto::virtual_enable_shared_from_this<A, O>> {
    using make_shared_class::pointer;
    A() { std::cout << "Building A()\n"; }
};
struct_trace(B),
    marto::enable_make_shared<B, marto::virtual_enable_shared_from_this<B, O>> {
    using make_shared_class::pointer;
    B() { std::cout << "Building B()\n"; }
};

struct_trace(Z), // marto::enable_shared_from_this<Z, marto::as_public<A, B>>,
    marto::enable_make_shared<Z, marto::as_public<A, B>> {
    using marto::enable_make_shared<Z,
                                    marto::as_public<A, B>>::make_shared_class;
    // using make_shared_class::make_shared;
    using make_shared_class::make_shared_extension;
    // using inherit_class::A::shared_from_this;
    Z() { std::cout << "Building Z()\n"; }

  protected:
    Z(int z1) { std::cout << "Building Z(int=" << z1 << ")\n"; }
};

struct_trace(Z2), marto::enable_make_shared<Z2, marto::as_public<A, B>> {
    int for_indent;
    Z2() { std::cout << "Building Z2()\n"; }

  protected:
    Z2(int z1) { std::cout << "Building Z2(int=" << z1 << ")\n"; }
};

struct_trace(W),
    marto::enable_make_shared<W, marto::as_public<marto::inherit_ctor<Z2>>> {
    using make_shared_class::make_shared_class;

  protected:
    W(int w1, int w2) : make_shared_class(w1 + w2) {
        std::cout << "Building W(int=" << w1 << ",int=" << w2 << ")\n";
    }
};

struct_trace(W2), Z2 {
    int for_indent;
    using Z2::Z2;

    // protected:
    W2(int w1, int w2) : Z2(w1 + w2) {
        std::cout << "Building W2(int=" << w1 << ",int=" << w2 << ")\n";
    }
};

} // namespace diamon2

namespace mse {

#define Param(X)                                                               \
    struct_trace(X) {                                                          \
        int val;                                                               \
        X(int v = 0) : val(v) {                                                \
            std::cout << "  **** building parameter " << marto::type_name<X>() \
                      << std::endl;                                            \
        }                                                                      \
    }

Param(P_ETB);
Param(P_V1T);
Param(P_ET);

struct_trace(ETB), marto::enable_shared_from_this<ETB>,
    marto::enable_make_shared<
        ETB, marto::as_other<marto::enable_shared_from_this<ETB>>> {
    ETB(P_ETB){};
};

template <class T>
template_struct_trace(V1, V1<T>), marto::extend_make_shared<V1<T>, T> {
    int for_indent;
    template <typename EMS_pointer, typename F, typename... Args>
    static EMS_pointer make_shared_extension(F && f, P_V1T p, P_V1T & p2,
                                             Args && ...args) {
        std::cerr << "   |||@@@ extension in " << marto::type_name<V1>()
                  << std::endl;
        std::cout << "   Reading extension param from "
                  << marto::type_name<V1>() << "=" << p.val << ", " << p2.val
                  << std::endl;
        p.val += 50;
        p2.val += 100;
        auto cb = std::function<EMS_pointer(Args...)>(std::forward<F>(f));
        return cb(std::forward<Args>(args)...);
    }
};

struct_trace(ET0),
    marto::enable_make_shared<
        ET0, marto::as_public<marto::inherit_ctor<ETB>, V1<ET0>>> {
    using YO = marto::enable_make_shared<
        ET0, marto::as_public<marto::inherit_ctor<ETB>, V1<ET0>>>;

    using YO::const_pointer;
    using YO::pointer;
    /* both next 'using' clauses are required because V1<ET0> is manually
     * inherited */
    // using YO::make_shared;
    using YO::make_shared_extension;
    // using marto::enable_shared_from_this<ET0, ETB>::shared_from_this;
    // using marto::enable_make_shared<ET, ETB,
    // marto::as_public<V1<ET>>>::pointer; using marto::enable_make_shared<ET,
    // ETB, marto::as_public<V1<ET>>>::make_shared;
    ET0(P_ET)
        : make_shared_class(P_ETB{}){
              // P_ET_f();
          };
    const_pointer get_pointer() const { return const_pointer(this); }
    pointer get_pointer() { return pointer(this); }
};

struct_trace(ET),
    marto::enable_shared_from_this<
        ET, marto::inherit_ctor<marto::enable_make_shared<
                ET, marto::as_public<marto::inherit_ctor<ETB>, V1<ET>>>>> {
    ET(P_ET) : shared_from_this_class(P_ETB{}){};

    const_pointer get_pointer() const { return const_pointer(this); }
    pointer get_pointer() { return pointer(this); }
};

struct_trace(EText),
    marto::enable_make_shared<
        EText, marto::as_public<marto::inherit_ctor<ET0>, V1<EText>>> {

    using make_shared_class::make_shared_class;
};

} // namespace mse

namespace {

class MemoryBaseTest : public TestingWithCOUT {
  protected:
    MemoryBaseTest() {}
    // A::pointer e1, e2, e3, e11, e12;
    // B::pointer qs1, qs2;
};

TEST_F(MemoryBaseTest, CollectorTest) {
    struct A {};
    struct B {};
    {
        using namespace marto::collect;
        using CT1 = sft<self<A>, B>;
        marto::static_assert_is_same<typename CT1::other_tuple_t,
                                     std::tuple<B>>("all other types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
    }
    {
        using namespace marto::collect;
        using CT1 = sft<with_self<A, B>>;
        marto::static_assert_is_same<typename CT1::other_tuple_t,
                                     std::tuple<B>>("all other types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
    }
    {
        using namespace marto::collect;
        using CT1 = sft<A, B>;
        marto::static_assert_is_same<typename CT1::other_tuple_t,
                                     std::tuple<A, B>>("all other types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<>>(
            "self type");
    }
}

TEST_F(MemoryBaseTest, DiamonTest) {
    using namespace diamon;
    std::cout << "Start\n";
    {
        A::pointer a2;
        {
            std::shared_ptr<B> b;
            std::shared_ptr<A> a;
            {
                std::shared_ptr<A> a1;
                a1 = marto::new_shared<A>();
                auto p = a1->shared_from_this();
            }

            {
                std::shared_ptr<Z> z = marto::new_shared<Z>();
                /* fail to compile if std::make_shared is used */
                std::shared_ptr<Z> z2 = marto::new_shared<Z>(42);
                ASSERT_EQ(z.use_count(), 1);
                ASSERT_EQ(RC<A>::nb, 2);
                ASSERT_EQ(RC<B>::nb, 2);
                ASSERT_EQ(RC<Z>::nb, 2);
                b = z->B::shared_from_this();
                ASSERT_EQ(b.use_count(), 2);
                a = z->A::shared_from_this();
                ASSERT_EQ(a.use_count(), 3);
                ASSERT_EQ(b.use_count(), 3);
                ASSERT_EQ(z.use_count(), 3);
                a2 = z->shared_from_this();
                ASSERT_EQ(a.use_count(), 4);
                ASSERT_EQ(b.use_count(), 4);
                ASSERT_EQ(z.use_count(), 4);
                ASSERT_EQ(a2.use_count(), 4);
                ASSERT_EQ(RC<A>::nb, 2);
                ASSERT_EQ(RC<B>::nb, 2);
                ASSERT_EQ(RC<Z>::nb, 2);
                auto a3 = a->shared_from_this();
                ASSERT_TRUE((std::is_same_v<decltype(a3), decltype(a2)>));
                auto z3 = z->shared_from_this();
                std::cerr << "z3: " << marto::type_name<decltype(z3)>()
                          << std::endl
                          << "z2: " << marto::type_name<decltype(z2)>()
                          << std::endl;
                ASSERT_TRUE((std::is_same_v<decltype(z3), decltype(z2)>));
                ASSERT_EQ(a.use_count(), 6);
                ASSERT_EQ(b.use_count(), 6);
                ASSERT_EQ(z.use_count(), 6);
                ASSERT_EQ(a2.use_count(), 6);
            }
            ASSERT_EQ(RC<A>::nb, 1);
            ASSERT_EQ(RC<B>::nb, 1);
            ASSERT_EQ(RC<Z>::nb, 1);
            ASSERT_EQ(a.use_count(), 3);
            ASSERT_EQ(b.use_count(), 3);
        }
        ASSERT_EQ(RC<A>::nb, 1);
        ASSERT_EQ(RC<B>::nb, 1);
        ASSERT_EQ(RC<Z>::nb, 1);
        ASSERT_EQ(a2.use_count(), 1);
        a2 = nullptr;
        ASSERT_EQ(RC<A>::nb, 0);
        ASSERT_EQ(RC<B>::nb, 0);
        ASSERT_EQ(RC<Z>::nb, 0);

        static_assert(marto::bits::has_make_shared<W>::value == true,
                      "W is built with make_shared support");
        // Correctly invoke std::make_shared
        std::shared_ptr<W> w = marto::new_shared<W>(1, 2);
        ASSERT_NE(w, nullptr);
        ASSERT_EQ(RC<Z2>::nb, 1);
        ASSERT_EQ(RC<W>::nb, 1);

        auto w2a = new W2(4, 8);
        // Call in fact Z::make_shared, so create a Z object
        // auto w2b = w2a->make_shared();
        auto w2b = marto::new_shared<Z>();
        ASSERT_NE(w2b, nullptr);
        ASSERT_EQ(std::dynamic_pointer_cast<W2>(w2b), nullptr);
        ASSERT_EQ(RC<Z2>::nb, 3);
        ASSERT_EQ(RC<W2>::nb, 1);
        delete (w2a);

        std::shared_ptr<W2> w2 = std::make_shared<W2>(43, 44);
    }
    std::string expected{R""""(Start
Building A()
Building A()
Building B()
Building Z()
Building A()
Building B()
Building Z(int=42)
Destroying Z()
Destroying Z()
Building A()
Building B()
Building Z2(int=3)
Building W(int=1,int=2)
Building A()
Building B()
Building Z2()
Building W2(int=4,int=8)
Building A()
Building B()
Building Z2()
Destroying W2()
Destroying Z2()
Building A()
Building B()
Building Z2()
Building W2(int=43,int=44)
Destroying W2()
Destroying Z2()
Destroying Z2()
Destroying W()
Destroying Z2()
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}

TEST_F(MemoryBaseTest, DiamonTest2) {
    using namespace diamon2;
    std::cout << "Start\n";
    {
        A::pointer a2;
        {
            std::shared_ptr<B> b;
            std::shared_ptr<A> a;
            {
                std::shared_ptr<Z> z = marto::new_shared<Z>();
                ASSERT_EQ(z.use_count(), 1);
                /* fail to compile if std::make_shared is used */
                std::shared_ptr<Z> z2 = marto::new_shared<Z>(42);
                ASSERT_EQ(z2.use_count(), 1);
                ASSERT_EQ(RC<A>::nb, 2);
                ASSERT_EQ(RC<B>::nb, 2);
                ASSERT_EQ(RC<Z>::nb, 2);
                a = z->A::shared_from_this();
                // std::cerr << "a.get="<< a.get() <<"\n";
                // a2 = z;
                // std::cerr << "a2.get="<< a2.get() <<"\n";
                // a2.reset();
                // std::cerr << "End debug\n";
                ASSERT_EQ(a.use_count(), 2);
                b = z->B::shared_from_this();
                ASSERT_EQ(b.use_count(), 3);
                ASSERT_EQ(b.use_count(), 3);
                ASSERT_EQ(z.use_count(), 3);
                a2 = z->shared_from_this();
                ASSERT_EQ(a.use_count(), 4);
                ASSERT_EQ(b.use_count(), 4);
                ASSERT_EQ(z.use_count(), 4);
                ASSERT_EQ(a2.use_count(), 4);
                ASSERT_EQ(RC<A>::nb, 2);
                ASSERT_EQ(RC<B>::nb, 2);
                ASSERT_EQ(RC<Z>::nb, 2);
                auto a3 = a->shared_from_this();
                ASSERT_TRUE((std::is_same_v<decltype(a3), decltype(a2)>));
                auto z3 = z->shared_from_this();
                std::cerr << "z3: " << marto::type_name<decltype(z3)>()
                          << std::endl
                          << "z2: " << marto::type_name<decltype(z2)>()
                          << std::endl;
                ASSERT_TRUE((std::is_same_v<decltype(z3), decltype(z2)>));
                ASSERT_EQ(a.use_count(), 6);
                ASSERT_EQ(b.use_count(), 6);
                ASSERT_EQ(z.use_count(), 6);
                ASSERT_EQ(a2.use_count(), 6);
            }
            ASSERT_EQ(RC<A>::nb, 1);
            ASSERT_EQ(RC<B>::nb, 1);
            ASSERT_EQ(RC<Z>::nb, 1);
            ASSERT_EQ(a.use_count(), 3);
            ASSERT_EQ(b.use_count(), 3);
        }
        ASSERT_EQ(RC<A>::nb, 1);
        ASSERT_EQ(RC<B>::nb, 1);
        ASSERT_EQ(RC<Z>::nb, 1);
        ASSERT_EQ(a2.use_count(), 1);
        a2 = nullptr;
        ASSERT_EQ(RC<A>::nb, 0);
        ASSERT_EQ(RC<B>::nb, 0);
        ASSERT_EQ(RC<Z>::nb, 0);

        // Correctly invoke std::make_shared
        std::shared_ptr<W> w = marto::new_shared<W>();
        ASSERT_NE(w, nullptr);
        ASSERT_EQ(RC<Z2>::nb, 1);
        ASSERT_EQ(RC<W>::nb, 1);

        auto w2a = new W2(4, 8);
        // Call in fact Z::make_shared, so create a Z object
        // auto w2b = w2a->make_shared();
        auto w2b = marto::new_shared<Z>();
        ASSERT_NE(w2b, nullptr);
        ASSERT_EQ(std::dynamic_pointer_cast<W2>(w2b), nullptr);
        ASSERT_EQ(RC<Z2>::nb, 3);
        ASSERT_EQ(RC<W2>::nb, 1);
        delete (w2a);

        std::shared_ptr<W2> w2 = std::make_shared<W2>(43, 44);
    }
    std::string expected{R""""(Start
Building O()
Building A()
Building B()
Building Z()
Building O()
Building A()
Building B()
Building Z(int=42)
Building O()
Building A()
Building B()
Building Z2()
Building O()
Building A()
Building B()
Building Z2(int=12)
Building W2(int=4,int=8)
Building O()
Building A()
Building B()
Building Z2()
Building O()
Building A()
Building B()
Building Z2(int=87)
Building W2(int=43,int=44)
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}

TEST_F(MemoryBaseTest, MakeSharedTest) {
    {
        void(0); /* for correct auto indentation... */
        {
            std::cout << "Start\n";
            mse::P_V1T p_v1t{1};
            mse::P_V1T p_v1t2{2};
            mse::P_ET p_et{};
            std::shared_ptr<mse::ET0> e =
                marto::new_shared<mse::ET0>(p_v1t, p_v1t2, p_et);
            ASSERT_EQ(e.use_count(), 1);
            ASSERT_EQ(p_v1t.val, 1);
            ASSERT_EQ(p_v1t2.val, 102);
            std::shared_ptr<mse::ET0> e2 = e->shared_from_this();
            ASSERT_EQ(e.use_count(), 2);
        }
    }
    {
        std::string expected{R""""(Start
  **** building parameter mse::P_V1T
  **** building parameter mse::P_V1T
  **** building parameter mse::P_ET
   Reading extension param from mse::V1<mse::ET0>=1, 2
  **** building parameter mse::P_ETB
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }

    {
        void(0); /* for correct auto indentation... */
        {
            std::cout << "Start\n";
            mse::P_V1T p_v1t{2};
            mse::P_V1T p_v1t2{4};
            mse::P_ET p_et{};
            std::shared_ptr<mse::ET> e =
                marto::new_shared<mse::ET>(p_v1t, p_v1t2, p_et);
            ASSERT_EQ(e.use_count(), 1);
            ASSERT_EQ(p_v1t.val, 2);
            ASSERT_EQ(p_v1t2.val, 104);
            std::shared_ptr<mse::ET> e2 = e->shared_from_this();
            ASSERT_EQ(e.use_count(), 2);
        }
    }
    {
        std::string expected{R""""(Start
  **** building parameter mse::P_V1T
  **** building parameter mse::P_V1T
  **** building parameter mse::P_ET
   Reading extension param from mse::V1<mse::ET>=2, 4
  **** building parameter mse::P_ETB
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }

    {
        void(0); /* for correct auto indentation... */
        {
            std::cout << "Start\n";
            mse::P_V1T p_v1ta{4};
            mse::P_V1T p_v1ta2{2};
            mse::P_V1T p_v1tb{8};
            mse::P_V1T p_v1tb2{16};
            mse::P_ET p_et{};
            std::shared_ptr<mse::EText> e = marto::new_shared<mse::EText>(
                p_v1ta, p_v1ta2, p_v1tb, p_v1tb2, p_et);
            ASSERT_EQ(e.use_count(), 1);
            ASSERT_EQ(p_v1ta.val, 4);
            ASSERT_EQ(p_v1ta2.val, 102);
            ASSERT_EQ(p_v1tb.val, 8);
            ASSERT_EQ(p_v1tb2.val, 116);
            std::shared_ptr<mse::EText> e2 = e->shared_from_this();
            ASSERT_EQ(e.use_count(), 2);
        }
    }
    {
        std::string expected{R""""(Start
  **** building parameter mse::P_V1T
  **** building parameter mse::P_V1T
  **** building parameter mse::P_V1T
  **** building parameter mse::P_V1T
  **** building parameter mse::P_ET
   Reading extension param from mse::V1<mse::EText>=4, 2
   Reading extension param from mse::V1<mse::ET0>=8, 16
  **** building parameter mse::P_ETB
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }
}

} // namespace
