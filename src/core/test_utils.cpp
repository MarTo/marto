#include "gtest-extensions.h"
#define MARTO_DEBUG_UTILS
#include <marto/utils.h>

#include <iostream>

#include <marto/debug.h>

template <typename T> struct RC {
    static int nb;
    RC() { nb++; }
    ~RC() { nb--; }
};
template <typename T> int RC<T>::nb = 0;

struct A : marto::trace<A>, RC<A> {
    std::string name() { return "A"; }
    static std::string sname() { return "a"; }
};
struct B : marto::trace<B>, RC<B> {
    std::string name() {
        assert(this);
        return "B";
    }
    static std::string sname() { return "b"; }
};
struct W : marto::trace<W>, RC<W>, marto::enable_visitor_for<W, A, B>, A, B {
    W() {}

    std::string name() {
        std::string str;
        for_each_base([&](auto *base) { str += base->name(); });
        str += "/-";
        for_each_base(this, [&](auto *base) { str += base->name(); });
        return str;
    }
    static std::string sname() {
        std::string str;
        static_for_each_base([&](auto *base) {
            using Base = std::decay_t<decltype(*base)>;
            str += Base::sname();
        });
        str += "/-";
        for_each_base(nullptr, [&](auto *base) {
            using Base = std::decay_t<decltype(*base)>;
            str += Base::sname();
        });
        return str;
    }
};

struct Z : marto::trace<Z>, RC<Z>, marto::enable_visitor_for_bases<A, B> {
    Z() {}

    std::string name() {
        std::string str;
        for_each_base([&](auto *base) { str += base->name(); });
        str += "/-";
        for_each_base(this, [&](auto *base) { str += base->name(); });
        return str;
    }
    static std::string sname() {
        std::string str;
        static_for_each_base([&](auto *base) {
            using Base = std::decay_t<decltype(*base)>;
            str += Base::sname();
        });
        str += "/-";
        for_each_base(nullptr, [&](auto *base) {
            using Base = std::decay_t<decltype(*base)>;
            str += Base::sname();
        });
        return str;
    }
};

#define template_struct_trace(X, XT) struct X : marto::trace<XT>, RC<XT>

#define struct_trace(X) template_struct_trace(X, X)

#define TracedClass(X)                                                         \
    struct_trace(X) {                                                          \
        X() {                                                                  \
            std::cout << "  **** building parameter " << marto::type_name<X>() \
                      << std::endl;                                            \
        }                                                                      \
        int get_##X() { return 42; }                                           \
    }

TracedClass(T1);
TracedClass(T2);
TracedClass(T3);
TracedClass(T4);
TracedClass(T5);
TracedClass(T6);

struct I1 : marto::trace<I1>,
            RC<I1>,
            marto::inherit<marto::collect::visibility_options<
                marto::collect::collect_unknown_as_public, T1, T2, T3>> {
    using type = inherit_class;
};

class I2 : marto::trace<I2>,
           RC<I2>,
           public marto::inherit<marto::as_protected<T1>, marto::as_public<T2>,
                                 T3, marto::as_protected<T4>> {
  public:
    using type = inherit_class;
    I2(int v) { std::cout << "Creating I2 with value " << v << std::endl; };
};

class I3 : marto::trace<I3>,
           RC<I3>,
           protected marto::inherit<
               marto::as_protected<T5, marto::inherit_ctor<I2>, T6>> {
    using inherit_class::inherit_class;
};

class P1
    : marto::trace<P1>,
      RC<P1>,
      marto::inherit<
          marto::with_self<P1, marto::collect::visibility_options<
                                   marto::collect::collect_unknown_as_public,
                                   marto::as_private<T1>, T2, T3>>> {
    using type = inherit_class;

    int get() {
        // check access to private T1
        return get_T3() + get_T2() + get_T1();
    }
};

class P2 : marto::trace<P2>,
           RC<P2>,
           marto::inherit<marto::self<P2>,
                          marto::collect::visibility_options<
                              marto::collect::collect_unknown_as_public,
                              marto::as_private<T1>, T2, T3>> {
    using type = inherit_class;

    int get() {
        // check access to private T1
        return get_T3() + get_T2() + get_T1();
    }
};

namespace {

class UtilsBaseTest : public TestingWithCOUT {
  protected:
    UtilsBaseTest() {}
};

TEST_F(UtilsBaseTest, VisitorTest) {
    {
        W *w = new W();
        Z *z = new Z();
        ASSERT_EQ(RC<A>::nb, 2);
        ASSERT_EQ(RC<B>::nb, 2);
        ASSERT_EQ(RC<W>::nb, 1);
        ASSERT_EQ(RC<Z>::nb, 1);
        ASSERT_EQ(z->name(), "AB/-AB");
        ASSERT_EQ(w->name(), "AB/-AB");
        ASSERT_EQ(z->sname(), "ab/-ab");
        ASSERT_EQ(w->sname(), "ab/-ab");
        delete (w);
        delete (z);
    }
}

TEST_F(UtilsBaseTest, TuplesTest) {
    {
        ASSERT_TRUE((marto::tuple_has_type<int, std::tuple<>>::value == false));
        ASSERT_TRUE(
            (marto::tuple_has_type<int, std::tuple<int>>::value == true));
        ASSERT_TRUE(
            (marto::tuple_has_type<int, std::tuple<float>>::value == false));
        ASSERT_TRUE((
            marto::tuple_has_type<int, std::tuple<float, int>>::value == true));
        ASSERT_TRUE((
            marto::tuple_has_type<int, std::tuple<int, float>>::value == true));
        ASSERT_TRUE(
            (marto::tuple_has_type<int, std::tuple<char, float, int>>::value ==
             true));
        ASSERT_TRUE(
            (marto::tuple_has_type<int, std::tuple<char, float, bool>>::value ==
             false));
        ASSERT_TRUE((marto::tuple_has_type<const int, std::tuple<int>>::value ==
                     false)); // we're using is_same so cv matters
        ASSERT_TRUE((marto::tuple_has_type<int, std::tuple<const int>>::value ==
                     false)); // we're using is_same so cv matters

        struct T1 {};
        struct T2 {
            T2(int) {}
        };
        struct T3 {};
        using A = std::tuple<T1, T2>;
        using B = std::tuple<T2, T3>;

        ASSERT_TRUE((std::is_same<std::tuple<T2>,
                                  marto::tuple_intersect<A, B>::type>::value));
    }
}

struct without_type {};

struct with_type {
    typedef int type;
};
template <class T> struct maybe_type;

TEST_F(UtilsBaseTest, CollectTest) {
    struct A {};
    struct B {};
    {
        static_assert(test_template_v<marto::inherit, marto::as_public<A>>);
        // static_assert(!test_template_v<marto::inherit,
        // marto::as_public<marto::as_protected<A>>>);
        // static_assert(!test_template_v<marto::inherit,
        // marto::inherit_ctor<A>, marto::inherit_ctor<B>>);

        using namespace marto::collect;
        using namespace marto::collect::bits::collect;

        static_assert(has_member_type<with_type>::value);
        static_assert(!has_member_type<without_type>::value);
        static_assert(!has_member_type<maybe_type<A>>::value);

        static_assert(has_member_type<
                      FinalCollector<phase::explore, long, empty>>::value);
        typedef
            typename FinalCollector<phase::explore, long, empty>::type mytuple;
        marto::static_assert_is_same<mytuple,
                                     std::tuple<std::pair<empty, long>>>(
            "Bad tuple");

        // static_assert(GetExploreResult<
        //               FinalCollector<phase::explore, long, empty>>::applied);
        static_assert(has_member_type<GetExploreResult<
                          FinalCollector<phase::explore, long, empty>>>::value);

        static_assert(has_member_type<GetConfigResult<collector::visibility<
                          phase::init_options, void, empty>>>::value);

        static_assert(
            !has_member_type<
                collector::visibility<phase::explore, long, empty>>::value);

        // static_assert(
        //     !GetExploreResult<visibility<phase::explore, long,
        //     empty>>::applied);
        static_assert(
            !has_member_type<GetExploreResult<
                collector::visibility<phase::explore, long, empty>>>::value);

        typedef TupleCollector<phase::explore, as_tuple<std::tuple<long, A, B>>,
                               empty>
            tcollected;
        marto::static_assert_is_same<typename tcollected::type,
                                     std::tuple<long, A, B>>("collect tuple");

        static_assert(
            !GetExploreResult<
                TupleCollector<phase::explore, as_tuple<std::tuple<long, A, B>>,
                               empty>>::flags::collected);

        marto::static_assert_is_same<
            GetExploreResult<TupleCollector<
                phase::explore, as_tuple<std::tuple<long, A, B>>, empty>>::type,
            std::tuple<long, A, B>>("TupleCollector");

        typedef FinalCollector<phase::explore, as_tuple<std::tuple<long, A, B>>,
                               empty>
            tcol2;
        static_assert(tcol2::flags::collected);
        static_assert(GetExploreResult<tcol2>::flags::collected);
        static_assert(GetExploreResult<tcol2>::flags::stop);

        using result_type = GetExploreResult<FinalCollector<
            phase::explore, as_tuple<std::tuple<long, A, B>>, empty>>;
        static_assert(result_type::flags::stop);
        static_assert(result_type::flags::collected);

        using flags =
            CollectorFlags<StopFlagDefault<result_type, false, result_type>>;
        static_assert(flags::stop);
        static_assert(flags::collected);

        typedef collector_apply<FinalCollector, phase::explore,
                                as_tuple<std::tuple<long, A, B>>, empty,
                                GetExploreResult>
            rt;
        static_assert(rt::applied);
        static_assert(rt::flags::collected);

        typedef collectors_apply_onetype<
            phase::explore, as_tuple<std::tuple<long, A, B>>, empty,
            GetExploreResult, FinalCollector>
            rt2;
        static_assert(rt2::flags::collected);

        using result_type2 = collectors_apply_onetype<
            phase::explore, as_tuple<std::tuple<long, A, B>>, empty,
            GetExploreResult, TupleCollector, FinalCollector>;
        static_assert(!result_type2::flags::collected);
        // static_assert(!collected_value<result_type2::flags,
        // true>::collected);
        marto::static_assert_is_same<
            collectors_apply_alltypes_tuple<
                phase::explore, std::tuple<long, A, B>, empty, GetExploreResult,
                TupleCollector, FinalCollector>::type,
            std::tuple<std::pair<empty, long>, // first
                       std::pair<empty, A>,    // second
                       std::pair<empty, B>     // third
                       >>("TupleCollector");
        marto::static_assert_is_same<
            collectors_apply_alltypes<
                phase::explore, as_tuple<std::tuple<long, A, B>>, empty,
                GetExploreResult, TupleCollector, FinalCollector>::type,
            std::tuple<std::pair<empty, long>, // first
                       std::pair<empty, A>,    // second
                       std::pair<empty, B>     // third
                       >>("TupleCollector");

        // static_assert(marto::always_false<typename collect_classes<void/*,
        // visibility*/>::type>);
        typedef collect_classes<int> cc1;
        marto::static_assert_is_same<typename cc1::initOptions, empty>(
            "Bad returned type");
        marto::static_assert_is_same<typename cc1::collected_types,
                                     std::tuple<std::pair<empty, int>>>(
            "Bad returned type");

        typedef collect_classes<long, collector::visibility> cc2;
        static_assert(cc2::initOptions::visibility_flags ==
                      bits::visibility::none);
        marto::static_assert_is_same<typename cc2::initResult::public_tuple_t,
                                     std::tuple<>>("Bad returned type");
        marto::static_assert_is_same<
            typename cc2::collected_types,
            std::tuple<std::pair<typename cc2::initOptions, long>>>(
            "Bad returned type");

        typedef collect_classes<as_tuple<std::tuple<long, A, B>>,
                                collector::visibility>
            cc3;
        static_assert(cc3::initOptions::visibility_flags ==
                      bits::visibility::none);
        marto::static_assert_is_same<typename cc3::initResult::public_tuple_t,
                                     std::tuple<>>("Bad returned type");
        marto::static_assert_is_same<
            typename cc3::collected_types,
            std::tuple<std::pair<typename cc3::initOptions, long>,
                       std::pair<typename cc3::initOptions, A>,
                       std::pair<typename cc3::initOptions, B>>>(
            "Bad returned type");

        typedef collect_classes<as_public<long, A, B>, collector::visibility>
            cc4;
        static_assert(cc4::initOptions::visibility_flags ==
                      bits::visibility::none);
        marto::static_assert_is_same<typename cc4::initResult::public_tuple_t,
                                     std::tuple<>>("Bad returned type");
        using visibility_options = std::remove_reference_t<decltype(std::get<0>(
            std::get<0>(std::declval<cc4::collected_types>())))>;
        marto::static_assert_is_same<
            typename cc4::collected_types,
            std::tuple<std::pair<visibility_options, long>,
                       std::pair<visibility_options, A>,
                       std::pair<visibility_options, B>>>("Bad returned type");

        marto::static_assert_is_same<typename cc4::type::public_tuple_t,
                                     std::tuple<long, A, B>>(
            "all public types");
    }
    {
        using namespace marto::collect;
        typedef collect_classes_t<
            as_tuple<std::tuple<
                short,
                as_public<long, as_private<inherit_ctor<A>, as_public<char>>,
                          B>>>,
            collector::visibility, collector::all_classes,
            collector::inherit_ctor>
            cc;
        marto::static_assert_is_same<typename cc::public_tuple_t,
                                     std::tuple<long, char, B>>(
            "all public types");
        marto::static_assert_is_same<typename cc::private_tuple_t,
                                     std::tuple<A>>("all private types");
        marto::static_assert_is_same<typename cc::other_tuple_t,
                                     std::tuple<short>>("all other types");
        marto::static_assert_is_same<typename cc::all_tuple_t,
                                     std::tuple<short, long, A, char, B>>(
            "all types");
        marto::static_assert_is_same<typename cc::inherit_ctor, std::tuple<A>>(
            "ctor to inherit");
        std::cerr << marto::type_name<cc>() << std::endl;
    }
    {
        using namespace marto::collect;
        typedef collect_classes_t<
            as_tuple<std::tuple<
                short, as_private<>,
                as_public<long, as_private<inherit_ctor<A>, as_public<char>>,
                          B>>>,
            collector::visibility, collector::all_classes,
            collector::inherit_ctor>
            cc;
        marto::static_assert_is_same<typename cc::public_tuple_t,
                                     std::tuple<long, char, B>>(
            "all public types");
        marto::static_assert_is_same<typename cc::private_tuple_t,
                                     std::tuple<A>>("all private types");
        marto::static_assert_is_same<typename cc::other_tuple_t,
                                     std::tuple<short>>("all other types");
        marto::static_assert_is_same<typename cc::all_tuple_t,
                                     std::tuple<short, long, A, char, B>>(
            "all types");
        marto::static_assert_is_same<typename cc::inherit_ctor, std::tuple<A>>(
            "ctor to inherit");
        std::cerr << marto::type_name<cc>() << std::endl;
    }
}

TEST_F(UtilsBaseTest, InheritanceTest) {
    {
        std::cerr << std::hex << "global_features_mask="
                  << marto::collect::bits::visibility::global_features_mask
                  << std::endl;
#if 0
        Z* z = new Z();
        ASSERT_EQ(RC<A>::nb, 2);
        ASSERT_EQ(RC<B>::nb, 2);
        ASSERT_EQ(RC<W>::nb, 1);
        ASSERT_EQ(RC<Z>::nb, 1);
        ASSERT_EQ(z->name(), "AB/-AB");
        ASSERT_EQ(w->name(), "AB/-AB");
        ASSERT_EQ(z->sname(), "ab/-ab");
        ASSERT_EQ(w->sname(), "ab/-ab");
        delete(w);
        delete(z);
#endif
        std::cout << "I1: " << marto::type_name<I1::type>() << std::endl;
        std::string expected{
            R""""(I1: marto::inherit_<true, std::tuple<>, std::tuple<>, std::tuple<>, std::tuple<>, std::tuple<T1, T2, T3>, std::tuple<>, std::tuple<>, std::tuple<> >
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);

        std::cout << "I2: " << marto::type_name<I2::type>() << std::endl;
        expected =
            R""""(I2: marto::inherit_<true, std::tuple<>, std::tuple<>, std::tuple<>, std::tuple<T1, T4>, std::tuple<T2>, std::tuple<>, std::tuple<>, std::tuple<> >
)"""";
        actual = get_buffer();
        EXPECT_EQ(expected, actual);

        I2 i2(12);
        std::cout << i2.get_T2() << "\n";
        // std::cout << i2.get_T3() << "\n"; // unexistant
        // std::cout << i2.get_T4() << "\n"; // unaccessible
        expected = R""""(  **** building parameter T1
  **** building parameter T4
  **** building parameter T2
Creating I2 with value 12
42
)"""";
        actual = get_buffer();
        EXPECT_EQ(expected, actual);
        I3 i3(24);
        // std::cout << i3.get_T2() << "\n"; // protected
        expected = R""""(  **** building parameter T5
  **** building parameter T1
  **** building parameter T4
  **** building parameter T2
Creating I2 with value 24
  **** building parameter T6
)"""";
        actual = get_buffer();
        EXPECT_EQ(expected, actual);
    }
}

} // namespace
