#include "test_transition.h"
#include <iostream>
#include <marto.h>

using namespace marto;

// Do test stuff
int main() {
    auto cdel = new_shared<Simulation>();
    Simulation *config = cdel.get();
    // Fill the hardcoded transition names
    new_shared<TransitionTest>("TransitionTest", *config);

    for (int i = 0; i < 3; i++) {
        new_shared<StandardQueue>(std::string("q") + std::to_string(i), *config,
                                  10);
    }
    auto p = std::make_unique<Point>(config, 0);
    for (int i = 0; i < 3; i++)
        p->at((queue_id_t)i)->addClient(i + 1);
    EventType::make(config, "My super event", 42.0, "TransitionTest");
    auto et = config->getEventType("My super event");
    std::cout << "et    : " << et << std::endl;
    std::cout << "et.get: " << et.get() << std::endl;
    std::cout << "et.use_count: " << et.use_count() << std::endl;
    auto e = std::make_shared<EventState>(et);
    // Fixme: handle randomGenerator
    e->generate(nullptr);

    std::cout << p.get() << std::endl;
    e->apply(p.get());
    std::cout << p.get() << std::endl;
    std::cout << "Success" << std::endl;
    return 0;
}
