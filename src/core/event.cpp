#include <cstdint>
#include <iostream>
#include <marto.h>
#include <marto/event.h>

#include <cassert>

namespace marto {

/****************************************************************************
 * marto::EventType
 */

#if 0
// TODO: wrong: we should write EventState, not EventType)
std::ostream &operator<<(std::ostream &out, const marto::EventType &ev) {
    out << "EventType : " << ev.name() << std::endl;
    out << "-> " << ev.transition_ << std::endl;
    out << "-> parameters : " << std::endl;
    for (auto it = ev.formalParameters.begin(); it != ev.formalParameters.end();
         it++) {
        // TODO : missing << for FormalOLDParameterValue
        out << "Un parametre trouvé " << std::endl;
    }
    return out;
}
#endif

EventType::EventType(Simulation *c, double evtRate,
                     shared_ptr<Transition> transition)
    : collection_class(c->global_parameters()),
      symbolicTransition_(
          new_shared<marto::SymbolicTransition>(std::move(transition))),
      _rate(evtRate) {
    // FIXME: handle an internal state so that parameters cannot be modified
    // once an EventType is used by an Event
}

EventType::pointer EventType::make(Simulation *config,
                                   const std::string eventName, double evtRate,
                                   const std::string &trName) {
    auto transition = config->getTransition(trName);
    return new_shared<EventType>(eventName, *config, config, evtRate,
                                 transition);
}

void EventType::registerParameter(const std::string name,
                                  SymbolicParameter::pointer sp) {
    // FIXME: see previous comment
    parameters()->add_symbolic_parameter(name, std::move(sp));
}

void EventType::compile(shared_ptr<SymbolicNamedParameters> frame) {
    CompilationExceptions e("while compiling MyEventType");
    try {
        SymbolicNamedParameters::compile(frame);
    } catch (...) {
        e.push_back(std::current_exception());
    }
    if (!transition()) {
        try {
            throw CompilationException(std::string("No transition set up"));
        } catch (...) {
            e.push_back(std::current_exception());
        }
    } else {
        try {
            symbolicTransition_->compile(
                std::static_pointer_cast<SymbolicNamedParameters>(
                    shared_from_this()));
        } catch (...) {
            try {
                std::throw_with_nested(CompilationException(
                    std::string("while compiling transition ") +
                    transition()->transition()->name()));
            } catch (...) {
                // TODO : remove the current entry
                e.push_back(std::current_exception());
            }
        }
    }
    if (!e.empty()) {
        throw e;
    }
}

/****************************************************************************
 * marto::EventState
 */

EventState::EventState(marto::shared_ptr<const EventType> type)
    : type_(std::move(type)), code_(type_->code()),
      parameters_(new_shared<ParameterValuesContainer>(
          type_->const_parameters(), EvaluationTiming::event,
          new_shared<ParameterValuesContainer>(
              type_->const_parameters(), EvaluationTiming::constant, nullptr)
          // nullptr
          /* FIXME: link to constant */)),
      status_(EVENT_STATUS_INVALID) {
    // FIXME : allocate through transition requirements
    // parameters_->allocate(this->type()->const_parameters());
    status_ = EVENT_STATUS_READY;
}

history_access_t EventState::load(HistoryIStream &istream) {
#if 0
    // FIXME
    auto fpit = type()->formalParameters.begin();
    auto pit = parameters.begin();
    for (; fpit != type()->formalParameters.end(); fpit++, pit++) {
        auto res = (*fpit)->load(istream, *pit);
        if (res != HISTORY_DATA_LOADED) {
            return res;
        }
    }
#endif
    std::cerr << __FUNCTION__ << ": FIXME" << std::endl;
    status_ = EventState::EVENT_STATUS_FILLED;
    return HISTORY_DATA_LOADED;
}

history_access_t EventState::store(HistoryOStream &ostream) const {
#if 0
    // FIXME
    auto fpit = type()->formalParameters.begin();
    auto pit = parameters.begin();
    for (; fpit != type()->formalParameters.end(); fpit++, pit++) {
        (*fpit)->store(ostream, *pit);
    }
#endif
    return HISTORY_DATA_STORED;
}

void EventState::generate(EventGenerationData *g) {
#if 0
    // FIXME
    size_t i = 0;
    for (auto fp : type()->formalParameters) {
        fp->generate(parameters[i], g);
        i++;
    }
#endif
    type()->transition()->compute_parameters_values(type()->const_parameters(),
                                                    parameters_, g);
    status_ = EVENT_STATUS_FILLED;
}

/****************************************************************************
 * marto::Event
 */

Event::Event(marto::shared_ptr<const EventType> type)
    : collection_class(std::move(type)) {

    // Too early, index set after ctor
    // assert(code() == map_element_of<HistoryIterator>::index());

    // for (size_t i = 0; i < max_param; i++) {
    //    parameters[i] = new OLDParameterValues();
    //}
}

} // namespace marto

#if 0 // FIXME DEAD CODE ?
using namespace marto;

template <class Kind, class Parameters> //
class QueueHeadFunctionParameters {
  public:
    marto::Parameter<Kind, QueueHeadFunctionParameters, marto::queue_id_t> from;
    // ParamList<Kind, queue_id_t, 2, 2> to;
    QueueHeadFunctionParameters(Parameters *params) : from(params, "from"){};
};
class HeadFunction
    : public SymbolicCallableFunction<HeadFunction>,
      public marto::UsingParameters<QueueHeadFunctionParameters, HeadFunction> {
};
#endif
