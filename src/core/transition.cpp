#include <marto.h>
#include <stdexcept>

/** doc from transition.cpp */
namespace marto {

SetImpl *Transition::apply(SetImpl *s, const EventState *ev) const {
    return s->accept(this, ev);
}

int Transition::apply(Set *s, const EventState *ev) const {
    SetImpl *src = s->realset();
    SetImpl *res =
        src->accept(this, ev); // specialized apply may have side effects
    s->realset(res);
    return 0;
}

// The following method is virtual pure :
// It has to be given explicitely for each concrete transition type
// Point *marto::Transition::apply(Point *s, EventState *ev) = 0;
// Non monototic transitions must also supply
// SetImpl *MonotonicTransition::apply(HyperRectangle *h, EventState *ev);

SetImpl *Transition::apply(Union *u, const EventState *ev) const {
    for (auto it = u->begin(); it != u->end(); it++)
        (*it) = apply(*it, ev);
    // TODO: merge results
    // Union of Union are union, point inside Union can be removed, etc.
    return u;
}

/** Apply transition to a set of states with hyperrectangle structure;
 *
 * The transition is considered as monotone
 */
SetImpl *MonotonicTransition::apply(HyperRectangle *h,
                                    const EventState *ev) const {
    apply(h->inf(), ev);
    apply(h->sup(), ev);
    return h;
}

} // namespace marto
