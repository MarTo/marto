#include <cassert>
#include <cstdint>
#include <marto/event.h>
#include <marto/history-stream.h>
#include <marto/history.h>

namespace marto {

/****************************************************************************
 * marto::HistoryChunk
 */

HistoryChunk::HistoryChunk(uint32_t capacity, HistoryChunk *prev,
                           HistoryChunk *next, Random::pointer rand,
                           History *hist)
    : allocOwner(true), eventsCapacity(capacity), nbEvents(0), nextChunk(next),
      prevChunk(prev), history(hist), random(rand) {
    size_t bufferSize;
    bufferMemory = history->allocChunkBuffer(&bufferSize);
    assert(bufferMemory != nullptr);
    bufferStart = bufferMemory;
    bufferEnd = bufferMemory + bufferSize;
}

HistoryChunk::~HistoryChunk() {
    if (allocOwner) {
        // TODO: a reference counter should probably be used when/if we use
        // the same buffer fore different chunks
        free(bufferMemory);
    }
}

HistoryChunk *HistoryChunk::getNextChunk() { return nextChunk; }

HistoryChunk *HistoryChunk::allocateNextChunk() {
    uint32_t capacity;
    if (marto_unlikely(eventsCapacity == UINT32_MAX)) {
        capacity = UINT32_MAX;
    } else {
        capacity = eventsCapacity - nbEvents;
    }
    HistoryChunk *newChunk =
        new HistoryChunk(capacity, this, nextChunk, random, history);
    if (nextChunk) {
        nextChunk->prevChunk = newChunk;
    }
    nextChunk = newChunk;
    eventsCapacity = nbEvents;
    return nextChunk;
}

/****************************************************************************
 * marto::HistoryIterator
 */

HistoryIterator::HistoryIterator(History *hist) {
    setNewChunk(hist->firstChunk);
    for (auto const &evt :
         hist->config()->map_of<EventType>::as_const_vector()) {
        new_shared<Event>(*this, evt);
    }
}

HistoryChunk *HistoryIterator::setNewChunk(HistoryChunk *chunk) {
    curChunk = chunk;
    if (marto_unlikely(chunk == nullptr)) {
        return nullptr;
    }
    position = curChunk->bufferStart;
    eventNumber = 0;
    return chunk;
};

marto::shared_ptr<Event> HistoryIterator::loadNextEvent() {
    assert(curChunk != nullptr);
    while (marto_unlikely(eventNumber >= curChunk->eventsCapacity)) {
        if (marto_unlikely(setNewChunk(curChunk->getNextChunk()) == nullptr)) {
            /* No more chunks */
            // TODO RAISE exception
            return marto::shared_ptr<Event>();
        }
    }
    if (marto_unlikely(eventNumber >= curChunk->nbEvents)) {
        /* No more events in current chunk */
        return marto::shared_ptr<Event>();
    }
    char *buffer = position;
    HistoryIStream istream(buffer, curChunk->bufferEnd - buffer);
    auto evRead = loadEventContent(istream);

    if (evRead.get()) {
        position += istream.objectSize();
        eventNumber++;
    }

    return evRead;
}

/*  an eventType is stored compactly as an integer
   (example of event type : arrival in queue 1)
   it is read from a table built from user config file
 */
marto::shared_ptr<Event>
HistoryIterator::loadEventContent(HistoryIStream &istream) {
    event_code_t code;
    if (istream >> code) { // eventype is encoded in the first integer of the
        // event buffer.
        marto::shared_ptr<Event> p_ev = vector_of<Event>::operator[](code);
        p_ev->load(istream);
        return p_ev;
    }
    return marto::shared_ptr<Event>();
}

history_access_t HistoryIterator::readyToStore() {
    assert(curChunk != nullptr);
    while (marto_unlikely(eventNumber >= curChunk->eventsCapacity)) {
        if (marto_unlikely(setNewChunk(curChunk->getNextChunk()) == nullptr)) {
            return HISTORY_END_HISTORY;
        }
    }
    /* We must be at the end of a chunk */
    if (eventNumber != curChunk->nbEvents) {
        return HISTORY_DATA_STORE_ERROR;
    }
    return HISTORY_END_DATA;
}

marto::shared_ptr<Event> HistoryIterator::generateNextEvent() {
    if (readyToStore() != HISTORY_END_DATA) {
        return marto::shared_ptr<Event>(); // HISTORY_DATA_STORE_ERROR;
    }
    // TODO
}

marto::shared_ptr<Event> HistoryIterator::getNextEvent() {
    auto p_ev = loadNextEvent();
    if (!p_ev.get()) {
        p_ev = generateNextEvent();
    }
    return p_ev;
}

history_access_t HistoryIterator::storeNextEvent(const EventState &ev) {
    bool new_chunk = false;
    do {
        /* We must be at the end of a chunk */
        if (readyToStore() != HISTORY_END_DATA) {
            return HISTORY_DATA_STORE_ERROR;
        }

        char *buffer = position;

        try {
            HistoryOStream ostream(
                buffer,
                curChunk->bufferEnd - buffer // available size
            );

            history_access_t access = storeEventContent(ostream, ev);
            if (access != HISTORY_DATA_STORED) {
                ostream.abort();
            } else {
                access =
                    ostream.finalize(); // used to store the event size in chunk
                if (access != HISTORY_DATA_STORED) {
                    ostream.abort();
                } else {
                    position += ostream.objectSize();
                    eventNumber++;
                    curChunk->nbEvents++;
                }
            }
            return access;
        } catch (HistoryOutOfBound const &h) {
            if (marto_unlikely(new_chunk)) {
                // event too big to be stored in one chunk
                return HISTORY_OBJECT_TOO_BIG;
            }
            if (marto_unlikely(setNewChunk(curChunk->allocateNextChunk()) ==
                               nullptr)) {
                return HISTORY_END_HISTORY;
            }
            new_chunk = true;
        }
    } while (true); // start over after creating new chunk
}

history_access_t HistoryIterator::storeEventContent(HistoryOStream &ostream,
                                                    const EventState &ev) {
    if (marto_unlikely(!ev.valid())) {
        return HISTORY_STORE_INVALID_EVENT;
    }
    if (ostream << ev.code()) {
        return ev.store(ostream);
    }
    return HISTORY_DATA_STORE_ERROR;
}

/****************************************************************************
 * marto::History
 */

History::History(Simulation *conf)
    : WithSimulation(conf), firstChunk(nullptr) {}

History::~History() {
    auto chunk = firstChunk;
    while (chunk) {
        auto dchunk = chunk;
        chunk = chunk->nextChunk;
        delete (dchunk);
    }
}

HistoryIterator *History::iterator() {
    if (firstChunk == nullptr) {
        // Empty history, creating a chunk
        // no need to restrict the number of events
        firstChunk = new HistoryChunk(UINT32_MAX, nullptr, nullptr,
                                      config()->newRandom(), this);
    }
    return new HistoryIterator(this);
}

void History::backward(uint32_t nbEvents) {
    if (nbEvents == 0) {
        return;
    }
    HistoryChunk *chunk = new HistoryChunk(nbEvents, nullptr, firstChunk,
                                           config()->newRandom(), this);
    if (firstChunk) {
        firstChunk->prevChunk = chunk;
    }
    firstChunk = chunk;
}
} // namespace marto
