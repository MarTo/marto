/* -*-c++-*- C++ mode for emacs */
/* Symbolic parameters implementation */

#include <marto/symbolic-parameters.h>

#ifndef MARTO_SYMBOLIC_PARAMETERS_IMPL_H
#define MARTO_SYMBOLIC_PARAMETERS_IMPL_H

#ifdef __cplusplus

#include <marto/parameters-values.h>
#include <marto/transition.h>

#include <marto/headers-impl.h>

namespace marto {

/****************************************************************************
 * marto::SymbolicFunction registration
 */
template <class TRegistered>
void FunctionRegistrationInfo::Register<TRegistered>::operator()(
    std::string name, Simulation *sim) {
    std::cerr << "Registering " << name << " in " << sim << std::endl;
    // TODO sim->add_symbolic_parameter(name, new TRegistered());
}

/****************************************************************************
 * marto::SymbolicNamedParameters
 */

inline void SymbolicNamedParameters::add_symbolic_parameter(
    std::string name, shared_ptr<SymbolicParameter> symbolic_param_value) {
    // TODO: check name validity
    new_shared<SymbolicNamedParameter>(std::move(name), *this,
                                       std::move(symbolic_param_value));
    // LOGINIT(new SymbolicNamedParameter(this->get_shared_ptr(),
    // std::move(name),
    //                                    std::move(symbolic_param_value)));
}

#if 0
inline void SymbolicNamedParameters::add_symbolic_parameter(
    std::string name, SymbolicParameter *symbolic_param_value) {
    return add_symbolic_parameter(std::move(name),
                                  symbolic_param_value->shared_from_this());
}
#endif

inline shared_ptr<SymbolicNamedParameter>
SymbolicNamedParameters::lookup(const std::string &name, bool recursive) const {
    try {
        return map_of<SymbolicNamedParameter>::operator[](name);
    } catch (std::out_of_range &e) {
        if (recursive && frame_up_) {
            return frame_up_->lookup(name, recursive);
        } else {
            throw UnknownName(name,
                              std::string("cannot find symbol with name '") +
                                  name + "'");
        }
    }
}

inline shared_ptr<SymbolicNamedParameter>
SymbolicNamedParameters::lookup(size_t index) const {
    return map_of<SymbolicNamedParameter>::operator[](
        index); // TODO: map.at(index) to check bounds?
}

inline shared_ptr<SymbolicParameter>
SymbolicNamedParameters::operator[](const std::string &name) const {
    return lookup(name)->compiled()->shared_from_this();
}

/****************************************************************************
 * marto::SymbolicTransition
 */

inline SymbolicTransition::SymbolicTransition(const Transition *t)
    : SymbolicTransition(t->shared_from_this()) {}

inline SymbolicTransition::operator std::string() const {
    std::string str = "*TRANSITION:" + transition_->name() + "*(";
    std::string sep;
    for (auto const &param : std::as_const(transition_->as_vector())) {
        str += sep;
        str += param->name();
        // str += (std::string) * (fn_params->lookup(param.first)->value());
        sep = ", ";
    }
    str += ")";
    return str;
}

inline void SymbolicTransition::compute_parameters_values(
    marto::shared_ptr<const SymbolicNamedParameters> sps,
    marto::shared_ptr<ParameterValuesContainer> pvc,
    EventGenerationData *g) const {
    transition()->compute_parameters_values(sps, pvc, g);
}

} // namespace marto

#endif // __cplusplus
#endif
