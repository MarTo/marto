/* -*-c++-*- C++ mode for emacs */

#include <marto/transition.h>

#ifndef MARTO_TRANSITION_IMPL_H
#define MARTO_TRANSITION_IMPL_H

#ifdef __cplusplus

#include <marto/headers-impl.h>

namespace marto {
namespace transition {} // namespace transition
} // namespace marto

#endif // __cplusplus
#endif
