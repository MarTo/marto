/* -*-c++-*- C++ mode for emacs */
/* Parameters when used in code */
#ifndef MARTO_PARAMETERS_VALUES_H
#define MARTO_PARAMETERS_VALUES_H

//#define MARTO_HEADER_NEED_PARAMETERS_VALUES_IMPL
//#ifndef MARTO_HEADER_NEED_IMPL
//#define MARTO_HEADER_NEED_IMPL
//#define MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_VALUES
//#endif

#ifdef __cplusplus

#include <cassert>

#include <marto/forward-decl.h>

#include <marto/parameters-timing.h>
#include <marto/parameters-types.h>
#include <marto/symbolic-parameters.h>

//#include <marto/debug.h>
//#define MARTO_DEBUG
//#undef MARTO_DEBUG

namespace marto {

/**
 */
class ParameterValuesContainer
    : public enable_make_shared<ParameterValuesContainer,
                                as_public<collect::add_shared_from_this>> {
  protected:
    ParameterValuesContainer(
        marto::shared_ptr<const SymbolicNamedParameters> sps,
        EvaluationTiming timing,
        marto::shared_ptr<ParameterValuesContainer> pvc_link = nullptr)
        : timing_(timing), /*sps_(std::move(sps)), offset_(timing + 1),*/
          pvc_link_(pvc_link), values_(), values_containers_() {
#ifdef MARTO_DEBUG
        std::cerr << marto::type_name(this) << " creating at " << this
                  << std::endl;
#endif

        auto it = EvaluationTiming::Iterator();
        if (pvc_link) {
            auto prev_timing = pvc_link->timing_;
            assert(prev_timing < timing);
            values_containers_ = pvc_link->values_containers_;
            it = EvaluationTiming::Iterator(prev_timing);
            ++it;
        }
        values_containers_.resize(timing + 1);
        /* this->shared_from_this() cannot be invoked yet. It must be
         * delayed after the end of the constructor. So delegating
         * this to the `make_shared_extension()` method.
         */
        // values_containers_[timing] =
        //     marto::weak_ptr<ParameterValuesContainer>(this->shared_from_this());
        /* All parameters for previous timings must have been handled or not be
         * present */
        for (EvaluationTiming t : it) {
            if (t == timing) {
                break;
            }
            assert(sps->nb_slots(t) == 0);
        }
        values_.resize(sps->nb_slots(timing));
#ifdef MARTO_DEBUG
        std::cerr << "pvc_link_ -> " << pvc_link_.get()
                  << " use_count()=" << pvc_link_.use_count() << std::endl;
        std::cerr << marto::type_name(this) << " created at " << this
                  << std::endl;
#endif
    }

  public:
    ~ParameterValuesContainer() {
#ifdef MARTO_DEBUG
        std::cerr << marto::type_name(this) << " destroyed at " << this
                  << std::endl;
#endif
    }

  private:
    template <typename Extend, typename Extended, typename Inherit,
              typename BasesTuple>
    friend struct extend_make_shared_;

    template <typename EMS_pointer, typename F, typename... Args>
    static pointer make_shared_extension(F &&f, Args &&...args) {
        auto cb = std::function<pointer(Args...)>(std::forward<F>(f));
#ifdef MARTO_DEBUG
        std::cerr << "** allocating with " << marto::type_name(&cb)
                  << std::endl;
#endif
        pointer ptr = cb(std::forward<Args>(args)...);
#ifdef MARTO_DEBUG
        std::cerr << "** allocated " << marto::type_name(ptr.get()) << " at "
                  << ptr.get() << ": count=" << ptr.use_count() << std::endl;
#endif

        ptr->values_containers_[ptr->timing_] =
            marto::weak_ptr<ParameterValuesContainer>(ptr);

#ifdef MARTO_DEBUG
        std::cerr << marto::type_name(ptr.get())
                  << ": count=" << ptr.use_count() << std::endl;
#endif

        return ptr;
    }

  public:
    //~ParameterValuesContainer() {
    //    std::cerr << "Deleting  " << type_name(this) << " at " << this <<
    //    std::endl;
    //}
    typedef MartoTypeBase::symbolic_parameter_pointer
        symbolic_parameter_pointer;

    template <class MartoType>
    typename MartoType::typed_values_pointer
    get(symbolic_parameter_pointer sp) {
        EvaluationTiming t = (EvaluationTiming)(*sp);
        size_t slot_idx = sp->slot();
#ifdef MARTO_DEBUG
        std::cerr << "Getting ParameterValues for " << sp << " ("
                  << (std::string)(*sp) << ") \""
                  << " -> " << (std::string)*sp << std::endl;
        std::cerr << "Time: " << t << " slot: " << slot_idx << std::endl;
#endif
        auto pvc_values = get(t, slot_idx);
        typename MartoType::typed_values_pointer values;
        if (pvc_values) {
#ifdef MARTO_DEBUG
            std::cerr << "pvc_values existing at " << pvc_values.get()
                      << ": use_count=" << pvc_values.use_count() << std::endl;
#endif
            values = std::dynamic_pointer_cast<TypedParameterValues<MartoType>>(
                pvc_values);
            if (!values) {
                throw marto::SimulationException(
                    type_name(sp.get()) + " '" + (std::string)(*sp) +
                    "' cannot get values of type " +
                    type_name<typename MartoType::value_t>());
            }
        } else {
            values = MartoType::allocate_parameter_values(sp);
            if (!values) {
                throw marto::SimulationException(
                    type_name(sp.get()) + " '" + (std::string)(*sp) +
                    "' cannot allocate values of type " +
                    type_name<typename MartoType::value_t>());
            }
#ifdef MARTO_DEBUG
            std::cerr << "pvc_values created at " << values.get()
                      << ": use_count=" << values.use_count() << std::endl;
#endif
            set<MartoType>(t, slot_idx, values);
        }
#ifdef MARTO_DEBUG
        std::cerr << "Values addr: " << values << std::endl;
#endif
        return values;
    }

  private:
    marto::shared_ptr<ParameterValues> get(EvaluationTiming t,
                                           size_t slot_idx) {
        assert(t != 0);
        ParameterValuesContainer::pointer container(values_containers_[t]);
        assert(container.use_count() >= 2);
        assert(slot_idx < container->values_.size());
#ifdef MARTO_DEBUG
        std::cerr << "Getting slot " << slot_idx << " for timing " << (int)t
                  << ": " << container->values_[slot_idx] << std::endl;
#endif
        return container->values_[slot_idx];
    }

    template <class MartoType>
    typename MartoType::typed_values_pointer
    set(EvaluationTiming t, size_t slot_idx,
        typename MartoType::typed_values_pointer values) {
        assert(t != 0);
        ParameterValuesContainer::pointer container(values_containers_[t]);
        assert(slot_idx < container->values_.size());
#ifdef MARTO_DEBUG
        std::cerr << "Allocating slot " << slot_idx << " for timing " << (int)t
                  << ": " << values << std::endl;
#endif
        container->values_[slot_idx] =
            std::static_pointer_cast<ParameterValues>(values);
#ifdef MARTO_DEBUG
        auto v = container->values_[slot_idx];
        std::cerr << "Stored values " << v.get()
                  << ": use_count=" << v.use_count() << std::endl;
#endif
        return values;
    }

  private:
    EvaluationTiming timing_;
    // marto::weak_ptr<SymbolicNamedParameters> sps_;
    marto::shared_ptr<ParameterValuesContainer> pvc_link_;
    std::vector<marto::shared_ptr<ParameterValues>> values_;
    // Implementation note:
    // As we put ourself into the vector, we must use weak_ptr to avoid cycle...
    // We already maintain the objects through the 'pvc_link_' list;
    std::vector<marto::weak_ptr<ParameterValuesContainer>> values_containers_;
};

class ParameterValues : public enable_shared_from_this<ParameterValues> {
  protected:
    ParameterValues(const ParametersTypeInfo &t) : typeinfo(t) {}

  public:
    // Polymorphic class
    virtual ~ParameterValues() = default;

    template <class value_t>
    typename MartoTypeStandard<value_t>::typed_values_pointer as() {
        assert(typeinfo == MartoTypeStandard<value_t>::info);
        return typename MartoTypeStandard<value_t>::typed_values_pointer(
            std::dynamic_pointer_cast<
                TypedParameterValues<MartoTypeStandard<value_t>>>(
                this->shared_from_this()));
    }

    const ParametersTypeInfo &typeinfo;
};

template <class MartoType>
class TypedParameterValues
    : public enable_make_shared<TypedParameterValues<MartoType>,
                                as_public<inherit_ctor<ParameterValues>>> {
  protected:
    using typename enable_make_shared<
        TypedParameterValues<MartoType>,
        as_public<inherit_ctor<ParameterValues>>>::make_shared_class;
    TypedParameterValues()
        : make_shared_class(MartoTypeStandard<value_t>::info) {
        // values.resize(s);
    }

  public:
    typedef typename MartoType::value_t value_t;
    virtual value_t operator[](size_t idx) const = 0;
    template <typename val_t = value_t> val_t get(size_t idx) const {
        static_assert(std::is_same_v<value_t, val_t>, "Wrong type requested");
        return (*this)[idx];
        // return martotype_trait<MartoType, val_t>::check_type((*this)[idx]);
    }
};
// template <class MartoType>
// template<>
// inline typename TypedParameterValues<MartoType>::value_t
// TypedParameterValues<MartoType>::get(size_t idx) const { return this->[idx];
// }

template <class MartoType>
class FixedLenParameterValues
    : public enable_make_shared<
          FixedLenParameterValues<MartoType>,
          as_public<inherit_ctor<TypedParameterValues<MartoType>>>> {

  public:
    using typename TypedParameterValues<MartoType>::value_t;

    FixedLenParameterValues(size_t s) : values_(s) {
#ifdef MARTO_DEBUG
        std::cerr << "Creating " << type_name(this) << " of size " << s
                  << " at " << this << std::endl;
#endif
        values_.resize(s);
    }

    FixedLenParameterValues(std::vector<value_t> v) : values_(std::move(v)) {
#ifdef MARTO_DEBUG
        std::cerr << "Creating " << type_name(this) << " from vector"
                  << " at " << this << std::endl;
#endif
        // values.resize(s);
    }

    ~FixedLenParameterValues() {
#ifdef MARTO_DEBUG
        std::cerr << "Destroying " << type_name(this) << " at " << this
                  << std::endl;
#endif
    }

    virtual value_t operator[](size_t idx) const { return values_.at(idx); }

    operator std::string() const {
        std::string str{"["};
        std::string sep;
        for (const auto &v : values_) {
            str += sep + std::to_string(v);
            sep = ", ";
        }
        return str + "]";
    }

  private:
    std::vector<value_t> values_;
};

} // namespace marto

#endif // __cplusplus

//#ifdef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_VALUES
//#undef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_VALUES
//#include <marto/headers-impl.h>
//#endif

#endif
