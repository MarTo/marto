/* -*-c++-*- C++ mode for emacs */
/* Queues management */
#ifndef MARTO_QUEUE_H
#define MARTO_QUEUE_H

#define MARTO_HEADER_NEED_QUEUE_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_QUEUE
#endif

#ifdef __cplusplus

#include <marto/collections.h>
#include <marto/forward-decl.h>
#include <marto/macros.h>
#include <marto/memory.h>
#include <marto/types.h>
#include <stdexcept>

namespace marto {

/** Static parameters for Queues
 *
 * One object of this type will be created by the config
 */
class Queue
#ifndef SWIG
    : public is_element<Queue, collect::add_shared_from_this,
                        as_protected<in_map<Simulation>>>
#endif
{
  protected:
    Queue(){};

  public:
    virtual ~Queue(void) {}
    /** Get the name of the queue
     */
    const std::string name() { return map_element_of<Simulation>::name(); };
    /** Get the queue id of the queue
     */
    queue_id_t id() { return (queue_id_t)map_element_of<Simulation>::index(); };
    /** Tell if the queue is special or classic

       Special queues:
       - do not need to be stored into the Point (ie their state is constant)
       - can have a specific, pre-reserved, ID
     */
    virtual bool hasNoState() { return false; }
    /** return a new queue state
     *
     * TODO: should check that the queue config is registered
     */
    virtual std::shared_ptr<QueueState> newQueueState() {
        return allocateQueueState();
    }
    virtual std::shared_ptr<QueueState> newQueueState(queue_value_t value) {
        return allocateQueueState(value);
    }
    /** return the minimum of the queue */
    virtual queue_value_t min() = 0;
    /** return the maximum of the queue */
    virtual queue_value_t max() = 0;

  protected:
    virtual std::shared_ptr<QueueState> allocateQueueState() = 0;
    virtual std::shared_ptr<QueueState>
    allocateQueueState(queue_value_t value) = 0;
};

/** Static parameters for state-less queues
 *
 */
class StateLessQueue :
#ifdef SWIG
        public Queue
#else
    public enable_make_shared<StateLessQueue, as_public<inherit_ctor<Queue>>>
#endif
{
  private:
    std::weak_ptr<QueueState> queueState;

    /** Inherit Queue contructors
     */
    using make_shared_class::make_shared_class;

  public:
    virtual bool hasNoState() { return true; }
    virtual std::shared_ptr<QueueState> newQueueState() {
        if (marto_unlikely(queueState.expired())) {
            queueState = allocateQueueState();
        }
        return std::shared_ptr<QueueState>(queueState);
    }
    /** return the minimum of the queue */
    virtual queue_value_t min() {
        throw std::invalid_argument("No state for 'state-less' queues");
    }
    /** return the maximum of the queue */
    virtual queue_value_t max() {
        throw std::invalid_argument("No state for 'state-less' queues");
    }

  protected:
    virtual std::shared_ptr<QueueState> allocateQueueState() = 0;
    virtual std::shared_ptr<QueueState>
    allocateQueueState(queue_value_t __marto_unused(value)) {
        throw std::invalid_argument("No initial state for 'state-less' queues");
    }
};

/** State of a queue
 *
 * Each queue can have different state (at different time and/or to store
 * envelops)
 */
class QueueState {
  public:
    QueueState() {}
    virtual ~QueueState() {}
    virtual queue_value_t value() const = 0;
    virtual Queue::pointer queue() const = 0;

  protected:
    virtual Queue *realQueue() const = 0;

  public:
    virtual bool isEmpty() = 0;
    virtual bool isFull() = 0;
    virtual queue_value_t
    addClient(queue_value_t nb =
                  1) = 0; ///< return #clients really added (up to full)
    virtual queue_value_t
    removeClient(queue_value_t nb =
                     1) = 0; ///< return #client really removed (until empty)
    // TODO: choose if we use raw or smart pointer in the interface
    virtual int compareTo(QueueState *) = 0;
#if 1
    virtual int compareTo(std::shared_ptr<QueueState> q) {
        return compareTo(q.get());
    };
#endif
};

// is T derived from Queue?
template <typename T> // http://en.cppreference.com/w/cpp/header/type_traits
struct is_Queue : std::conditional<std::is_base_of<Queue, T>::value,
                                   std::true_type, std::false_type>::type {};
/** QueueState specialized for Queue
 *
 * QueueState is an abstract class. Each instance should be linked to
 * its Queue object (also an abstract class).
 *
 * TypedQueueState<Q> is derived from QueueState with a Q pointer attribute.
 * Q must be derivated from Queue (checked at compile time)
 *
 * The queue() method returns an Q (Queue derivated) object that
 * avoid dynamic or even static cast when accessing to the related
 * Queue object.
 */
template <class Q> class TypedQueueState : public QueueState {
    // compile-time assertion: Q must be derived from Queue
    static_assert(is_Queue<Q>::value, "Q must be derived from Queue");

  private:
    Queue::pointer const _queue;

  public:
    virtual Q *realQueue() const override {
        return static_cast<Q *>(_queue.get());
    }

  protected:
    virtual Queue::pointer queue() const override { return _queue; }

  public:
    TypedQueueState(Q *queue)
        : QueueState(), _queue(queue->shared_from_this()) {}
    TypedQueueState(Queue::pointer queue) : QueueState(), _queue(queue) {
        assert(dynamic_cast<Q>(queue.get()));
    }
    virtual ~TypedQueueState() {}
    virtual Queue::pointer queueBase() const { return queue(); }
};

/** Queue configuration for standard queues
 *  Name must be unique
 *  TODO: autonaming function for long queue lists
 */
class StandardQueue :
#ifdef SWIG
        public Queue
#else
    public enable_make_shared<StandardQueue, as_public<Queue>>
#endif
{
  private:
    const queue_value_t _capacity;

  protected:
    StandardQueue(int param_capacity) : _capacity(param_capacity) {
        if (param_capacity < 0) {
            throw std::invalid_argument("StandardQueue: negative capacity");
        }
    };

  public:
    queue_value_t capacity() const { return _capacity; }
    /** return the minimum of the queue */
    virtual queue_value_t min() { return 0; }
    /** return the maximum of the queue */
    virtual queue_value_t max() { return capacity(); }

  protected:
    virtual std::shared_ptr<QueueState> allocateQueueState();
    virtual std::shared_ptr<QueueState> allocateQueueState(queue_value_t value);
};

/** OutsideQueue is a pseudo-queue that mimics the external world
 * for arrivals, departures and "drop" events
 * This special queue is unique and has no state
 */
class OutsideQueue :
#ifdef SWIG
        public StateLessQueue
#else
    public enable_make_shared<OutsideQueue, as_public<StateLessQueue>>
#endif
{
    using make_shared_class::make_shared_class;

  protected:
    virtual std::shared_ptr<QueueState> allocateQueueState();
};
} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_QUEUE
#undef MARTO_HEADER_NEED_IMPL_FROM_QUEUE
#include <marto/headers-impl.h>
#endif

#endif
