/* -*-c++-*- C++ mode for emacs */
/* State representation */
#ifndef MARTO_STATE_H
#define MARTO_STATE_H

#define MARTO_HEADER_NEED_STATE_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_STATE
#endif

#ifdef __cplusplus

#include <functional>
#include <iostream>
#include <list>
#include <marto/forward-decl.h>
#include <marto/memory.h>
#include <marto/types.h>
#include <memory>
#include <vector>

namespace marto {

#ifndef SWIG
std::ostream &operator<<(std::ostream &out, const marto::Point &p);
#endif

/** Set implementation ; internal structure
 */
class SetImpl {
    /** used to call the transition:apply function
     * corresponding to the SetImpl type
     */
  public:
    virtual ~SetImpl() {}
    virtual SetImpl *accept(const Transition *t, const EventState *ev) = 0;
};

/** One point in the state space
 */
class Point : protected std::vector<std::shared_ptr<QueueState>>,
              public SetImpl,
              protected WithSimulation {
  public:
    typedef std::vector<std::shared_ptr<QueueState>>::value_type value_type;
    /** No default constructor, a Simulation must always be provided */
    Point() = delete;
    /** Create a point (queue states) from registered queue
     *
     * The queue states will have a default value
     */
    Point(Simulation *config);
    /* do not process callbacks with swig */
#ifndef SWIG
    /** Create a point (queue states) from registered queue
     *
     * The queue states will be initialized from the callback return value
     */
    typedef queue_value_t(initCallback_t)(queue_id_t id, shared_ptr<Queue> q,
                                          Point *p, void *arg);
    Point(Simulation *config, initCallback_t *callback, void *arg);
    /** callback type to help to initialize queues when creating a Point
     *
     * The computed queue_value_t will be given to the Queue
     * newQueueState (hence also allocate) method.
     */
    typedef std::function<queue_value_t(queue_id_t id, shared_ptr<Queue> q,
                                        Point *p)>
        initLambdaCallback_t;
    /** Create a point (queue states) from registered queue
     *
     * The queue states will be initialized from the lambda callback return
     * value
     */
    Point(Simulation *config, const initLambdaCallback_t &lambdaCallback);
    /** Create a point (queue states) from registered queue
     *
     * The queue states will be initialized from the value
     */
#endif
    Point(Simulation *config, queue_value_t value);
    // Specialisation: a Point always gives a Point
    virtual Point *accept(const Transition *t, const EventState *ev);
    /** accessing the various queues */
    value_type at(queue_id_t id) const {
        return std::vector<value_type>::at(id);
    }
#ifndef SWIG
    using std::vector<value_type>::begin;
    using std::vector<value_type>::end;
    using std::vector<value_type>::cbegin;
    using std::vector<value_type>::cend;
#endif
    const std::vector<queue_value_t> states() const;
};

/** Subset of the state space with a shape of hyperrectangle
 */
class HyperRectangle : public SetImpl {
  private:
    marto::Point *inf_, *sup_;

  public:
    /** Create a HypepRectangle from two Points
     *
     * the ownership of the two points are transfered to the HyperRectangle
     */
    HyperRectangle(Point *p1, Point *p2) : inf_(p1), sup_(p2) {}
    virtual ~HyperRectangle() {
        delete (inf_);
        inf_ = nullptr;
        delete (sup_);
        sup_ = nullptr;
    }
    virtual SetImpl *accept(const Transition *t, const EventState *ev);
    marto::Point *inf() { return inf_; };
    marto::Point *sup() { return sup_; };
};

#ifndef SWIG
// TODO: not correctly handled by swig */
/** Union of subsets of the state space
 */
class Union : public SetImpl {
  public:
    virtual SetImpl *accept(const Transition *t, const EventState *ev);

  private:
    typedef std::list<SetImpl *> unionList;
    unionList list;

  public:
    unionList::iterator begin() { return list.begin(); };
    unionList::iterator end() { return list.end(); };
    unionList::const_iterator begin() const { return list.begin(); };
    unionList::const_iterator end() const { return list.end(); };
};
#endif

/** Class that is a bridge to the actual set implementation
 */
class Set {
  public:
    /** Create a set of state that will evolve
     *
     * Note: the provided SetImpl will have its ownership transfered
     * to this object. Ie, it will be deleted if required by a
     * transition (so it must be properly allocated).
     */
    Set(SetImpl *s) : _realset(s) { assert(s != nullptr); }
    SetImpl *realset(void) { return _realset; }; // read
  private:
    /** the apply(Set *s, EventState *ev) method in Transition will
     * change the set if required. It will handle deletion if need be.
     */
    friend class Transition;
    /** update the set, should be used only by Transition::apply
     */
    void realset(SetImpl *s) {
        assert(s != nullptr);
        _realset = s;
    }; // write
    SetImpl *_realset;
};
} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_STATE
#undef MARTO_HEADER_NEED_IMPL_FROM_STATE
#include <marto/headers-impl.h>
#endif

#endif
