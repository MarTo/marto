/* -*-c++-*- C++ mode for emacs */
/* Parameters when used in code / templates implementation */

#include <marto/parameters.h>

#ifndef MARTO_PARAMETERS_IMPL_H
#define MARTO_PARAMETERS_IMPL_H

#ifdef __cplusplus

#include <marto/parameters-values.h>
#include <marto/symbolic-parameters.h>

#include <marto/headers-impl.h>

//#define MARTO_DEBUG
//#undef MARTO_DEBUG

namespace marto {

inline RequiredUntypedParameter::RequiredUntypedParameter(
    CallableFunctionBase *callable_function, const char *name) {
    // Required parameters are attributes, not allocated objects.
    // They must *never* be deallocated, so we increase the ref counter.
    // marto::RefCounted<RequiredUntypedParameter>::ref();

    init_shared(name, *callable_function);
    // RequiredUntypedParameter::
    //     sft_no_dynamic_allocation(); //
    //     marto::RefCounted<RequiredUntypedParameter>::disable_ref_counting();
}

inline const SymbolicBindingsInfo &
EffectiveParametersBase::bindings_info() const {
    return caller_.lock()->bindings_info();
}

/** Specialisation of the Parameter template for required parameters
 */
template <template <class, class> class ParametersClass, class ParameterType,
          template <class> class MartoTypeKind>
class Parameter<KindRequiredParameter, ParametersClass, ParameterType,
                MartoTypeKind> : public RequiredUntypedParameter {
  public:
    typedef MartoTypeKind<ParameterType> MartoType;
    Parameter(UsingParametersWithCF<ParametersClass> *params, const char *name)
        : RequiredUntypedParameter(&params->callable_function(), name){};
};

template <class MartoType> class EffectiveParameter {
  public:
    typedef typename MartoType::value_t value_t;
    typedef typename MartoType::typed_values_pointer typed_values_pointer;
    typedef typename MartoType::symbolic_parameter_pointer
        symbolic_parameter_pointer;

  protected:
    class EffectiveParameterInfo {
      public:
        size_t index;
        typed_values_pointer values;
        symbolic_parameter_pointer sp;
        template <template <class, class> class ParametersClass>
        EffectiveParameterInfo(EffectiveParameters<ParametersClass> *params) {
            index = params->register_parameter();
            sp = params->bindings_info()[index];
#ifdef MARTO_DEBUG
            std::cerr << "EffectiveParameterInfo for "
                      << "[" << index << "] \""
                      << params->required_parameters.callable_function()[index]
                             ->name()
                      << "\"" << std::endl;
#endif
            auto pvc = params->values_container();
            values = pvc->template get<MartoType>(sp);
#ifdef MARTO_DEBUG
            std::cerr << "Values addr: " << values << std::endl;
#endif
        }
    };

  public:
    EffectiveParameter(const EffectiveParameterInfo &info)
        : index_(info.index), values_(info.values), sp_(info.sp) {}
    typename MartoType::value_t operator[](size_t nb) const {
        return (*values())[nb];
    }
    operator typename MartoType::value_t() const { return (*this)[0]; }
    virtual const RequiredUntypedParameter *param_info() const = 0;
    size_t count() const {
        return 1; // FIXME
    }

  protected:
    typed_values_pointer values() const { return values_; }
    size_t index() const { return index_; }
    symbolic_parameter_pointer symbolic_parameter() const { return sp_; }

  private:
    const size_t index_;
    const typed_values_pointer values_;
    const symbolic_parameter_pointer sp_;
};

/** Specialisation of the Parameter template when using parameters values
 */
template <template <class, class> class ParametersClass, class ParameterType,
          template <class> class MartoTypeKind>
class Parameter<KindParameterValues, ParametersClass, ParameterType,
                MartoTypeKind>
    : public EffectiveParameter<MartoTypeKind<ParameterType>> {
  public:
    typedef MartoTypeKind<ParameterType> MartoType;
    typedef Parameter<KindRequiredParameter, ParametersClass, ParameterType,
                      MartoTypeKind>
        required_parameter_type;
    using typename EffectiveParameter<MartoType>::typed_values_pointer;
    using typename EffectiveParameter<MartoType>::symbolic_parameter_pointer;

  protected:
    using EffectiveParameter<MartoType>::index;

  private:
    EffectiveParameters<ParametersClass> &params_;
    const required_parameter_type *required_parameter;

  public:
    Parameter(EffectiveParameters<ParametersClass> *params, const char *name)
        : EffectiveParameter<MartoType>(
              typename EffectiveParameter<MartoType>::EffectiveParameterInfo(
                  params)),
          params_(*params),
          required_parameter(static_cast<required_parameter_type *>(
              params_.required_parameters.callable_function()[index()].get())) {
        assert(required_parameter->index() == index());
        assert(required_parameter->name() == name);
#ifdef MARTO_DEBUG
        std::cout << name << "/" << index() << " checked" << std::endl;
#endif
    };
    virtual const RequiredUntypedParameter *param_info() const override {
        return required_parameter;
    }
};

} // namespace marto

#endif // __cplusplus
#endif
