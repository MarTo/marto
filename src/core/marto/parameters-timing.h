/* -*-c++-*- C++ mode for emacs */
/* Parameters timing */
#ifndef MARTO_PARAMETERS_TIMING_H
#define MARTO_PARAMETERS_TIMING_H

#ifdef __cplusplus

#include <cstdint>
#include <marto/forward-decl.h>
#include <type_traits>

namespace marto {

class EvaluationTiming {
  public:
    enum Value_ : uint8_t { undefined, none, constant, event, state };

  private:
    enum Constant_MIN : uint8_t { MIN };
    enum Constant_MAX : uint8_t { MAX };

  public:
    EvaluationTiming() = default;
    constexpr EvaluationTiming(Value_ t) : timing_(t) {}
    constexpr EvaluationTiming(Constant_MIN) : timing_(undefined) {}
    constexpr EvaluationTiming(Constant_MAX) : timing_(state) {}

    // Allow switch and comparisons.
    constexpr operator Value_() const {
        // std::cerr << "Converting " << this << " to " << (int)timing_ <<
        // std::endl;
        return timing_;
    }
    explicit operator bool() = delete; // Prevent usage: if(timing)

    // not constexpr, see
    // https://stackoverflow.com/questions/36863503/struct-is-non-literal-type
    static const EvaluationTiming min;
    static const EvaluationTiming max;

  private:
    Value_ timing_;

  public:
    class Iterator {
        typedef Value_ C;
        typedef typename std::underlying_type<C>::type val_t;
        val_t val;

      public:
        Iterator(const EvaluationTiming &f) : val(static_cast<val_t>((C)f)) {}
        inline Iterator();
        Iterator operator++() {
            ++val;
            return *this;
        }
        EvaluationTiming operator*() { return static_cast<C>(val); }
        Iterator begin() { return *this; } // default ctor is good
        inline Iterator end();
        bool operator!=(const Iterator &i) { return val != i.val; }
    };
};
inline constexpr EvaluationTiming EvaluationTiming::min{EvaluationTiming::MIN};
inline constexpr EvaluationTiming EvaluationTiming::max{EvaluationTiming::MAX};
inline EvaluationTiming::Iterator EvaluationTiming::Iterator::end() {
    static const Iterator endIter = ++Iterator(max); // cache it
    return endIter;
}
inline EvaluationTiming::Iterator::Iterator() : val(static_cast<val_t>(min)) {}

} // namespace marto

#endif // __cplusplus

#endif
