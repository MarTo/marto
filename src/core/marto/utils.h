/* -*-c++-*- C++ mode for emacs */
/* Various utilities for marto */
#ifndef MARTO_UTILS_H
#define MARTO_UTILS_H

/* No need of external forward-declaration for this file
 *
 * Forward-declaration: none
 */

#ifdef __cplusplus

#include <marto/macros.h>
#include <tuple>
//#define DEBUG_MARTO

#ifdef DEBUG_MARTO
#include <iostream>
#include <marto/debug.h>
#endif

#include <tuple>

namespace marto {

//////////////////////////////////////////////////////////////////
/* Allows to check for virtual inheritance
 */

// First, a type trait to check whether a type can be static_casted to another
template <typename From, typename To, typename = void>
struct can_static_cast : std::false_type {};

template <typename From, typename To>
struct can_static_cast<
    From, To, std::void_t<decltype(static_cast<To>(std::declval<From>()))>>
    : std::true_type {};

// Then, we apply the fact that a virtual base is first and foremost a base,
// that, however, cannot be static_casted to its derived class.
template <typename Base, typename Derived>
struct is_virtual_base_of
    : std::conjunction<std::is_base_of<Base, Derived>,
                       std::negation<can_static_cast<Base *, Derived *>>> {};

template <typename Base, typename Derived>
static constexpr bool is_virtual_base_of_v =
    is_virtual_base_of<Base, Derived>::value;

//////////////////////////////////////////////////////////////////
/* C++20 trait
 */
template <class T> struct type_identity { using type = T; };

//////////////////////////////////////////////////////////////////
/* Get parameters types and return type of a function */
template <typename f> struct function_traits_helper;

template <typename R, typename... T>
struct function_traits_helper<R (*)(T...)> {
  public:
    static const std::size_t nargs = sizeof...(T);
    typedef std::tuple<T...> arguments_type;
    typedef R result_type;
};

template <typename F>
struct function_traits : function_traits_helper<std::add_pointer_t<F>> {};

template <class... T> constexpr bool always_false = false;

//////////////////////////////////////////////////////////////////
/* Allows one to delay an action to the end of live of the object, unless
 * disabled */
template <typename F> struct FinalAction {
    FinalAction(F f) : clean_{f} {}
    ~FinalAction() {
        if (enabled_)
            clean_();
    }
    void disable() { enabled_ = false; };

  private:
    F clean_;
    bool enabled_{true};
};

template <typename F> FinalAction<F> finally(F f) { return FinalAction<F>(f); }

//////////////////////////////////////////////////////////////////
/* Pointer deleter
 *
 * Delete the pointer when the object is destroyed
 */

template <class pointer_type> class PointerDeleter {
  private:
    pointer_type *adr_ptr;

  public:
    PointerDeleter(pointer_type *adr_ptr_) : adr_ptr(adr_ptr_) {
#ifdef DEBUG_MARTO
        std::cerr << "Initializing deleter for " << adr_ptr
                  << " type: " << marto::type_name<pointer_type>() << std::endl;
#endif
    }
    void ensure_allocation() const {}
    ~PointerDeleter() {
        if (*adr_ptr) {
#ifdef DEBUG_MARTO
            std::cerr << "Destroying pointer " << adr_ptr
                      << " type: " << marto::type_name<pointer_type>()
                      << std::endl;
#endif
            delete (*adr_ptr);
            adr_ptr = nullptr;
        } else {
            // should not occur. Asserting to be sure.
            assert(*adr_ptr);
        }
    }
};

namespace internal {

//////////////////////////////////////////////////////////////////
/* No-op deleter
 *
 * To be used with object (shared_ptr) that must not call delete
 */

template <typename T> struct no_deleter {
    void operator()(T const *__marto_unused(p)) const {
#ifdef MARTO_DEBUG
        std::cerr << "NOT DELETING " << p << std::endl;
#endif
    }
};

//////////////////////////////////////////////////////////////////
/* Select a type in a parameter pack
 */
template <int idx, typename... Args> struct select;

template <typename Arg, typename... Args> struct select<0, Arg, Args...> {
    typedef Arg type;
    static const bool value = true;
};

template <int idx> struct select<idx> {
    typedef void type;
    static const bool value = false;
};

template <int idx, typename Arg, typename... Args>
struct select<idx, Arg, Args...> {
  private:
    using select_rec = select<idx - 1, Args...>;

  public:
    typedef typename select_rec::type type;
    static const bool value = select_rec::value;
};

template <int idx, typename... Args>
using select_t = typename select<idx, Args...>::type;

template <int idx, typename... Args>
using select_v = typename select<idx, Args...>::value;

template <int idx, typename Tuple, typename Enable = void> struct tuple_select {
    typedef void type;
    static const bool value = false;
};

template <int idx, typename Tuple>
    struct tuple_select < idx,
    Tuple, std::enable_if_t<idx<std::tuple_size_v<Tuple>>> {
  public:
    typedef std::tuple_element_t<idx, Tuple> type;
    static const bool value = true;
};

} // namespace internal

//////////////////////////////////////////////////////////////////
/* Operations on tuple types
 */
/** concatenation of tuple types */
template <typename... Ts>
using tuple_cat_t = decltype(std::tuple_cat(std::declval<Ts>()...));

/** Check if a type is present in a tuple */
template <typename T, typename Tuple> struct tuple_has_type;

/** Intersection of tuples */
template <typename T, typename... Us>
struct tuple_has_type<T, std::tuple<Us...>>
    : std::disjunction<std::is_same<T, Us>...> {};

template <typename S1, typename S2> struct tuple_intersect {
    template <typename> struct build_intersection;
    template <std::size_t... Indices>
    struct build_intersection<std::index_sequence<Indices...>> {
        using type = tuple_cat_t<std::conditional_t<
            tuple_has_type<std::tuple_element_t<Indices, S1>, S2>::value,
            std::tuple<std::tuple_element_t<Indices, S1>>, std::tuple<>

            >...>;
    };

    using type = typename build_intersection<
        std::make_index_sequence<std::tuple_size<S1>::value>>::type;
};

template <typename S1, typename S2>
using tuple_intersect_t = typename tuple_intersect<S1, S2>::type;

/** empty tuple */
template <typename T> struct is_empty_tuple;

template <typename... Ts>
struct is_empty_tuple<std::tuple<Ts...>> : std::false_type {};

template <> struct is_empty_tuple<std::tuple<>> : std::true_type {};

template <typename T>
inline constexpr bool is_empty_tuple_v = is_empty_tuple<T>::value;

/** check tuple type */
template <typename T> struct is_tuple : std::false_type {};

template <typename... Ts>
struct is_tuple<std::tuple<Ts...>> : std::true_type {};

template <typename T> inline constexpr bool is_tuple_v = is_tuple<T>::value;

/** get tuple elements */
template <typename T> struct tuple_first {
    static constexpr bool value = false;
    using type = void;
};

template <typename T, typename... Ts> struct tuple_first<std::tuple<T, Ts...>> {
    static constexpr bool value = true;
    using type = T;
};

/* also specialize for pair */
template <typename T, typename U> struct tuple_first<std::pair<T, U>> {
    static constexpr bool value = true;
    using type = T;
};

template <typename T> using tuple_first_t = typename tuple_first<T>::type;

template <template <typename...> class T, typename Tuple, typename... Args>
struct tuple_apply_template;

template <template <typename...> class T, typename... TupleTypes,
          typename... Args>
struct tuple_apply_template<T, std::tuple<TupleTypes...>, Args...> {
    using type = T<Args..., TupleTypes...>;
    constexpr static bool value = true;
};

template <template <typename...> class T, typename Tuple, typename... Args>
using tuple_apply_template_t =
    typename tuple_apply_template<T, Tuple, Args...>::type;

//////////////////////////////////////////////////////////////////
/* Static assert which will better print Base and Derived types on error
 */
template <typename Base, typename Derived>
constexpr inline void static_assert_is_base_of() {
    static_assert(std::is_base_of<Base, Derived>::value,
                  "Derived must inherit from Base");
}

template <bool value, typename... Ts>
constexpr inline void static_assert_show_types(const char *) {
    static_assert(value, "Bad types");
}

template <typename T, typename U>
constexpr inline void static_assert_is_same(const char *msg) {
    static_assert_show_types<std::is_same_v<T, U>, T, U>(msg);
}

template <typename BasesTuple, typename Derived>
struct static_assert_is_base_of_tuple;

template <typename Derived>
struct static_assert_is_base_of_tuple<std::tuple<>, Derived> {
    constexpr void operator()() {}
};

template <typename Base, typename... Bases, typename Derived>
struct static_assert_is_base_of_tuple<std::tuple<Base, Bases...>, Derived> {
    constexpr void operator()() {
        static_assert_is_base_of<Base, Derived>();
        static_assert_is_base_of_tuple<std::tuple<Bases...>, Derived>()();
    }
};

template <bool b1, bool b2, typename... Bases> struct static_assert_bool_eq {
    static_assert(b1 == b2);
    typedef std::true_type type;
};

//////////////////////////////////////////////////////////////////
/* Allows one to iterate on classes
 *
 * Bases must be inherited by Self
 */
template <typename Self, typename... Bases> struct enable_visitor_for {
    template <typename Visitor> void for_each_base(Visitor &&visitor) {
        (void(visitor(static_cast<Bases *>(static_cast<Self *>(this)))), ...);
    }

    template <typename Visitor>
    static void static_for_each_base(Visitor &&visitor) {
        (void(visitor(static_cast<Bases *>(nullptr))), ...);
    }

    template <typename Visitor>
    static void for_each_base(Self *self, Visitor &&visitor) {
        (void(visitor(static_cast<Bases *>(self))), ...);
    }

    ~enable_visitor_for() {
        static_assert_is_base_of<enable_visitor_for, Self>();
        static_for_each_base([&](auto *base) {
            using Base = std::decay_t<decltype(*base)>;
            static_assert_is_base_of<Base, Self>();
        });
    }
};

/* Allows one to iterate on classes that we depend on
 */
template <typename... Bases>
struct enable_visitor_for_bases
    : enable_visitor_for<enable_visitor_for_bases<Bases...>, Bases...>,
      Bases... {};

//////////////////////////////////////////////////////////////////
/** @defgroup collect Classes collect
 *
 * Tools to collect as tuples for inheritance
 *  @{
 *
 *
 *  @defgroup collect-core Core collect mechanism
 *
 * Templates used to provide the core functionnality of class collect
 *  @{
 */

namespace collect {

enum class phase {
    init_options,
    init_result,
    explore,
    classify,
    cleanup,
};

template <class... Us> struct as_tuple;

namespace bits {

namespace collect {

struct empty {};

// TODO: rewrite the flags system. It is not designed correctly even
// if 'it works'

template <class T> class has_member_type {
    template <class U> static std::true_type foo(typename U::type *);

    template <class U> static std::false_type foo(...);

  public:
    using type = decltype(foo<T>(nullptr));
    static const bool value = type::value;
};

template <class T> class has_member_options {
    template <class U> static std::true_type foo(typename U::options *);

    template <class U> static std::false_type foo(...);

  public:
    using type = decltype(foo<T>(nullptr));
    static const bool value = type::value;
};

template <class T> class has_member_flags {
    template <class U> static std::true_type foo(typename U::flags *);

    template <class U> static std::false_type foo(...);

  public:
    using type = decltype(foo<T>(nullptr));
    static const bool value = type::value;
};

template <class U, bool Default, class Void = void> struct stop_value {
    static constexpr bool stop = Default;
};

template <class U, bool Default>
struct stop_value<U, Default, std::conditional_t<U::stop, void, void>> {
    static constexpr bool stop = U::stop;
};

template <class U, bool Default, class Void = void> struct collected_value {
    static constexpr bool collected = Default;
};

template <class U, bool Default>
struct collected_value<U, Default,
                       std::conditional_t<U::collected, void, void>> {
    static constexpr bool collected = U::collected;
};

template <bool val = true, class... Inherit> struct StopFlag : Inherit... {
    static constexpr bool stop = val;
};

template <bool val = true, class... Inherit> struct CollectedFlag : Inherit... {
    static constexpr bool collected = val;
};

template <class R, bool val, class Inherit, typename Void = void>
struct StopFlagDefault_ {
    using type = StopFlag<val>;
};

template <class R, bool val, class Inherit>
struct StopFlagDefault_<R, val, Inherit,
                        std::enable_if_t<!std::is_same_v<Inherit, void> &&
                                         has_member_flags<R>::value>> {
    using type = StopFlag<stop_value<typename R::flags, val>::stop,
                          typename Inherit::flags>;
};

template <class R, bool val, class Inherit>
struct StopFlagDefault_<R, val, Inherit,
                        std::enable_if_t<std::is_same_v<Inherit, void> &&
                                         !has_member_flags<R>::value>> {
    using type = StopFlag<val>;
};

template <class R, bool val, class Inherit>
struct StopFlagDefault_<R, val, Inherit,
                        std::enable_if_t<std::is_same_v<Inherit, void> &&
                                         has_member_flags<R>::value>> {
    using type = StopFlag<stop_value<typename R::flags, val>::stop>;
};

template <class R, bool val, class Inherit = void>
using StopFlagDefault = typename StopFlagDefault_<R, val, Inherit>::type;

template <class R, bool val, class Inherit, typename Void = void>
struct CollectedFlagDefault_ {
    using type = CollectedFlag<val>;
};

template <class R, bool val, class Inherit>
struct CollectedFlagDefault_<R, val, Inherit,
                             std::enable_if_t<!std::is_same_v<Inherit, void> &&
                                              has_member_flags<R>::value>> {
    using type =
        CollectedFlag<collected_value<typename R::flags, val>::collected,
                      typename Inherit::flags>;
};

template <class R, bool val, class Inherit>
struct CollectedFlagDefault_<R, val, Inherit,
                             std::enable_if_t<std::is_same_v<Inherit, void> &&
                                              !has_member_flags<R>::value>> {
    using type = CollectedFlag<val>;
};

template <class R, bool val, class Inherit>
struct CollectedFlagDefault_<R, val, Inherit,
                             std::enable_if_t<std::is_same_v<Inherit, void> &&
                                              has_member_flags<R>::value>> {
    using type =
        CollectedFlag<collected_value<typename R::flags, val>::collected>;
};

template <class R, bool val, class Inherit = void>
using CollectedFlagDefault =
    typename CollectedFlagDefault_<R, val, Inherit>::type;

template <class... Flags> struct CollectorFlags : Flags... {};

//////////////////////////////////////////////////////////////
/** Try to apply a collector on a `<phase, type, options>`

   - `type` is used
     - in the `explore` phase for the currently analyzed type.
     - in the `init_result` phase for the initial options.
     - not in the `explore` phase
     - in the `classify` phase with a `std::pair<options, type>`
     - in the `cleanup` phase with the result of the previous phase
   - `options` contains the result::options of the invocation of the
     previous explore. Its real content depends on the phase:
     - in the `init_option` phase, it contains the built options
     - in the `init_result` phase, it contains the built result
     - in the `explore` phase, it contains the current options
     - in the `classify` phase, it contains the built result
     - in the `cleanup` phase, it contains the built result

   If the application fails, `std::tuple<>` is returned in `type`
   and `options` is left unchanged.

   Else, the collector result is given to the `GetResult` template
   that must build the final result with
   - `options`: the (updated) options, that inherits from `Options`
   - `type`:
     - in `config` phase: unused (optional, will be forced to void)
     - in `explore` phase: a tuple with the types to recursively analyze
     - in `collect` phase: a structure with all the required results
   - `flags`: a structure with two booleans:
     - `stop`: a boolean telling weather we must stop the recursion on
       the `Collectors`. It must be used in the `explore` phase to tell
       that the `type` has been handled.
     - `collected`: a boolean telling the handling of the type is fully
       done, the next type can be handled (set by the FinalCollector
       in `classify` phase)

*/
/* default case, `Collector` not applied
 */
template <template <phase p_, class U_, class Options_> class Collector,
          phase p, class U, class Options, template <class R> class GetResult,
          class Void = void>
struct collector_apply {
    static constexpr bool applied = false;
    using type = std::tuple<>;
    using options = Options;
    using flags = CollectorFlags<StopFlag<false>>;
};

/* `Collector` applied case */
template <template <phase p_, class U_, class Options_> class Collector,
          phase p, class U, class Options, template <class R> class GetResult>
struct collector_apply<Collector, p, U, Options, GetResult,
                       std::enable_if_t<has_member_type<
                           GetResult<Collector<p, U, Options>>>::value>> {
  private:
    using result_type = GetResult<Collector<p, U, Options>>;

  public:
    static constexpr bool applied = true;
    using type = typename result_type::type;
    using options = typename result_type::options;
    static_assert(std::is_base_of_v<Options, options>,
                  "Bad collector, invalid management of Options");
    using flags =
        CollectorFlags<StopFlagDefault<result_type, false, result_type>>;
};

//////////////////////////////////////////////////////////////
/** Recursively calls each `Collectors...` with `<phase, type, options>`
 *
 * Returns the result of the invocation of the last `Collector` until
 * `stop` become true.
 *
 * Returns the info (`type`, `options`, and `flags`) from the last
 * applied collector.
 *
 * See also `collector_apply`
 */

template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_onetype_rec;

/* At least one collector => go */
template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase cp, class cU, class cOptions> class Collector,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_onetype_rec<p, U, Options, GetResult, Collector,
                                    Collectors...> {
  private:
    using result_type = collector_apply<Collector, p, U, Options, GetResult>;
    using rec_type = std::conditional_t<
        /* Need to recurse? (default yes) */
        result_type::flags::stop,
        /* No => we stop */
        result_type,
        /* Yes => recursion on Collectors */
        collectors_apply_onetype_rec<p, U, typename result_type::options,
                                     GetResult, Collectors...>>;

  public:
    using type = typename rec_type::type;
    using options = typename rec_type::options;
    using flags = typename rec_type::flags;
};

template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
using collectors_apply_onetype =
    collectors_apply_onetype_rec<p, U, Options, GetResult, Collectors...>;

//////////////////////////////////////////////////////////////
/** Recursively calls `collectors_apply_onetype`
 *
 * Calls `collectors_apply_onetype` recursively on types returned in
 * `type` of the previous invocation, until `flags::collected` become
 * true.
 *
 * It is used in the `explore` phase to handle markers.
 *
 * By default, collectors are stopped as soon as one is applied (but
 * restarted on the returned type(s))
 *
 * Only `type` from the last applied collector is kept. As it should
 * be `FinalCollector`, `type` will contains a tuple of pairs of
 * options and user type.
 */
template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_alltypes;

/* Recursion on U as tuple of type(s)
 *
 * We return final (collected) type here. Only `type` should be here.
 */
template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_alltypes_tuple;

template <phase p, class... Us, class Options,
          template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_alltypes_tuple<p, std::tuple<Us...>, Options, GetResult,
                                       Collectors...> {
    using type = tuple_cat_t<typename collectors_apply_alltypes<
        p, Us, Options, GetResult, Collectors...>::type...>;
};

/* Recursion on U, restarting if required
 *
 * We return final (collected) type here
 */
template <phase p, class U, class Options, template <class R> class GetResult,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collectors_apply_alltypes {
    using result_type =
        collectors_apply_onetype<p, U, Options, GetResult, Collectors...>;

    using rec_type = std::conditional_t<
        /* did we recurse on all the current type? */
        result_type::flags::collected,
        /* yes */
        result_type,
        /* no */
        collectors_apply_alltypes_tuple<p, typename result_type::type,
                                        typename result_type::options,
                                        GetResult, Collectors...>>;

    using type = typename rec_type::type;
};

//////////////////////////////////////////////////////////////
/** Recursively calls `collectors_apply_onetype`
 *
 * Calls `collectors_apply_onetype` iteratively on types in the tuple.
 * The result (in options) is given from one invocation to the next
 * (so that the full result can be built)
 *
 * Only `options` from the last applied collector is kept.
 */
template <phase p, class U, class Result, template <class R> class GetResult,
          template <phase p_, class U_, class Result_> class... Collectors>
struct collectors_apply_iter_tuple {
    // default case for empty tuple

    // checking we really have a tuple
    static_assert(is_tuple_v<U>);
    using options = Result;
};

/* Iteration on the tuple components
 */
template <phase p, class U, class... Us, class Result,
          template <class R> class GetResult,
          template <phase p_, class U_, class Result_> class... Collectors>
struct collectors_apply_iter_tuple<p, std::tuple<U, Us...>, Result, GetResult,
                                   Collectors...> {
  private:
    using result_type =
        collectors_apply_onetype<p, U, Result, GetResult, Collectors...>;

    using iter_type = collectors_apply_iter_tuple<p, std::tuple<Us...>,
                                                  typename result_type::options,
                                                  GetResult, Collectors...>;

  public:
    using options = typename iter_type::options;
};

/*********************
 * Normalizing results
 */

/* a `phase::init_options` collector must return a struct with an
 * `options` type
 *
 * also used for other phases where the result is in `options`
 */
template <class R, class Void = void> struct GetConfigResultHelper {};

template <class R>
struct GetConfigResultHelper<R,
                             std::enable_if_t<has_member_options<R>::value>> {
    using options = typename R::options;
    using type = std::tuple<void>;
    using flags = CollectorFlags<StopFlagDefault<R, false>,
                                 CollectedFlagDefault<R, true>>;
};

template <class R> using GetConfigResult = GetConfigResultHelper<R>;

/* a `phase::explore` collector must return a struct with a `type =
 * std::tuple<...>` type
 *
 * We must ensure that GetExploreResult returns a structure, even if R
 * cannot be instanciate (i.e. declared but not defined)
 */
template <class R, class Void = void> struct GetExploreResultHelper {};

template <class R>
struct GetExploreResultHelper<R, std::enable_if_t<has_member_type<R>::value>> {
    using options = typename R::options;
    using type = typename R::type;
    /* GetResult called => a explore has been applied. By default, stopping */
    using flags = CollectorFlags<StopFlagDefault<R, true>,
                                 CollectedFlagDefault<R, false>>;
};

template <class R> using GetExploreResult = GetExploreResultHelper<R>;

/*********************
 * Specific collectors
 */

/* A collector to handle as_tuple<> in explore phase */
template <phase p, class U, class Options> struct TupleCollector;

template <class Options>
struct TupleCollector<phase::explore, as_tuple<std::tuple<>>, Options> {
    using options = Options;
    using type = std::tuple<>;
};

template <class... Us, class Options>
struct TupleCollector<phase::explore, as_tuple<std::tuple<Us...>>, Options> {
    using options = Options;
    using type = std::tuple<Us...>;
};

/* A collector to stop collectors enumeration, always inserted in last position
 */
template <phase p, class U, class Options> struct FinalCollector {
    using options = Options;
    using type = std::tuple<void>;
    using flags = CollectorFlags<StopFlag<true>>;
};

/* at `explore` phase, we must stop the recursion on types */
template <class U, class Options>
struct FinalCollector<phase::explore, U, Options> {
    using options = Options;
    using type = std::tuple<std::pair<options, U>>;
    using flags = CollectorFlags<CollectedFlag<>>;
    // static constexpr bool collected = true;
};

struct CollectorResult {};

/*********************
 * Main code
 */

template <class U,
          template <phase p_, class U_, class Options_> class... Collectors>
struct collect_classes {
  private:
    using InitOptions =
        typename collectors_apply_onetype<phase::init_options, void, empty,
                                          GetConfigResult, Collectors...,
                                          FinalCollector>::options;
    using InitResult =
        typename collectors_apply_onetype<phase::init_result, InitOptions,
                                          empty, GetConfigResult, Collectors...,
                                          FinalCollector>::options;
    using collected_classes =
        typename collectors_apply_alltypes<phase::explore, U, InitOptions,
                                           GetExploreResult, TupleCollector,
                                           Collectors..., FinalCollector>::type;
    using result = typename collectors_apply_iter_tuple<
        phase::classify, collected_classes, InitResult, GetConfigResult,
        Collectors..., FinalCollector>::options;
    using clean_result = typename collectors_apply_onetype<
        phase::cleanup, result, CollectorResult, GetConfigResult, Collectors...,
        FinalCollector>::options;

  public:
#ifdef MARTO_DEBUG_UTILS
    // only defined in test_utils.cpp in order to test intermediate results
    using initOptions = InitOptions;
    using collected_types = collected_classes;
    using initResult = InitResult;
#endif
    using type = clean_result;
};

template <class InitResult, class U,
          template <phase p_, class U_, class Options_> class... Collectors>
struct append_classes {
  private:
    static_assert(std::is_base_of_v<CollectorResult, InitResult>,
                  "Bad class given as previous results");
    using InitOptions =
        typename collectors_apply_onetype<phase::init_options, void, empty,
                                          GetConfigResult, Collectors...,
                                          FinalCollector>::options;
    using collected_classes =
        typename collectors_apply_alltypes<phase::explore, U, InitOptions,
                                           GetExploreResult, TupleCollector,
                                           Collectors..., FinalCollector>::type;
    using result = typename collectors_apply_iter_tuple<
        phase::classify, collected_classes, InitResult, GetConfigResult,
        Collectors..., FinalCollector>::options;
    using clean_result = typename collectors_apply_onetype<
        phase::cleanup, result, CollectorResult, GetConfigResult, Collectors...,
        FinalCollector>::options;

  public:
    using type = clean_result;
};

} // namespace collect

} // namespace bits

template <class U,
          template <phase p_, class U_, class Options_> class... Collectors>
using collect_classes_t =
    typename bits::collect::collect_classes<U, Collectors...>::type;

template <class Collected, class U,
          template <phase p_, class U_, class Options_> class... Collectors>
using append_classes_t =
    typename bits::collect::append_classes<Collected, U, Collectors...>::type;

/** @}
 *
 *****************************************************************
 * @defgroup collect-visibility
 *
 * define and provide a `visibility` collector that allows one to
 * get private, protected, public, and other classes
 * @{
 */

namespace collector {

template <phase p, class U, class Options> struct visibility;

}

template <class... Us> struct as_public;
template <class... Us> struct as_protected;
template <class... Us> struct as_private;
template <class... Us> struct as_other;
template <class... Us> struct as_virtual;

namespace bits::visibility {

enum flags {
    none = 0,
    collect_public = 1 << 0,
    collect_protected = 1 << 1,
    collect_private = 1 << 2,
    collect_other = 1 << 3,
    collect_mask =
        collect_public | collect_protected | collect_private | collect_other,
    collect_virtual = 1 << 8,
    global_features_mask = (-1 & ~((1 << 16) - 1)),
    collect_unknown_as_public = 1 << 16, /* instead of other by default */
    collect_no_override = 1 << 17,       /* forbid incompatible type, instead of
                                            keeping the more internal */
};

constexpr enum flags operator|(enum flags a, enum flags b) {
    return static_cast<enum flags>(
        static_cast<std::underlying_type_t<enum flags>>(a) |
        static_cast<std::underlying_type_t<enum flags>>(b));
}
constexpr enum flags operator&(enum flags a, enum flags b) {
    return static_cast<enum flags>(
        static_cast<std::underlying_type_t<enum flags>>(a) &
        static_cast<std::underlying_type_t<enum flags>>(b));
}
constexpr enum flags operator~(enum flags a) {
    return static_cast<enum flags>(
        ~static_cast<std::underlying_type_t<enum flags>>(a));
}

constexpr enum flags select_collect_type(enum flags features,
                                         enum flags selected) {
    if (!(features & collect_no_override)) {
        features = features & ~collect_mask;
    }
    return features | selected;
}

template <class Result, class pub = std::tuple<>, class prot = std::tuple<>,
          class priv = std::tuple<>, class vpub = std::tuple<>,
          class vprot = std::tuple<>, class vpriv = std::tuple<>,
          class other = std::tuple<>>
struct result : Result {
    using public_tuple_t = pub;
    using protected_tuple_t = prot;
    using private_tuple_t = priv;
    using vpublic_tuple_t = vpub;
    using vprotected_tuple_t = vprot;
    using vprivate_tuple_t = vpriv;
    using other_tuple_t = other;
};

template <class Options, flags value = visibility::none>
struct options : Options {
    static constexpr flags visibility_flags = value;
};

} // namespace bits::visibility

static constexpr bits::visibility::flags collect_unknown_as_public =
    bits::visibility::collect_unknown_as_public;
template <enum bits::visibility::flags, typename... Bases>
struct visibility_options;

namespace collector {

template <class Options> struct visibility<phase::init_options, void, Options> {
  public:
    using options = bits::visibility::options<Options>;
};

template <class Options, class Result>
struct visibility<phase::init_result, Options, Result> {
  public:
    using options = bits::visibility::result<Result>;
};

template <class... Us, class Options>
struct visibility<phase::explore, as_virtual<Us...>, Options> {
    using options = std::conditional_t<
        // correct visibility set?
        Options::visibility_flags & bits::visibility::collect_virtual,
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<Options,
                                  Options::visibility_flags |
                                      bits::visibility::collect_virtual>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <class... Us, class Options>
struct visibility<phase::explore, as_public<Us...>, Options> {
    using options = std::conditional_t<
        // correct visibility set?
        (Options::visibility_flags &bits::visibility::collect_mask) ==
            select_collect_type(Options::visibility_flags,
                                bits::visibility::collect_public),
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<
            Options, select_collect_type(Options::visibility_flags,
                                         bits::visibility::collect_public)>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <class... Us, class Options>
struct visibility<phase::explore, as_protected<Us...>, Options> {
    using options = std::conditional_t<
        // correct visibility set?
        (Options::visibility_flags &bits::visibility::collect_mask) ==
            bits::visibility::collect_protected,
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<
            Options, select_collect_type(Options::visibility_flags,
                                         bits::visibility::collect_protected)>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <class... Us, class Options>
struct visibility<phase::explore, as_private<Us...>, Options> {
    using options = std::conditional_t<
        // correct visibility set?
        (Options::visibility_flags &bits::visibility::collect_mask) ==
            bits::visibility::collect_private,
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<
            Options, select_collect_type(Options::visibility_flags,
                                         bits::visibility::collect_private)>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <class... Us, class Options>
struct visibility<phase::explore, as_other<Us...>, Options> {
    using options = std::conditional_t<
        // correct visibility set?
        (Options::visibility_flags &bits::visibility::collect_mask) ==
            bits::visibility::collect_other,
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<
            Options, select_collect_type(Options::visibility_flags,
                                         bits::visibility::collect_other)>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <enum bits::visibility::flags opt, class... Us, class Options>
struct visibility<phase::explore, visibility_options<opt, Us...>, Options> {
    static_assert((opt & ~bits::visibility::global_features_mask) == 0,
                  "Invalid visibility option selected");
    using options = std::conditional_t<
        // correct visibility set?
        (Options::visibility_flags | opt) == Options::visibility_flags,
        // yes => do nothing
        Options,
        // no => update visibility
        bits::visibility::options<Options, Options::visibility_flags | opt>>;
    using type = std::tuple<as_tuple<std::tuple<Us...>>>;
};

template <class U, class Options, class Result>
struct visibility<phase::classify, std::pair<Options, U>, Result> {
  private:
    static constexpr bits::visibility::flags features =
        Options::visibility_flags;
    using prev_public =
        std::conditional_t<(features & bits::visibility::collect_virtual) != 0,
                           typename Result::vpublic_tuple_t,
                           typename Result::public_tuple_t>;
    using prev_protected =
        std::conditional_t<(features & bits::visibility::collect_virtual) != 0,
                           typename Result::vprotected_tuple_t,
                           typename Result::protected_tuple_t>;
    using prev_private =
        std::conditional_t<(features & bits::visibility::collect_virtual) != 0,
                           typename Result::vprivate_tuple_t,
                           typename Result::private_tuple_t>;
    using public_tuple_t = std::conditional_t<
        (features & bits::visibility::collect_public) != 0 ||
            ((features & bits::visibility::collect_mask) == 0 &&
             (features & bits::visibility::collect_unknown_as_public) != 0),
        tuple_cat_t<prev_public, std::tuple<U>>, prev_public>;
    using protected_tuple_t = std::conditional_t<
        (features & bits::visibility::collect_protected) != 0,
        tuple_cat_t<prev_protected, std::tuple<U>>, prev_protected>;
    using private_tuple_t =
        std::conditional_t<(features & bits::visibility::collect_private) != 0,
                           tuple_cat_t<prev_private, std::tuple<U>>,
                           prev_private>;
    using other_tuple_t = std::conditional_t<
        (features & bits::visibility::collect_other) != 0 ||
            (((features & bits::visibility::collect_mask) == 0) &&
             (features & bits::visibility::collect_unknown_as_public) == 0),
        tuple_cat_t<typename Result::other_tuple_t, std::tuple<U>>,
        typename Result::other_tuple_t>;

  public:
    using options = std::conditional_t<
        (features & bits::visibility::collect_virtual) != 0,
        bits::visibility::result<Result, typename Result::public_tuple_t,
                                 typename Result::protected_tuple_t,
                                 typename Result::private_tuple_t,
                                 public_tuple_t, protected_tuple_t,
                                 private_tuple_t, other_tuple_t>,
        bits::visibility::result<
            Result, public_tuple_t, protected_tuple_t, private_tuple_t,
            typename Result::vpublic_tuple_t,
            typename Result::vprotected_tuple_t,
            typename Result::vprivate_tuple_t, other_tuple_t>>;
};

template <class Result, class CleanResult>
struct visibility<phase::cleanup, Result, CleanResult> {
    using options = bits::visibility::result<
        CleanResult, typename Result::public_tuple_t,
        typename Result::protected_tuple_t, typename Result::private_tuple_t,
        typename Result::vpublic_tuple_t, typename Result::vprotected_tuple_t,
        typename Result::vprivate_tuple_t, typename Result::other_tuple_t>;
};

} // namespace collector

/** @}
 *
 *****************************************************************
 * @defgroup collect-all_classes
 *
 * define and provide a `all_classes` collector that allows one to
 * get all collected classes
 * @{
 */

namespace collector {

template <phase p, class U, class Options> struct all_classes;

}

namespace bits::all_classes {

template <class Result, class all = std::tuple<>> struct result : Result {
    using all_tuple_t = all;
};

} // namespace bits::all_classes

namespace collector {

template <class Options, class Result>
struct all_classes<phase::init_result, Options, Result> {
  public:
    using options = bits::all_classes::result<Result>;
};

template <class U, class Options, class Result>
struct all_classes<phase::classify, std::pair<Options, U>, Result> {
  public:
    using options = bits::all_classes::result<
        Result, tuple_cat_t<typename Result::all_tuple_t, std::tuple<U>>>;
};

template <class Result, class CleanResult>
struct all_classes<phase::cleanup, Result, CleanResult> {
    using options =
        bits::all_classes::result<CleanResult, typename Result::all_tuple_t>;
};

} // namespace collector

/** @}
 *
 *****************************************************************
 * @defgroup collect-ctor
 *
 * define and provide a `ctor` collector that allows one to
 * get a class whose constructors should be inherited.
 *
 * It also provides a way to disable the default copy constructor in
 * the inherited class (can be useful when virtual inheritance occurs)
 *
 * @{
 */

namespace collector {

template <phase p, class U, class Options> struct inherit_ctor;

}

template <class U> struct inherit_ctor;
struct disable_default_copy_ctor;

namespace bits::ctor {

template <class Options, bool value = false> struct options : Options {
    static constexpr bool is_ctor = value;
};

template <class Result, class ctor = std::tuple<>,
          bool default_copy_ctor = true>
struct result : Result {
    using inherit_ctor = ctor;
    static constexpr bool keep_default_copy_ctor = default_copy_ctor;
};

} // namespace bits::ctor

namespace collector {

template <class Options>
struct inherit_ctor<phase::init_options, void, Options> {
  public:
    using options = bits::ctor::options<Options>;
};

template <class Options, class Result>
struct inherit_ctor<phase::init_result, Options, Result> {
  public:
    using options = bits::ctor::result<Result>;
};

template <class U, class Options>
struct inherit_ctor<phase::explore, collect::inherit_ctor<U>, Options> {
    using options = std::conditional_t<
        // already set?
        Options::is_ctor,
        // yes => do nothing
        Options,
        // no => update is_ctor
        bits::ctor::options<Options, true>>;
    using type = std::tuple<U>;
};

template <class Options>
struct inherit_ctor<phase::explore, collect::disable_default_copy_ctor,
                    Options> {
    using options = Options;
    using type = std::tuple<std::tuple<disable_default_copy_ctor>>;
    using flags = bits::collect::CollectorFlags<bits::collect::CollectedFlag<>>;
};

template <class U, class Options, class Result>
struct inherit_ctor<phase::classify, std::pair<Options, U>, Result> {
    static_assert(is_empty_tuple_v<typename Result::inherit_ctor> ||
                      !Options::is_ctor,
                  "Several classes declared with inherit_ctor<...>!");
    static_assert(!Options::is_ctor || std::is_class_v<U>,
                  "inherit_ctor<...> must be used with a class type");
    using options =
        std::conditional_t<Options::is_ctor,
                           bits::ctor::result<Result, std::tuple<U>,
                                              Result::keep_default_copy_ctor>,
                           Result>;
};

template <class Result>
struct inherit_ctor<phase::classify, std::tuple<disable_default_copy_ctor>,
                    Result> {
    using options =
        bits::ctor::result<Result, typename Result::inherit_ctor, false>;
};

template <class Result, class CleanResult>
struct inherit_ctor<phase::cleanup, Result, CleanResult> {
    using options =
        bits::ctor::result<CleanResult, typename Result::inherit_ctor,
                           Result::keep_default_copy_ctor>;
};

} // namespace collector

/** @}
 *
 *****************************************************************
 * @defgroup collect-self
 *
 * Allows one to declare a `self` classe, i.e. the class that
 * will inherit the current one (CRTP way)
 * @{
 */

namespace collector {

template <phase p, class U, class Options> struct self;

}

template <class T> struct self;
template <class T, class... Ts> struct with_self;

namespace bits::self {

template <class Options, class Self = std::tuple<>> struct options : Options {
    using self_tuple_t = Self;
};

template <class Result, class Self = std::tuple<>> struct result : Result {
    using self_tuple_t = Self;
};

} // namespace bits::self

namespace collector {

template <class Options> struct self<phase::init_options, void, Options> {
  public:
    using options = bits::self::options<Options>;
};

template <class Options, class Result>
struct self<phase::init_result, Options, Result> {
  public:
    using options = bits::self::result<Result>;
};

template <class T, class Options>
struct self<phase::explore, collect::self<T>, Options> {
    using options = Options;
    using type = std::tuple<collect::self<T>>;
    using flags = bits::collect::CollectorFlags<bits::collect::CollectedFlag<>>;
};

template <class T, class... Ts, class Options>
struct self<phase::explore, with_self<T, Ts...>, Options> {
    static_assert(
        is_empty_tuple_v<typename Options::self_tuple_t> ||
            std::is_same_v<typename Options::self_tuple_t, std::tuple<T>>,
        "Several different self<...> defined!");
    using options = bits::self::result<Options, std::tuple<T>>;
    using type = std::tuple<collect::self<T>, as_tuple<std::tuple<Ts...>>>;
};

template <class T, class Result>
struct self<phase::classify, collect::self<T>, Result> {
    static_assert(
        is_empty_tuple_v<typename Result::self_tuple_t> ||
            std::is_same_v<typename Result::self_tuple_t, std::tuple<T>>,
        "Several different self<...> defined!");
    using options = bits::self::result<Result, std::tuple<T>>;
};

template <class Result, class CleanResult>
struct self<phase::cleanup, Result, CleanResult> {
    using options =
        bits::self::result<CleanResult, typename Result::self_tuple_t>;
};

} // namespace collector

/** @}
 *****************************************************************
 */

template <template <collect::phase p_, class U_, class Options_>
          class... Collectors>
struct collectors_list;

template <class...> struct collectors_list_merge;

template <class... List>
using collectors_list_merge_t = typename collectors_list_merge<List...>::type;

template <> struct collectors_list_merge<> { using type = collectors_list<>; };

template <template <collect::phase p_, class U_, class Options_>
          class... Collectors>
struct collectors_list_merge<collectors_list<Collectors...>> {
    using type = collectors_list<Collectors...>;
};

template <
    template <collect::phase p_, class U_, class Options_> class... Collectors1,
    template <collect::phase p_, class U_, class Options_> class... Collectors2,
    class... CollectorsLists>
struct collectors_list_merge<collectors_list<Collectors1...>,
                             collectors_list<Collectors2...>,
                             CollectorsLists...> {
    using type =
        collectors_list_merge_t<collectors_list<Collectors1..., Collectors2...>,
                                CollectorsLists...>;
};

namespace bits {

template <class CollectorsList, typename... Bases>
struct collect_from_collector_list;

template <template <phase p_, class U_, class Options_> class... Collectors,
          typename... Bases>
struct collect_from_collector_list<collectors_list<Collectors...>, Bases...> {
    using type =
        collect_classes_t<as_tuple<std::tuple<Bases...>>, Collectors...>;
};

template <class CollectorsList, class PreviousCollect, typename... Bases>
struct append_from_collector_list;

template <template <phase p_, class U_, class Options_> class... Collectors,
          class PreviousCollect, typename... Bases>
struct append_from_collector_list<collectors_list<Collectors...>,
                                  PreviousCollect, Bases...> {
    using type =
        append_classes_t<PreviousCollect, as_tuple<std::tuple<Bases...>>,
                         Collectors...>;
};

} // namespace bits

template <class CollectorsList, typename... Bases>
using do_collect_classes =
    typename bits::collect_from_collector_list<CollectorsList, Bases...>::type;

template <class CollectorsList, class PreviousCollect, typename... Bases>
using do_append_classes =
    typename bits::append_from_collector_list<CollectorsList, PreviousCollect,
                                              Bases...>::type;

using raw_collectors = collectors_list<>;

using basic_collectors = typename collectors_list_merge<
    collectors_list<collector::self, collector::visibility,
                    collector::inherit_ctor, collector::all_classes>,
    raw_collectors>::type;

template <typename... Bases>
using basic = do_collect_classes<basic_collectors, Bases...>;

} // namespace collect

using collect::as_other;
using collect::as_private;
using collect::as_protected;
using collect::as_public;
using collect::collect_classes_t;
using collect::inherit_ctor;
using collect::self;
using collect::with_self;

namespace bits {

template <typename...> struct empty_class {};

} // namespace bits

template <bool keep_default_copy_ctor, typename CtorTuple, typename SelfTuple,
          typename PrivateTuple, typename ProtectedTuple, typename PublicTuple,
          typename vPrivateTuple, typename vProtectedTuple,
          typename vPublicTuple>
struct inherit_;

/* Add an empty ctor class if none */
template <bool keep_default_copy_ctor, typename SelfTuple,
          typename PrivateTuple, typename ProtectedTuple, typename PublicTuple,
          typename vPrivateTuple, typename vProtectedTuple,
          typename vPublicTuple>
struct inherit_<keep_default_copy_ctor, std::tuple<>, SelfTuple, PrivateTuple,
                ProtectedTuple, PublicTuple, vPrivateTuple, vProtectedTuple,
                vPublicTuple>
    : inherit_<keep_default_copy_ctor,
               std::tuple<bits::empty_class<
                   inherit_<keep_default_copy_ctor, std::tuple<>, SelfTuple,
                            PrivateTuple, ProtectedTuple, PublicTuple,
                            vPrivateTuple, vProtectedTuple, vPublicTuple>>>,
               SelfTuple,
               tuple_cat_t<std::tuple<bits::empty_class<inherit_<
                               keep_default_copy_ctor, std::tuple<>, SelfTuple,
                               PrivateTuple, ProtectedTuple, PublicTuple,
                               vPrivateTuple, vProtectedTuple, vPublicTuple>>>,
                           PrivateTuple>,
               ProtectedTuple, PublicTuple, vPrivateTuple, vProtectedTuple,
               vPublicTuple> {
  private:
    using prev_inherit = typename inherit_<
        keep_default_copy_ctor, std::tuple<bits::empty_class<inherit_>>,
        SelfTuple,
        tuple_cat_t<std::tuple<bits::empty_class<inherit_>>, PrivateTuple>,
        ProtectedTuple, PublicTuple, vPrivateTuple, vProtectedTuple,
        vPublicTuple>::inherit_class;

  public:
    using prev_inherit::prev_inherit;
    using inherit_class = inherit_;
};

/* keeping default move ctor */
template <typename ctor_class, typename SelfTuple, typename... PrivateBases,
          typename... ProtectedBases, typename... PublicBases,
          typename... vPrivateBases, typename... vProtectedBases,
          typename... vPublicBases>
struct inherit_<true, std::tuple<ctor_class>, SelfTuple,
                std::tuple<PrivateBases...>, std::tuple<ProtectedBases...>,
                std::tuple<PublicBases...>, std::tuple<vPrivateBases...>,
                std::tuple<vProtectedBases...>, std::tuple<vPublicBases...>>
    : virtual private vPrivateBases...,
      virtual protected vProtectedBases...,
      virtual public vPublicBases...,
      private PrivateBases...,
      protected ProtectedBases...,
      public PublicBases... {
    friend std::conditional_t<is_empty_tuple_v<SelfTuple>,
                              bits::empty_class<void>,
                              tuple_first_t<SelfTuple>>;

  public:
    using ctor_class::ctor_class;
    using inherit_class = inherit_;
};

/* removing default move ctor */
template <typename ctor_class, typename SelfTuple, typename... PrivateBases,
          typename... ProtectedBases, typename... PublicBases,
          typename... vPrivateBases, typename... vProtectedBases,
          typename... vPublicBases>
struct inherit_<false, std::tuple<ctor_class>, SelfTuple,
                std::tuple<PrivateBases...>, std::tuple<ProtectedBases...>,
                std::tuple<PublicBases...>, std::tuple<vPrivateBases...>,
                std::tuple<vProtectedBases...>, std::tuple<vPublicBases...>>
    : virtual private vPrivateBases...,
      virtual protected vProtectedBases...,
      virtual public vPublicBases...,
      private PrivateBases...,
      protected ProtectedBases...,
      public PublicBases... {
    friend std::conditional_t<is_empty_tuple_v<SelfTuple>,
                              bits::empty_class<void>,
                              tuple_first_t<SelfTuple>>;

    inherit_(inherit_ &&) = delete;

  public:
    using ctor_class::ctor_class;
    using inherit_class = inherit_;
};

template <typename Collected,
          template <bool, typename, typename, typename, typename, typename,
                    typename, typename, typename>
          typename Inherit = inherit_>
struct inherit_collected {
    using type = Inherit<
        Collected::keep_default_copy_ctor, typename Collected::inherit_ctor,
        typename Collected::self_tuple_t, typename Collected::private_tuple_t,
        typename Collected::protected_tuple_t,
        typename Collected::public_tuple_t,
        typename Collected::vprivate_tuple_t,
        typename Collected::vprotected_tuple_t,
        typename Collected::vpublic_tuple_t>;
};

template <typename... Bases>
using inherit = typename inherit_collected<collect::basic<Bases...>>::type;

/** @}
 *****************************************************************
 */
} // namespace marto

#endif // __cplusplus
#endif
