/* -*-c++-*- C++ mode for emacs */
/* Code that can be called with marto parameters */
#ifndef MARTO_CALLABLE_FUNCTION_H
#define MARTO_CALLABLE_FUNCTION_H

#define MARTO_HEADER_NEED_CALLABLE_FUNCTION_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_CALLABLE_FUNCTION
#endif

#ifdef __cplusplus

#include <marto/collections.h>
#include <marto/debug.h>
#include <marto/forward-decl.h>
#include <marto/parameters.h>

namespace marto {

#ifndef SWIG // do-forward-decl

class CallableFunctionBase
    : public is_collection<CallableFunctionBase,
                           collect::add_virtual_shared_from_this,
                           has_map_of<RequiredUntypedParameter>> {
  public:
    CallableFunctionBase() {}
};

template <class Self> class CallableFunction : public CallableFunctionBase {};

#endif // SWIG // do-forward-decl

} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_CALLABLE_FUNCTION
#undef MARTO_HEADER_NEED_IMPL_FROM_CALLABLE_FUNCTION
#include <marto/headers-impl.h>
#endif

#endif
