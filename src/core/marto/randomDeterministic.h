/* -*-c++-*- C++ mode for emacs */
/***********************************************************************\
 *
 * File:           RandomDeterministic.h
 *
 * Fake random number generation library for MarTo
 *
\***********************************************************************/
#ifndef MARTO_RANDOM_DETERMINISTIC_H
#define MARTO_RANDOM_DETERMINISTIC_H

#ifdef __cplusplus

#include <marto/random.h>
#include <vector>

namespace marto {

/* forward declaration for friend */
class RandomDeterministicStreamGenerator;
/** \brief test class that whose streams will return sequences provided at
 * initialisation time
 */
class RandomDeterministic : public RandomFabric {
    friend RandomDeterministicStreamGenerator;

  private:
    const std::vector<std::vector<double>> streams;

    RandomDeterministic(std::vector<std::vector<double>> s)
        : streams(std::move(s)){};

  public:
    static RandomFabric::pointer
    makeFabric(std::vector<std::vector<double>> s) {
        return RandomFabric::pointer(new RandomDeterministic(std::move(s)));
    }
    ~RandomDeterministic(){};
    virtual RandomHistoryStreamGenerator::pointer
    newRandomStreamGenerator() override;
    virtual RandomHistoryStreamGenerator::pointer
    newRandomStreamGenerator(HistoryIStream &istream) override;
};

} // namespace marto

#endif // __cplusplus
#endif
