/* -*-c++-*- C++ mode for emacs */
/* Parameters management */
#ifndef MARTO_SYMBOLIC_PARAMETERS_H
#define MARTO_SYMBOLIC_PARAMETERS_H

#define MARTO_HEADER_NEED_SYMBOLIC_PARAMETERS_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS
#endif

#ifdef __cplusplus

#include <marto/callable-function.h>
#include <marto/collections.h>
#include <marto/except.h>
#include <marto/forward-decl.h>
#include <marto/macros.h>
#include <marto/parameters-timing.h>

//#define MARTO_DEBUG

#ifdef MARTO_DEBUG
#include <marto/debug.h>
#define LOGINIT(x) loginit(x)
#else
#define LOGINIT(x) (x)
#endif

namespace marto {

#ifndef SWIG
class FunctionRegistrationInfo {
  public:
    typedef void register_function(std::string);
    typedef void apply_function(Simulation *sim);
    template <class TRegistered> struct Register {
        void operator()(std::string name, Simulation *sim);
    };
};

class HasEvaluationTiming {
  public:
    virtual explicit operator EvaluationTiming() const = 0;
    virtual void update_evaluation_timing(EvaluationTiming t) = 0;
    void update_evaluation_timing_from_parameter(const HasEvaluationTiming &e) {
        update_evaluation_timing((EvaluationTiming)e);
    }
    void
    update_evaluation_timing_from_parameter(const HasEvaluationTiming *pe) {
        update_evaluation_timing((EvaluationTiming)(*pe));
    }
    EvaluationTiming timing() { return (EvaluationTiming)(*this); }
    virtual ~HasEvaluationTiming() {}
};
#endif

/** collection of SymbolicNamedParameter */
class SymbolicNamedParameters
#ifndef SWIG
    : public is_collection<SymbolicNamedParameters,
                           has_map_of<SymbolicNamedParameter>,
                           enable_shared_from_this<SymbolicNamedParameters>>
#endif
{
  private:
    friend class SymbolicNamedParameter;

  public:
#ifdef SWIG
    typedef shared_ptr<SymbolicNamedParameters> pointer;
#endif
    typedef shared_ptr<SymbolicNamedParameter> element_pointer;
    typedef shared_ptr<const SymbolicNamedParameter> const_element_pointer;
    SymbolicNamedParameters(pointer frame_up = nullptr)
        : nb_slots_(), frame_up_(std::move(frame_up)) {
        if (!frame_up_) {
            nb_slots_.resize(EvaluationTiming::max + 1);
        }
    };
    virtual ~SymbolicNamedParameters() {}
    inline void
    add_symbolic_parameter(std::string name,
                           shared_ptr<SymbolicParameter> symbolic_param_value);

    inline element_pointer lookup(const std::string &name,
                                  bool recursive = true) const;
    inline shared_ptr<SymbolicNamedParameter> lookup(size_t index) const;

#ifndef SWIG
    inline shared_ptr<SymbolicParameter>
    operator[](const std::string &name) const;
    inline shared_ptr<const SymbolicParameter>
    get(const std::string &name) const {
        return std::const_pointer_cast<const SymbolicParameter>((*this)[name]);
    }

    // auto names() const { return map.names(); }

    virtual void compile(pointer frame = nullptr);
#endif

    typedef vector_type::const_iterator const_iterator;
    const_iterator begin() const noexcept { return as_vector().begin(); }
    const_iterator end() const noexcept { return as_vector().end(); }

    size_t nb_slots(EvaluationTiming t) const noexcept {
        return frame_up_ ? frame_up_->nb_slots(t) : nb_slots_[t];
    }

    size_t allocate_slot(EvaluationTiming t) {
        switch (t) {
        case EvaluationTiming::undefined:
            throw std::domain_error(
                "Internal error: cannot allocate undefined slot");
        case EvaluationTiming::none:
            throw std::domain_error(
                "Internal error: invalid timing slot 'none'");
        case EvaluationTiming::constant:
        case EvaluationTiming::event:
            if (frame_up_) {
                return frame_up_->allocate_slot(t);
            }
            return nb_slots_[t]++;
        case EvaluationTiming::state:
            throw std::domain_error(
                "Internal error: cannot allocate state slot");
        }
        throw std::domain_error("Internal error: unknown timing slot");
    }

  private:
    std::vector<size_t> nb_slots_;
    pointer frame_up_;

  public:
    // void pre_allocate(Evaluation::Timing t);
  private:
    // marto::shared_ptr<ParameterValuesContainer> slots_;
};

/** Base virtual class to manage parameters
 *
 * A SymbolicParameter comes from the configuration. It is attached to
 * static objects (Simulation, EventType, Queue, etc.) It will latter
 * be evaluated/computed, perhaps differently depending on the history
 * point (i.e. the real Event) or even on the state (index functions
 * using queue state values), requiring different
 * EffectiveParameterBlocks.
 */
class SymbolicParameter
#ifndef SWIG
    : public virtual HasEvaluationTiming,
#ifdef MARTO_DEBUG
      public DN,
#endif
      public virtual_enable_shared_from_this<SymbolicParameter>
#endif // SWIG
{
#ifndef SWIG
  public:
    /** parameter that must be evaluated at a time depending on its data */
    class WithData : public virtual HasEvaluationTiming {
      private:
        EvaluationTiming evaluation_timing_;

      public:
        WithData() : evaluation_timing_(EvaluationTiming::undefined) {}
        virtual operator EvaluationTiming() const final override {
            return evaluation_timing_;
        }
        virtual void
        update_evaluation_timing(EvaluationTiming t) final override {
            if (t > evaluation_timing_) {
                evaluation_timing_ = t;
            }
        }
    };

    /** parameter that must never be evaluated
     *
     * Such a parameter should compile into another parameter
     */
    class WithoutData : public virtual HasEvaluationTiming {
      public:
        virtual operator EvaluationTiming() const final override {
            return EvaluationTiming::undefined;
        }
        virtual void update_evaluation_timing(EvaluationTiming) final override {
            throw CompilationException(
                "Cannot set compile time on this parameter");
        }
    };

    /** parameter that must not be evaluated at runtime
     *
     * Contrary to `WithoutData`, such a parameter can be compiled
     * into itself. However, it won't be evaluated. It is used for
     * `SymbolicTransition` that is evaluated differently.
     */
    class WithNoData : public virtual HasEvaluationTiming {
      public:
        virtual operator EvaluationTiming() const final override {
            return EvaluationTiming::none;
        }
        virtual void update_evaluation_timing(EvaluationTiming) final override {
            throw CompilationException(
                "Cannot set compile time on this parameter");
        }
    };

    /** parameter that can always be evaluated at runtime
     *
     * Such a parameter can be computed only once, it is a constant
     */
    class WithConstantData : public virtual HasEvaluationTiming {
      public:
        virtual operator EvaluationTiming() const final override {
            return EvaluationTiming::constant;
        }
        virtual void update_evaluation_timing(EvaluationTiming) final override {
            throw CompilationException(
                "Cannot set compile time on this parameter");
        }
    };
#endif
  protected:
    // No direct instanciation. Only derived classes.
    SymbolicParameter(){};

  public:
    /* virtual, so that if the object is destroyed due to RefCounted,
     * the correct destructor will be called */
    virtual ~SymbolicParameter(){};

#ifndef SWIG
    virtual SymbolicParameter *compiled() const = 0;

    /** Compilation of a SymbolicParameter
     *
     * - the compilation occurs in the context of a SymbolicNamedParameters
     * - recursive compilation is forbidden
     * - errors must detected as well as possible (invalid parameters/names,
     * etc)
     * - SymbolicParameter what will be kept (i.e. returned by 'compiled()'
     * method) must register itself to SymbolicNamedParameters in order to get a
     * slot for effective parameters.
     *
     * Most of this will be done in the 'do_compile' method. This method
     * mostly handle recursive compilation.
     */
    void compile(const SymbolicNamedParameters::pointer &parameters) {
        switch (compilation_state) {
        case NOT_COMPILED:
            break;
        case COMPILING:
            throw CompilationException(
                std::string("Recursive compilation of ") +
                (std::string) * this);
        case COMPILED:
            return;
        case COMPILATION_FAILED:
            throw CompilationException(
                std::string(
                    "Don't recompiling an already failed compilation of ") +
                (std::string) * this);
        }
        compilation_state = COMPILING;
        try {
            do_compile(parameters);
        } catch (...) {
            compilation_state = COMPILATION_FAILED;
            auto infos = compile_info(parameters);
            if (infos == "") {
                throw;
            }
            std::throw_with_nested(CompilationException(infos));
        }
        compilation_state = COMPILED;
        if (compiled() == this) {
            auto timing = (EvaluationTiming)(*this);
            if (timing != EvaluationTiming::none) {
                slot_ = parameters->allocate_slot(timing);
            }
        }
    };

    /** Virtual method to realy do the compilation */
    virtual void
    do_compile(const SymbolicNamedParameters::pointer &parameters) = 0;

    /** Information for error messages
     *
     * When the compilation fails (for this SymbolicParameter or a
     * dependent one), if this method returns a non empty string,
     * the error message will be augmented with this string.
     */
    virtual std::string compile_info(const SymbolicNamedParameters::pointer
                                         &__marto_unused(parameters)) const {
        return std::string();
    }

    virtual marto::shared_ptr<ParameterValues> provide_parameter_values(
        const ParametersTypeInfo &__marto_unused(marto_type)) const {
        return nullptr;
    }

    virtual operator std::string() const = 0;

    size_t slot() const { return slot_; }

  private:
    typedef enum {
        NOT_COMPILED = 0,
        COMPILING = 1,
        COMPILED = 2,
        COMPILATION_FAILED = 3,
    } compile_t;
    compile_t compilation_state{NOT_COMPILED};
    size_t slot_{(size_t)-1};
#endif
};

/** Base class for all parameters that can be managed by the user/the
 * config files.
 *
 * Special parameters (such as function implementation) does not derive
 * from this class, but all others do.
 */
class SymbolicRealParameter : public SymbolicParameter {
  protected:
    // No direct instanciation. Only derived classes.
    SymbolicRealParameter() : real_(nullptr){};

  public:
    virtual SymbolicParameter *compiled() const override {
        if (!real_) {
            throw CompilationException(std::string("Compilation not run on ") +
                                       (std::string)(*this));
        }
        return real_;
    }
    virtual void
    do_compile(const SymbolicNamedParameters::pointer &parameters) override {
        if (!real_) {
            real_ = compute_real(parameters);
        }
    }

  protected:
    virtual SymbolicParameter *compute_real(
        const SymbolicNamedParameters::pointer &__marto_unused(parameters)) {
        return this;
    }

  private:
    SymbolicParameter *real_;
};

#ifndef SWIG
/** Class to manage an association between a name and a SymbolicParameter
 *
 * These object are stored into a SymbolicNamedParameters collection.
 * They are used:
 * - to store config and system parameters (in Simulation, EventType, etc.)
 * - to store functions arguments (in SymbolicFunctionCall)
 */
class SymbolicNamedParameter
    : public is_element<SymbolicNamedParameter, in_map<SymbolicNamedParameters>,
                        SymbolicRealParameter>,
      public SymbolicParameter::WithoutData {

  protected:
    // TODO : find the correct class to move this as private
    //  friend class SymbolicNamedParameters;
    SymbolicNamedParameter(shared_ptr<SymbolicParameter> symbolic_param_value)
        : symbolic_param_value_(std::move(symbolic_param_value)){};

  public:
    shared_ptr<SymbolicParameter> value() { return symbolic_param_value_; }
    SymbolicNamedParameters::pointer getContext() {
        return this->collection().shared_from_this();
    }

    virtual void
    do_compile(const SymbolicNamedParameters::pointer &parameters) override {
        symbolic_param_value_.get()->compile(parameters);
        SymbolicRealParameter::do_compile(parameters);
    }
    // inherited:
    // const std::string &name() const { return MapElement::name(); }

    virtual std::string
    compile_info(const SymbolicNamedParameters::pointer &__marto_unused(
        parameters)) const override {
        return std::string("while compiling '") + name() + "' parameter";
    }
    virtual operator std::string() const override {
        return name() + " -> " + symbolic_param_value_->operator std::string();
    }

  protected:
    virtual SymbolicParameter *
    compute_real(const SymbolicNamedParameters::pointer &__marto_unused(
        parameters)) override {
        return symbolic_param_value_->compiled();
    }

  protected:
    shared_ptr<SymbolicParameter> symbolic_param_value_;

#ifdef MARTO_DEBUG
    virtual void more_info(bool start) {
        SymbolicRealParameter::more_info(start);
        std::cerr << ": " << map_element_of<SymbolicNamedParameters>::name()
                  << " -> " << symbolic_param_value_.get();
    }
#endif
};

/** SymbolicParameter containing a name that must be resolved
 *
 * This will be used for variables (i.e. other parameter used by name)
 * and for functions call (functions name)
 */
class SymbolicNameLookup
    : public enable_make_shared<SymbolicNameLookup, SymbolicRealParameter> {
  public:
    typedef enum {
        Current,
        EventType,
        Queue,
        Simulation,
    } Context;

    virtual operator std::string() const override { return name; }

  protected:
    /* must be allocated through `new_shared<*>(...)` */
    SymbolicNameLookup(std::string n, Context c = Current, std::string cn = "")
        : name(std::move(n)), context(std::move(c)),
          context_name(std::move(cn)) {}

    const std::string name;
    const Context context;
    const std::string context_name;

  protected:
    virtual SymbolicParameter *
    compute_real(const SymbolicNamedParameters::pointer &parameters) override {

        if (context != Current) {
            throw NotImplementedException();
        }
        try {
            auto snp = parameters->lookup(name);
            snp->compile(snp->getContext());
            return snp->compiled();
        } catch (UnknownName &e) {
            throw;
        }
    }

#ifdef MARTO_DEBUG
    virtual void more_info(bool __marto_unused(start)) {
        std::cerr << ": " << name << "[" << context << "]";
    }
#endif
};

/** Class used for each function call or transition
 *
 * It allows one to know which symbolic parameter must be used in
 * order to get the values of the function's or transition's
 * parameters
 *
 * These objects are created at compile-time and associated to each
 * function call (to the caller) and to each transition (to the
 * transition itself)
 */
class SymbolicBindingsInfo {
  protected:
    std::vector<shared_ptr<SymbolicNamedParameter>> reverse_args;
    std::vector<shared_ptr<const SymbolicParameter>> compiled_reverse_args;

  public:
    void arguments_number(size_t nb) {
        assert(reverse_args.size() == 0);
        reverse_args.resize(nb);
        compiled_reverse_args.resize(nb);
    }
    size_t arguments_number() const { return reverse_args.size(); }
    void add(size_t idx, shared_ptr<SymbolicNamedParameter> p) {
        assert(idx < arguments_number());
        reverse_args[idx] = p;
        auto comp = p->compiled();
        assert(comp);
        compiled_reverse_args[idx] = comp->shared_from_this();
    }
    shared_ptr<const SymbolicParameter> operator[](size_t idx) const {
        assert(idx < arguments_number());
        return compiled_reverse_args[idx];
    }
};

class SymbolicBindings
    : public virtual_enable_shared_from_this<SymbolicBindings> {
  protected:
    SymbolicBindingsInfo bindings_info_;

  public:
    const SymbolicBindingsInfo &bindings_info() const { return bindings_info_; }
};

/** Class used to handle a function call
 */
class SymbolicFunctionCall
    : public enable_make_shared<SymbolicFunctionCall,
                                collect::inherit_ctor<SymbolicNameLookup>,
                                SymbolicParameter::WithData, SymbolicBindings> {
  private:
    typedef SymbolicNameLookup super;
    int next_numbered_registered_parameters;
    std::vector<std::pair<std::string, bool>> caller_param_names;

    SymbolicNamedParameters::pointer caller_params;

    void forbreak() {
#ifdef MARTO_DEBUG
        std::cerr << "break" << std::endl;
#endif
    }

    template <typename... Types>
    void add_arguments_(bool named, std::string arg_name,
                        shared_ptr<SymbolicParameter> symbolic_param_value,
                        Types... args) {
        forbreak();
#ifdef MARTO_DEBUG
        std::cerr << "* add_arguments in " << type_name(this) << std::endl;
#endif
        if (named) {
            // TODO: check name validity
#ifdef MARTO_DEBUG
            std::cerr << "Registering named argument " << arg_name << " @"
                      << symbolic_param_value.get() << std::endl;
#endif
        }
        try {
            caller_params->add_symbolic_parameter(
                arg_name, std::move(symbolic_param_value));
            //                                                     registered_parameters.try_emplace(
            //          arg_name, std::move(symbolic_param_value));
        } catch (ExistingName &e) {
#ifdef MARTO_DEBUG
            std::cerr << "Fail to register, throwing for " << arg_name
                      << std::endl;
#endif
            throw ExistingName(
                arg_name, std::string("duplicate parameter name \"") +
                              arg_name + "\" in function call \"" + name + '"');
        }
        // caller_param_names.push_back(std::make_pair(std::move(arg_name),
        // named));
        caller_param_names.push_back(
            std::make_pair(std::move(arg_name), named));
#ifdef MARTO_DEBUG
        std::cerr << "Registered named argument" << std::endl;
#endif
        add_arguments(std::move(args)...);
    }

  public:
    void add_arguments() {}
    template <typename... Types>
    void add_arguments(shared_ptr<SymbolicParameter> symbolic_param_value,
                       Types... args) {
        add_arguments_(false,
                       std::to_string(next_numbered_registered_parameters++),
                       std::move(symbolic_param_value), std::move(args)...);
    }
    template <typename... Types>
    void add_arguments(std::string arg_name,
                       shared_ptr<SymbolicParameter> symbolic_param_value,
                       Types... args) {
        add_arguments_(true, std::move(arg_name),
                       std::move(symbolic_param_value), std::move(args)...);
    }

  public:
    template <typename... Types>
    SymbolicFunctionCall(std::string func_name, Types... args)
        : make_shared_class /* SymbolicNameLookup */ (std::move(func_name)),
          next_numbered_registered_parameters(0), caller_param_names(),
          caller_params(new_shared<SymbolicNamedParameters>()),
          function_(nullptr) {
        // TODO: check name validity
        add_arguments(std::move(args)...);
    }

    virtual operator std::string() const override {
        std::string str = name + "(";
        std::string sep;
        for (auto param : caller_param_names) {
            str += sep;
            if (param.second) {
                str += param.first + "=";
            }
            str +=
                (std::string) * (caller_params->lookup(param.first)->value());
            sep = ", ";
        }
        str += ")";
        return str;
    }

    virtual std::string
    compile_info(const SymbolicNamedParameters::pointer &__marto_unused(
        parameters)) const override {
        return std::string("while compiling function call '") +
               (std::string)(*this) + "'";
    }

  protected:
    void compute_arguments(const SymbolicNamedParameters::pointer &parameters);

    virtual SymbolicParameter *
    compute_real(const SymbolicNamedParameters::pointer &parameters) override;

  private:
    SymbolicCallableFunctionBase *function_;

#ifdef MARTO_DEBUG
  protected:
    virtual void more_info(bool __marto_unused(start)) {
        SymbolicNameLookup::more_info(start);
        std::cerr << "(";
        std::string sep("");
        for (const auto &sn : caller_param_names) {
            std::cerr << sep;
            if (sn.second) {
                std::cerr << sn.first << "=";
            }
            std::cerr << caller_params->lookup(sn.first).get();
            sep = ", ";
        }
        std::cerr << ")";
    }
#endif
};

/** Class to manage an event transition
 */
class SymbolicTransition
    : public enable_make_shared<SymbolicTransition, SymbolicRealParameter,
                                SymbolicParameter::WithNoData,
                                SymbolicBindings> {
  private:
    const shared_ptr<const Transition> transition_;

  public:
    SymbolicTransition(shared_ptr<const Transition> t)
        : transition_(std::move(t)) {}
    SymbolicTransition(shared_ptr<Transition> t)
        : SymbolicTransition(smartptr_const_cast(t)) {}
    //: SymbolicTransition(std::const_pointer_cast<const Transition>(t)) {}
    SymbolicTransition(const Transition *t);

    virtual operator std::string() const override;

    virtual std::string
    compile_info(const SymbolicNamedParameters::pointer &__marto_unused(
        parameters)) const override {
        return std::string("while compiling event transition '") +
               (std::string)(*this) + "'";
    }
    shared_ptr<const Transition> transition() const { return transition_; }

    shared_ptr<const Transition> operator*() const { return transition_; }

    shared_ptr<const Transition> operator->() const { return transition_; }

    void compute_parameters_values(
        marto::shared_ptr<const SymbolicNamedParameters> sps,
        marto::shared_ptr<ParameterValuesContainer> pvc,
        EventGenerationData *g) const;

  protected:
    void compute_arguments(const SymbolicNamedParameters::pointer &parameters);

    virtual SymbolicParameter *
    compute_real(const SymbolicNamedParameters::pointer &parameters) override;
};

class SymbolicCallableFunctionBase
    : public enable_make_shared<SymbolicCallableFunctionBase, SymbolicParameter,
                                SymbolicParameter::WithConstantData,
                                CallableFunctionBase> {
  protected:
    virtual SymbolicParameter *compiled() const override {
        return const_cast<SymbolicParameter *>(
            static_cast<const SymbolicParameter *>(this));
    }

    virtual void do_compile(const SymbolicNamedParameters::pointer
                                &__marto_unused(parameters)) override {
        // TODO compile default arguments
    }

  public:
    virtual operator std::string() const override {
        std::string str{"("};
        std::string sep;
        for (const shared_ptr<RequiredUntypedParameter> &arg :
             std::as_const(as_vector())) {
            str += sep + arg->name();
            sep = ", ";
        }
        return str + ")";
    }
};

template <class Self>
class SymbolicCallableFunction : public SymbolicCallableFunctionBase {};

namespace bits {

template <class Inherit, class Params>
struct TransitionBaseCollected
    : public Inherit,
      public /* FIXME : used in test_params2 */ Params {
    using Inherit::Inherit;

    using Inherit::apply;
    using transition_class = TransitionBaseCollected;
};

template <template <typename...> class Cont, typename... Ts> struct LazyType {
    using type = Cont<Ts...>;
};

template <typename T, typename... TCol> struct is_callable_function_helper {
  private:
    using Collect_ = collect::parameters<
        with_self<T, as_public<SymbolicCallableFunction<T>, TCol...>>>;

  public:
    using type = typename enable_make_shared_collected<T, Collect_>::type;
};

} // namespace bits

// Forward-declaration: stop
template <typename T, typename... TCol>
using is_callable_function =
    typename bits::is_callable_function_helper<T, TCol...>::type;
// Forward-declaration: restart

#endif // SWIG

} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS
#undef MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS
#include <marto/headers-impl.h>
#endif

#endif
