/* -*-c++-*- C++ mode for emacs */
/* Marto generic collections */
#ifndef MARTO_COLLECTIONS_H
#define MARTO_COLLECTIONS_H

/* No need of external forward-declaration for this file
 *
 * Forward-declaration: none
 */

#ifdef __cplusplus

#include <boost/core/noncopyable.hpp>
#include <map>
#include <marto/except.h>
#include <marto/forward-decl.h>
#include <marto/macros.h>
#include <marto/memory.h>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

//#define MARTO_DEBUG

#ifdef MARTO_DEBUG
#include <iostream>
#include <marto/debug.h>
#endif

namespace marto {

class ConfigBis;

struct RefCount {
    RefCount() : counter_(0) {}
    ~RefCount() { assert(counter_ == 0); }

  private:
    struct RefCountTake {
        RefCountTake(RefCount &rc) : ref_count_(rc) { ref_count_.acq(); }

        ~RefCountTake() { ref_count_.rel(); }

      private:
        RefCount &ref_count_;
    };

    void acq() { counter_++; }
    void rel() { counter_--; }

  public:
    RefCountTake gard() { return RefCountTake(*this); }

    size_t count() { return counter_; }

    operator size_t() { return count(); }

  private:
    size_t counter_;
};

/** @defgroup collection Collections for MarTo
 *
 * Templates to manage collections of MarTo objects
 *  @{
 */

namespace collections {

#ifndef SWIG
template <class T> struct CollectionElementTypes {
    using element_type = T;
    using element_smart_pointer = marto::shared_ptr<T>;
    using const_element_smart_pointer = marto::shared_ptr<const T>;
    using vector_type = std::vector<element_smart_pointer>;
    using const_vector_type = std::vector<const_element_smart_pointer>;
    using size_type = typename vector_type::size_type;
};

template <class U, class T> struct CollectionTypes : CollectionElementTypes<T> {
    using collection_type = U;
    using typename CollectionElementTypes<T>::element_type;
    using typename CollectionElementTypes<T>::element_smart_pointer;
    using typename CollectionElementTypes<T>::const_element_smart_pointer;
    using typename CollectionElementTypes<T>::vector_type;
    using typename CollectionElementTypes<T>::const_vector_type;
    using typename CollectionElementTypes<T>::size_type;
};

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

/** @defgroup vector Vectors
 *
 *  The class U must inherit of Vector<U,T,n>
 *  and the class T must inherit of VectorElement<U,T,n>
 *
 *  `n` can be chosen by the programmer (default to 0) in case
 *  several <U,T> vectors exists (same U and T)
 *
 *  The last template argument can be used to defined (recursivelly) additionnal
 * collections
 *  @{
 */

template <class U, class T, int n = 0> struct VectorElement;
template <class T, class Inherit> struct VectorElements;
template <class U, class T, int n = 0> struct Vector;
template <class U, class Inherit> struct Vectors;

/** class template to define types linked to vector elements */
template <class U, class T> class VectorTypes : public CollectionTypes<U, T> {};

/** class template to handle vector element information
 *
 * All contents associated to a vector element is managed here
 *
 * This class is inherited by T through `extend_make_shared`
 */
template <class U, class T, int n>
struct VectorElement : protected extend_make_shared<VectorElement<U, T, n>, T> {
  private:
    using CT = VectorTypes<U, T>;

  public:
    using size_type = typename CT::size_type;
    using element_smart_pointer = typename CT::element_smart_pointer;

  public:
    U &collection() const {
        assert(collection_);
        return *collection_;
    }
    Vector<U, T, n> &vector() const {
        assert(collection_);
        return *(Vector<U, T, n> *)collection_;
    }
    size_type index() const { return index_; }

  protected:
  private:
    size_type index_;
    U *collection_ = nullptr;

  protected:
    friend extend_make_shared<VectorElement<U, T, n>, T>;
    template <typename EMS_pointer, typename F, typename... Args>
    static element_smart_pointer make_shared_extension(F &&f, U &collection,
                                                       Args &&...args) {
#ifdef MARTO_DEBUG
        std::cerr << "=> " << marto::type_name<VectorElement>()
                  << "::make_shared_extension<"
                  << marto::type_name<EMS_pointer>() << ">" << std::endl;
#endif
        auto gard = count.gard();
        auto cb =
            std::function<element_smart_pointer(Args...)>(std::forward<F>(f));
        element_smart_pointer ptr = cb(std::forward<Args>(args)...);
        ((VectorElement<U, T, n> *)ptr.get())->collection_ = &collection;
        ((VectorElement<U, T, n> *)ptr.get())->index_ =
            /* plain pointer cast to avoid problem of private inheritence */
            ((Vector<U, T, n> *)(&collection))->push_back(ptr);
        // collection.template vector_of<T,n>::push_back(ptr);
#ifdef MARTO_DEBUG
        std::cerr << "<= " << marto::type_name<VectorElement>()
                  << "::make_shared_extension<"
                  << marto::type_name<EMS_pointer>() << ">" << std::endl;
#endif
        return ptr;
    }

  protected:
    VectorElement() {
#ifdef MARTO_DEBUG
        // std::cerr << "=> "<<marto::type_name<>(this) << " (this)" <<
        // std::endl;
        std::cerr << "== " << marto::type_name<VectorElement>()
                  << " count=" << (int)count << std::endl;
#endif
#ifndef MARTO_OPTIMIZE
        if (!count) {
            throw std::logic_error("Direct vector element allocation "
                                   "forbidden, `make_shared()` must be used.");
        }
#endif
    }

    ~VectorElement() {
        static_assert_is_base_of<VectorElement, T>();
        static_assert_is_base_of<Vector<U, T, n>, U>();
    }

  private:
#ifndef MARTO_OPTIMIZE
    static thread_local RefCount count;
#endif
};
#ifndef MARTO_OPTIMIZE
template <class U, class T, int n>
thread_local RefCount VectorElement<U, T, n>::count{};
#endif

/** In case of several vector elements, allows one to select one */
template <class T, typename Inherit> struct VectorElements : Inherit {
    using Inherit::Inherit;
    using collection_class = VectorElements;

    template <class U, int n = 0> using element_of = VectorElement<U, T, n>;
    template <class U, int n = 0>
    using vector_element_of = VectorElement<U, T, n>;

  protected:
    ~VectorElements() { static_assert_is_base_of<VectorElements, T>(); }
};

/** Contents associated to a vector */
template <class U, class T, int n> struct Vector {
  private:
    using CT = VectorTypes<U, T>;

  public:
    /** importing types from VectorTypes */
    using vector_type = typename CT::vector_type;
    using const_vector_type = typename CT::const_vector_type;
    using size_type = typename CT::size_type;
    using element_smart_pointer = typename CT::element_smart_pointer;

  protected:
    /** basic constructor */
    Vector() {}
    /** constructor with vector size */
    Vector(size_type size) { vector_.reserve(size); };
    ~Vector() {
        static_assert_is_base_of<VectorElement<U, T, n>, T>();
#ifdef MARTO_DEBUG
        std::cerr << "Destroying " << type_name(this) << ": " << this
                  << std::endl;
#endif
    }

  private:
    friend struct VectorElement<U, T, n>;
    size_type push_back(element_smart_pointer ptr) {
        size_type idx = vector_.size();
#ifdef MARTO_DEBUG
        std::cerr << "[MartoVector] push_back " << this << " for " << ptr.get()
                  << " use_count=" << ptr.use_count() << " size=" << idx
                  << std::endl;
#endif
        vector_.push_back(std::move(ptr));
#ifdef MARTO_DEBUG
        std::cerr << " new size=" << vector_.size() << std::endl;
#endif
        return idx;
    }

  public:
    const vector_type as_vector() const { return std::as_const(vector_); }
    const const_vector_type as_const_vector() const {
        const_vector_type cv;
        cv.reserve(vector_.size());
        for (auto const &e : vector_) {
            cv.pushback(e);
        }
        return cv;
    }
    T &at(size_type idx) const { return vector_.at(idx); }
    element_smart_pointer operator[](size_type idx) const {
        return vector_[idx];
    }
    size_type size() const { return vector_.size(); }

  private:
    vector_type vector_;
};

/** In case of several vectors, allow to select one */
template <class U, class Inherit> struct Vectors : Inherit {
    using Inherit::Inherit;
    using collection_class = Vectors;

    template <class T, int n = 0> using vector_of = Vector<U, T, n>;

  protected:
    ~Vectors() { static_assert_is_base_of<Vectors, U>(); }
};

///@}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/

/** @defgroup map Maps and vectors toguether
 *
 *  The class U must inherit of Map<U,T,n>
 *  and the class T must inherit of MapElement<U,T,n>
 *
 *  `n` can be chosen by the programmer (default to 0) in case
 *  several <U,T> maps exists (same U and T)
 *
 *  The last template argument can be used to defined (recursivelly) additionnal
 * collections
 *  @{
 */

template <class U, class T, int n = 0> struct MapElement;
template <class T, typename Inherit> struct MapElements;
template <class U, class T, int n = 0> struct Map;
template <class U, typename Inherit> struct Maps;

/** class template to define types linked to maps elements */
template <class U, class T> class MapTypes : public CollectionTypes<U, T> {
  private:
    using CT = CollectionTypes<U, T>;

  public:
    using map_key_type = std::string;
    using typename CT::element_smart_pointer;
    using map_type = std::map<map_key_type, element_smart_pointer>;
};

namespace internal {

template <typename X, typename T, typename U>
const T &maxVal(const T &var1, const U &var2) {
    if (var1 < var2)
        return var1;
    else
        return var2;
}

template <class T, class U = void> struct check_is_map {
    static constexpr bool value = false;
};

template <class T>
struct check_is_map<T, typename std::remove_reference_t<T>::is_map> {
    static constexpr bool value = true;
};

template <typename... Ts> struct check_next_is_map {
    static constexpr bool value = false;
};

template <class T, typename... Ts> struct check_next_is_map<T, Ts...> {
    static constexpr bool value = check_is_map<T>::value;
};

} // namespace internal

/** class template to handle map element information
 *
 * This class is inherited by T through `extend_make_shared`
 */
template <class U, class T, int n>
struct MapElement : protected extend_make_shared<MapElement<U, T, n>, T> {
  private:
    using CT = MapTypes<U, T>;

  public:
    using size_type = typename CT::size_type;
    using element_smart_pointer = typename CT::element_smart_pointer;

  public:
    U &collection() const {
        assert(collection_);
        return *collection_;
    }
    Map<U, T, n> &map() const {
        assert(collection_);
        return *(Map<U, T, n> *)collection_;
    }
    size_type index() const { return index_; }
    const std::string &name() const { return name_; }

  protected:
  private:
    std::string name_;
    size_type index_;
    U *collection_ = nullptr;

  protected:
    friend extend_make_shared<MapElement<U, T, n>, T>;
    template <typename EMS_pointer, typename F, typename... Args>
    static element_smart_pointer make_shared_extension(F &&f, std::string name,
                                                       U &collection,
                                                       Args &&...args) {
#ifdef MARTO_DEBUG
        std::cerr << "=> " << marto::type_name<MapElement>()
                  << "::make_shared_extension<" << marto::type_name<T>() << ">("
                  << name << ", " << &collection << ")" << std::endl;
        assert(name != "");
#endif
        Map<U, T, n> *map = (Map<U, T, n> *)(&collection);
        if (map->map_.count(name) != 0) {
#ifdef MARTO_DEBUG
            std::cerr << "Duplicate for " << name << std::endl;
            std::cerr << map->map_[name] << std::endl;
            std::cerr << map->map_[name].get() << std::endl;
#endif
            throw ExistingName(std::move(name));
        }
        element_smart_pointer ptr;
        auto gard = count.gard();
        if constexpr (internal::check_next_is_map<Args...>::value) {
            auto cb =
                std::function<element_smart_pointer(std::string, Args...)>(
                    std::forward<F>(f));
            ptr = cb(name, std::forward<Args>(args)...);
        } else {
            auto cb = std::function<element_smart_pointer(Args...)>(
                std::forward<F>(f));
            ptr = cb(std::forward<Args>(args)...);
        }
        ((MapElement<U, T, n> *)ptr.get())->collection_ = &collection;
        ((MapElement<U, T, n> *)ptr.get())->name_ = name;
        ((MapElement<U, T, n> *)ptr.get())->index_ =
            map->push_back(std::move(name), ptr);
#ifdef MARTO_DEBUG
        std::cerr << "<= " << marto::type_name<MapElement>()
                  << "::make_shared_extension<" << marto::type_name<T>() << ">"
                  << std::endl;
#endif
        return ptr;
    }

    MapElement() {
#ifdef MARTO_DEBUG
        // std::cerr << "=> "<<marto::type_name<>(this) << " (this)" <<
        // std::endl;
        std::cerr << "== " << marto::type_name<MapElement>()
                  << " count=" << (int)count << std::endl;
#endif
#ifndef MARTO_OPTIMIZE
        if (!count) {
            // FIXME: code correct if make_shared used
            // throw std::logic_error(
            //    "Direct map element allocation forbidden, `make_shared()` must
            //    be used.");
        }
#endif
    }

    ~MapElement() {
        static_assert_is_base_of<MapElement, T>();
        static_assert_is_base_of<Map<U, T, n>, U>();
    }

  private:
#ifndef MARTO_OPTIMIZE
    static thread_local RefCount count;
#endif
};
#ifndef MARTO_OPTIMIZE
template <class U, class T, int n>
thread_local RefCount MapElement<U, T, n>::count{};
#endif

/** In case of several map elements, allows one to select one */
template <class T, typename Inherit> struct MapElements : Inherit {
    using Inherit::Inherit;
    using collection_class = MapElements;

    template <class U, int n = 0> using element_of = MapElement<U, T, n>;
    template <class U, int n = 0> using map_element_of = MapElement<U, T, n>;

  protected:
    ~MapElements() { static_assert_is_base_of<MapElements, T>(); }
};

/** Contents associated to a map */
template <class U, class T, int n> struct Map {
  private:
    using CT = MapTypes<U, T>;

  public:
    /** importing types from MapTypes */
    using vector_type = typename CT::vector_type;
    using const_vector_type = typename CT::const_vector_type;
    using size_type = typename CT::size_type;
    using element_type = typename CT::element_type;
    using element_smart_pointer = typename CT::element_smart_pointer;
    using map_type = typename CT::map_type;
    using map_key_type = typename CT::map_key_type;

  public:
    /** basic constructor */
    Map() {}
    /** constructor with vector size */
    Map(size_type size) : Map() { vector_.reserve(size); };
    ~Map() {
#ifdef MARTO_DEBUG
        std::cerr << "Destroying " << type_name(this) << ": " << this
                  << std::endl;
#endif
    }

  private:
    friend struct MapElement<U, T, n>;
    size_type push_back(std::string name, element_smart_pointer ptr) {
        size_type idx = vector_.size();
#ifdef MARTO_DEBUG
        std::cerr << "Registering " << name << " (" << ptr.get()
                  << "/ use_count=" << ptr.use_count() << ") into " << this
                  << std::endl;
        assert(name != "");
#endif
        auto res = map_.try_emplace(std::move(name), ptr);
        if (!res.second) {
#ifdef MARTO_DEBUG
            std::cerr << "!!!! Duplicate for " << name << std::endl;
            std::cerr << map_[name] << std::endl;
#endif
            /* should never occurs: we check for presence before
             * creating the object...
             */
            throw std::logic_error(name + " already registered!!!");
        }
        // the insertion occurs
#ifdef MARTO_DEBUG
        std::cerr << "Registered " << res.first->first << " with " << &ptr
                  << std::endl;
#endif
        vector_.push_back(std::move(ptr));
        return idx;

#ifdef MARTO_DEBUG
        std::cerr << "[Map] push_back " << this << " for " << ptr.get()
                  << " use_count=" << ptr.use_count() << " size=" << idx
                  << std::endl;
#endif
        vector_.push_back(std::move(ptr));
#ifdef MARTO_DEBUG
        std::cerr << " new size=" << vector_.size() << std::endl;
#endif
        return idx;
    }

  public:
    const vector_type &as_vector() const { return std::as_const(vector_); }
    const const_vector_type as_const_vector() const {
        const_vector_type cv;
        cv.reserve(vector_.size());
        for (auto const &e : vector_) {
            cv.push_back(std::static_pointer_cast<element_type const>(e));
        }
        return cv;
    }
    // const MartoMap &operator()() const { return std::as_const(*this); }
    element_smart_pointer at(size_type idx) const { return vector_.at(idx); }
    element_smart_pointer at(const std::string &name) const {
        return map_.at(name);
    }
    element_smart_pointer operator[](size_type idx) const {
        return vector_.at(idx);
    }
    element_smart_pointer operator[](const std::string &name) const {
        return map_.at(name);
    }
    auto at_element(size_type idx) const {
        return (MapElement<U, T, n> *)(at(idx).get());
    }
    auto at_element(const std::string &name) const {
        return (MapElement<U, T, n> *)(at(name).get());
    }

    size_type size() const { return vector_.size(); }

    const std::vector<map_key_type> names() const {
        std::vector<map_key_type> names;
        names.reserve(map_.size());
        for (auto const &entry : map_) {
            names.push_back(entry.first);
        }
        return std::as_const(names);
    }

  private:
    vector_type vector_;
    map_type map_;

    template <typename T1, typename U1> friend struct internal::check_is_map;

  public: // FIXME: g++ bug, friend not taken into account :-(
    using is_map = void;
};

/** In case of several maps, allows one to select one */
template <class U, class Inherit> struct Maps : Inherit {
    using Inherit::Inherit;
    using collection_class = Maps;

    template <class T, int n = 0> using map_of = Map<U, T, n>;

  protected:
    ~Maps() { static_assert_is_base_of<Maps, U>(); }
};
}

/** @}
 *
 *****************************************************************
 * @defgroup collect-collections
 *
 * define and provide a `collections` collector that allows one to
 * get maps/vectors (or their elements)
 * @{
 */

namespace collect {

namespace collector::collections_impl {

template <phase p, class U, class Options> struct collections;

}

template <typename, int n = 0> struct in_vector {};
template <typename, int n = 0> struct has_vector_of {};
template <typename, int n = 0> struct in_map {};
template <typename, int n = 0> struct has_map_of {};

namespace bits::collections {

template <class Options, bool iv = false, bool vo = false, bool im = false,
          bool mo = false>
struct options : Options {
    static constexpr bool in_vector = iv;
    static constexpr bool has_vector_of = vo;
    static constexpr bool in_map = im;
    static constexpr bool has_map_of = mo;
};

template <class Result, int iv = 0, int vo = 0, int im = 0, int mo = 0>
struct result : Result {
    static constexpr int in_vector = iv;
    static constexpr int has_vector_of = vo;
    static constexpr int in_map = im;
    static constexpr int has_map_of = mo;
};

} // namespace bits::collections

namespace collector {

namespace collections_impl {

using namespace marto::collections;

template <class Options>
struct collections<phase::init_options, void, Options> {
  public:
    using options = bits::collections::options<Options>;
};

template <class Options, class Result>
struct collections<phase::init_result, Options, Result> {
  public:
    using options = bits::collections::result<Result>;
};

template <class U, int n, class Options>
struct collections<phase::explore, in_vector<U, n>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set");
    using T = tuple_first_t<typename Options::self_tuple_t>;
    using options = bits::collections::options<Options, true>;
    using type = std::tuple<VectorElement<U, T, n>>;
};

template <class T, int n, class Options>
struct collections<phase::explore, has_vector_of<T, n>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set");
    using U = tuple_first_t<typename Options::self_tuple_t>;
    using options =
        bits::collections::options<Options, Options::in_vector, true>;
    using type = std::tuple<Vector<U, T, n>>;
};

template <class U, int n, class Options>
struct collections<phase::explore, in_map<U, n>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set");
    using T = tuple_first_t<typename Options::self_tuple_t>;
    using options = bits::collections::options<Options, Options::in_vector,
                                               Options::has_vector_of, true>;
    using type = std::tuple<MapElement<U, T, n>>;
};

template <class T, int n, class Options>
struct collections<phase::explore, has_map_of<T, n>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set");
    using U = tuple_first_t<typename Options::self_tuple_t>;
    using options = bits::collections::options<Options, Options::in_vector,
                                               Options::has_vector_of,
                                               Options::in_map, true>;
    using type = std::tuple<Map<U, T, n>>;
};

template <class U, class Options, class Result>
struct collections<phase::classify, std::pair<Options, U>, Result> {
    using options = bits::collections::result<
        Result, Result::in_vector + Options::in_vector,
        Result::has_vector_of + Options::has_vector_of,
        Result::in_map + Options::in_map,
        Result::has_map_of + Options::has_map_of>;
};

template <class Result, class CleanResult>
struct collections<phase::cleanup, Result, CleanResult> {
    using options =
        bits::collections::result<CleanResult, Result::in_vector,
                                  Result::has_vector_of, Result::in_map,
                                  Result::has_map_of>;
};

} // namespace collections_impl

using collections_impl::collections;

} // namespace collector

using collections_collectors =
    collectors_list_merge_t<sft_collectors,
                            collectors_list<collector::collections>>;

template <typename... Bases>
using collections = do_collect_classes<collections_collectors, Bases...>;

} // namespace collect

/** @}
 *****************************************************************
 */

namespace collections {

namespace bits {

template <typename X, typename... TCol> struct is_in_collection_helper {
  private:
    using Collect_ = collect::collections<with_self<X, as_public<TCol...>>>;

    using EMS = typename enable_make_shared_collected<X, Collect_>::type;

    using VE_type = std::conditional_t<(Collect_::in_vector == 0), EMS,
                                       VectorElements<X, EMS>>;
    using VC_type = std::conditional_t<(Collect_::has_vector_of == 0), VE_type,
                                       Vectors<X, VE_type>>;
    using ME_type = std::conditional_t<(Collect_::in_map == 0), VC_type,
                                       MapElements<X, VC_type>>;
    using MC_type = std::conditional_t<(Collect_::has_map_of == 0), ME_type,
                                       Maps<X, ME_type>>;

  public:
    using type = MC_type;
};

} // namespace bits

template <typename T, typename... TCol>
using is_element = typename bits::is_in_collection_helper<T, TCol...>::type;

template <typename T, typename... TCol>
using is_collection = typename bits::is_in_collection_helper<T, TCol...>::type;

#endif // SWIG

} // namespace collections

using collect::has_map_of;
using collect::has_vector_of;
using collect::in_map;
using collect::in_vector;
using collections::is_collection;
using collections::is_element;

///@}
} // namespace marto

#undef MARTO_DEBUG

#endif // __cplusplus
#endif
