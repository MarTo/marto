/* -*-c++-*- C++ mode for emacs */
/* Types for parameters */
#ifndef MARTO_PARAMETERS_TYPES_H
#define MARTO_PARAMETERS_TYPES_H

#define MARTO_HEADER_NEED_PARAMETERS_TYPES_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_TYPES
#endif

#ifdef __cplusplus

#include <marto/parameters-types.h>

#include <marto/types.h>

namespace marto {

class ParametersTypeInfo {
    template <typename Type> friend class MartoTypeStandard;

  private:
    constexpr ParametersTypeInfo(size_t ts, size_t ta)
        : type_sizeof_(ts), type_alignof_(ta){};
    // ParametersTypeInfo(size_t ts, size_t ta, std::string name)
    //    : type_sizeof_(ts), type_alignof_(ta), name_(std::move(name)){};

  public:
    size_t type_sizeof() const { return type_sizeof_; }
    size_t type_alignof() const { return type_alignof_; }
    // const std::string &name() const { return name_; }
    bool operator==(const ParametersTypeInfo &b) const { return this == &b; }

  private:
    size_t type_sizeof_;
    size_t type_alignof_;
    // const std::string name_;
};

class MartoTypeBase {
  protected:
    constexpr MartoTypeBase() {}

  public: // TODO: move it in another class?
    typedef typename marto::shared_ptr<const SymbolicParameter>
        symbolic_parameter_pointer;
};

template <typename Type> class MartoTypeStandard : public MartoTypeBase {
  private:
    constexpr MartoTypeStandard() {}

  public:
    typedef Type value_t;
    typedef marto::shared_ptr<TypedParameterValues<MartoTypeStandard>>
        typed_values_pointer;
    using MartoTypeBase::symbolic_parameter_pointer;

    static constexpr ParametersTypeInfo info{
        sizeof(Type), alignof(Type)
        /*, typeid(Type).name()*/
    }; //() {
    //        static constexpr MartoType info_{};
    //        return info_;
    //    }
    static marto::shared_ptr<TypedParameterValues<MartoTypeStandard>>
    allocate_parameter_values(symbolic_parameter_pointer sp);
};

///** unique queue identifier */
// typedef uint32_t queue_id_t;

} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_TYPES
#undef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS_TYPES
#include <marto/headers-impl.h>
#endif

#endif
