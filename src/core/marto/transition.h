/* -*-c++-*- C++ mode for emacs */
/* Transition representation */
#ifndef MARTO_TRANSITION_H
#define MARTO_TRANSITION_H

#define MARTO_HEADER_NEED_TRANSITION_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_TRANSITION
#endif

#ifdef __cplusplus

#include <marto/callable-function.h>
#include <marto/collections.h>
#include <marto/forward-decl.h>
#include <marto/simulation.h>

namespace marto {

template <class Kind, class Parameters> //
class NoParameters {
  public:
    NoParameters(Parameters *) {}
};

class Transition
    : public is_element<Transition, as_protected<in_map<Simulation>>,
                        as_public<marto::CallableFunction<Transition>>> {
  protected:
    /** Transition constructor
     *
     * The transition will be registered into the configuration
     * with make_shared.
     */
    Transition(){};

  public:
    virtual ~Transition() {}

    /** in: s, ev
     *  out: ? (FIXME)
     * Apply transition to state in Set s
     * May change the type of the state contained in Set s, from
     * HyperRectangle to Union for instance
     */
    int apply(Set *s, const EventState *ev) const;

    /** Technical apply method
     *
     * Does nothing except calling apply for the proper SetImpl
     * subtype
     *
     * If the returned SetImpl is not the same as the called one, the
     * method must delete the previous SetImpl (or use it as part of
     * the new SetImpl)
     */
    SetImpl *apply(SetImpl *s, const EventState *ev) const;

    /** Apply transition to a point
     *
     * Must be implemented in each concrete Transition class
     *
     * Specialisation: a Point always gives a Point
     * Implementation: the Point must be modified in-place (no allocation)
     */
    virtual Point *apply(Point *p, const EventState *ev) const = 0;

    /** Apply transition to a set of states with hyperrectangle structure
     *
     * no default for the generic case (MonotonicTransition adds such a default)
     */
    virtual SetImpl *apply(HyperRectangle *h, const EventState *ev) const = 0;

    /** Apply transition to a set of states with union structure
     *
     * default : applies transition to each component of the union
     */
    virtual SetImpl *apply(Union *u, const EventState *ev) const;

    /** give the name of the transition */
    const std::string &name() const {
        return map_element_of<Simulation>::name();
    }

    virtual void compute_parameters_values(
        marto::shared_ptr<const SymbolicNamedParameters> sps,
        marto::shared_ptr<ParameterValuesContainer> pvc,
        EventGenerationData *g) const = 0;
};

// SWIG does not handle template template arguement
#ifndef SWIG // do-forward-decl
template <class Self, template <class, class>
                      class Parameters // do-forward-decl
                      = NoParameters   // NO-forward-decl
          >                            // do-forward-decl
class TransitionParameters : public virtual Transition,
                             public marto::UsingParameters<Parameters, Self> {

    virtual void compute_parameters_values(
        marto::shared_ptr<const SymbolicNamedParameters> sps,
        marto::shared_ptr<ParameterValuesContainer> pvc,
        EventGenerationData *g) const override {
        (void)sps;
        (void)pvc;
        (void)g;
        // TODO call compute ?
    }
};
#endif // SWIG // do-forward-decl

class MonotonicTransition : public virtual Transition {

  public:
    using marto::Transition::apply;

    /** Apply transition to a set of states with hyperrectangle structure
     *
     * default : applies transition to each point of the hyperrectangle
     */
    virtual SetImpl *apply(HyperRectangle *h,
                           const EventState *ev) const override;
};

#if 0
namespace transition {

class ParametersBase {
  protected:
    const marto::Transition *_transition;
    const marto::EventState *event;

    ParametersBase(const ::marto::Transition *tr, const ::marto::EventState *ev)
        : _transition(tr), event(ev) {}

  public:
    const marto::Transition *transition() const { return _transition; }
};

} // namespace transition
#endif

namespace collect {

namespace collector::transition_impl {

template <phase p, class U, class Options> struct transition;

}

struct is_monotonic;

namespace bits::transition {

template <class Result, class transition = std::tuple<>,
          class params = std::tuple<>>
struct result : Result {
    using transition_tuple_t = transition;
    using parameters_tuple_t = params;
};

} // namespace bits::transition

namespace collector {

namespace transition_impl {

using namespace marto::transition;

template <class Options, class Result>
struct transition<phase::init_result, Options, Result> {
  public:
    using options = bits::transition::result<Result>;
};

template <class Options>
struct transition<phase::explore, is_monotonic, Options> {
    using type = std::tuple<MonotonicTransition>;
    using options = Options;
};

template <template <class Kind, class Parameters> class P, class Options>
struct transition<phase::explore, with_parameters<P>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set but required by with_parameter<...>");
    using T = tuple_first_t<typename Options::self_tuple_t>;
    using type = std::tuple<std::tuple<with_parameters<P>, T>>;
    using options = Options;
    using flags = bits::collect::CollectorFlags<bits::collect::CollectedFlag<>>;
};

template <class U, class Options, class Result>
struct transition<phase::classify, std::pair<Options, U>, Result> {
    using transition_tuple_t =
        std::conditional_t<std::is_base_of_v<marto::Transition, U>,
                           std::tuple<U>, std::tuple<>>;
    static_assert(is_empty_tuple_v<transition_tuple_t> ||
                      is_empty_tuple_v<typename Result::transition_tuple_t>,
                  "several bases inherit from marto::Transition");
    using options = bits::transition::result<
        Result,
        tuple_cat_t<transition_tuple_t, typename Result::transition_tuple_t>,
        typename Result::parameters_tuple_t>;
};

template <class T, template <class Kind, class Parameters> class P,
          class Result>
struct transition<phase::classify, std::tuple<with_parameters<P>, T>, Result> {
    static_assert(is_empty_tuple_v<typename Result::parameters_tuple_t>,
                  "multiple transition parameters set!");
    // Implementation note: the TransitionParameters<T, P> class is
    // added when the collect is done.
    using options =
        bits::transition::result<Result, typename Result::transition_tuple_t,
                                 std::tuple<TransitionParameters<T, P>>>;
    using flags = bits::collect::CollectorFlags<bits::collect::CollectedFlag<>>;
};

template <class Result, class CleanResult>
struct transition<phase::cleanup, Result, CleanResult> {
    using options =
        bits::transition::result<CleanResult,
                                 typename Result::transition_tuple_t,
                                 typename Result::parameters_tuple_t>;
};

} // namespace transition_impl

using transition_impl::transition;

} // namespace collector

using transition_collectors =
    collectors_list_merge_t<collectors_list<collector::transition>,
                            sft_collectors>;

template <typename... Bases>
using transition = do_collect_classes<transition_collectors, Bases...>;

} // namespace collect

namespace transition {

namespace bits {

template <class Inherit, class Params>
struct TransitionBaseCollected
    : public Inherit,
      public /* FIXME : used in test_params2 */ Params {
    using Inherit::Inherit;

    using Inherit::apply;
    using transition_class = TransitionBaseCollected;
};

template <template <typename...> class Cont, typename... Ts> struct LazyType {
    using type = Cont<Ts...>;
};

template <typename T, typename... TCol> struct is_transition_helper {
  private:
    using Collect_ = collect::transition<with_self<T, as_public<TCol...>>>;

    using Params = std::conditional_t<
        is_empty_tuple_v<typename Collect_::parameters_tuple_t>,
        TransitionParameters<tuple_first_t<typename Collect_::self_tuple_t>,
                             NoParameters>,
        tuple_first_t<typename Collect_::parameters_tuple_t>>;

    using RealCollect_ = typename std::conditional_t<
        is_empty_tuple_v<typename Collect_::transition_tuple_t>,
        LazyType<collect::do_append_classes, collect::transition_collectors,
                 Collect_, as_public<collect::as_virtual<Transition>>>,
        type_identity<Collect_>>::type;

    using BaseTransitionClass = TransitionBaseCollected<
        typename enable_make_shared_collected<T, RealCollect_>::type, Params>;

  public:
    using type = BaseTransitionClass;
};

} // namespace bits

// Forward-declaration: stop
template <typename T, typename... TCol>
using is_transition = typename bits::is_transition_helper<T, TCol...>::type;
// Forward-declaration: restart

} // namespace transition

using transition::is_transition;

} // namespace marto
/* Pseudo code d'explication :
   - ma poltique :
   class MaPolitique : Transition {
   SetImpl apply(Point *p, EventState *ev) {
   calcul(p);
   return p;
   }
   }

   - enveloppe :
   class MesEnveloppes : MaPolitique {
   SetImpl apply(HyperRectangle *h, EventState *ev) {
   calcul(h);
   return h;
   }
   }

   ou

   class MesEnveloppes : Transition {
   SetImpl apply(Point *p, EventState *ev) {
   calcul(p);
   return p;
   }

   SetImpl apply(HyperRectangle *h, EventState *ev) {
   calcul(h);
   return h;
   }
   }
 */
#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_TRANSITION
#undef MARTO_HEADER_NEED_IMPL_FROM_TRANSITION
#include <marto/headers-impl.h>
#endif

#endif
