/* -*-c++-*- C++ mode for emacs */
/* Autoregistration against marto::Simulation framework */
#ifndef MARTO_AUTOREGISTER_H
#define MARTO_AUTOREGISTER_H

/* No need of external forward-declaration for this file
 *
 * Forward-declaration: none
 */

#ifdef __cplusplus

#include <marto/forward-decl.h>
#include <marto/utils.h>
#include <memory>
#include <string>
#include <vector>

#ifdef DEBUG_AUTOREGISTER
#include <iostream>
#include <marto/debug.h>
#endif

namespace marto {

namespace autoregister {

template <class RegistryType, typename RegArgs,
          template <class TRegistered> class TApplyOperator, typename ApplyArgs>
class FactoryApply; /* no definition, only specialization must be used */

template <class RegistryType, template <class TRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
class FactoryApply<RegistryType, std::tuple<RegArgs...>, TApplyOperator,
                   std::tuple<ApplyArgs...>> {
  public:
    FactoryApply() {}
    virtual ~FactoryApply() {}
    virtual void apply(ApplyArgs &&...) = 0;
};

template <class RegistryType, class TRegistered, typename RegArgs,
          template <class TTRegistered> class TApplyOperator,
          typename ApplyArgs>
class FactoryApplyImpl; /* no definition, only specialization must be used */

template <class RegistryType, class TRegistered,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
class FactoryApplyImpl<RegistryType, TRegistered, std::tuple<RegArgs...>,
                       TApplyOperator, std::tuple<ApplyArgs...>>
    : public FactoryApply<RegistryType, std::tuple<RegArgs...>, TApplyOperator,
                          std::tuple<ApplyArgs...>> {

    std::tuple<RegArgs...> reg_args_tuple;

  public:
    FactoryApplyImpl(RegArgs &&...args)
        : reg_args_tuple(make_tuple(
              args...)) /* TODO: check that data are really copied here */
    {}
    virtual void apply(ApplyArgs &&...applyArgs) override {
        return std::apply(TApplyOperator<TRegistered>(),
                          std::tuple_cat(reg_args_tuple,
                                         std::forward_as_tuple(applyArgs...)));
    }
};

template <class RegistryType, class TRegistered, typename RegArgs,
          template <class TTRegistered> class TApplyOperator,
          typename ApplyArgs>
class AutoRegister; /* no definition, only specialization must be used */

template <class RegistryType, typename RegArgs,
          template <class TTRegistered> class TApplyOperator,
          typename ApplyArgs>
class FactoriesApplyCollection; /* no definition, only specialization must be
                                   used */

template <class RegistryType,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
class FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                               TApplyOperator, std::tuple<ApplyArgs...>> {
    template <class IRegistryType, class ITRegistered, typename IRegArgs,
              template <class ITTRegistered> class ITApplyOperator,
              typename IApplyArgs>
    friend class AutoRegister;

  public:
    typedef FactoryApply<RegistryType, std::tuple<RegArgs...>, TApplyOperator,
                         std::tuple<ApplyArgs...>>
        factory_type;
    typedef FactoriesApplyCollection registry_type;

  private:
    typedef std::vector<std::unique_ptr<factory_type>> vector_type;
    /* using a pointer to a vector to avoid to rely on the linker to
     * correctly schedule the various constructors
     */
    static vector_type *factories;
    /* using another object to delete the previous pointer at the end
     * of the program
     *
     * Due to unknown allocation order, factories_deleter can be
     * initialized after factories is used (hence the pointer above,
     * that is statically initialized in the BSS section)
     */
    static PointerDeleter<vector_type *> factories_deleter;

  protected:
    static int factory_registration(factory_type *factory);

  public:
    static void apply(ApplyArgs... args) {
        if (factories) {
            for (auto &factory : *factories) {
                factory->apply(std::forward<ApplyArgs>(args)...);
            }
        }
#ifdef DEBUG_AUTOREGISTER
        else {
            std::cerr << "No factories registered for " << &factories
                      << std::endl;
        }
#endif
    }
};

template <class RegistryType,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
typename FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                                  TApplyOperator,
                                  std::tuple<ApplyArgs...>>::vector_type
    *FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                              TApplyOperator,
                              std::tuple<ApplyArgs...>>::factories = nullptr;

template <class RegistryType,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
PointerDeleter<typename FactoriesApplyCollection<
    RegistryType, std::tuple<RegArgs...>, TApplyOperator,
    std::tuple<ApplyArgs...>>::vector_type *>
    FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                             TApplyOperator,
                             std::tuple<ApplyArgs...>>::factories_deleter{
        &FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                                  TApplyOperator,
                                  std::tuple<ApplyArgs...>>::factories};

template <class RegistryType,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
int FactoriesApplyCollection<
    RegistryType, std::tuple<RegArgs...>, TApplyOperator,
    std::tuple<ApplyArgs...>>::factory_registration( // no-forward-decl
    factory_type *factory) {                         // no-forward-decl
    // required to force gcc to allocate and initialize factories_deleter object
    factories_deleter.ensure_allocation();
    if (factories == nullptr) {
        factories = new vector_type;
#ifdef DEBUG_AUTOREGISTER
        std::cerr << "Created vector for "
                  << marto::type_name<FactoriesApplyCollection<
                         RegistryType, std::tuple<RegArgs...>, TApplyOperator,
                         std::tuple<ApplyArgs...>>>()
                  << " at " << factories << std::endl;
#endif
    }
    factories->push_back(std::unique_ptr<factory_type>(factory));
#ifdef DEBUG_AUTOREGISTER
    std::cerr << "Registering factory at addresse " << factory << " in "
              << (&factories) << std::endl;
#endif
    return 0;
}

template <class RegistryType, class TRegistered,
          template <class TTRegistered> class TApplyOperator,
          typename... RegArgs, typename... ApplyArgs>
class AutoRegister<RegistryType, TRegistered, std::tuple<RegArgs...>,
                   TApplyOperator, std::tuple<ApplyArgs...>> {

  public:
    typedef FactoriesApplyCollection<RegistryType, std::tuple<RegArgs...>,
                                     TApplyOperator, std::tuple<ApplyArgs...>>
        registry_type;
    typedef FactoryApplyImpl<RegistryType, TRegistered, std::tuple<RegArgs...>,
                             TApplyOperator, std::tuple<ApplyArgs...>>
        factory_type;

  protected:
    inline static int class_registration(RegArgs... reg_args) {
        static int registration = registry_type::factory_registration(
            new factory_type(std::forward<RegArgs>(reg_args)...));
        return registration;
    }
};

template <class T> struct RegistryInfoVoid { typedef void type; };

template <class T, class TDefault, class U = void>
struct register_function_default {
    using type = TDefault;
};

template <class T, class TDefault>
struct register_function_default<
    T, TDefault,
    typename RegistryInfoVoid<typename T::register_function>::type> {
    using type = typename T::register_function;
};

template <class RegistryInfo>
class RegistryInfoWithDefault : public RegistryInfo {
  public:
    /* add register_function typename if not defined in RegistryInfo */
    typedef typename register_function_default<
        RegistryInfo,
        std::remove_pointer_t<typeof(void (*)(std::type_info const *))>>::type
        register_function;
};

} // namespace autoregister

template <class RegistryInfo, class Self>
class AutoRegister
    : public autoregister::AutoRegister<
          autoregister::RegistryInfoWithDefault<RegistryInfo>, Self,
          typename function_traits<
              typename autoregister::RegistryInfoWithDefault<
                  RegistryInfo>::register_function>::arguments_type,
          RegistryInfo::template Register,
          typename function_traits<
              typename RegistryInfo::apply_function>::arguments_type> {
  private:
    using AutoRegisterBase = autoregister::AutoRegister<
        autoregister::RegistryInfoWithDefault<RegistryInfo>, Self,
        typename function_traits<typename autoregister::RegistryInfoWithDefault<
            RegistryInfo>::register_function>::arguments_type,
        RegistryInfo::template Register,
        typename function_traits<
            typename RegistryInfo::apply_function>::arguments_type>;

  public:
    using typename AutoRegisterBase::factory_type;
    using typename AutoRegisterBase::registry_type;

  protected:
    // using AutoRegisterBase::class_registration;

/** Allows one to easily force the autoregistration */
#define marto_autoregister(...)                                                \
  private:                                                                     \
    inline static int autoregister_ = class_registration(__VA_ARGS__)
#define marto_autoregister_ctx(ctx, ...)                                       \
  private:                                                                     \
    inline static int autoregister_ = ctx::class_registration(__VA_ARGS__)
};

template <class RegistryInfo>
class Registry
    : public autoregister::FactoriesApplyCollection<
          autoregister::RegistryInfoWithDefault<RegistryInfo>,
          typename function_traits<
              typename autoregister::RegistryInfoWithDefault<
                  RegistryInfo>::register_function>::arguments_type,
          RegistryInfo::template Register,
          typename function_traits<
              typename RegistryInfo::apply_function>::arguments_type> {
  public:
    using typename autoregister::FactoriesApplyCollection<
        autoregister::RegistryInfoWithDefault<RegistryInfo>,
        typename function_traits<typename autoregister::RegistryInfoWithDefault<
            RegistryInfo>::register_function>::arguments_type,
        RegistryInfo::template Register,
        typename function_traits<typename RegistryInfo::apply_function>::
            arguments_type>::registry_type;
};

template <class RegistryInfo, class Self>
class AutoRegisterWithTypeid : public AutoRegister<RegistryInfo, Self> {
  private:
    /* On g++ (<= 11.2 at least), typeid does not work on class not
     * yet fully defined when initializing directly a static field.
     * It works with clang++ however
     *
     * So, instead of `marto_autoregister_ctx(AR, &typeid(Self))`, we
     * go through a function helper.
     */
    marto_autoregister_ctx(AutoRegisterWithTypeid,
                           &AutoRegisterWithTypeid::autoregistration_typeid());

    static const std::type_info &autoregistration_typeid() {
        return typeid(Self);
    }

    // forcing autoregister_ generation (and initialisation...)
    template <auto> struct value_tag {};
    value_tag<&autoregister_> unused() = delete;
};

} // namespace marto
#endif

#endif
