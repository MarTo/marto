/* -*-c++-*- C++ mode for emacs */
/* Forward class definition */
#ifndef MARTO_FORWARD_DECL_H
#define MARTO_FORWARD_DECL_H

#ifdef __cplusplus

/* No need of external forward-declaration for this file
 *
 * Forward-declaration: none
 *
 * This file is generated from the main Makefile with the
 * update-forward-decl-header target
 *
 * DO NOT MODIFY IT WITHOUT GOOD REASONS: your change will probably be
 * overwritten
 */

namespace marto {

// Start of auto-generated part. DO NOT MODIFY.

/* callable-function.h */
#ifndef SWIG
class CallableFunctionBase;
template <class Self> class CallableFunction;
#endif

/* debug.h */
class DN;

/* event.h */
class EventState;
class Event;
class EventType;

/* except.h */
class HistoryOutOfBound;
class HistoryIncompleteObject;
class UnknownName;
class ExistingName;
class TypeError;
class DLOpenError;
class NotImplementedException;
class CompilationException;
class SimulationException;
class CompilationExceptions;

/* history.h */
class HistoryChunk;
class EventGenerationData;
class HistoryIterator;
class History;

/* history-stream.h */
class IsWritable;
class IsReadable;
class IsStreamable;
class HistoryStreamBase;
class HistoryIStream;
class HistoryOStream;

/* macros.h */

/* parameters.h */
#ifndef SWIG
class KindParameterValues;
class EffectiveParametersBase;
template <template <class, class> class ParametersClass>
class EffectiveParametersHelper;
template <template <class, class> class ParametersClass>
class EffectiveParameters;
class KindRequiredParameter;
class RequiredParameters;
class RequiredUntypedParameter;
class UsingParametersCFHelper;
template <template <class, class> class ParametersClass>
class UsingParametersWithCF;
template <template <class, class> class ParametersClass, class CF>
class UsingParameters;
template <class Kind, template <class, class> class ParametersClass,
          class ParameterType, template <class> class MartoTypeKind>
class Parameter;
#endif

/* parameters-timing.h */
class EvaluationTiming;

/* parameters-types.h */
class ParametersTypeInfo;
class MartoTypeBase;
template <typename Type> class MartoTypeStandard;

/* parameters-values.h */
class ParameterValuesContainer;
class ParameterValues;
template <class MartoType> class TypedParameterValues;
template <class MartoType> class FixedLenParameterValues;

/* queue.h */
class Queue;
class StateLessQueue;
class QueueState;
template <class Q> class TypedQueueState;
class StandardQueue;
class OutsideQueue;

/* randomDeterministic.h */
class RandomDeterministicStreamGenerator;
class RandomDeterministic;

/* random.h */
class RandomHistory;
class RandomStream;
class RandomHistoryStream;
class RandomStreamGenerator;
class RandomHistoryStreamGenerator;
class RandomFabric;
class Random;

/* randomLecuyer.h */
class RandomLecuyerStreamGenerator;
class RandomLecuyer;

/* simulation.h */
class Simulation;

/* state.h */
class SetImpl;
class Point;
class HyperRectangle;
class Union;
class Set;

/* symbolic-parameters.h */
class FunctionRegistrationInfo;
class HasEvaluationTiming;
class SymbolicNamedParameters;
class SymbolicParameter;
class SymbolicRealParameter;
class SymbolicNamedParameter;
class SymbolicNameLookup;
class SymbolicBindingsInfo;
class SymbolicBindings;
class SymbolicFunctionCall;
class SymbolicTransition;
class SymbolicCallableFunctionBase;
template <class Self> class SymbolicCallableFunction;

/* symbolic-parameters-library.h */
class SymbolicVariable;
template <typename value_t> class SymbolicConstantParameter;
template <typename value_t> class SymbolicIncrementalParameter;

/* transition.h */
template <class Kind, class Parameters> //
class NoParameters;
class Transition;
#ifndef SWIG
template <class Self, template <class, class> class Parameters>
class TransitionParameters;
#endif
class MonotonicTransition;
namespace transition {
class ParametersBase;
} // namespace transition
namespace transition {} // namespace transition

/* types.h */

// End of auto-generated part. DO NOT MODIFY.

} // namespace marto
#include <cassert>
namespace marto {
/** Partial class to inherit that offerts a link to a Simulation object
 */
class WithSimulation {
  private:
    Simulation *_config;

  public:
    WithSimulation(Simulation *c) : _config(c) { assert(c != nullptr); }
    Simulation *config() const { return _config; }
};

} // namespace marto
#endif // __cplusplus
#endif
