/* -*-c++-*- C++ mode for emacs */
/* Symbolic parameters that can be instanciated */
#ifndef MARTO_SYMBOLIC_PARAMETERS_LIBRARY_H
#define MARTO_SYMBOLIC_PARAMETERS_LIBRARY_H

//#define MARTO_HEADER_NEED_SYMBOLIC_PARAMETERS_LIBRARY_IMPL
//#ifndef MARTO_HEADER_NEED_IMPL
//#define MARTO_HEADER_NEED_IMPL
//#define MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS_LIBRARY
//#endif

#ifdef __cplusplus

#include <marto/parameters-values.h>
#include <marto/symbolic-parameters.h>

namespace marto {

class SymbolicVariable
#ifndef SWIG
    : public enable_make_shared<SymbolicVariable,
                                inherit_ctor<SymbolicNameLookup>,
                                SymbolicParameter::WithoutData>
#endif
{
  public:
    SymbolicVariable(std::string n) : make_shared_class(std::move(n)) {}
};

template <typename value_t>
class SymbolicConstantParameter :
#ifdef SWIG
    public SymbolicRealParameter
#else
    public enable_make_shared<
        SymbolicConstantParameter<value_t>,
        as_public<SymbolicRealParameter, SymbolicParameter::WithData>>
#endif
{
  protected:
    typedef MartoTypeStandard<value_t> MartoType;

  private:
    // typedef typename MartoType::value_t value_t;
    typedef typename MartoType::typed_values_pointer typed_values_pointer;
    typedef typename MartoType::symbolic_parameter_pointer
        symbolic_parameter_pointer;

  protected:
    SymbolicConstantParameter(std::vector<value_t> v)
        : values_(
              new_shared<FixedLenParameterValues<MartoType>>(std::move(v))) {
#ifdef MARTO_DEBUG
        std::cerr << "Creating " << type_name(this) << " at " << this
                  << "\n  FixedLenParameterValues (" << values_.get()
                  << ") use_count=" << values_.use_count() << std::endl;
#endif
    }
#ifndef SWIG
    SymbolicConstantParameter(value_t v1)
        : SymbolicConstantParameter(std::vector<value_t>{v1}) {}
    SymbolicConstantParameter(value_t v1, value_t v2)
        : SymbolicConstantParameter(std::vector<value_t>{v1, v2}) {}
#endif
  public:
    virtual ~SymbolicConstantParameter() {
#ifdef MARTO_DEBUG
        std::cerr << "Destroying " << type_name(this) << " at " << this
                  << "\n  FixedLenParameterValues (" << values_.get()
                  << ") use_count=" << values_.use_count() << std::endl;
#endif
    }

  protected:
    virtual operator std::string() const override {
        return (std::string)(*values_);
    }

    virtual void do_compile(const shared_ptr<SymbolicNamedParameters>
                                &__marto_unused(parameters)) override {
        SymbolicRealParameter::do_compile(parameters);
        SymbolicParameter::WithData::update_evaluation_timing(
            EvaluationTiming::constant);
        // std::cerr << "**** [" << this <<  "] timing set in " <<
        // (std::string)(*this) << std::endl;
        // TODO compile default arguments
    }

    virtual marto::shared_ptr<ParameterValues> provide_parameter_values(
        const ParametersTypeInfo &__marto_unused(marto_type)) const override {
        return std::static_pointer_cast<ParameterValues>(values_);
    }
#if 0
    virtual typed_values_pointer allocate(symbolic_parameter_pointer) const {
        return marto::static_pointer_cast<
            typename typed_values_pointer::element_type>(values_);
    }
#endif

  protected:
    marto::shared_ptr<FixedLenParameterValues<MartoType>> values_;
};

/** class similar to  SymbolicConstantParameter but increasing its data at each
 * new computation
 *
 */
template <typename value_t>
class SymbolicIncrementalParameter : public SymbolicConstantParameter<value_t> {
  protected:
    typedef typename SymbolicConstantParameter<value_t>::MartoType MartoType;
#ifndef SWIG
    using SymbolicConstantParameter<value_t>::values_;
#endif

  private:
    typedef typename MartoType::typed_values_pointer typed_values_pointer;
    typedef typename MartoType::symbolic_parameter_pointer
        symbolic_parameter_pointer;
    int generation{0};

  public:
    SymbolicIncrementalParameter(std::vector<value_t> v)
        : SymbolicConstantParameter<value_t>(v) {}
    SymbolicIncrementalParameter(value_t v1)
        : SymbolicConstantParameter<value_t>(v1) {}
    SymbolicIncrementalParameter(value_t v1, value_t v2)
        : SymbolicConstantParameter<value_t>(v1, v2) {}
    // virtual ~SymbolicConstantParameter() {
    //    std::cerr << "Deleting  " << type_name(this) << " at " << this <<
    //    std::endl;
    //}

  protected:
    virtual operator std::string() const override {
        std::string str{"[(+"};
        str += std::to_string(generation) + "): ";
        std::string sep;
        for (const auto &v : values_) {
            str += sep + std::to_string(v);
            sep = ", ";
        }
        return str + "]";
    }

    virtual void do_compile(const shared_ptr<SymbolicNamedParameters>
                                &__marto_unused(parameters)) override {
        SymbolicRealParameter::do_compile(parameters);
        SymbolicParameter::WithData::update_evaluation_timing(
            EvaluationTiming::event);
        // std::cerr << "**** [" << this <<  "] timing set in " <<
        // (std::string)(*this) << std::endl;
        // TODO compile default arguments
    }

    virtual marto::shared_ptr<ParameterValues> provide_parameter_values(
        const ParametersTypeInfo &__marto_unused(marto_type)) const override {

        std::vector<value_t> real_values(values_);
        for (auto &v : real_values) {
            v += generation;
        }
        std::cerr << "Creating FixedLenParameterValues for "
                     "SymbolicIncrementalParameter"
                  << std::endl;
        auto param_values = std::static_pointer_cast<SymbolicParameter>(
            new_shared<FixedLenParameterValues<MartoType>>(
                std::move(real_values)));

        return param_values;
    }

#if 0
    virtual typed_values_pointer allocate(symbolic_parameter_pointer) const {
        std::vector<value_t> real_values(values_);
        for (auto &v : real_values) {
            v += generation;
        }
        typed_values_pointer param_values =
            RefCounted<SymbolicParameter>::new_shared<
                FixedLenParameterValues<MartoType>,
                TypedParameterValues<MartoType>>(std::move(real_values));

        return param_values;
    }
#endif
};

} // namespace marto

#endif // __cplusplus

//#ifdef MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS_LIBRARY
//#undef MARTO_HEADER_NEED_IMPL_FROM_SYMBOLIC_PARAMETERS_LIBRARY
//#include <marto/headers-impl.h>
//#endif

#endif
