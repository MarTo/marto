/* -*-c++-*- C++ mode for emacs */
/* Queues management implementation */
#ifndef MARTO_QUEUE_IMPL_H
#define MARTO_QUEUE_IMPL_H

#ifdef __cplusplus

#include <marto/queue.h>

#include <marto/macros.h>
#include <marto/simulation.h>

#include <marto/headers-impl.h>

namespace marto {} // namespace marto

#endif // __cplusplus
#endif
