/* -*-c++-*- C++ mode for emacs */
/* Global variables (configuration) */
#ifndef MARTO_SIMULATION_H
#define MARTO_SIMULATION_H

#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_SIMULATION
#endif

#ifdef __cplusplus

#include <cassert>
#include <functional>
#include <ltdl.h>
//#include <map>
#include <marto/collections.h>
//#include <marto/event.h>
#include <marto/forward-decl.h>
//#include <marto/queue.h>
#include <marto/random.h>
//#include <marto/transition.h>
#include <string>
#include <unistd.h>
//#include <vector>

namespace marto {

class Simulation
#ifndef SWIG
    : public is_collection<Simulation, collect::add_shared_from_this,
                           has_map_of<Transition>, has_map_of<EventType>,
                           has_map_of<Queue>>
#endif
{
    /** \brief forbid copy of this kind of objects */
    Simulation(const Simulation &) = delete;
    /** \brief forbid assignment of this kind of objects */
    Simulation &operator=(const Simulation &) = delete;

  private:
    // friend class Transition;
    // collections::MartoMap<Simulation, Transition> transitions;
    // friend class EventType;
    // friend class HistoryIterator;
    // collections::MartoMap<Simulation, EventType> eventTypes;
    // friend class Queue;
    // collections::MartoMap<Simulation, Queue> queues;
    friend class Point; //< Point constructor iterates over 'queues'.
                        // TODO: to encapsulate ?
    RandomFabric::pointer randomFabric;

    /** \brief sum of rates of all registered eventTypes
     */
    double ratesSum;

  protected:
    /** \brief constructor with an explicit RandomFabric */
    Simulation(RandomFabric::pointer r);
    /** \brief constructor with our RandomFabric (based on Lecuyer) */
    Simulation();

  public:
    ~Simulation();
    /** \brief retrieve the eventType by its number
     */
    shared_ptr<EventType> getEventType(unsigned num);
    /** \brief give an eventType randomly chosen
     *
     * rate of eventType are taken into account
     */

    shared_ptr<EventType> getRandomEventType(Random::pointer g);
    /** \brief retrieve the transition by its name
     *
     * \return the transition
     * generate an UnknownTransition exception if the transition is not
     * registered
     */
    shared_ptr<Transition> getTransition(const std::string &name);
    /** \brief retrieve the queue by its name
     *
     * \return the queue
     * generate an UnknownQueue exception if the queue name is not
     * registered
     */
    shared_ptr<Queue> getQueue(const std::string &name);

    /** \brief retrieve all names of registered Transitions, EventTypes and
     * Queues
     *
     * \return a vector of all names
     */
    const std::vector<std::string> getTransitionNames();
    const std::vector<std::string> getEventNames();
    const std::vector<std::string> getQueueNames();
    /** \brief retrieve the number of queue, use to make automatic naming of a
     * queue object
     *
     *  \return an int represent the number of queue
     */
    int getQueueSize() { return map_of<Queue>::size(); }
    /** \brief retrieve the eventType by its name
     */
    shared_ptr<EventType> getEventType(const std::string &name) {
        return map_of<EventType>::operator[](name);
    }

  private:
    /** \brief generate a new Random object to be associated with a chunk
     *
     *  Only one thread can use it at any time
     */
    Random::pointer newRandom();
    friend class History;

  public:
    /** \brief to be used only for test. No direct access should be required */
    Random::pointer newDebugRandom() { return newRandom(); }

  public:
    typedef std::function<void(lt_dlhandle handle)> loadLibraryCallback_t;
    /** \brief load a user defined library of transitions
     *
     * libname must be given without the specific platform extension
     * (.so, .dyndl, ...)
     *
     * callback will be executed with the handle of the loaded library in
     * parameter
     */
    void loadTransitionLibrary(const std::string &libname,
                               loadLibraryCallback_t callback);
    typedef void transitionInitCallback_t(Simulation *);
    /** \brief load a user defined library of transitions
     *
     * libname must be given without the specific platform extension
     * (.so, .dyndl, ...)
     *
     * The symbol initCallback must be defined in the library, and of type
     * transitionInitCallback_t
     */
    void loadTransitionLibrary(const std::string &libname,
                               const std::string &initCallback);
    void loadTransitionLibrary(const std::string &libname) {
        loadTransitionLibrary(libname, "initTransitionLibrary");
    };
    /** \brief load the default transitions library
     */
    void loadTransitionLibrary() { loadTransitionLibrary("stdmarto"); };

  private:
    static void cleanupLoadTransition();
    static std::vector<lt_dlhandle> dlhandles;

  public:
    marto::shared_ptr<SymbolicNamedParameters> global_parameters() {
        return global_parameters_;
    }

  private:
    marto::shared_ptr<SymbolicNamedParameters> global_parameters_;
};

} // namespace marto
#endif

#ifdef MARTO_HEADER_NEED_IMPL_FROM_SIMULATION
#undef MARTO_HEADER_NEED_IMPL_FROM_SIMULATION
#include <marto/headers-impl.h>
#endif

#endif
