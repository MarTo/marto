/* -*-c++-*- C++ mode for emacs */
/* Event Generator template implementation */

#include <marto/event.h>

#ifndef MARTO_EVENT_IMPL_H
#define MARTO_EVENT_IMPL_H

#ifdef __cplusplus

#include <marto/macros.h>
#include <marto/parameters-values.h>
#include <marto/transition.h>

#include <marto/headers-impl.h>

namespace marto {

template <class Transition, template <class, class> class Params>
inline marto::shared_ptr<EffectiveParameters<Params>>
EventState::effectiveTransitionParameters(
    const TransitionParameters<Transition, Params> *transition) const {
    return transition->effectiveParameters(nullptr, type()->transition());
}

inline bool EventState::valid() const { return status_ == EVENT_STATUS_FILLED; }

inline void EventState::clear() {
    if (status_ == EVENT_STATUS_FILLED) {
#if 0
        // TODO: clean computation of some parameters?
        size_t fpsize = this->type()->formalParameters.size();
        size_t i = 0;
        for (auto p : parameters) {
            if (i == fpsize) {
                break;
            }
            p->reset();
        }
#endif
    }
    status_ = EVENT_STATUS_READY;
}

inline int EventState::apply(Set *s) const {
    return (*type()->transition())->apply(s, this);
}

inline SetImpl *EventState::apply(SetImpl *s) const {
    return (*type()->transition())->apply(s, this);
}

inline Point *EventState::apply(Point *p) const {
    return (*type()->transition())->apply(p, this);
}

template <class MartoType>
typename std::enable_if<std::is_base_of_v<MartoTypeBase, MartoType>,
                        typename MartoType::typed_values_pointer>::type
EventState::getParameterValues(const std::string &name) const {
    auto sb = type_->const_parameters()->get(name);
    return parameters_->template get<MartoType>(sb);
}

inline const std::string &EventType::transitionName() const {
    return (*symbolicTransition_)->name();
}

} // namespace marto

#endif // __cplusplus
#endif
