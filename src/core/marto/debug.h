/* -*-c++-*- C++ mode for emacs */
/* Various stuff to help to debug */
#ifndef MARTO_DEBUG_H
#define MARTO_DEBUG_H

#ifdef __cplusplus

#include <cstdlib>
#include <cxxabi.h>
#include <string>

#include <iostream>
#include <sstream>

#include <marto/macros.h>

namespace marto {

inline std::string type_name(const std::type_info &tid) {
    int status;
    std::string tname = tid.name();
    char *demangled_name =
        abi::__cxa_demangle(tname.c_str(), NULL, NULL, &status);
    if (status == 0) {
        tname = demangled_name;
        std::free(demangled_name);
    }
    return tname;
}

template <typename T> std::string type_name() { return type_name(typeid(T)); }

template <typename T> std::string type_name(T *v) {
    std::stringstream ss;
    ss << type_name(typeid(*v)) << "@" << std::hex << v;
    const std::string s = ss.str();
    return s;
    // return type_name(typeid(*v)) + "@" + std::string(v);
}

template <typename T> struct trace {
    trace() { std::cerr << marto::type_name<T>() << " @" << this << std::endl; }
    ~trace() {
        std::cerr << "~" << marto::type_name<T>() << " @" << this << std::endl;
    }
};

template <class T> // no-forward-decl
T *loginit(T *object) {
    // T* object = new T(std::move(args)...);
#ifdef MARTO_DEBUG
    // object->rec_tid(typeid(*object));
    // object->print_info(true);
#endif
    return object;
}

class DN {
  protected:
    // No direct instanciation. Only derived classes.
    DN() { rec_tid(typeid(*this)); };

  public:
    void rec_tid(const std::type_info &type_id) { tid = &type_id; }
    virtual void print_info(bool start = true) {
        std::cerr << (start ? "=> Creating" : "<= Destroying") << " "
                  << type_name(*tid) << " at " << this;
        more_info(false);
        std::cerr << std::endl;
    }

  private:
    const std::type_info *tid;

  protected:
    virtual ~DN() { print_info(false); }
    virtual void more_info(bool __marto_unused(start) = true) {}
};

} // namespace marto

#endif // __cplusplus
#endif
