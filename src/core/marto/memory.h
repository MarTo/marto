/* -*-c++-*- C++ mode for emacs */
/* Marto generic collections */
#ifndef MARTO_MEMORY_H
#define MARTO_MEMORY_H

/* No need of external forward-declaration for this file
 *
 * Forward-declaration: none
 */

#ifdef __cplusplus

#include <cassert>
#include <cstddef>
#include <functional>
#include <marto/macros.h>
#include <marto/utils.h>
#include <memory>
#include <utility>

//#define MARTO_DEBUG
//#define MARTO_DEBUG_SHARED_PTR

//#include <marto/debug.h>

#ifdef MARTO_DEBUG
#include <marto/debug.h>
#define LOG_ENTER                                                              \
    std::cerr << "=> " << __FUNCTION__ << " in " << type_name(this)            \
              << " (this=" << this << ")\n  " << __PRETTY_FUNCTION__           \
              << std::endl
#define LOG_ENTER_STATIC                                                       \
    std::cerr << "=> " << __FUNCTION__ << " in static method\n  "              \
              << __PRETTY_FUNCTION__ << std::endl
#define RETURN(_ret)                                                           \
    {                                                                          \
        auto valret = (_ret);                                                  \
        std::cerr << " <= .get=" << valret.get()                               \
                  << ", use_count=" << valret.use_count() << " as "            \
                  << type_name(&valret) << "\n   from " << __FUNCTION__        \
                  << "    in " << type_name(this) << " (this=" << this         \
                  << ")\n  " << __PRETTY_FUNCTION__ << std::endl;              \
        return valret;                                                         \
    }
#else
template <class T> struct trace {};
#define RETURN(_ret) return _ret
#endif

namespace marto {

#ifdef SWIG
using std::shared_ptr;
#else // SWIG

using std::weak_ptr;
#ifndef MARTO_DEBUG_SHARED_PTR
using std::shared_ptr;
#else
/********************************************************************/
/* Used to trace marto::shared_ptr used */
template <typename T> struct shared_ptr;

#ifndef MARTO_DEBUG
template <typename T> struct trace {};
#endif

template <typename T> struct trace<shared_ptr<T>> {
    trace() {
        std::cerr << marto::type_name<shared_ptr<T>>() << " @" << this
                  << " for " << static_cast<shared_ptr<T> *>(this)->get()
                  << " [this=" << this << ", use_count="
                  << static_cast<shared_ptr<T> *>(this)->use_count() << "]"
                  << std::endl;
    }
    ~trace() {
        std::cerr << "~" << marto::type_name<shared_ptr<T>>() << " @" << this
                  << " for " << static_cast<shared_ptr<T> *>(this)->get()
                  << " [this=" << this << ", use_count="
                  << static_cast<shared_ptr<T> *>(this)->use_count() << "]"
                  << std::endl;
    }
};

template <typename T>
struct shared_ptr : std::shared_ptr<T>, trace<shared_ptr<T>> {
    using std::shared_ptr<T>::shared_ptr;
    shared_ptr(std::shared_ptr<T> ptr) : std::shared_ptr<T>(ptr) {
        std::cerr << "Converting" << std::endl;
    }
};
/********************************************************************/
#endif

//////////////////////////////////////////////////////////////////////
/// forward declarations
//////////////////////////////////////////////////////////////////////

template <typename T> struct enable_make_shared_extended_base;

template <typename T> struct extend_make_shared__base;

template <typename Extend, typename Extended, typename Collected>
struct extend_make_shared_collected;

template <typename Extend, typename Extended, typename Inherit,
          typename BasesTuple>
struct extend_make_shared_;

template <typename T, typename Collected>
struct enable_shared_from_this_collected;

template <typename T, typename Inherit, typename InheritSharedFromThisClass,
          typename SharedFromThisClassVirtualBaseTuple>
struct enable_shared_from_this_;

template <typename T, typename B = void> struct virtual_enable_shared_from_this;

///////////////////////////////////////////////////////////////////////

namespace collect {

namespace collector {

template <phase p, class U, class Options> struct sft;

} // namespace collector

using sft_collectors =
    collectors_list_merge_t<basic_collectors, collectors_list<collector::sft>>;

template <typename... Bases>
using sft = do_collect_classes<sft_collectors, Bases...>;

} // namespace collect

template <typename T, typename... Bases>
using enable_shared_from_this = typename enable_shared_from_this_collected<
    T, collect::sft<collect::with_self<
           T, collect::visibility_options<
                  marto::collect::collect_unknown_as_public, Bases...>>>>::type;

//////////////////////////////////////////////////////////////////////
/// traits
//////////////////////////////////////////////////////////////////////

namespace bits {

template <typename T, typename... Args>
using has_make_shared = std::conditional_t<
    std::is_base_of_v<enable_make_shared_extended_base<void>, T>,
    std::true_type, std::false_type>;

/* check for a `build_make_shared` method to build a particular type */
template <typename T>
using has_make_shared_extension_for =
    std::is_base_of<extend_make_shared__base<T>, T>;

/* check for any `build_make_shared` methods */
template <typename T>
using has_build_make_shared =
    std::is_base_of<extend_make_shared__base<void>, T>;

/* check for a `build_make_shared` method that create a `U` object */
template <typename T, typename U>
using has_build_make_shared_for =
    std::is_base_of<extend_make_shared__base<U>, T>;

/* check for an `shared_from_this method` */
template <typename T> struct has_shared_from_this_method {
  private:
    struct dummy { /* something */
    };
    template <typename C, typename P>
    static auto test_method(P *p)
        -> decltype(std::declval<C>().shared_from_this(), std::true_type());
    template <typename, typename> static std::false_type test_method(...);
    // does not work, we get a double reference :-(
    // with empty tuple, we get 'std::tuple<> &&'
    // -> decltype(std::declval<C>().shared_from_this(),
    //             std::declval<
    //             //std::tuple<>
    //             typename C::shared_from_this_virtual_base_tuple
    //             >());
    template <typename C, typename P>
    static auto test_tuple(P *p) ->
        typename C::shared_from_this_virtual_base_tuple;
    template <typename, typename> static std::false_type test_tuple(...);

    typedef decltype(test_method<T, dummy>(nullptr)) type_method;
    static constexpr bool value_method =
        !std::is_same_v<std::false_type, type_method>;

    typedef decltype(test_tuple<T, dummy>(nullptr)) type_tuple;

  public:
    static constexpr bool value = !std::is_same_v<std::false_type, type_tuple>;
    using type = std::conditional_t<value, type_tuple, std::tuple<>>;

  private:
    typename static_assert_bool_eq<value_method, value>::type check;
};

// /* check for an `shared_from_this method` */
// template <typename T> struct has_shared_from_this {
//   private:
//     struct dummy { /* something */
//     };
//     template <typename C, typename P>
//     static auto test(P *p)
//         -> decltype(std::declval<C>().shared_from_this(),
//         std::declval<typename C::shared_from_this_virtual_base_tuple>());
//     template <typename, typename> static std::false_type test(...);

//   public:
//     typedef decltype(test<T, dummy>(nullptr)) type;
//     static constexpr bool value = std::is_same<std::true_type, type>::value;
// };

/* check for an `shared_from_this method` */
template <typename T> struct has_shared_from_this_tuple;

template <> struct has_shared_from_this_tuple<std::tuple<>> {
  public:
    // type with shared_from_this info (or false_type)
    typedef std::false_type type;
    // true iff a shared_from_this info has been found
    static constexpr bool value = false;
    // tuple with the base virtual type if any
    using virtual_base_tuple = std::tuple<>;
};

template <typename T> struct has_shared_from_this_tuple<std::tuple<T>> {
  private:
    using Status = has_shared_from_this_method<T>;

  public:
    static constexpr bool value = Status::value;
    using type = typename std::conditional_t<value, T, std::false_type>;
    using virtual_base_tuple =
        typename std::conditional_t<value, typename Status::type, std::tuple<>>;
};

template <bool T_has_sft, bool U_has_sft, bool has_common_virt, class T,
          class U>
struct check_sft {
    // true unless no_shared_from_this has been found
    static constexpr bool value = T_has_sft || U_has_sft;
};

template <class T, class U> struct check_sft<true, true, false, T, U> {
    static_assert(always_false<check_sft>,
                  "Several classes provide has_shared_from_this without common "
                  "virtual inheritance");
};

template <typename T, typename... Others>
struct has_shared_from_this_tuple<std::tuple<T, Others...>> {
  private:
    using IT = has_shared_from_this_tuple<std::tuple<T>>;
    using IO = has_shared_from_this_tuple<std::tuple<Others...>>;

  public:
    using virtual_base_tuple =
        std::conditional_t<IT::value && IO::value,
                           tuple_intersect_t<typename IT::virtual_base_tuple,
                                             typename IO::virtual_base_tuple>,
                           tuple_cat_t<typename IT::virtual_base_tuple,
                                       typename IO::virtual_base_tuple>>;

  private:
    using Check =
        check_sft<IT::value, IO::value, !is_empty_tuple_v<virtual_base_tuple>,
                  typename IT::type, typename IO::type>;

  public:
    static constexpr bool value = Check::value;
    using type =
        std::conditional_t<IT::value, typename IT::type, typename IO::type>;
};

template <typename... Ts>
using has_shared_from_this = has_shared_from_this_tuple<std::tuple<Ts...>>;

} // namespace bits

template <class T> auto smartptr_const_cast(const T &smart_ptr) {
    return std::const_pointer_cast<std::add_const_t<typename T::element_type>>(
        smart_ptr);
}

///////////////////////////////////////////////////////////////////////

namespace collect {

namespace collector {

template <phase p, class U, class Options> struct sft;

}

// do not force the addition of an enable_shared_from_this class if none present
struct no_shared_from_this;
// add an enable_shared_from_this class
struct add_shared_from_this;
// add a virtual_enable_shared_from_this class
struct add_virtual_shared_from_this;

namespace bits::sft {

template <class Options, bool want = true> struct options : Options {
    // true unless no_shared_from_this has been found
    static constexpr bool sft_want = want;
};

template <class Result, bool want = true> struct result : Result {
    // true unless no_shared_from_this has been found
    static constexpr bool sft_want = want;
};

} // namespace bits::sft

namespace collector {

template <class Options> struct sft<phase::init_options, void, Options> {
  public:
    using options = bits::sft::options<Options>;
};

template <class Options, class Result>
struct sft<phase::init_result, Options, Result> {
  public:
    using options = bits::sft::result<Result>;
};

template <class Options>
struct sft<phase::explore, no_shared_from_this, Options> {
    using options = std::conditional_t<
        // already cleaned?
        !Options::sft_want,
        // yes => do nothing
        Options,
        // no => update sft_want
        bits::sft::options<Options, false>>;
    using type = std::tuple<std::tuple<no_shared_from_this, options>>;
    static constexpr bool collected = true;
};

template <class Options>
struct sft<phase::explore, add_shared_from_this, Options> {
    using options = Options;
    using type = std::tuple<
        enable_shared_from_this<tuple_first_t<typename Options::self_tuple_t>>>;
};

template <class Options>
struct sft<phase::explore, add_virtual_shared_from_this, Options> {
    using options = Options;
    using type = std::tuple<virtual_enable_shared_from_this<
        tuple_first_t<typename Options::self_tuple_t>>>;
};

template <class Options, class Result>
struct sft<phase::classify, std::tuple<no_shared_from_this, Options>, Result> {
    using options = bits::sft::result<Result, Options::sft_want>;
};

template <class Result, class CleanResult>
struct sft<phase::cleanup, Result, CleanResult> {
    using options = bits::sft::result<CleanResult, Result::sft_want>;
};

} // namespace collector

} // namespace collect

using collect::no_shared_from_this;

//////////////////////////////////////////////////////////////////////
/// make_shared<T>(...)
//////////////////////////////////////////////////////////////////////

namespace bits {

template <typename T> struct enable_make_shared_factory {
    template <typename... Args> static auto new_shared(Args &&...args) {
        if constexpr (bits::has_make_shared<
                          T, Args...>::value) { //, Args...>::value) {
            if constexpr (std::is_same_v<shared_ptr<T>,
                                         decltype(T::make_shared(
                                             std::forward<Args>(args)...))>) {
                return T::make_shared(std::forward<Args>(args)...);
            } else {
                static_assert(always_false<T>,
                              "enable_make_shared for a derived class");
            }
        } else {
            static_assert(always_false<T>, "no enable_make_shared");
        }
    }
};

} // namespace bits

template <typename T, typename... Args> auto new_shared(Args &&...args) {
    return bits::enable_make_shared_factory<T>::template new_shared<Args...>(
        std::forward<Args>(args)...);
}

//////////////////////////////////////////////////////////////////////
/// enable_make_shared
//////////////////////////////////////////////////////////////////////

enum class BuildSharedPhase {
    RecurseForSelf,
    SelfExtension,
    RecurseForInherited,
    Back,
};

static inline constexpr BuildSharedPhase nextPhase(const BuildSharedPhase p) {
    assert(p < BuildSharedPhase::Back);
    return static_cast<BuildSharedPhase>(
        static_cast<std::underlying_type_t<BuildSharedPhase>>(p) + 1);
}

template <typename T, BuildSharedPhase p, int val, typename S>
struct StackFrame {
    using type = T;
    static constexpr BuildSharedPhase phase = p;
    static constexpr int value = val;
    using Stack = S;
    static constexpr bool bottom = false;
};

template <typename BuildObject> struct StackFrameCreate {
    using target = BuildObject;
    static constexpr bool bottom = true;
};

/* Used for trait */
template <typename T> struct enable_make_shared_extended_base;

template <> struct enable_make_shared_extended_base<void> {};

template <typename T>
struct enable_make_shared_extended_base
    : enable_make_shared_extended_base<void> {
    typedef shared_ptr<T> pointer;
    typedef shared_ptr<const T> const_pointer;
};

/** Class to add a public static method `T::make_shared(...)`

    The method will call a `T` ctor and return a smart pointer.

    `T` ctors should probably be protected (to avoid to be used directly).
 */
template <typename T, typename extend_make_shared_type>
struct enable_make_shared_ : extend_make_shared_type,
                             enable_make_shared_extended_base<T> {
    /** define `T::pointer` type as a smart pointer */
    typedef shared_ptr<T> pointer;
    typedef shared_ptr<const T> const_pointer;
    typedef enable_make_shared_ make_shared_class;

    using extend_make_shared_type::extend_make_shared_type;

    // TODO: make private with correct friend?
    struct make_shared_allocate {
        /** call a `T` ctor and return a smart pointer */
        template <typename... Args>
        static pointer make_shared_base(Args &&...args) {
            // Using a "fake" derived class that constructs our shared
            // thing using a public constructor, in case the targeted
            // constructor is not public...
            struct EnableMakeShared : public T {
                EnableMakeShared(Args &&...cargs)
                    : T(std::forward<Args>(cargs)...) {
#if defined(MARTO_DEBUG) // || 1
                    std::cerr << "=> "
#if 0
                              << marto::type_name(this) << " created at "
                              << this
                              << "\n  for "
#endif
                              << marto::type_name<T>() << " ("
                              << static_cast<T *>(this) << ")" << std::endl;
#endif
                }
                ~EnableMakeShared() {
#if defined(MARTO_DEBUG) // || 1
                    std::cerr << "<= "
#if 0
                              << marto::type_name(this) << " destroyed at "
                              << this
                              << "\n  for ~"
#endif
                              << marto::type_name<T>() << " ("
                              << static_cast<T *>(this) << ")" << std::endl;
#endif
                }
            };

#ifdef MARTO_DEBUG
            LOG_ENTER_STATIC;
            std::cerr << "  " << marto::type_name<enable_make_shared_>()
                      << "::make_shared<" << marto::type_name<T>() << ">"
                      << std::endl;
#endif

            auto ptr =
                std::make_shared<EnableMakeShared>(std::forward<Args>(args)...);
#ifdef MARTO_DEBUG
            if (bits::has_shared_from_this<T>::value) {
                auto ptr2 = ptr->shared_from_this();
            }
            pointer r = ptr,
                    ret = shared_ptr<T>(ptr, static_cast<T *>(ptr.get()));
            std::cerr << "ptr is: " << type_name(&ptr) << "\n"
                      << "ret is: " << type_name(&ret) << "\n"
                      << "r   is: " << type_name(&r) << std::endl;
            std::cerr << "ptr contains: " << type_name(ptr.get()) << "\n"
                      << "ret contains: " << type_name<decltype(ret.get())>()
                      << "\n"
                      << "r   contains: " << type_name(r.get()) << std::endl;
            // return ret;
            return pointer(ptr, static_cast<T *>(ptr.get()));
#else
            return ptr;
#endif
        }
    };

  private:
    friend struct bits::enable_make_shared_factory<T>;
    /** call a `T` ctor and return a smart pointer */
    template <typename... Args> static pointer make_shared(Args &&...args) {
#ifdef MARTO_DEBUG
        LOG_ENTER_STATIC;
#endif
        return extend_make_shared_type::template build_make_shared<
            StackFrameCreate<make_shared_allocate>, Args...>(
            std::forward<Args>(args)...);
    }

  public:
    // TODO: make private with correct friend?
    struct make_shared_initialize {
        /** return a smart pointer from the object pointer */
        template <typename... Args>
        static pointer make_shared_base(typename pointer::element_type *p) {
            typedef std::tuple<Args...> input_types;
            static_assert(std::is_same_v<input_types, std::tuple<>>,
                          "Invalid arguments types");
            return p->shared_from_this();
        }
    };

    /** call `T` extensions and return the provided pointer
        using shared_from_this */
    template <typename... Args> pointer init_shared(Args &&...args) {
#ifdef MARTO_DEBUG
        LOG_ENTER_STATIC;
#endif
        typename pointer::element_type *p =
            static_cast<typename pointer::element_type *>(this);
        p->sft_no_dynamic_allocation();
        return extend_make_shared_type::template build_make_shared<
            StackFrameCreate<make_shared_initialize>, Args...,
            typename pointer::element_type *>(
            std::forward<Args>(args)...,
            // FIXME: why p does not work?
            static_cast<typename pointer::element_type *>(this)
            // p
        );
    }

    ~enable_make_shared_() {
        static_assert_is_base_of<enable_make_shared_, T>();
    }
};

template <typename T, typename Collected, typename HasSharedFromThis,
          bool implement_shared_from_this>
struct enable_make_shared_collected_choice {
    using extend_make_shared_type =
        typename extend_make_shared_collected<T, T, Collected>::type;
    using type = enable_make_shared_<T, extend_make_shared_type>;
};

template <typename T, typename Collected, typename HasSharedFromThis>
struct enable_make_shared_collected_choice<T, Collected, HasSharedFromThis,
                                           true> {
    /*
     * T : enabled_make_shared_ : enable_shared_from_this : extend_make_shared_
     * : inherit_<Collected>
     */
    using extend_make_shared_type =
        typename extend_make_shared_collected<T, T, Collected>::type;

    using enable_shared_from_this_type = enable_shared_from_this_<
        T, extend_make_shared_type, typename HasSharedFromThis::type,
        typename HasSharedFromThis::virtual_base_tuple>;

    using type = enable_make_shared_<T, enable_shared_from_this_type>;
};

template <typename T, typename Collected, bool sft_want = Collected::sft_want,
          bool has_SFT_value = bits::has_shared_from_this_tuple<
              typename Collected::all_tuple_t>::value>
struct enable_make_shared_collected {
    using has_SFT =
        bits::has_shared_from_this_tuple<typename Collected::all_tuple_t>;
    using has_SFT_other =
        bits::has_shared_from_this_tuple<typename Collected::other_tuple_t>;
    static constexpr bool implement_shared_from_this =
        has_SFT_other::value ? false /* non inherited class implement
                                      * shared_from_this, do nothing
                                      * here. It is up to the user to
                                      * correctly implement its
                                      * class. */
                             : has_SFT::value;

    static_assert(has_SFT::value == has_SFT_value,
                  "Invalid internal use of enable_make_shared_collected");

    using check =
        typename static_assert_bool_eq<has_SFT::value, sft_want, T>::type;

    using type = typename enable_make_shared_collected_choice<
        T, Collected, has_SFT, implement_shared_from_this>::type;
};

template <typename T, typename Collected>
struct enable_make_shared_collected<T, Collected, true, false>
    : enable_make_shared_collected<
          T, collect::do_append_classes<
                 collect::sft_collectors, Collected,
                 as_public<with_self<T, collect::add_shared_from_this>>>> {

    // static_assert(always_false<enable_make_shared_collected>, "Missing a
    // shared_from_this class");

    using nCollected = collect::do_append_classes<
        collect::sft_collectors, Collected,
        as_public<with_self<T, collect::add_shared_from_this>>>;
    static_assert(bits::has_shared_from_this_tuple<
                      typename nCollected::all_tuple_t>::value == true,
                  "Args");
};

template <typename T, typename... Bases>
using enable_make_shared = typename enable_make_shared_collected<
    T, collect::sft<with_self<
           T, collect::visibility_options<
                  marto::collect::collect_unknown_as_public, Bases...>>>>::type;

/**************/
/* Base class of a class with a `build_make_shared` method
 *
 * T is the type that is extended.
 *
 * for a `make_shared` class, T is `void`
 *
 * Only used for traits
 */
template <typename T> struct extend_make_shared__base;

template <> struct extend_make_shared__base<void> {};

template <typename Extend, typename Extended, typename Inherit,
          typename BasesTuple>
struct extend_make_shared__base<
    extend_make_shared_<Extend, Extended, Inherit, BasesTuple>>
    : extend_make_shared__base<Extended> {};

template <typename T>
struct extend_make_shared__base : extend_make_shared__base<void> {};

struct A {};

/**************/

/** Class to add a public static template method `build_make_shared(...)`

    - `Extend`: the class providing the extension
    - `Extended`: the class that benefit from the extension

    EMS_extended is the class we want to add a support for the
    `make_shared()` method:
    - because we want to recursively call it in dependent classes
    - and/or because we provide a `make_shared_extension` method
      in `EMS_extended` to be used
 */
template <typename Extend, typename Extended, typename Inherit,
          typename BasesTuple>
struct extend_make_shared_
    : extend_make_shared__base<
          extend_make_shared_<Extend, Extended, Inherit, BasesTuple>>,
      Inherit {

    ~extend_make_shared_() {
        static_assert_is_base_of<extend_make_shared_, Extend>();
        static_assert_is_base_of<Extend, Extended>();
        static_assert_is_base_of_tuple<BasesTuple, Extended>()();
        // TODO: static_assert(has_make_shared_for<Extended, Extended>)
        // "Bad reflexion. Class error when calling template?");
    }
    /*
     *  EMS: class to return (can be a derived class of T)
     *
     *  THIS::build_make_shared<EMS, Stack, Args...)(args...)
     *
     *      EMS_extended::make_shared_extend<EMS, <THIS, 0, Stack>,
     * Args...>(args...)
     *          => THIS::build_make_shared_rec<EMS, <THIS, 0, Stack>,
     * NArgs...>(nargs...)
     *      => THIS::build_make_shared_rec<EMS, <THIS, 0, Stack>,
     * Args...>(args...)
     *
     *  THIS::build_make_shared_rec<EMS, <TRet, Tval, Stack>, Args...)(args...)
     *      if (Base[Tval]::build_make_shared)
     *          Base[Tval]::build_make_shared<EMS, push(THIS, Tval+1, Stack),
     * Args...>(args...)
     *      elsif (Base[Tval])
     *          THIS::build_make_shared_rec<EMS,
     * TRet, Tval+1, Stack, Args...>(args...)
     *      else SRet, Sval, SStack =
     *          pop(Stack)
     *          SRet::build_make_shared_rec<EMS, <SRet, Sval+1, SStack>,
     * Args...>(args...)
     *
     */
    using typename Inherit::inherit_class;
    /* comment to avoid reordering of the two using clauses by the source
     * reformater */
    using inherit_class::inherit_class;

    using pointer = shared_ptr<Extended>;

  protected:
    using extend_make_shared_class = extend_make_shared_;
    /* method that is a no-op (just invoking the call-back) used in
     * case `Extended` class do not provide one
     */
    template <typename pointer2, typename F, typename... Args>
    static pointer make_shared_extension(F &&f, Args &&...args) {
#ifdef MARTO_DEBUG
        std::cerr << "  ||| default extension called" << std::endl;
#endif
        static_assert(std::is_same_v<pointer, pointer2>,
                      "pointer are different!");
        auto cb = std::function<pointer(Args...)>(std::forward<F>(f));
        return cb(std::forward<Args>(args)...);
    }

  public:
    /* Stack :
       the stack top element will be called at the end (keeping it on the stack)

       as a special cases, if the top element is the bottom stack,
       instead, the object will be created.
     */
    template <class Stack, typename... Args>
    static pointer build_make_shared(Args &&...args) {
#ifdef MARTO_DEBUG
        std::cerr << __FUNCTION__ //<< " for " << type_name<T>()
                  << "\n  " << __PRETTY_FUNCTION__
                  << "\n  Extended=" << type_name<Extended>()
                  << "\n  Extend=" << type_name<Extend>()
                  << "\n  Stack=" << type_name<Stack>() << std::endl;
#endif
        /* This method implements the recursive handling of classes
         * providing support for `make_shared`. It can be the fact
         * that we provide a `make_shared` method and/or we provide a
         * `make_shared_extension` method to add additionnal
         * traitments or parameters.
         *
         * We call the `make_shared_extension()` method in
         * `EMS_extended` before going to the recursion on listed
         * (should be inherited) classes.
         *
         * `EMS_extended` will inherit the provided
         * `make_shared_extension()` method that is a no-op (just
         * invoking the call-back) if none are provided.
         */
        return build_make_shared_rec<
            StackFrame<extend_make_shared_, BuildSharedPhase(0), 0, Stack>>(
            std::forward<Args>(args)...);
    }

    template <class Stack, typename... Args>
    static auto build_make_shared_rec(Args &&...args) -> pointer {
#ifdef MARTO_DEBUG
        std::cerr << __FUNCTION__ //<< " for " << type_name<T>()
                  << "\n  " << __PRETTY_FUNCTION__
                  << "\n  Extended=" << type_name<Extended>()
                  << "\n  Extend=" << type_name<Extend>() << "\n  Stack="
                  << type_name<Stack>()
                  //<< "\n  BasesTuple=" << type_name<BasesTuple>()
                  << std::endl;
#endif
        if constexpr (Stack::phase == BuildSharedPhase::Back) {
            /* all phases handled, going back */
#ifdef MARTO_DEBUG
            std::cerr << "  ||| ending recursion" << std::endl;
#endif
            /* poping stack */
            using StackE = typename Stack::Stack;
            if constexpr (!StackE::bottom) {
#ifdef MARTO_DEBUG
                std::cerr << "    with stack value = " << StackE::value
                          << std::endl;
#endif
                return (pointer)StackE::type::template build_make_shared_rec<
                    StackE, Args...>(std::forward<Args>(args)...);
            } else {
                pointer ret = StackE::target::template make_shared_base(
                    std::forward<Args>(args)...);
#ifdef MARTO_DEBUG
                std::cerr << "    with object " << ret.get()
                          << " use_count=" << ret.use_count() << std::endl;
#endif
                return ret;
            }
        } else if constexpr (Stack::phase == BuildSharedPhase::SelfExtension) {
            /* Calling `make_shared_extension` in Extend */
#ifdef MARTO_DEBUG
            std::cerr << "  ||| calling extension in " << type_name<Extend>()
                      << std::endl;
#endif
            auto variadic_lambda = [&](auto &&...params) -> pointer {
                return build_make_shared_rec<
                    StackFrame<extend_make_shared_, nextPhase(Stack::phase), 0,
                               typename Stack::Stack>>(
                    std::forward<decltype(params)>(params)...);
            };
            return Extend::template make_shared_extension<pointer>(
                variadic_lambda, std::forward<Args>(args)...);
        } else {
            /* recursing on Bases */
            // using BaseInfo = internal::select<Stack::value, Bases...>;
            using BaseInfo = internal::tuple_select<Stack::value, BasesTuple>;
            // static_assert(BaseInfo::value == BaseInfo2::value);
            // static_assert(std::is_same_v<typename BaseInfo::type,typename
            // BaseInfo2::type>);
            if constexpr (!BaseInfo::value) {
                /* all bases handled, next phase */
#ifdef MARTO_DEBUG
                std::cerr << "  ||| ending phase" << std::endl;
#endif
                return build_make_shared_rec<
                    StackFrame<extend_make_shared_, nextPhase(Stack::phase), 0,
                               typename Stack::Stack>>(
                    std::forward<Args>(args)...);
            } else {
                /* one more base, recursion required */
                using Base = typename BaseInfo::type;
                using Stack2 =
                    StackFrame<typename Stack::type, Stack::phase,
                               Stack::value + 1, typename Stack::Stack>;

                if constexpr (Stack::phase ==
                                  BuildSharedPhase::RecurseForSelf &&
                              bits::has_build_make_shared_for<
                                  Base, Extended>::value) {
#ifdef MARTO_DEBUG
                    std::cerr << "  ||| recursing for " << type_name<Base>()
                              << std::endl;
#endif
                    return Base::template build_make_shared<Stack2, Args...>(
                        std::forward<Args>(args)...);
                } else if constexpr (Stack::phase == BuildSharedPhase::
                                                         RecurseForInherited &&
                                     bits::has_make_shared<Base>::value) {

#ifdef MARTO_DEBUG
                    std::cerr << "  ||| recursing in " << type_name<Base>()
                              << std::endl;
#endif
                    auto bms =
                        Base::template build_make_shared<Stack2, Args...>(
                            std::forward<Args>(args)...);
                    if constexpr (is_virtual_base_of_v<
                                      typename decltype(bms)::element_type,
                                      typename pointer::element_type>) {
                        return std::dynamic_pointer_cast<
                            typename pointer::element_type>(bms);
                    } else {
                        auto ret = std::static_pointer_cast<
                            typename pointer::element_type>(bms);
#ifdef MARTO_DEBUG
                        std::cerr
                            << "converting " << marto::type_name(bms.get())
                            << " use_count=" << bms.use_count()
                            << "\n        to " << marto::type_name(ret.get())
                            << " use_count=" << ret.use_count() << std::endl;
#endif
                        return ret;
                    }

                } else {
#ifdef MARTO_DEBUG
                    std::cerr << "  ||| skipping recursion in "
                              << type_name<Base>() << std::endl;
#endif
                    return build_make_shared_rec<Stack2, Args...>(
                        std::forward<Args>(args)...);
                }
            }
        }
    }
};

template <typename Extend, typename Extended, typename Collected>
struct extend_make_shared_collected {
    using type =
        extend_make_shared_<Extend, Extended,
                            typename inherit_collected<Collected>::type,
                            typename Collected::all_tuple_t>;
};

template <typename Extend, typename Extended, typename... Bases>
using extend_make_shared = typename extend_make_shared_collected<
    Extend, Extended,
    collect::sft<with_self<
        Extend, collect::visibility_options<
                    marto::collect::collect_unknown_as_public, Bases...>>>>::
    type;

//////////////////////////////////////////////////////////////////////
/// enable_shared_from_this
//////////////////////////////////////////////////////////////////////
/** provides a `shared_from_this` method correctly typed
 *
 * More precisely,
 * - this class provides `pointer` and `const_pointer` smart pointer types
 * - `T` is the type of the object we want a smart pointer from `this`
 * - `shared_from_this()` exists with and without `const`
 * - `B`, if not `void` (default), will be inherited by the current class
 * - if the current class inherit from an object already having a
 *   `shared_from_this`, then `shared_from_this` is delegated through `B`
 */

template <typename T, typename Inherit,
          typename SharedFromThisClassVirtualBaseTuple>
struct enable_shared_from_this_common : public Inherit {
    typedef shared_ptr<T> pointer;
    typedef shared_ptr<const T> const_pointer;

    /* Import ctors from Inherit class */
    using Inherit::Inherit;

  private:
    /* internal use to track virtual inheritance */
    template <typename> friend struct bits::has_shared_from_this_method;
    template <typename, typename, typename, typename>
    friend struct enable_shared_from_this_;
    template <typename, typename> friend struct virtual_enable_shared_from_this;
    typedef SharedFromThisClassVirtualBaseTuple
        shared_from_this_virtual_base_tuple;

    enable_shared_from_this_common() {
        static_assert(std::is_base_of<enable_shared_from_this_common, T>::value,
                      "Bad reflexion. Class error when calling template?");
    }
};

/* General case, we inherit a `shared_from_this` method from a base class */
template <typename T, typename Inherit, typename InheritSharedFromThisClass,
          typename SharedFromThisClassVirtualBaseTuple>
struct enable_shared_from_this_
    : public enable_shared_from_this_common<
          T, Inherit, SharedFromThisClassVirtualBaseTuple> {
  private:
    using enable_shared_from_this_common_class =
        enable_shared_from_this_common<T, Inherit,
                                       SharedFromThisClassVirtualBaseTuple>;

  public:
    using typename enable_shared_from_this_common_class::const_pointer;
    using typename enable_shared_from_this_common_class::pointer;
    typedef enable_shared_from_this_ shared_from_this_class;

    /* Import ctors from Inherit class */
    using enable_shared_from_this_common_class::
        enable_shared_from_this_common_class;

    // using B = InheritSharedFromThisClass;
    static_assert(bits::has_shared_from_this<InheritSharedFromThisClass>::value,
                  "Base B must have a shared_from_this() method");

  public:
    pointer shared_from_this() {
#ifdef MARTO_DEBUG
        LOG_ENTER;
        std::cerr << "shared_from_this (using "
                  << type_name<InheritSharedFromThisClass>()
                  << "): "
                  //<< "use_count=" << B::shared_from_this().use_count()
                  << "\n   for " << type_name<T>() << "\n   from "
                  << type_name<InheritSharedFromThisClass>() << "\n   in "
                  << type_name<decltype(*this)>() << " this=" << this
                  << std::endl;
#endif
        RETURN(pointer(InheritSharedFromThisClass::shared_from_this(),
                       static_cast<T *>(this)));
    }

    const_pointer shared_from_this() const {
        return const_pointer(InheritSharedFromThisClass::shared_from_this(),
                             static_cast<const T *>(this));
    }
};

/* Base case, no inheritence. Using std::enable_shared_from_this */
template <typename T, typename Inherit,
          typename SharedFromThisClassVirtualBaseTuple>
struct enable_shared_from_this_<T, Inherit, std::false_type,
                                SharedFromThisClassVirtualBaseTuple>
    : public std::enable_shared_from_this<T>,
      public enable_shared_from_this_common<
          T, Inherit, SharedFromThisClassVirtualBaseTuple> {
    static_assert(is_empty_tuple_v<SharedFromThisClassVirtualBaseTuple>,
                  "Invalid non empty virtual base");

  private:
    using enable_shared_from_this_common_class =
        enable_shared_from_this_common<T, Inherit,
                                       SharedFromThisClassVirtualBaseTuple>;

  public:
    using typename enable_shared_from_this_common_class::const_pointer;
    using typename enable_shared_from_this_common_class::pointer;
    typedef enable_shared_from_this_ shared_from_this_class;

    /* Import ctors from Inherit class */
    using enable_shared_from_this_common_class::
        enable_shared_from_this_common_class;

  private:
    /* true if requested by user.
     *
     * All smartpointers will be allocated with no deleter.
     */
    bool no_dynamic_allocation_ = false;
    /* true if a smartpointer (with deleter) has been requested
     *
     * Usually, the first smartpointer is allocated in make_shared.
     */
    mutable bool dynamic_allocation_ = false;

  protected:
    void sft_no_dynamic_allocation() {
        assert(!dynamic_allocation_);    // smartpointer with deleter already
                                         // allocated
        assert(!no_dynamic_allocation_); // called twice. To be relaxed?
        no_dynamic_allocation_ = true;
    }

  public:
    pointer shared_from_this() {
#ifdef MARTO_DEBUG
        LOG_ENTER;
        typedef std::enable_shared_from_this<T> B;
        std::cerr << "shared_from_this (using std): "
                  //<< "use_count=" << B::shared_from_this().use_count()
                  << "\n   for " << type_name<T>() << "\n   from "
                  << type_name<B>() << "\n   in "
                  << type_name<decltype(*this)>() << " this=" << this
                  << std::endl;
#endif
        if (std::enable_shared_from_this<T>::weak_from_this().expired()) {
            /* No `shared_ptr` existing, so `this` has not been created by
             * *::make_shared. Creating a fake `shared_ptr` (no deleter
             * when `delete` is invoked).
             */
#ifdef MARTO_DEBUG
            auto weak = std::enable_shared_from_this<T>::weak_from_this();
            std::cerr << "weak: expired=" << weak.expired()
                      << ", use_count=" << weak.use_count() << "\n   for "
                      << type_name<T>() << "\n   in "
                      << type_name<enable_shared_from_this_>() << std::endl;
            assert(0);
#endif
            assert(no_dynamic_allocation_);
            RETURN(shared_ptr<T>(static_cast<T *>(this),
                                 internal::no_deleter<T>()));
        }
        if (!no_dynamic_allocation_ && !dynamic_allocation_) {
            dynamic_allocation_ = true;
        }
        RETURN(std::enable_shared_from_this<T>::shared_from_this());
    }

    const_pointer shared_from_this() const {
        if (std::enable_shared_from_this<T>::weak_from_this().expired()) {
            /* No `shared_ptr` existing, so `this` has not been created by
             * *::make_shared. Creating a fake `shared_ptr` (no `delete`
             * call).
             */
#ifdef MARTO_DEBUG
            auto weak = std::enable_shared_from_this<T>::weak_from_this();
            std::cerr << "weak: expired=" << weak.expired()
                      << ", use_count=" << weak.use_count() << std::endl;
#endif
            assert(no_dynamic_allocation_);
            return shared_ptr<const T>(static_cast<const T *>(this),
                                       internal::no_deleter<T>());
        }
        if (!no_dynamic_allocation_ && !dynamic_allocation_) {
            dynamic_allocation_ = true;
        }
        return std::enable_shared_from_this<T>::shared_from_this();
    }
};

template <typename T, typename Collected>
struct enable_shared_from_this_collected {
    using HasSharedFromThis =
        bits::has_shared_from_this_tuple<typename Collected::all_tuple_t>;
    using type = enable_shared_from_this_<
        T, typename inherit_collected<Collected>::type,
        typename HasSharedFromThis::type,
        typename HasSharedFromThis::virtual_base_tuple>;
};

//////////////////////////////////////////////////////////////////////
/// virtual inheritence for enable_shared_from_this
//////////////////////////////////////////////////////////////////////

/* class to allows one multiple inheritences of enable_shared_from_this */
template <typename B>
struct virtual_enable_shared_from_this_base
    : trace<virtual_enable_shared_from_this_base<B>>,
      marto::enable_shared_from_this<virtual_enable_shared_from_this_base<B>,
                                     B> {
  public:
    auto shared_from_this() {
#ifdef MARTO_DEBUG
        LOG_ENTER;
        typedef virtual_enable_shared_from_this_base T;
        std::cerr
            << "virtual_shared_from_this_base(for " << type_name<B>()
            << "): "
            //<< "use_count=" <<
            // enable_shared_from_this<virtual_enable_shared_from_this_base<void>>::shared_from_this().use_count()
            << "\n   for " << type_name<T>() << "\n   from " << type_name<B>()
            << "\n   in " << type_name<decltype(*this)>() << " this=" << this
            << std::endl;
#endif
        auto ret = marto::enable_shared_from_this<
            virtual_enable_shared_from_this_base<B>, B>::shared_from_this();
        // static_assert(std::is_same_v<decltype(ret),typename B::pointer>);
        RETURN(ret);
    }
    auto shared_from_this() const {
        auto ret = marto::enable_shared_from_this<
            virtual_enable_shared_from_this_base<B>, B>::shared_from_this();
        // static_assert(std::is_same_v<decltype(ret),typename
        // B::const_pointer>);
        return ret;
        // return enable_shared_from_this<
        //     virtual_enable_shared_from_this_base<void>>::shared_from_this();
    }

    virtual ~virtual_enable_shared_from_this_base() {}
};

template <>
struct virtual_enable_shared_from_this_base<void>
    : trace<virtual_enable_shared_from_this_base<void>>,
      marto::enable_shared_from_this<
          virtual_enable_shared_from_this_base<void>> {
  private:
    typedef marto::enable_shared_from_this<
        virtual_enable_shared_from_this_base<void>>
        B;

  public:
    auto shared_from_this() {
#ifdef MARTO_DEBUG
        LOG_ENTER;
        typedef virtual_enable_shared_from_this_base T;
        std::cerr
            << "virtual_shared_from_this_base(void): "
            //<< "use_count=" <<
            // enable_shared_from_this<virtual_enable_shared_from_this_base<void>>::shared_from_this().use_count()
            << "\n   for " << type_name<T>() << "\n   from " << type_name<B>()
            << "\n   in " << type_name<decltype(*this)>() << " this=" << this
            << std::endl;
#endif
        auto ret = marto::enable_shared_from_this<
            virtual_enable_shared_from_this_base<void>>::shared_from_this();
        static_assert(std::is_same_v<decltype(ret), B::pointer>);
        RETURN(ret);
    }
    auto shared_from_this() const {
        auto ret = enable_shared_from_this<
            virtual_enable_shared_from_this_base<void>>::shared_from_this();
        static_assert(std::is_same_v<decltype(ret), B::const_pointer>);
        return ret;
        // return enable_shared_from_this<
        //     virtual_enable_shared_from_this_base<void>>::shared_from_this();
    }

    virtual ~virtual_enable_shared_from_this_base() {}
};

template <typename T, typename B /* = void // in forward-declaration*/>
struct virtual_enable_shared_from_this
    : trace<virtual_enable_shared_from_this<T, B>>,
      virtual virtual_enable_shared_from_this_base<B> {
    typedef shared_ptr<T> pointer;
    typedef shared_ptr<const T> const_pointer;

  private:
    template <typename> friend struct bits::has_shared_from_this_method;
    template <typename, typename, typename, typename>
    friend struct enable_shared_from_this_;
    template <typename, typename> friend struct virtual_enable_shared_from_this;
    using shared_from_this_virtual_base_tuple = std::conditional_t<
        is_empty_tuple_v<typename virtual_enable_shared_from_this_base<
            B>::shared_from_this_virtual_base_tuple>,
        std::conditional_t<std::is_void_v<B>,
                           std::tuple<std::enable_shared_from_this<void>>,
                           std::tuple<B>>,
        typename virtual_enable_shared_from_this_base<
            B>::shared_from_this_virtual_base_tuple>;

  public:
    pointer shared_from_this() {
#ifdef MARTO_DEBUG
        LOG_ENTER;
        std::cerr
            << "virtual_shared_from_this(2): "
            //<< "use_count=" <<
            // virtual_enable_shared_from_this_base<B>::shared_from_this().use_count()
            << "\n   for " << type_name<T>() << "\n   from " << type_name<B>()
            << "\n   in " << type_name<decltype(*this)>() << " this=" << this
            << std::endl;
#endif
        RETURN(shared_ptr<T>(
            virtual_enable_shared_from_this_base<B>::shared_from_this(),
            static_cast<T *>(this)));
    }
    const_pointer shared_from_this() const {
        return shared_ptr<const T>(
            virtual_enable_shared_from_this_base<B>::shared_from_this(),
            static_cast<const T *>(this));
    }

    virtual ~virtual_enable_shared_from_this() {
        static_assert_is_base_of<virtual_enable_shared_from_this, T>();
        // static_assert(std::is_base_of<decltype(*this),T>::value, "Bad
        // reflexion. Class error when calling template?");
#ifdef MARTO_DEBUG
        std::cerr << "class name "
                  << type_name<virtual_enable_shared_from_this>() << std::endl;
        std::cerr << "*this " << type_name<decltype(*this)>() << std::endl;
#endif
    }
};

#endif // SWIG

} // namespace marto

#undef MARTO_DEBUG

#endif // __cplusplus
#endif
