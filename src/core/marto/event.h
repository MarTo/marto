/* -*-c++-*- C++ mode for emacs */
/* Event Generator */
#ifndef MARTO_EVENT_H
#define MARTO_EVENT_H

#define MARTO_HEADER_NEED_EVENT_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_EVENT
#endif

#ifdef __cplusplus

#include <list>
#include <map>
#include <marto/collections.h>
#include <marto/forward-decl.h>
#include <marto/memory.h>
#include <marto/parameters-types.h>
#include <marto/simulation.h>
#include <marto/symbolic-parameters.h>
#include <marto/types.h>
#include <stddef.h>
#include <stdint.h>
#include <string>

namespace marto {

#ifndef SWIG
std::ostream &operator<<(std::ostream &out, const EventType &ev);
#endif

/** EventState is the dynamic version of an EventType
 *
 * It contains all info (parameter values) about real a event.
 *
 */
class EventState {
    friend EventType;

  private:
    const marto::shared_ptr<const EventType> type_;
    const event_code_t code_;

  protected:
    event_code_t code() const { return code_; };

  public:
    /** Create an empty (unusable) EventState
     */
#ifndef SWIG
    EventState(marto::shared_ptr<const EventType> type);
#endif
    EventState(marto::shared_ptr<EventType> type)
        : EventState(smartptr_const_cast(type)){};
    /** Virtual destructor
     */
    virtual ~EventState(){};
    /** Creates (generate) a new Event of current type
     *
     * The rate is used before to choose the EventType.
     *
     * EventGenerationData must contain a Random generator in a good state
     */
    void generate(EventGenerationData *g);

    /** An event is valid when all its parameters are available */
    inline bool valid() const;

    /** \brief clear an event
     *
     * All dynamic parameters are emptied
     */
    inline void clear();

    marto::shared_ptr<const EventType> type() const { return type_; };

    int apply(Set *s) const;
    SetImpl *apply(SetImpl *s) const;
    Point *apply(Point *p) const;

    template <class value_t>
    typename std::enable_if<
        !std::is_base_of_v<MartoTypeBase, value_t>,
        typename MartoTypeStandard<value_t>::typed_values_pointer>::type
    getParameterValues(const std::string &name) const {
        return getParameterValues<MartoTypeStandard<value_t>>(name);
    }
#ifndef SWIG
    template <class MartoType>
    typename std::enable_if<std::is_base_of_v<MartoTypeBase, MartoType>,
                            typename MartoType::typed_values_pointer>::type
    getParameterValues(const std::string &name) const;
#endif

#ifndef SWIG
    template <class Transition, template <class, class> class Params>
    marto::shared_ptr<EffectiveParameters<Params>>
    effectiveTransitionParameters(
        const TransitionParameters<Transition, Params> *transition) const;
#endif

  private:
    marto::shared_ptr<ParameterValuesContainer> parameters_;

  protected:
    /** \brief load actual parameters from history to an event
     *
     * \return EVENT_LOADED if the load is successful
     */
    virtual history_access_t load(HistoryIStream &istream);
    /** \brief store actual parameters of an event into history
     *
     * \return EVENT_STORED if the store is successful
     */
    virtual history_access_t store(HistoryOStream &ostream) const;

  private:
    enum eventStatus {
        EVENT_STATUS_INVALID,
        EVENT_STATUS_READY,
        EVENT_STATUS_FILLED
    };

    enum eventStatus status_;

    friend HistoryIterator;
    void loaded() { status_ = EVENT_STATUS_FILLED; }
};

//////////////////////////////////////////////////////////////////////////////
/** each iterator will use a collection of Event, one per EventType
 *
 */
class Event :
#ifdef SWIG
        public EventState
#else
    public is_element<Event, as_public<collect::inherit_ctor<EventState>>,
                      as_protected<in_vector<HistoryIterator>>>
#endif
{
  public:
    // typedef marto::shared_ptr<Event> pointer;
    // typedef marto::shared_ptr<const Event> const_pointer;
    /** Create an empty (unusable) Event
     */
    Event(marto::shared_ptr<const EventType> type);

  protected:
    event_code_t code() const {
        auto code_ = EventState::code();
        assert(code_ == vector_element_of<HistoryIterator>::index());
        return code_;
    }
};

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
class EventType
#ifndef SWIG
    : public is_element<
          EventType,
          // collect::add_shared_from_this, // already
          // provided by SymbolicNamedParameters
          in_map<Simulation>,
          // TODO: make it a parameter
          as_public<collect::inherit_ctor<SymbolicNamedParameters>>>
#endif
{
    friend class EventState;
    friend class HistoryIterator;

  protected:
    /** \brief create a new EventType in the configuration
     *
     * \param trName indicates which transition function will be used.
     * \param eventName is a long, detailed name for the event.
     * \param fp include all parameters needed to generate the event
     *
     * \note the EventType will be registered into the provided configuration
     */
    // TODO: Simulation really needed ?
    EventType(Simulation *c, double evtRate, shared_ptr<Transition> transition);

  public:
    typedef marto::shared_ptr<EventType> pointer;
    typedef marto::shared_ptr<const EventType> const_pointer;
    static pointer make(Simulation *config, const std::string eventName,
                        double evtRate, const std::string &trName);
    virtual ~EventType() = default;

    marto::shared_ptr<SymbolicNamedParameters> parameters() {
        return marto::shared_ptr<SymbolicNamedParameters>(
            this->shared_from_this());
    }

    marto::shared_ptr<const SymbolicNamedParameters> const_parameters() const {
        return marto::shared_ptr<const SymbolicNamedParameters>(
            this->shared_from_this());
    }

    /** \brief define a new FormalOLDParameterValues for this EventType
     *
     * TODO: update this doc
     *
     * \param name: name of the formal parameter. Must be unique per EventType.
     * \param fp: pointer to a formal parameter.
     *
     * An exception is thrown if 'name' is already used in this EventType
     */
    void registerParameter(const std::string, shared_ptr<SymbolicParameter> sp);

    shared_ptr<Simulation> config() {
        return element_of<Simulation>::collection().shared_from_this();
    };

  private:
#ifndef SWIG
    friend std::ostream &operator<<(std::ostream &out, const EventType &ev);
#endif
    shared_ptr<marto::SymbolicTransition> symbolicTransition_;
    double _rate;
    // friend shared_ptr<EventType> Simulation::getRandomEventType(Random *);
    friend class Simulation;

  public:
    /** Code of this kind of event */
    event_code_t code() const { return element_of<Simulation>::index(); };
    /** \brief returns the name of the EventType
     */
    const std::string &name() const { return element_of<Simulation>::name(); }
    double rate() const { return _rate; }
    const std::string &transitionName() const;
    shared_ptr<const SymbolicTransition> transition() const {
        return smartptr_const_cast(symbolicTransition_);
    }
    virtual void
    compile(shared_ptr<SymbolicNamedParameters> frame = nullptr) override;
};
} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_EVENT
#undef MARTO_HEADER_NEED_IMPL_FROM_EVENT
#include <marto/headers-impl.h>
#endif

#endif
