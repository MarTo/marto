/* -*-c++-*- C++ mode for emacs */
/* include required implementation headers of marto headers */

#ifdef MARTO_HEADER_NEED_IMPL

#ifdef MARTO_HEADER_NEED_HISTORYSTREAM_IMPL
#include <marto/history-stream-impl.h>
#undef MARTO_HEADER_NEED_HISTORYSTREAM_IMPL
#endif

#ifdef MARTO_HEADER_NEED_PARAMETERS_IMPL
#include <marto/parameters-impl.h>
#undef MARTO_HEADER_NEED_PARAMETERS_IMPL
#endif

#ifdef MARTO_HEADER_NEED_PARAMETERS_TYPES_IMPL
#include <marto/parameters-types-impl.h>
#undef MARTO_HEADER_NEED_PARAMETERS_TYPES_IMPL
#endif

#ifdef MARTO_HEADER_NEED_QUEUE_IMPL
#include <marto/queue-impl.h>
#undef MARTO_HEADER_NEED_QUEUE_IMPL
#endif

#ifdef MARTO_HEADER_NEED_STATE_IMPL
#include <marto/state-impl.h>
#undef MARTO_HEADER_NEED_STATE_IMPL
#endif

#ifdef MARTO_HEADER_NEED_SYMBOLIC_PARAMETERS_IMPL
#include <marto/symbolic-parameters-impl.h>
#undef MARTO_HEADER_NEED_SYMBOLIC_PARAMETERS_IMPL
#endif

#ifdef MARTO_HEADER_NEED_TRANSITION_IMPL
#include <marto/transition-impl.h>
#undef MARTO_HEADER_NEED_TRANSITION_IMPL
#endif

#ifdef MARTO_HEADER_NEED_EVENT_IMPL
#include <marto/event-impl.h>
#undef MARTO_HEADER_NEED_EVENT_IMPL
#endif

#undef MARTO_HEADER_NEED_IMPL

#endif
