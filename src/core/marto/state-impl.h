/* -*-c++-*- C++ mode for emacs */
/* Queues management implementation */
#ifndef MARTO_STATE_IMPL_H
#define MARTO_STATE_IMPL_H

#ifdef __cplusplus

#include <marto/state.h>

#include <marto/macros.h>
#include <marto/queue.h>
#include <marto/simulation.h>
#include <marto/transition.h>

#include <marto/headers-impl.h>

namespace marto {

inline const std::vector<queue_value_t> Point::states() const {
    std::vector<queue_value_t> v(0);
    v.reserve(config()->map_of<Queue>::size());
    for (const auto &q : (*this)) {
        v.push_back(q->value());
    }
    return std::as_const(v);
}

} // namespace marto

#endif // __cplusplus
#endif
