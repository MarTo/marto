/* -*-c++-*- C++ mode for emacs */
/* Parameters when used in code */
#ifndef MARTO_PARAMETERS_H
#define MARTO_PARAMETERS_H

#define MARTO_HEADER_NEED_PARAMETERS_IMPL
#ifndef MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL
#define MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS
#endif

#ifdef __cplusplus

#include <stdlib.h>

#include <marto/callable-function.h>
#include <marto/collections.h>
#include <marto/forward-decl.h>
#include <marto/memory.h>

namespace marto {

/** Overview

    This header defines what is needed to have objects using
    parameters. It will be mainly functions or transitions that must
    be executed with parameters.

    An object needing parameters must define a class template with two
    arguments that will be used to defined the parameters as
    attributes. These attributes must be initialized from the
    constructor, giving at least a name (and forwarding the Parameters
    parameter of the template constructor):

    ~~~~(.hpp)
    template <class Kind, class Parameters> //
    class JSQParameters {
        public:
        marto::Parameter<Kind, JSQParameters, marto::queue_id_t> from;
        marto::Parameter<Kind, JSQParameters, marto::queue_id_t> to;
        JSQParameters(Parameters *params)
        : from(params, "from"), to(params, "to"){};
    };
    ~~~~

    The object needing parameters must then inherit from the
    `UsingParameters` template. This template has two parameters:
    - the previously defined template (with parameters as attribute)
    - the current class (for
      [CRTP](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern))

    Note that `UsingParameters` template make the assumption that the
    current class also inherits from `CallableFunctionBase`. If this
    is not the case, then `UsingParametersWithCF` must be used (and
    the constructor must then be given the `CallableFunctionBase`
    address).

    When instanciated, such an object will automatically register all
    declared parameters into the `CallableFunctionBase` object.

    The `CallableFunctionBase` object can be evaluated from various
    contexts, with different values for the parameters (and this can
    occurs in parallel). So, no parameter values can be stored in all
    these objects.

    Values comes from `SymbolicParameters` collections of
    `SymbolicParameter`s. Once again, these `SymbolicParameters` can be
    used in different contexts (random values per event, etc.) so a
    `SymbolicParameter` does not contain any value (but for a constant
    `SymbolicParameter`).

    `SymbolicParameters` (and `SymbolicParameter`s) objects must be
    compiled. This will check that all references exist and are
    correctly found.

    `ParameterValuesContainer` can be created from a
    `SymbolicParameters` object. A `ParameterValuesContainer` will
    allocate all buffers required to store the values computed/managed
    by a `SymbolicParameters` object.

    The `effectiveParameters(ParameterValuesContainer,
    SymbolicBindings)` method of a `UsingParameters` object can then
    be called to obtain an object allowing one to access to the values
    of the parameters. `ParameterValuesContainer` will be used to
    compute/store the real values. `SymbolicBindings`, created at
    compile time, contains the caller information allowing one to find
    `SymbolicParameter`s of the current parameters.

*/

#ifndef SWIG // do-forward-decl
//////////////////////////////////////
/** Class name only used to specialize templates
 *
 * This class name is used to specialize parameter into effective
 * parameters (i.e. parameters from which we can get values).
 */
class KindParameterValues;

/** Base class inherited when using parameters user in C++ code
 *
 * This class inherit from the ParametersClass template
 * that will define named attributes.
 *
 * It must be instanciated only by using the effectiveParameters
 * of the UsingParametersWithCF<ParametersClass> class.
 */
class EffectiveParametersBase {
  private:
    size_t nb_registered_parameters;
    marto::shared_ptr<ParameterValuesContainer> pvc_;
    marto::weak_ptr<const SymbolicBindings> caller_;

  public:
    EffectiveParametersBase(marto::shared_ptr<ParameterValuesContainer> pvc,
                            marto::weak_ptr<const SymbolicBindings> caller)
        : nb_registered_parameters(0), pvc_(std::move(pvc)),
          caller_(std::move(caller)) {
        assert(pvc_);
        assert(!caller_.expired());
    }
    //~EffectiveParametersBase() {
    //    std::cerr << "Deleting  " << type_name(this) << " at " << this <<
    //    std::endl;
    //    //std::cerr << "Also deleting PVC " << pvc_ << std::endl;
    //}

    size_t register_parameter() { return nb_registered_parameters++; }
    inline const SymbolicBindingsInfo &bindings_info() const;
    marto::shared_ptr<ParameterValuesContainer> values_container() const {
        return pvc_;
    }
};

template <template <class, class> class ParametersClass>
class EffectiveParametersHelper {
  public:
    // we must not modify required_parameters that can be shared
    // with several EffectiveParameters, hence the const
    const UsingParametersWithCF<ParametersClass> &required_parameters;

  protected:
    EffectiveParametersHelper(const UsingParametersWithCF<ParametersClass> &d)
        : required_parameters(d){};
};

/** Class instanciated when using parameters user in C++ code */
template <template <class, class> class ParametersClass>
class EffectiveParameters
    : public EffectiveParametersBase,
      public EffectiveParametersHelper<ParametersClass>,
      public ParametersClass<KindParameterValues,
                             EffectiveParameters<ParametersClass>> {
    friend class UsingParametersWithCF<ParametersClass>;

  private:
    /* Implementation notes:

       Allocation is restricted through
       UsingParametersWithCF<ParametersClass>::effectiveParameters()

       EffectiveParametersHelper is used to ensure that
       required_parameters is available (and initialized) when
       ParametersClass<KindParameterValues,
       EffectiveParameters<ParametersClass>> is initialized.
    */
    EffectiveParameters(const UsingParametersWithCF<ParametersClass> &d,
                        marto::shared_ptr<ParameterValuesContainer> pvc,
                        marto::weak_ptr<const SymbolicBindings> caller)
        : EffectiveParametersBase(std::move(pvc), std::move(caller)),
          EffectiveParametersHelper<ParametersClass>(d),
          ParametersClass<KindParameterValues,
                          EffectiveParameters<ParametersClass>>(this){};

    //~EffectiveParameters() {
    //    std::cerr << "Deleting  " << type_name(this) << " at " << this <<
    //    std::endl;
    //}
};

//////////////////////////////////////
/** Class name only used to specialize templates
 *
 * This class name is used to specialize parameter into required
 * parameters (i.e. declared parameters in the code).
 */
class KindRequiredParameter;

class RequiredParameters {};

class RequiredUntypedParameter
    : public is_element<RequiredUntypedParameter, collect::add_shared_from_this,
                        in_map<CallableFunctionBase>> {
  public:
    typedef std::size_t size_t;
    /* TOTO :
     * - reference a kind of typeid/typeinfo for supported type
     *   with sizeof/alignof/...
     * - encode a type dependency/requirement
     */

  protected:
    typedef element_of<CallableFunctionBase> map_element_t;

  public:
    RequiredUntypedParameter(CallableFunctionBase *callable_function,
                             const char *name);
    const std::string &name() const { return this->map_element_t::name(); }
    map_element_t::size_type index() const {
        return this->map_element_t::index();
    }
};

/** helper class so that callable function is quickly available */
class UsingParametersCFHelper {
  public:
    UsingParametersCFHelper(CallableFunctionBase *f)
        : callable_function_(f){
              // std::cerr << "Constructor of " << type_name(typeid(*this)) << "
              // for " << this << std::endl;
              // std::cerr << "CallableFunctionBase = " << f << std::endl;
          };
    CallableFunctionBase &callable_function() const {
        return *callable_function_;
    }

  private:
    CallableFunctionBase *callable_function_;
};

template <template <class, class> class ParametersClass>
class UsingParametersWithCF
    : public UsingParametersCFHelper,
      public RequiredParameters,
      public ParametersClass<KindRequiredParameter,
                             UsingParametersWithCF<ParametersClass>> {
    /* Implementation notes:
       We inherit from ParametersClass so that its attributes (parameters)
       are directly available. However, these attributes register themself
       to the CallableFunctionBase object using the callable_function()
       function. So, we use inheritance with UsingParametersCFHelper before
       ParametersClass to be sure that callable_function() function works
       soon enough (i.e. that callable_function_ attribute is initialized
       soon enough).

       User must ensure that CallableFunctionBase is initialized before
       (in particular when it is self)
    */
  public:
    UsingParametersWithCF(CallableFunctionBase *f)
        : UsingParametersCFHelper(f),
          ParametersClass<KindRequiredParameter,
                          UsingParametersWithCF<ParametersClass>>(this){
              // std::cerr << "Constructor of " << type_name(typeid(*this)) << "
              // for " << this << std::endl;
              // std::cerr << "CallableFunctionBase = " << f << std::endl;

          };

    marto::shared_ptr<EffectiveParameters<ParametersClass>> effectiveParameters(
        marto::shared_ptr<ParameterValuesContainer> pvc,
        marto::shared_ptr<const SymbolicBindings> caller) const {
        // using make_shared<> is not possible as the constructor is
        // private, only accessible to this friend class
        //
        // return make_shared<EffectiveParameters<ParametersClass>>(*this);
        auto ptr = new EffectiveParameters<ParametersClass>(*this, pvc, caller);
        return marto::shared_ptr<EffectiveParameters<ParametersClass>>(ptr);
    };
};

template <template <class, class> class ParametersClass, class CF>
class UsingParameters : public UsingParametersWithCF<ParametersClass> {
  public:
    UsingParameters() : UsingParametersWithCF<ParametersClass>(compute_CF()) {
        // std::cerr << "Constructor of " << type_name(typeid(*this)) << " for "
        //          << this << std::endl;
    }

  private:
    CallableFunctionBase *compute_CF() {
        /* Implementation note:
           Using a regular cast (CF*) instead of static_cast<CF*> to avoid
           compile-time error if typeof(this) is an inaccessible base of CF
        */
        auto cf = static_cast<CallableFunctionBase *>((CF *)this);
        // auto cf = (CallableFunctionBase *)((CF *)this);

        // std::cerr << "compute_CF of " << type_name(typeid(*this)) << " for "
        // << this << std::endl; std::cerr << "CallableFunctionBase = " << cf <<
        // std::endl;
        return cf;
    }
};

//////////////////////////////////////

template <class Kind, template <class, class> class ParametersClass,
          class ParameterType,                 // do-forward-decl
          template <class> class MartoTypeKind // do-forward-decl
          = MartoTypeStandard                  // NO-forward-decl
          >                                    // do-forward-decl
class Parameter;
#endif // SWIG // do-forward-decl

namespace collect {

namespace collector::parameters_impl {

template <phase p, class U, class Options> struct parameters;

}

template <template <class Kind, class Parameters> class Param>
struct with_parameters;

namespace collector {

namespace parameters_impl {

template <template <class Kind, class Parameters> class P, class Options>
struct parameters<phase::explore, with_parameters<P>, Options> {
    static_assert(!is_empty_tuple_v<typename Options::self_tuple_t>,
                  "self<...> not set but required by with_parameter<...>");
    using Self = tuple_first_t<typename Options::self_tuple_t>;
    using type = std::tuple<UsingParameters<P, Self>>;
    using options = Options;
};

} // namespace parameters_impl

using parameters_impl::parameters;

} // namespace collector

using parameters_collectors =
    collectors_list_merge_t<collectors_list<collector::parameters>,
                            sft_collectors>;

template <typename... Bases>
using parameters = do_collect_classes<parameters_collectors, Bases...>;

} // namespace collect

} // namespace marto

#endif // __cplusplus

#ifdef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS
#undef MARTO_HEADER_NEED_IMPL_FROM_PARAMETERS
#include <marto/headers-impl.h>
#endif

#endif
