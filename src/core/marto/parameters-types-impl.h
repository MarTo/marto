/* -*-c++-*- C++ mode for emacs */

#include <marto/parameters-types.h>

#ifndef MARTO_PARAMETERS_TYPES_IMPL_H
#define MARTO_PARAMETERS_TYPES_IMPL_H

#ifdef __cplusplus

#include <marto/parameters-values.h>
#include <marto/symbolic-parameters.h>

#include <marto/headers-impl.h>

namespace marto {

/****************************************************************************
 * marto::MartoTypeStandard
 */
template <typename Type>
marto::shared_ptr<TypedParameterValues<MartoTypeStandard<Type>>>
MartoTypeStandard<Type>::allocate_parameter_values(
    symbolic_parameter_pointer sp) {
    typedef MartoTypeStandard<Type> MartoType;
    auto sp_values = sp->provide_parameter_values(info);
    if (sp_values) {
        /* The SymbolicParameter provides an ParameterValues
           (probably a constant one).

           Checking it has the good type and returning it
        */
        auto values =
            std::dynamic_pointer_cast<TypedParameterValues<MartoType>>(
                sp_values);
        if (!values) {
            throw marto::SimulationException(
                type_name(sp.get()) + " '" + (std::string)(*sp) +
                "' does not provide values of type " + type_name<Type>());
        }
        return values;
    }

    return new_shared<FixedLenParameterValues<MartoType>>(
        /* FIXME: */ 2);
}

} // namespace marto

#endif // __cplusplus
#endif
