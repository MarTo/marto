/* -*-c++-*- C++ mode for emacs */
/* Exceptions in marto */
#ifndef MARTO_EXCEPT_H
#define MARTO_EXCEPT_H

#ifdef __cplusplus

#include <cassert>
#include <stdexcept>
#include <string>
#include <vector>

namespace marto {

/** Exception launched when trying to read or write out of the allocated space
 * of the history */
class HistoryOutOfBound : public std::runtime_error {
  public:
    HistoryOutOfBound(std::string s) : std::runtime_error(s){};
};

/** Exception launched when trying to read a event not yet all written */
class HistoryIncompleteObject : public std::runtime_error {
  public:
    HistoryIncompleteObject(std::string s) : std::runtime_error(s){};
};

/** Exception launched when trying to access an unknown object */
class UnknownName : public std::out_of_range {
    const std::string name_;

  public:
    UnknownName(std::string n, std::string msg = "")
        : std::out_of_range((msg != "") ? std::move(msg) : n + " is unknown"),
          name_(std::move(n)){};
    const std::string &name() const { return name_; }
};

/** Exception launched when trying to register an already registered name */
class ExistingName : public std::runtime_error {
    const std::string name_;

  public:
    ExistingName(std::string n, std::string msg = "")
        : std::runtime_error((msg != "") ? std::move(msg)
                                         : n + " already exists"),
          name_(std::move(n)){};
    const std::string &name() const { return name_; }
};

/** Exception launched a type error is detected at runtime */
class TypeError : public std::runtime_error {
  public:
    TypeError(std::string s) : std::runtime_error(s){};
};

/** Exception launched when a library cannot be loaded */
class DLOpenError : public std::runtime_error {
  public:
    DLOpenError(std::string s) : std::runtime_error(s){};
};

class NotImplementedException : public std::logic_error {
  public:
    NotImplementedException(
        const char *error = "Functionality not yet implemented!")
        : std::logic_error(error){};
};

class CompilationException : public std::runtime_error {
  private:
    std::string what_arg;

  public:
    CompilationException(std::string info)
        : std::runtime_error(std::move(info)), what_arg() {}
    virtual const char *what() const noexcept {
        const_cast<CompilationException *>(this)->what_arg =
            compilation_what(*this);
        return what_arg.c_str();
    }
    size_t size() const { return size(*this); }
    virtual size_t size(const std::exception &e) const {
        try {
            std::rethrow_if_nested(e);
        } catch (const CompilationException &e) {
            return e.size(e);
        } catch (const std::exception &e) {
            // with basic exception, use the basic implementation:
            // no need to go through exceptions_ if this is a
            // CompilationExceptions
            return CompilationException::size(e);
        } catch (...) {
            return 1;
        }
        return 1;
    }
    virtual const std::string compilation_what(const std::exception &e,
                                               int level = 0) const {
        std::string str(level, ' ');
        const CompilationException *ece =
            dynamic_cast<const CompilationException *>(&e);
        if (ece) {
            str += ece->std::runtime_error::what();
        } else {
            str += e.what();
        }
        str += "\n";
        try {
            std::rethrow_if_nested(e);
        } catch (const CompilationException &e) {
            str += e.compilation_what(e, level + 1);
        } catch (const std::exception &e) {
            // with basic exception, use the basic implementation:
            // no need to go through exceptions_ if this is a
            // CompilationExceptions
            str += CompilationException::compilation_what(e, level + 1);
        } catch (...) {
        }
        return str;
    }
};

class SimulationException : public CompilationException {
  public:
    SimulationException(std::string info)
        : CompilationException(std::move(info)) {}
};

class CompilationExceptions : public CompilationException {
  protected:
    std::vector<std::exception_ptr> exceptions_;

  public:
    CompilationExceptions(std::string info)
        : CompilationException(std::move(info)), exceptions_() {}
    void push_back(std::exception_ptr e) { exceptions_.push_back(e); }
    using CompilationException::size;
    size_t size(const std::exception &e) const override {
        size_t nb{0};
        assert(&e == this);
        for (auto eptr : exceptions_) {
            try {
                std::rethrow_exception(eptr);
                ++nb;
            } catch (const CompilationException &e) {
                nb += e.size(e);
            } catch (const std::exception &e) {
                // with basic exception, use the basic implementation:
                // no need to go through exceptions_
                nb += CompilationException::size(e);
            } catch (...) {
                nb += 1;
            }
        }
        return nb;
    }
    virtual const std::string compilation_what(const std::exception &e,
                                               int level = 0) const override {
        std::string str = CompilationException::compilation_what(e, level);
        for (auto eptr : exceptions_) {
            try {
                std::rethrow_exception(eptr);
            } catch (const CompilationException &e) {
                str += e.compilation_what(e, level + 1);
            } catch (const std::exception &e) {
                // with basic exception, use the basic implementation:
                // no need to go through exceptions_
                str += CompilationException::compilation_what(e, level + 1);
            } catch (...) {
            }
        }
        return str;
    }
    bool empty() const { return exceptions_.size() == 0; }
};

static inline std::string nested_what(const std::exception &e, int level = 0) {
    std::string str(level, ' ');
    str += e.what();
    str += '\n';
    try {
        std::rethrow_if_nested(e);
    } catch (const std::exception &e) {
        str += nested_what(e, level + 1);
    } catch (...) {
    }
    return str;
}

} // namespace marto

#endif // __cplusplus

#endif
