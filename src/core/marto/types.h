/* -*-c++-*- C++ mode for emacs */
/* Global basic types */
#ifndef MARTO_TYPES_H
#define MARTO_TYPES_H

#ifdef __cplusplus

#include <boost/serialization/strong_typedef.hpp>
#include <limits>
#include <map>
#include <marto/forward-decl.h>
#include <marto/memory.h>
#include <string>
#include <unistd.h>
#include <vector>

namespace marto {

typedef enum {
    HISTORY_DATA_LOADED = 0, ///< the data has been correctly loaded
    HISTORY_DATA_STORED = 0, ///< the data has been correctly stored
    HISTORY_END_DATA,        ///< no more objects into the history for now
    HISTORY_END_HISTORY,     ///< the end of history is reached
    HISTORY_OBJECT_TOO_BIG, ///< the object cannot be stored into an empty chunk
    HISTORY_STORE_INVALID_EVENT, ///< trying to store an invalid event
    HISTORY_DATA_LOAD_ERROR,     ///< generic error while loading data
    HISTORY_DATA_STORE_ERROR,    ///< generic error while storing data
} history_access_t;

#ifndef SWIG
/** unique queue identifier */
BOOST_STRONG_TYPEDEF(uint32_t, queue_id_t)
#endif

#ifndef SWIG
/** values_size_t is used to represent the lenght of parameter lists */
typedef size_t values_size_t;
constexpr values_size_t no_limit =
    (values_size_t)std::numeric_limits<size_t>::max();
#endif

/** queue content */
typedef int32_t queue_value_t;

/** type used to store the event code */
typedef unsigned event_code_t;

} // namespace marto

#endif // __cplusplus
#endif
