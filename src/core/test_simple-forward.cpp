#include "test_transition.h"
#include "gtest/gtest.h"
#include <marto.h>
#include <marto/symbolic-parameters-library.h>
#include <stdbool.h>

namespace {

using namespace marto;

/** Common base for history tests (in the google framework)
 */
class SimpleForwardBaseTest : public ::testing::Test {
  protected:
    SimpleForwardBaseTest() {
        c = new_shared<Simulation>();
        c->loadTransitionLibrary();
        std::cerr << "Transitions library loaded" << std::endl;
        new_shared<TransitionTest>("TransitionTest", *c);
        // let us first test the MM1
        eta = EventType::make(c.get(), "Arrival MM1", 0.5, "ArrivalReject");
        etb = EventType::make(c.get(), "Departure MM1", 1, "Departure");
        auto q = new_shared<StandardQueue>("Q1", *c, 10);
        std::vector<queue_id_t>
            v; // event parameters ; same for both eventTypes here
        v.push_back(q->id()); // only one queue symbolized with its id
        eta->registerParameter(
            "to", new_shared<SymbolicConstantParameter<queue_id_t>>(v));
        etb->registerParameter(
            "from", new_shared<SymbolicConstantParameter<queue_id_t>>(v));
        //  for testing larger states
        new_shared<StandardQueue>("Q2", *c, 10); // unused in MM1 test
        eva = new EventState(eta);
        evb = new EventState(etb);
        h = new History(c.get());
    }

    virtual ~SimpleForwardBaseTest() {
        // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    virtual void SetUp() {
        ASSERT_TRUE(c);
        ASSERT_TRUE(eta);
        ASSERT_TRUE(etb);
        ASSERT_TRUE(eva);
        ASSERT_TRUE(evb);
        ASSERT_TRUE(h);
    }

    virtual void TearDown() {
        // Code here will be called immediately after each test (right
        // before the destructor).
    }

    Simulation::pointer c;
    EventType::pointer eta, etb;
    EventState *eva, *evb;
    History *h;
};

TEST_F(SimpleForwardBaseTest, StateTest) {
    Point p1(c.get());
    std::cout << "p1=" << p1 << std::endl;
    for (auto s : p1.states()) {
        ASSERT_EQ(0, s);
    }
    ASSERT_EQ(2, p1.states().size()); //< 2 queues created
    // p1.at(0)->addClient(1);
    Point p2(c.get(), 8);
    std::cout << "p2=" << p2 << std::endl;
    for (auto s : p2.states()) {
        ASSERT_EQ(8, s);
    }
    ASSERT_EQ(2, p2.states().size()); //< 2 queues created
}

/* Testing M/M/1 trajectory with hard-coded history  */
TEST_F(SimpleForwardBaseTest, SimpleForwardMM1) {
    auto itw = h->iterator();
    ASSERT_TRUE(itw);
    // Fixme : generator
    // we fill the history in advance with arrivals and departures.
    eva->generate(nullptr);
    int nba = 10;
    int nbd = nba;
    for (int i = 0; i < nba; i++) {
        ASSERT_EQ(HISTORY_DATA_STORED, itw->storeNextEvent(*eva));
    }
    evb->generate(nullptr);
    for (int i = 0; i < nbd; i++) {
        ASSERT_EQ(HISTORY_DATA_STORED, itw->storeNextEvent(*evb));
    }
    eva->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, itw->storeNextEvent(*eva));
    evb->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, itw->storeNextEvent(*evb));

    // Creating state for simulation
    Point *state_pt = new Point(c.get());
    // reading history and updating state
    auto itr = h->iterator();
    while (true) {
        std::cout << state_pt << std::endl;
        Event::pointer e = itr->loadNextEvent();
        if (!e) {
            // if (itr->loadNextEvent(e) == HISTORY_END_DATA)
            break; // end of history
        }
        e->apply(state_pt);
    }
    for (auto s : state_pt->states()) {
        ASSERT_EQ(0, s);
    }
}
} // namespace
