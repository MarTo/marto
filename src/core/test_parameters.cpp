#include "test_transition.h"
#include "gtest/gtest.h"
#include <marto.h>
// TODO: add to marto.h
#include <marto/symbolic-parameters-library.h>

namespace {

using namespace marto;

TEST(Parameters, RegisterConstantParameters) {
    auto c = new_shared<Simulation>();
    new_shared<TransitionTest>("TransitionTest", *c);
    EventType::pointer et1 =
        EventType::make(c.get(), "Ev type", 42.0, "TransitionTest");
    std::vector<int> v1 = {5, 6};
    std::vector<int> v2 = {15, 16, 17};
    ASSERT_NO_THROW(et1->registerParameter(
        "to", new_shared<SymbolicConstantParameter<int>>(v1)));
    // Cannot recompile latter... to fix ? //FIXME ?
    // ASSERT_THROW(et1->compile(), marto::CompilationExceptions);
    ASSERT_NO_THROW(et1->registerParameter(
        "from", new_shared<SymbolicConstantParameter<int>>(v2)));
    ASSERT_THROW(et1->registerParameter(
                     "to", new_shared<SymbolicConstantParameter<int>>(v2)),
                 ExistingName);
    et1->compile();
    auto ev1 = std::make_shared<EventState>(et1);
    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_THROW(ev1->getParameterValues<int>("other"), marto::UnknownName);
    ASSERT_TRUE(ev1->getParameterValues<int>("from") != nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("to") != nullptr);
    auto p = ev1->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get(1));
    ASSERT_THROW(p->get(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";
    ASSERT_THROW(ev1->getParameterValues<unsigned int>("from"),
                 marto::SimulationException);

    EXPECT_DEATH(ev1->getParameterValues<int>("from")->as<unsigned int>(),
                 ".*Assertion `typeinfo == MartoTypeStandard<value_t>::info' "
                 "failed\\..*");
    auto p_ = ev1->getParameterValues<int>("from");
    std::cerr << "Before bug" << std::endl;
    p = p_->as<int>();
    ASSERT_EQ(15, p->get<int>(0));
    ASSERT_EQ(16, p->get<int>(1));
    ASSERT_THROW(p->get<int>(3), std::out_of_range)
        << "Accessing a non-existant parameter value!";
    ASSERT_EQ(15, p->get<int>(0));
    // Compile time error:
    // ASSERT_THROW(p->get<unsigned long>(0), TypeError);
}

TEST(Parameters, GenerateEventsWithConstantParameters) {
    auto c = new_shared<Simulation>();
    new_shared<TransitionTest>("TransitionTest", *c);
    EventType::pointer et1 =
        EventType::make(c.get(), "My super event", 42.0, "TransitionTest");
    std::vector<int> v1 = {5, 6};
    std::vector<int> v2 = {15, 16, 17};
    et1->registerParameter("to",
                           new_shared<SymbolicConstantParameter<int>>(v1));
    et1->registerParameter("from",
                           new_shared<SymbolicConstantParameter<int>>(v2));
    et1->compile();
    et1->registerParameter(
        "other",
        new_shared<SymbolicConstantParameter<int>>(
            v2)); // FIXME, must fail (no registering after compilation)
    auto ev1 = std::make_unique<EventState>(et1);
    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("from") != nullptr);
    auto p = ev1->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("from") != nullptr);
    p = ev1->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    EventType::pointer et2 =
        EventType::make(c.get(), "My super event 2", 42.0, "TransitionTest");
    et2->registerParameter("from",
                           new_shared<SymbolicConstantParameter<int>>(v2));

    // FIXME: should trigger a compilation automatically
    auto ev2 = std::make_unique<EventState>(et2);

    // Fixme: add a randomGenerator
    ev2->generate(nullptr);
    ASSERT_THROW(ev2->getParameterValues<int>("to"), marto::UnknownName);
    ASSERT_THROW(ev2->getParameterValues<int>("from"),
                 marto::CompilationException)
        << "Compilation not done";
    ASSERT_THROW(et2->compile(), marto::CompilationException)
        << "Error while compiling";
    // FIXME: registerParameter should fail as an EventState exists
    et2->registerParameter("to",
                           new_shared<SymbolicConstantParameter<int>>(v1));
    ASSERT_THROW(et2->compile(), marto::CompilationException)
        << "Recompiling a failed setup is not (yet?) supported";

    et2 = EventType::make(c.get(), "My super event 2 bis", 42.0,
                          "TransitionTest");
    et2->registerParameter("to",
                           new_shared<SymbolicConstantParameter<int>>(v1));
    et2->registerParameter("from",
                           new_shared<SymbolicConstantParameter<int>>(v2));
    et2->compile();

    auto ev2b = std::make_unique<EventState>(et2);

    ASSERT_TRUE(ev2b->getParameterValues<int>("from") != nullptr);
    p = ev2b->getParameterValues<int>("from")->as<int>();
    ASSERT_EQ(15, p->get<int>(0));
    ASSERT_EQ(16, p->get<int>(1));
    ASSERT_EQ(17, p->get<int>(2));
    ASSERT_THROW(p->get<int>(3), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev1->getParameterValues<int>("from") != nullptr);
    p = ev1->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";
}

TEST(Parameters, StoreAndLoadEventsWithConstantParameters) {
    auto c = new_shared<Simulation>();
    new_shared<TransitionTest>("TransitionTest", *c);
    EventType::pointer et1 =
        EventType::make(c.get(), "Event Type 1", 42.0, "TransitionTest");
    std::vector<int> v1to = {5, 6};
    std::vector<int> v1from = {7, 8};
    et1->registerParameter("to",
                           new_shared<SymbolicConstantParameter<int>>(v1to));
    et1->registerParameter("from",
                           new_shared<SymbolicConstantParameter<int>>(v1from));
    EventType::pointer et2 =
        EventType::make(c.get(), "Event type 2", 42.0, "TransitionTest");
    std::vector<int> v2 = {15, 16, 17};
    et2->registerParameter("to",
                           new_shared<SymbolicConstantParameter<int>>(v2));
    et2->registerParameter("from",
                           new_shared<SymbolicConstantParameter<int>>(v2));
    et1->compile();
    et2->compile();
    History *h = new History(c.get());
    // TODO : return shared_ptr
    auto it = h->iterator();
    ASSERT_TRUE(it);

    auto ev1 = std::make_unique<EventState>(et1);
    auto ev2 = std::make_unique<EventState>(et2);
    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, it->storeNextEvent(*ev1));
    ev1->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, it->storeNextEvent(*ev1));
    ev2->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, it->storeNextEvent(*ev2));
    ev1->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORED, it->storeNextEvent(*ev1));
    delete (it);

    it = h->iterator();
    ev1->generate(nullptr);
    ASSERT_EQ(HISTORY_DATA_STORE_ERROR, it->storeNextEvent(*ev1));

    Event::pointer ev(it->loadNextEvent());
    ASSERT_TRUE(ev);
    ASSERT_EQ(et1, ev->type());
    ASSERT_TRUE(ev->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev->getParameterValues<int>("from") != nullptr);
    auto p = ev->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    ev = it->loadNextEvent();
    ASSERT_TRUE(ev);
    ASSERT_EQ(et1, ev->type());
    // ASSERT_EQ(HISTORY_DATA_LOADED, it->loadNextEvent(e));
    ASSERT_TRUE(ev->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev->getParameterValues<int>("from") != nullptr);
    p = ev->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    ev = it->loadNextEvent();
    ASSERT_TRUE(ev);
    ASSERT_EQ(et2, ev->type());
    // ASSERT_EQ(HISTORY_DATA_LOADED, it->loadNextEvent(e));
    ASSERT_TRUE(ev->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev->getParameterValues<int>("from") != nullptr);
    p = ev->getParameterValues<int>("from")->as<int>();
    ASSERT_EQ(15, p->get<int>(0));
    ASSERT_EQ(16, p->get<int>(1));
    ASSERT_EQ(17, p->get<int>(2));
    ASSERT_THROW(p->get<int>(3), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    ev = it->loadNextEvent();
    ASSERT_TRUE(ev);
    ASSERT_EQ(et1, ev->type());
    ASSERT_TRUE(ev->getParameterValues<int>("to") != nullptr);
    ASSERT_TRUE(ev->getParameterValues<int>("from") != nullptr);
    p = ev->getParameterValues<int>("to")->as<int>();
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";

    delete (it);
    delete (h);
}

// TODO : put these 4 events into an history and reload them

} // namespace
