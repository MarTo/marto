#include <cstdlib>
#include <ltdl.h>
#include <marto.h>
#include <marto/randomLecuyer.h>
#include <marto/utils.h>

//#define DEBUG_REGISTER

#ifdef DEBUG_REGISTER
#include <iostream>
#endif

namespace marto {

Simulation::Simulation(RandomFabric::pointer r)
    : randomFabric(r), ratesSum(0.0) {
    static int initialized = 0;
    if (!initialized) {
        initialized = 1;
#if DEBUG_REGISTER
        std::cerr << "registering atexit(cleanup) in " << getpid() << std::endl;
#endif
        std::atexit(Simulation::cleanupLoadTransition);
    }
}

Simulation::Simulation() : Simulation(RandomLecuyer::makeFabric()) {}

std::vector<lt_dlhandle> Simulation::dlhandles;

void Simulation::cleanupLoadTransition() {
#if DEBUG_REGISTER
    std::cerr << "cleanupLoadTransition() in " << getpid() << std::endl;
#endif
    for (auto &handle : dlhandles) {
#if DEBUG_REGISTER
        std::cerr << "closing handle " << handle << std::endl;
#endif
        auto ret = lt_dlclose(handle);
        if (ret != 0) {
            std::cerr << lt_dlerror() << std::endl;
            assert(ret == 0);
        }
        lt_dlexit();
    }
}

Simulation::~Simulation() {}

Transition::pointer Simulation::getTransition(const std::string &name) {
    try {
        return map_of<Transition>::operator[](name);
    } catch (const std::out_of_range &oor) {
        throw UnknownName(name);
    }
}

Queue::pointer Simulation::getQueue(const std::string &name) {
    try {
        return map_of<Queue>::operator[](name);
    } catch (const std::out_of_range &oor) {
        throw UnknownName(name);
    }
}

EventType::pointer Simulation::getEventType(unsigned num) {
    assert(num < map_of<EventType>::size());
    return map_of<EventType>::operator[](num);
}

const std::vector<std::string> Simulation::getTransitionNames() {
    return map_of<Transition>::names();
}

const std::vector<std::string> Simulation::getEventNames() {
    return map_of<EventType>::names();
}

const std::vector<std::string> Simulation::getQueueNames() {
    return map_of<Queue>::names();
}

EventType::pointer Simulation::getRandomEventType(Random::pointer g) {
    // TODO: compute ratesSum
    if (ratesSum == 0) {
        for (auto et : map_of<EventType>::as_vector()) {
            ratesSum += et->rate();
        }
    }
    double r = g->Uab(0.0, ratesSum);
    double partialRatesSum = 0.0;
    for (auto et : map_of<EventType>::as_vector()) {
        partialRatesSum += et->rate();
        if (partialRatesSum >= r) {
            return et;
        }
    }
    assert(partialRatesSum < r && false);
}

Random::pointer Simulation::newRandom() { return Random::make(randomFabric); }

void Simulation::loadTransitionLibrary(const std::string &libname,
                                       loadLibraryCallback_t cb) {
    int err = 0;
    lt_dladvise advise;

    err = lt_dlinit();
    auto delete_lt = finally([] { lt_dlexit(); });
    if (err != 0) {
        throw DLOpenError(std::string("lt_dlinit error: ") +
                          std::to_string(err));
    }
    char *marto_library_path = getenv("MARTO_LIBRARY_PATH");
    if (marto_library_path) {
        // TODO: improve to accept list of directories
        // Taking care of the separator (';' on windows, ':' elsewhere...)
        // find a library ?
        err = lt_dladdsearchdir(marto_library_path);
        if (err != 0) {
            throw DLOpenError(std::string("lt_dladdsearchdir error: ") +
                              lt_dlerror());
        }
    }
    err = lt_dladdsearchdir(PKGLIBDIR);
    if (err != 0) {
        throw DLOpenError(std::string("lt_dladdsearchdir error: ") +
                          lt_dlerror());
    }
    err = lt_dladvise_init(&advise);
    auto delete_a = finally([&advise] { lt_dladvise_destroy(&advise); });
    if (err != 0) {
        throw DLOpenError(std::string("lt_dladvise_init error: ") +
                          lt_dlerror());
    }
    err = lt_dladvise_ext(&advise);
    if (err != 0) {
        throw DLOpenError(std::string("lt_dladvise_ext error: ") +
                          lt_dlerror());
    }
    err = lt_dladvise_local(&advise);
    if (err != 0) {
        throw DLOpenError(std::string("lt_dladvise_local error: ") +
                          lt_dlerror());
    }
    lt_dlhandle handle = 0;
    handle = lt_dlopenadvise(libname.c_str(), advise);
    // lt_dladvise_destroy(&advise);

    if (handle == 0) {
        throw DLOpenError(std::string("Cannot load ") + libname + ": " +
                          lt_dlerror());
    }

    delete_lt.disable();
    dlhandles.push_back(handle);
    cb(handle);
}

void Simulation::loadTransitionLibrary(const std::string &libname,
                                       const std::string &initCallback) {
    loadTransitionLibrary(libname, [libname, initCallback,
                                    this](lt_dlhandle handle) {
        transitionInitCallback_t *initaddr =
            (transitionInitCallback_t *)lt_dlsym(handle, initCallback.c_str());

        if (initaddr == nullptr) {
            throw DLOpenError(std::string("Cannot find ") + initCallback +
                              " in " + libname);
        }
        (*initaddr)(this);
    });
}
} // namespace marto
