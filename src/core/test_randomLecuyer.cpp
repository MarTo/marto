#include "test_transition.h"
#include "gtest/gtest.h"
#include <marto.h>
#include <marto/randomLecuyer.h>
#include <stdbool.h>

namespace {

using namespace marto;

/** Checking that RandomLecuyer gives us different values */
TEST(RandomLecuyer, GetContent) {

    RandomFabric::pointer rf = RandomLecuyer::makeFabric();
    ASSERT_TRUE(rf);
    Simulation::pointer c = new_shared<Simulation>(rf);
    ASSERT_TRUE(c);
    Random::pointer r = c->newDebugRandom();
    ASSERT_TRUE(r);
    double values[6] = {0.0};
    values[0] = r->U01();
    values[1] = r->U01();
    ASSERT_EQ(0 != 1, values[0] != values[1]);
    RandomStream::pointer rs1 = r->newRandomStream();
    ASSERT_TRUE(rs1);
    RandomStream::pointer rs2 = r->newRandomStream();
    ASSERT_TRUE(rs2);
    ASSERT_TRUE(rs1 != rs2);
}

} // namespace
