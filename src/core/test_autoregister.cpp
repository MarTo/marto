
#ifdef __clang__
// MemoryBlock can delay early stream output, so it works
//#define DISABLE_MEMORYBLOCK_TEST
#elif defined(__GNUC__) || defined(__GNUG__)
// DEBUG_AUTOREGISTER cannot be used with clang++ as
// std::cout/std::cerr are unavailable at early initialisation time
//#define DEBUG_AUTOREGISTER
#endif

#include "gtest/gtest.h"
#include <algorithm>
#include <marto/autoregister.h>
#include <marto/debug.h>

class Base;

// ensure all objects will be destroyed
static std::vector<std::unique_ptr<Base>> all_elements;

class RegistrationInfo {
  public:
    typedef void register_function(std::string);
    typedef void apply_function(marto::Simulation *);
    template <class TRegistered> struct Register {
        void operator()(std::string name, marto::Simulation *config) {
            std::cerr << "Registering " << name << std::endl;
            new TRegistered(config, std::move(name));
        }
    };
};

class Base {
    const std::string n;

  public:
    Base(marto::Simulation *sim, std::string name) : n(name) {
        std::cout << "Base(" << sim << ", " << name << ") created at " << this
                  << std::endl;
        std::cout << "Real type: " << marto::type_name(this) << std::endl;
        all_elements.push_back(std::unique_ptr<Base>(this));
    }
    std::string name() { return n; }
};

class A : public Base, public marto::AutoRegister<RegistrationInfo, A> {
    marto_autoregister("CA");

  public:
    A(marto::Simulation *sim, std::string name) : Base(sim, std::move(name)) {}
};

class B : public Base, public marto::AutoRegister<RegistrationInfo, B> {
    marto_autoregister("CB");
    friend RegistrationInfo;
    B(marto::Simulation *sim, std::string name) : Base(sim, std::move(name)) {}
};

class C : public Base, public marto::AutoRegister<RegistrationInfo, A> {
    marto_autoregister("CC");
    C(marto::Simulation *sim, std::string name) : Base(sim, std::move(name)) {}
};

/////////////////////////////////////////////////////////////////////////////////
#include <memory-block.h>

class RegistrationWithBlockInfo {
  public:
    typedef void register_function(std::string, MemoryBlock);
    typedef void apply_function(int, MemoryBlock, std::vector<std::string> *);
    template <class TRegistered> struct Register {
        void operator()(std::string name, MemoryBlock mreg, int val,
                        MemoryBlock mapply, std::vector<std::string> *v) {
            std::cout << "Registering " << name << " ("
                      << marto::type_name<TRegistered>() << ")"
                      << " with MemoryBlock(" << mreg.Length() << ") @" << &mreg
                      << " called with (" << val << ", MemoryBlock("
                      << mapply.Length() << ") @" << &mapply << "))"
                      << std::endl;
            v->push_back(marto::type_name<TRegistered>());
            v->push_back(name);
        }
    };
};

template <class Self>
class RE : public marto::AutoRegister<RegistrationWithBlockInfo, Self> {};

class R : public marto::Registry<RegistrationWithBlockInfo> {};

class MA : public RE<MA> {
    marto_autoregister("MA name", MemoryBlock(100));

    MA() { assert(0); }
};

class MB : public RE<MB> {
    marto_autoregister("MB name", MemoryBlock(200));
    MB() { assert(0); }
};

/////////////////////////////////////////////////////////////////////////////////

class RIWithTypeid {
  public:
    typedef void apply_function(int, MemoryBlock, std::vector<std::string> *);
    template <class TRegistered> struct Register {
        void operator()(std::type_info const *ti, int val, MemoryBlock mapply,
                        std::vector<std::string> *v) {
            std::cout << "Registering " << marto::type_name(*ti) << " ("
                      << marto::type_name<TRegistered>() << ")"
                      << " called with (" << val << ", MemoryBlock("
                      << mapply.Length() << ") @" << &mapply << "))"
                      << std::endl;
            v->push_back(marto::type_name<TRegistered>());
            if (marto::type_name(*ti) == "int") {
                ASSERT_EQ(marto::type_name<TRegistered>(), "AutoC");
            } else {
                ASSERT_EQ(*ti, typeid(TRegistered))
                    << marto::type_name(*ti)
                    << " != " << marto::type_name<TRegistered>();
            }
        }
    };
};

template <class Self>
class Autoreg : public marto::AutoRegisterWithTypeid<RIWithTypeid, Self> {};

class AutoA : public Autoreg<AutoA> {};

class AutoB : public Autoreg<AutoB> {
    static int b;
};

class AutoC : public marto::AutoRegister<RIWithTypeid, AutoC> {
    marto_autoregister(&typeid(int));
};

/////////////////////////////////////////////////////////////////////////////////

namespace {

class AutoregistrationTest : public ::testing::Test {
  protected:
    AutoregistrationTest() { MemoryBlock::initialisation_done(std::cout); }
};

TEST_F(AutoregistrationTest, BasicTest) {
    marto::Registry<RegistrationInfo>::apply(nullptr);
    ASSERT_EQ(all_elements.size(), 2);
    int a = 0, b = 1;
    if (all_elements[0]->name() != "CA") {
        b = 0;
        a = 1;
    }
    ASSERT_EQ(all_elements[a]->name(), "CA");
    ASSERT_EQ(all_elements[b]->name(), "CB");
    marto::Registry<RegistrationInfo>::apply((marto::Simulation *)1);
    ASSERT_EQ(all_elements.size(), 4);
}

TEST_F(AutoregistrationTest, MemoryBlockTest) {
    std::vector<std::string> v;
    std::cout << "apply(10, MemoryBlock(10))" << std::endl;
    R::registry_type::apply(10, MemoryBlock(10), &v);
    std::cout << "apply(20, MemoryBlock(20))" << std::endl;
    R::registry_type::apply(20, MemoryBlock(20), &v);
    std::cout << "apply done" << std::endl;
    ASSERT_EQ(v.size(), 8);
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(v[i], v[i + 4]);
    }
    std::sort(v.begin(), v.begin() + 4);
    std::sort(v.begin() + 4, v.end());
    std::vector<std::string> v_expected{"MA", "MA name", "MB", "MB name",
                                        "MA", "MA name", "MB", "MB name"};
    ASSERT_EQ(v, v_expected);
}

TEST_F(AutoregistrationTest, TypeidTest) {
    AutoA a;
    std::cout << &a << std::endl;
    std::cout << "apply(54, MemoryBlock(54))" << std::endl;
    std::vector<std::string> v;
    marto::Registry<RIWithTypeid>::apply(54, MemoryBlock(54), &v);
    std::sort(v.begin(), v.end());
    std::cout << "apply done" << std::endl;
    ASSERT_EQ(v.size(), 3);
    ASSERT_EQ(v[0], "AutoA");
    ASSERT_EQ(v[1], "AutoB");
    ASSERT_EQ(v[2], "AutoC");
}

} // namespace
