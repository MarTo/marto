#include "gtest/gtest.h"

#include "test_transition.h"

#include <marto.h>
#include <marto/state.h>

#undef MARTO_DEBUG
//#define MARTO_DEBUG

#include <marto/callable-function.h>
#include <marto/parameters-values.h>
#include <marto/parameters.h>
#include <marto/symbolic-parameters-library.h>
#include <marto/symbolic-parameters.h>

namespace {

using namespace marto;

//////////////////////////////////////
template <class Kind, class Parameters> //
class JSQParameters {
  public:
    marto::Parameter<Kind, JSQParameters, marto::queue_id_t> from;
    marto::Parameter<Kind, JSQParameters, marto::queue_id_t> to;
    // ParamList<Kind, queue_id_t, 2, 2> to;
    JSQParameters(Parameters *params)
        : from(params, "from"), to(params, "to" /* TODO 2, 2 */){};
};
//////////////////////////////////////

class JSQ3 : public is_transition<JSQ3, collect::is_monotonic,
                                  collect::with_parameters<JSQParameters>> {
    // marto::MonotonicTransition,
    //          public marto::TransitionParameters<JSQ3, JSQParameters> {

  public:
    using transition_class::apply;
    using transition_class::transition_class;

    Point *apply(Point *p,
                 const EventState *ev) const { // Event ev contains transition
                                               // specification (parameters)
        // fromList is a random sequence of Queues (specified in ev) to prevent
        // access to random generation and protect monotonicity
        // marto::ParameterValues<marto::Queue>
        // as defined in the
        // configuration file
        // auto *toList = ev->getParameter("to");

        // auto from = fromList->get<queue_id_t>(0); // the only source queue
        // parameters.getFrom(ev);
        auto param = effectiveParameters(nullptr, nullptr);
        auto &from = param->from;
        auto &to = param->to;
        auto to0 = to[0];
        auto to1 = to[1];

        // auto to0 =
        //    toList->get<queue_id_t>(0); // the first random destination queue
        // auto to1 =
        //    toList->get<queue_id_t>(1); // second random destination queue

        if (!p->at(from)->isEmpty()) {
            if (p->at(to0)->compareTo(p->at(to1)) > 0)
                p->at(to1)->addClient();
            else
                p->at(to0)->addClient();
            p->at(from)->removeClient();
        }
        return p;
    }
};

#if 0
TEST(Params2, RegisterConstantParameters) {
    auto c = new Simulation();
    new JSQ3(c, "JSQ3");
    EventType::pointer et1 = EventType::make(c, "Ev type", 42.0, "JSQ3");
    std::vector<int> v1 = {5, 6};
    std::vector<int> v2 = {15, 16, 17};
    ASSERT_EQ(true,
              et1->registerParameter("to", new FormalConstantList<int>(2, v1)));
    ASSERT_EQ(
        et1->registerParameter("from", new FormalConstantList<int>(2, v2)),
        true);
    ASSERT_THROW(
        et1->registerParameter("to", new FormalConstantList<int>(2, v2)),
        ExistingName);
    EventState *ev1 = new EventState(et1);
    // Fixme: add a randomGenerator
    ev1->generate(nullptr);
    ASSERT_TRUE(ev1->getParameter("to") != nullptr);
    ASSERT_TRUE(ev1->getParameter("from") != nullptr);
    ASSERT_EQ(nullptr, ev1->getParameter("other"));
    auto p = ev1->getParameter("to");
    ASSERT_EQ(5, p->get<int>(0));
    ASSERT_EQ(6, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";
    p = ev1->getParameter("from");
    ASSERT_EQ(15, p->get<int>(0));
    ASSERT_EQ(16, p->get<int>(1));
    ASSERT_THROW(p->get<int>(2), std::out_of_range)
        << "Accessing a non-existant parameter value!";
    ASSERT_EQ(15, p->get<int>(0));
    ASSERT_THROW(p->get<unsigned long>(0), TypeError);
}
#endif

//#define TEST(name, name2) void name()

class MyEventType :
#ifdef MARTO_DEBUG
    public DN,
#endif
    public enable_make_shared<MyEventType,
                              inherit_ctor<SymbolicNamedParameters>> {

  public:
    MyEventType(SymbolicNamedParameters::pointer frame_up = nullptr)
        : make_shared_class(frame_up){};

    void setTransition(marto::Transition *t) {
        /* Create a SymbolicTransition based on the provided
           transition.

           To recall, SymbolicTransition is a shell that allows one to
           evaluate a Transition by binding its parameters (there is
           one SymbolicTransition by EventType, several of them can
           refer to the same Transition)
        */
        transition_ = new_shared<marto::SymbolicTransition>(t);
    }

    virtual void
    compile(shared_ptr<SymbolicNamedParameters> frame = nullptr) override {
        CompilationExceptions e("while compiling MyEventType");
        try {
            SymbolicNamedParameters::compile(frame);
        } catch (...) {
            e.push_back(std::current_exception());
        }
        if (!transition_) {
            try {
                throw CompilationException(std::string("No transition set up"));
            } catch (...) {
                e.push_back(std::current_exception());
            }
        } else {
            try {
                transition_->compile(
                    SymbolicNamedParameters::shared_from_this());
            } catch (...) {
                try {
                    std::throw_with_nested(CompilationException(
                        std::string("while compiling transition ") +
                        transition_->transition()->name()));
                } catch (...) {
                    // TODO : remove the current entry
                    e.push_back(std::current_exception());
                }
            }
        }
        if (!e.empty()) {
            throw e;
        }
    }

    shared_ptr<marto::SymbolicTransition> transition() const {
        return transition_;
    }

  private:
    shared_ptr<marto::SymbolicTransition> transition_;
};

//////////////////////////////////////

TEST(Params, BasicParameters) {
    auto fpool = new_shared<SymbolicNamedParameters>();

    auto a_fpool = new_shared<SymbolicConstantParameter<int>>(1, 2);
    fpool->add_symbolic_parameter("a", a_fpool);
    auto b_fpool = new_shared<SymbolicConstantParameter<int>>(3, 4);
    fpool->add_symbolic_parameter("b", b_fpool);

    auto va_fpool = new_shared<SymbolicVariable>("a");
    fpool->add_symbolic_parameter("va", va_fpool);
    auto vb_fpool = new_shared<SymbolicVariable>("b");
    fpool->add_symbolic_parameter("vb", vb_fpool);
    auto vc_fpool = new_shared<SymbolicVariable>("c");
    fpool->add_symbolic_parameter("vc", vc_fpool);

    auto via_fpool = new_shared<SymbolicVariable>("va");
    fpool->add_symbolic_parameter("via", via_fpool);
    auto vib_fpool = new_shared<SymbolicVariable>("vb");
    fpool->add_symbolic_parameter("vib", vib_fpool);
    auto vic_fpool = new_shared<SymbolicVariable>("vc");
    fpool->add_symbolic_parameter("vic", vic_fpool);

    auto ev = marto::shared_ptr<marto::SymbolicNamedParameters>(
        new_shared<MyEventType>(fpool));
    auto a_ev = new_shared<SymbolicConstantParameter<int>>(5, 6);
    ev->add_symbolic_parameter("a", a_ev);
    auto c_ev = new_shared<SymbolicConstantParameter<int>>(7, 8);
    ev->add_symbolic_parameter("c", c_ev);
    auto va_ev = new_shared<SymbolicVariable>("a");
    ev->add_symbolic_parameter("va", va_ev);
    auto vc_ev = new_shared<SymbolicVariable>("c");
    ev->add_symbolic_parameter("vc", vc_ev);
    auto via2_ev = new_shared<SymbolicVariable>("via");
    ev->add_symbolic_parameter("via2", via2_ev);
    auto vd_ev = new_shared<SymbolicVariable>("d");
    ev->add_symbolic_parameter("vd", vd_ev);

    auto rec1_ev = new_shared<SymbolicVariable>("rec2");
    ev->add_symbolic_parameter("rec1", rec1_ev);
    auto rec2_ev = new_shared<SymbolicVariable>("rec1");
    ev->add_symbolic_parameter("rec2", rec2_ev);

    ASSERT_THROW(ev->add_symbolic_parameter(
                     "a", new_shared<SymbolicConstantParameter<int>>(9, 10)),
                 marto::ExistingName)
        << "Duplicate name a";

    try {
        ev->compile();
        ASSERT_EQ("Bad successful compilation", "Should not be reached");
    } catch (...) {
        ASSERT_THROW(throw, CompilationExceptions);
        try {
            throw;
        } catch (CompilationExceptions &compilation_exceptions) {
            std::cerr << compilation_exceptions.what() << std::endl;
            ASSERT_EQ(compilation_exceptions.size(), (size_t)6);
        }
    }
    ASSERT_EQ((*ev)["a"], a_ev);
    ASSERT_EQ((*ev)["b"], b_fpool);
    ASSERT_EQ((*ev)["c"], c_ev);
    ASSERT_EQ((*ev)["va"], a_ev);
    ASSERT_EQ((*ev)["via"], a_fpool);
    ASSERT_EQ((*ev)["vb"], b_fpool);
    ASSERT_EQ((*ev)["vib"], b_fpool);
    ASSERT_EQ((*ev)["vc"], c_ev);
    ASSERT_EQ((*ev)["via2"], a_fpool);

    ASSERT_THROW((*ev)["vic"], CompilationException);
    ASSERT_THROW((*ev)["vd"], CompilationException);

    for (auto sym : fpool->names()) {
        try {
            auto real = (*fpool)[sym];
            ASSERT_EQ(real.get(), real->compiled());
            std::cout << "Entry " << sym << " compiled into " << real
                      << std::endl;
        } catch (std::exception &e) {
            std::cerr << e.what();
        }
    }
    for (auto sym : ev->names()) {
        try {
            (*ev)[sym]->compile(ev);
            auto real = (*ev)[sym];
            ASSERT_EQ(real.get(), real->compiled());
            std::cout << "Entry " << sym << " compiled into " << real
                      << std::endl;
        } catch (std::exception &e) {
            std::cerr << e.what();
        }
    }
}

//////////////////////////////////////
template <class Kind, class Parameters> //
class QueueHeadFunctionParameters {
  public:
    marto::Parameter<Kind, QueueHeadFunctionParameters, marto::queue_id_t> from;
    // ParamList<Kind, queue_id_t, 2, 2> to;
    QueueHeadFunctionParameters(Parameters *params) : from(params, "from"){};
};
class HeadFunction
    : public is_callable_function<
          HeadFunction, collect::with_parameters<QueueHeadFunctionParameters>> {
};
//////////////////////////////////////

TEST(Params, FunctionParameters) {
    auto fpool = new_shared<SymbolicNamedParameters>();
    auto ev = new_shared<MyEventType>(fpool);

    fpool->add_symbolic_parameter("head",
                                  std::static_pointer_cast<SymbolicParameter>(
                                      new_shared<HeadFunction>()));

    auto a0 = new_shared<SymbolicConstantParameter<int>>(100, 101);
    auto a1 = new_shared<SymbolicConstantParameter<int>>(110, 111);
    ev->add_symbolic_parameter("a1", a1);
    auto a2 = new_shared<SymbolicConstantParameter<int>>(120, 121);

    ASSERT_THROW(
        new_shared<SymbolicFunctionCall>("f1dup0", "a", a0, "a", a1, a2),
        marto::ExistingName)
        << "Duplicate name in f1dup0";

    auto sa = new_shared<SymbolicFunctionCall>("f0");
    ev->add_symbolic_parameter("bar0", sa);

    auto sa0 = new_shared<SymbolicFunctionCall>("f1", a0);
    auto sa1 = new_shared<SymbolicFunctionCall>("f1", a1);
    auto sa2 = new_shared<SymbolicFunctionCall>("f1", a2);
    auto sa3 = new_shared<SymbolicFunctionCall>("f1", sa0, sa1, sa2);

    auto sb0 = new_shared<SymbolicFunctionCall>("f1n", "pa0", a0);
    auto sb1 = new_shared<SymbolicFunctionCall>("f1n", "pa1", a1);
    auto sb2 = new_shared<SymbolicFunctionCall>("f1n", "pa2", a2);
    auto sb3 = new_shared<SymbolicFunctionCall>("f1n", "sb1", sb1, "sb2", sb2,
                                                "sb0", sb0);

    ASSERT_THROW(
        new_shared<SymbolicFunctionCall>("f1dup", "a", sa0, "a", sa1, sa2),
        marto::ExistingName)
        << "Duplicate name in f1dup";

    auto bar1 = new_shared<SymbolicFunctionCall>(
        "head", sa3, "sb3", sb3,
        new_shared<SymbolicConstantParameter<int>>(130, 131));

    ev->add_symbolic_parameter("bar1", bar1);

    auto sx1 = ev->lookup("bar0");
    ASSERT_EQ(sx1->value(), sa);
    ASSERT_THROW(ev->lookup("barX"), UnknownName)
        << "barX should be an unknown name";

    auto h0 = new_shared<SymbolicFunctionCall>("head");
    ev->add_symbolic_parameter("h0", h0);
    auto h1n = new_shared<SymbolicFunctionCall>("head", "from", a0);
    ev->add_symbolic_parameter("h1n", h1n);
    auto h1n2 = new_shared<SymbolicFunctionCall>("head", "to", a0);
    ev->add_symbolic_parameter("h1n2", h1n2);
    auto h1n3 = new_shared<SymbolicFunctionCall>("head", "from", a0, "to", a2);
    ev->add_symbolic_parameter("h1n3", h1n3);
    auto h1p = new_shared<SymbolicFunctionCall>("head", a0);
    ev->add_symbolic_parameter("h1p", h1p);
    auto h1p2 = new_shared<SymbolicFunctionCall>("head", a0, a2);
    ev->add_symbolic_parameter("h1p2", h1p2);
    auto a1f = new_shared<SymbolicFunctionCall>("a1", a0);
    ev->add_symbolic_parameter("a1f", a1f);
    auto h2 = new_shared<SymbolicFunctionCall>(
        "head", new_shared<SymbolicFunctionCall>(
                    "head", new_shared<SymbolicVariable>("a1")));
    ev->add_symbolic_parameter("h2", h2);

    auto sim = new_shared<Simulation>();
    auto jsq3 = new_shared<JSQ3>("JSQ3", *sim);
    ev->setTransition(jsq3.get());

    try {
        ev->compile();
        ASSERT_EQ("Bad successful compilation", "Should not be reached");
    } catch (...) {
        ASSERT_THROW(throw, CompilationExceptions);
        try {
            throw;
        } catch (CompilationExceptions &compilation_exceptions) {
            std::cerr << compilation_exceptions.what() << std::endl;
            ASSERT_EQ(compilation_exceptions.size(), (size_t)11);
        }
    }
    for (auto sym : ev->names()) {
        try {
            auto real = (*ev)[sym];
            std::cerr << "Entry " << sym << " compiled into " << real
                      << std::endl;
            ASSERT_EQ(real.get(), real->compiled());
        } catch (std::exception &e) {
            std::cerr << e.what();
        }
    }

    ev = nullptr;
}

TEST(Params, TransitionParameters) {
    auto fpool = new_shared<marto::SymbolicNamedParameters>();
    auto ev = new_shared<MyEventType>(fpool);

    auto to = new_shared<SymbolicConstantParameter<marto::queue_id_t>>(
        (marto::queue_id_t)100, (marto::queue_id_t)101);
    fpool->add_symbolic_parameter("to", to);
    auto from = new_shared<SymbolicConstantParameter<marto::queue_id_t>>(
        (marto::queue_id_t)110, (marto::queue_id_t)111);
    ev->add_symbolic_parameter("from", from);

    auto sim = new_shared<Simulation>();
    auto jsq3 = new_shared<JSQ3>("JSQ3", *sim);
    ev->setTransition(jsq3.get());

    try {
        ASSERT_NO_THROW(ev->compile());
    } catch (...) {
        ASSERT_THROW(throw, CompilationExceptions);
        try {
            throw;
        } catch (CompilationExceptions &compilation_exceptions) {
            std::cerr << compilation_exceptions.what() << std::endl;
            ASSERT_EQ(compilation_exceptions.size(), (size_t)11);
        }
    }
    for (auto sym : fpool->names()) {
        try {
            auto real = (*fpool)[sym];
            ASSERT_EQ(real.get(), real->compiled());
            std::cout << "Entry " << sym << " compiled into " << real
                      << " from fpool " << (std::string) * ((*fpool)[sym])
                      << std::endl;
        } catch (std::exception &e) {
            std::cerr << e.what();
        }
    }
    for (auto sym : ev->names()) {
        try {
            auto real = (*ev)[sym];
            std::cerr << "Entry " << sym << " compiled into " << real
                      << " from ev " << (std::string) * ((*ev)[sym])
                      << std::endl;
            ASSERT_EQ(real.get(), real->compiled());
        } catch (std::exception &e) {
            std::cerr << e.what();
        }
    }

    // ev->allocate(EvaluationTiming::constant);

    auto pvc_c =
        new_shared<ParameterValuesContainer>(ev, EvaluationTiming::constant);
    EXPECT_DEATH(
        new_shared<ParameterValuesContainer>(ev, EvaluationTiming::event),
        ".*Assertion `sps->nb_slots[(]t[)] == 0' failed..*");
    auto pvc_h = new_shared<ParameterValuesContainer>(
        ev, EvaluationTiming::event, pvc_c);

    auto param = jsq3->effectiveParameters(
        pvc_h, ev->transition()); // marto::weak_ptr<const
                                  // SymbolicBindings>(ev->transition()));
    queue_id_t pfrom = param->from[0];
    ASSERT_EQ(param->from[0], 110);

    // delete pvc_h;
    ev = nullptr;
}

} // namespace
