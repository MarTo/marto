#include <marto/symbolic-parameters.h>

namespace marto {

/****************************************************************************
 * marto::SymbolicNamedParameters
 */

void SymbolicNamedParameters::compile(pointer frame) {
    CompilationExceptions e("while compiling parameters");
    if (frame_up_) {
        try {
            frame_up_->compile();
        } catch (...) {
            e.push_back(std::current_exception());
        }
    }
    if (!frame) {
        frame = this->shared_from_this();
    }
    for (auto const &snp : map_of<SymbolicNamedParameter>::as_vector()) {
        try {
            snp->compile(frame);
            // std::cerr << snp->name() << " compiled" << std::endl;
        } catch (...) {
            try {
                std::throw_with_nested(
                    CompilationException(std::string("Entry ") + snp->name() +
                                         " cannot be compiled"));
            } catch (...) {
                // TODO : remove the current entry
                e.push_back(std::current_exception());
            }
        }
    }
    if (!e.empty()) {
        throw e;
    }
}

/****************************************************************************
 * marto::SymbolicFunctionCall
 */

SymbolicParameter *SymbolicFunctionCall::compute_real(
    const SymbolicNamedParameters::pointer &parameters) {
    auto symbolic_function_code = super::compute_real(parameters);
    function_ =
        dynamic_cast<SymbolicCallableFunctionBase *>(symbolic_function_code);
    if (!function_) {
        throw CompilationException(name + " is not a callable function");
    }
    update_evaluation_timing_from_parameter(symbolic_function_code);

    compute_arguments(parameters);
    return this;
}

void SymbolicFunctionCall::compute_arguments(
    const SymbolicNamedParameters::pointer &parameters) {

    caller_params->compile(parameters);
    for (auto const &p : *caller_params) {
        update_evaluation_timing_from_parameter(*p);
    }

    assert(function_);
    /* Implementation note:
     *
     * resize and not reserve, as latter access to [index] requires
     * the corresponding element to exist.
     * Access is not done in regular order, so push_back cannot be used.
     */
    bindings_info_.arguments_number(function_->size());
    CompilationExceptions e("while binding arguments with prototype '" + name +
                            (std::string)*function_ + "'");
    size_t idx = 0;
    for (auto const &[arg_name, arg_named] :
         std::as_const(caller_param_names)) {
        // std::cerr << "handling " << arg_name << std::endl;
        if (!arg_named) {
            ++idx;
            /* unnamed parameter, for second pass */
            continue;
        }
        shared_ptr<RequiredUntypedParameter> rup;
        try {
            try {
                rup = (*function_)[arg_name];
            } catch (const std::out_of_range &oor) {
                throw UnknownName(arg_name,
                                  std::string("Unknown parameter name '") +
                                      arg_name +
                                      "' provided. Ignoring this argument.");
            }
            bindings_info_.add(rup->index(), caller_params->lookup(idx));
        } catch (UnknownName &ex) {
            e.push_back(std::current_exception());
        }
        ++idx;
    }
    auto &&callee_arguments = function_->as_vector();
    auto cur_callee_argument = callee_arguments.begin();
    auto last_callee_argument = callee_arguments.end();
    idx = 0;
    for (auto const &[arg_name, arg_named] :
         std::as_const(caller_param_names)) {
        // std::cerr << "handling bis " << arg_name << std::endl;
        if (arg_named) {
            ++idx;
            /* named parameter, already handled */
            continue;
        }
        /* a parameter need to be handled */
        while (cur_callee_argument != last_callee_argument &&
               bindings_info_[(*cur_callee_argument)->index()]) {
            ++cur_callee_argument;
        }
        try {
            if (cur_callee_argument == last_callee_argument) {
                throw CompilationException(
                    std::string("Extra parameter: ") +
                    (std::string)(*caller_params->lookup(idx)) +
                    ". Ignoring this argument.");
            }
            // std::cerr << "storing '" << arg_name << "' at " <<
            // (*cur_callee_argument)->index() << std::endl;
            bindings_info_.add((*cur_callee_argument)->index(),
                               caller_params->lookup(idx));
        } catch (CompilationException &ex) {
            e.push_back(std::current_exception());
        }
        ++idx;
    }

    while (cur_callee_argument != last_callee_argument &&
           bindings_info_[(*cur_callee_argument)->index()]) {
        ++cur_callee_argument;
    }
    if (cur_callee_argument != last_callee_argument) {
        try {
            throw CompilationException(std::string("Missing parameter(s)"));
        } catch (...) {
            e.push_back(std::current_exception());
        }
    }

    if (!e.empty()) {
        throw e;
    }
    // std::cerr << "Mapping for " << (std::string)(*this) << std::endl;
    // for (auto const &
    return;
}

/****************************************************************************
 * marto::SymbolicTransition
 */

SymbolicParameter *SymbolicTransition::compute_real(
    const SymbolicNamedParameters::pointer &parameters) {

    compute_arguments(parameters);
    return this;
}

void SymbolicTransition::compute_arguments(
    const SymbolicNamedParameters::pointer &parameters) {

    CompilationExceptions e("while binding arguments");
    const CallableFunctionBase *function =
        static_cast<const CallableFunctionBase *>(transition_.get());
    /* Implementation note:
     *
     * resize and not reserve, as latter access to [index] requires
     * the corresponding element to exist.
     * Access is not done in regular order, so push_back cannot be used.
     */
    bindings_info_.arguments_number(function->size());
    // iter on transition required parameters
    for (const shared_ptr<RequiredUntypedParameter> &arg :
         std::as_const(function->as_vector())) {
        try {
            // std::cerr << "handling " << arg->name() << std::endl;
            bindings_info_.add(arg->index(), parameters->lookup(arg->name()));
        } catch (...) {
            e.push_back(std::current_exception());
        }
    }
    if (!e.empty()) {
        throw e;
    }
    return;
}

} // namespace marto
