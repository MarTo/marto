// memory-block.h, used for tests only
#pragma once
#include <algorithm>
#include <iostream>

#ifdef __clang__
/* With clang, std::out cannot be used in early initialisation.
 *
 * As we do it in a test, then we delay the output until it is possible.
 *
 */
#define MEMORYBLOCK_DELAY_OUTPUT 1
#else
#define MEMORYBLOCK_DELAY_OUTPUT 1
#endif

template <int v> class EarlyCout;

template <> class EarlyCout<0> {
  public:
    inline static std::ostream &std_cout() { return std::cout; }
    inline static void initialisation_done(std::ostream &) {}
    template <class CharT, class Traits>
    static inline std::basic_ostream<CharT, Traits> &
    endl(std::basic_ostream<CharT, Traits> &os) {
        return os << std::endl;
    }
};

template <> class EarlyCout<1> {
  private:
    class ostringstream {
        friend class EarlyCout<1>;

      private:
        int flushed = 0;
        char *buffer = nullptr;
        size_t buffer_size = 0;
        size_t len = 0;
        int flush_buffer(std::ostream &out) {
            if (len) {
                out << "Flushing early output" << std::endl;
                out << buffer;
                free(buffer);
                len = 0;
                out << "Flushed early output" << std::endl;
            }
            flushed = 1;
            return 0;
        }

      public:
        ostringstream() {}
        ostringstream &operator<<(const std::string &s) {
            return operator<<((const char *)s.c_str());
        };
        ostringstream &operator<<(const char *s) {
            // std::cerr << "\\\\" << s << "//" << std::endl;
            // std::cerr << (void*)buffer << "[" << len << "/" << buffer_size <<
            // "]" << std::endl;
            if (flushed) {
                std::cout << s;
                return *this;
            }
            size_t l = strlen(s);
            if (len + l + 1 >= buffer_size) {
                buffer = (char *)realloc(buffer, (len + l + 1 + 4095) & ~4095);
                assert(buffer);
            }
            strcpy(buffer + len, s);
            len += l;
            // std::cerr << "len = " << len << std::endl;
            return *this;
        };
        ostringstream &operator<<(const long &i) {
            char b[30];
            int size = sprintf(b, "%li", i);
            assert(size < 30);
            b[size] = 0;
            return operator<<(b);
        };
    };

    static int ostringstream_init_done;
    static ostringstream *std_cout_ptr;
    static marto::PointerDeleter<ostringstream *> std_cout_ptr_deleter;
    static ostringstream &allocate_std_cout() {
        if (!std_cout_ptr) {
            assert(!ostringstream_init_done);
            std_cout_ptr = new ostringstream();
        }
        return *std_cout_ptr;
    }

  public:
    static ostringstream &std_cout() { return allocate_std_cout(); };
    inline static void initialisation_done(std::ostream &out) {
        // std::cerr << "****************************************************"
        // << std::endl;
        if (std_cout_ptr) {
            std_cout_ptr->flush_buffer(out);
            ostringstream_init_done = 1;
        }
    }
    static const char *const endl;
};

int EarlyCout<1>::ostringstream_init_done = 0;
EarlyCout<1>::ostringstream *EarlyCout<1>::std_cout_ptr = nullptr;
const char *const EarlyCout<1>::endl = "\n";
marto::PointerDeleter<EarlyCout<1>::ostringstream *>
    EarlyCout<1>::std_cout_ptr_deleter(&EarlyCout<1>::std_cout_ptr);

class MemoryBlock : public EarlyCout<MEMORYBLOCK_DELAY_OUTPUT> {
  public:
    // using EarlyCout<MEMORYBLOCK_DELAY_OUTPUT>::std_cout;

    // Simple constructor that initializes the resource.
    explicit MemoryBlock(size_t length)
        : _length(length), _data(new int[length]) {
        std_cout() << "In MemoryBlock(size_t). length = " << _length << "."
                   << endl;
    }

    // Destructor.
    ~MemoryBlock() {
        std_cout() << "In ~MemoryBlock(). length = " << _length << ".";

        if (_data != nullptr) {
            std_cout() << " Deleting resource.";
            // Delete the resource.
            delete[] _data;
        }

        std_cout() << endl;
    }

    // Copy constructor.
    MemoryBlock(const MemoryBlock &other)
        : _length(other._length), _data(new int[other._length]) {
        std_cout() << "In MemoryBlock(const MemoryBlock&). length = "
                   << other._length << ". Copying resource." << endl;

        std::copy(other._data, other._data + _length, _data);
    }

    // Copy assignment operator.
    MemoryBlock &operator=(const MemoryBlock &other) {
        std_cout() << "In operator=(const MemoryBlock&). length = "
                   << other._length << ". Copying resource." << endl;

        if (this != &other) {
            // Free the existing resource.
            delete[] _data;

            _length = other._length;
            _data = new int[_length];
            std::copy(other._data, other._data + _length, _data);
        }
        return *this;
    }

    // Move constructor.
    MemoryBlock(MemoryBlock &&other) noexcept : _length(0), _data(nullptr) {
        std_cout() << "In MemoryBlock(MemoryBlock&&). length = "
                   << other._length << ". Moving resource." << endl;

        // Copy the data pointer and its length from the
        // source object.
        _data = other._data;
        _length = other._length;

        // Release the data pointer from the source object so that
        // the destructor does not free the memory multiple times.
        other._data = nullptr;
        other._length = 0;
    }

    // Move assignment operator.
    MemoryBlock &operator=(MemoryBlock &&other) noexcept {
        std_cout() << "In operator=(MemoryBlock&&). length = " << other._length
                   << "." << endl;

        if (this != &other) {
            // Free the existing resource.
            delete[] _data;

            // Copy the data pointer and its length from the
            // source object.
            _data = other._data;
            _length = other._length;

            // Release the data pointer from the source object so that
            // the destructor does not free the memory multiple times.
            other._data = nullptr;
            other._length = 0;
        }
        return *this;
    }

    // Retrieves the length of the data resource.
    size_t Length() const { return _length; }

  private:
    size_t _length; // The length of the resource.
    int *_data;     // The resource.
};
