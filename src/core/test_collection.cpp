#include "gtest-extensions.h"
#include <marto/collections.h>
#include <marto/debug.h>

#include <iostream>

#define TEST_MAPS

namespace {
struct A {};
struct B {};
__attribute__((unused)) int foo() {
    {
        using namespace marto::collect;
        using CT1 = collections<with_self<A, B>>;
        marto::static_assert_is_same<typename CT1::other_tuple_t,
                                     std::tuple<B>>("all other types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
    }
    {
        using namespace marto::collect;
        using CT1 = collections<with_self<A, as_public<B>>>;
        marto::static_assert_is_same<typename CT1::public_tuple_t,
                                     std::tuple<B>>("all public types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
    }
    {
        using namespace marto::collect;
        using CT1 = collections<with_self<A, as_public<in_vector<B>>>>;
        marto::static_assert_is_same<
            typename CT1::public_tuple_t,
            std::tuple<marto::collections::VectorElement<B, A, 0>>>(
            "all public types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
        static_assert(CT1::in_vector == 1);
        static_assert(CT1::has_vector_of == 0);
    }
    {
        using namespace marto::collect;
        using CT = marto::collections::bits::is_in_collection_helper<
            A, as_public<in_vector<B>>>;
#ifdef is_in_collection_helper_public
        // these tests are for debug. private types in is_in_collection_helper
        // must be set public
        using CT1 = typename CT::Collect_;
        marto::static_assert_is_same<
            typename CT1::public_tuple_t,
            std::tuple<marto::collections::VectorElement<B, A, 0>>>(
            "all public types");

        marto::static_assert_is_same<typename CT1::self_tuple_t, std::tuple<A>>(
            "self type");
        static_assert(CT1::in_vector == 1);
        static_assert(CT1::has_vector_of == 0);
#endif
        std::cerr << marto::type_name<typename CT::type>() << std::endl;
        // marto::static_assert_is_same<typename CT::type, void>("test");
    }
    return 0;
}

} // namespace

namespace inherit {

using namespace marto;

struct Transition;

struct Simulation
    : public is_collection<Simulation, as_public<has_map_of<Transition>>> {};

struct Transition
    : public is_element<
          Transition,
          as_public<
              collect::
                  add_shared_from_this, // collect::add_virtual_shared_from_this,
              in_map<Simulation>>> {

  protected:
    Transition() {}

  public:
    virtual int add(int a, int b) = 0;
    virtual double add(double a, double b) = 0;
    virtual char add(char a, char b) = 0;

    virtual ~Transition() {}
};

struct MonotonicTransition : public virtual Transition {
    // using Transition::Transition;

    using Transition::add;
    virtual char add(char a, char b) { return a + b; }

    virtual ~MonotonicTransition() {}
};

struct TransitionTest
    : public enable_make_shared<TransitionTest, MonotonicTransition> {

    using MonotonicTransition::add;

    virtual int add(int a, int b) { return a + b; }
    virtual double add(double a, double b) { return a + b; }

    virtual ~TransitionTest() {
        static_assert(marto::bits::has_make_shared<Transition>::value);
        static_assert(marto::bits::has_make_shared<TransitionTest>::value);
        static_assert(marto::bits::has_make_shared<MonotonicTransition>::value);
    }
};

} // namespace inherit

namespace inherit2 {

using namespace marto;

struct Transition;

struct Simulation
    : public is_collection<Simulation, as_public<has_map_of<Transition>>> {};

struct Transition
    : public is_element<
          Transition,
          as_public<
              collect::
                  add_shared_from_this, // collect::add_virtual_shared_from_this,
              in_map<Simulation>>> {

  protected:
    Transition() {}

  public:
    virtual int add(int a, int b) = 0;
    virtual double add(double a, double b) = 0;
    virtual char add(char a, char b) = 0;

    virtual int compute() = 0;

    virtual ~Transition() {}
};

struct MonotonicTransition : public virtual Transition {
    // using Transition::Transition;

    using Transition::add;
    virtual char add(char a, char b) { return a + b; }

    virtual ~MonotonicTransition() {}
};

struct Parameters : public virtual Transition {

    virtual int compute() override { return 1; }
};

struct TransitionTest
    : public enable_make_shared<TransitionTest, MonotonicTransition>,
      public Parameters {

    using MonotonicTransition::add;

    virtual int add(int a, int b) { return a + b; }
    virtual double add(double a, double b) { return a + b; }

    virtual ~TransitionTest() {
        static_assert(marto::bits::has_make_shared<Transition>::value);
        static_assert(marto::bits::has_make_shared<TransitionTest>::value);
        static_assert(marto::bits::has_make_shared<MonotonicTransition>::value);
    }
};

} // namespace inherit2

namespace static_alloc {

using namespace marto;

struct Parameters;

struct Param : public is_element<Param, in_map<Parameters>> {
    Param(Parameters *p, std::string n) { this->init_shared(n, *p); }
};

struct Parameters : public is_collection<Parameters, has_map_of<Param>> {
    Param p1;
    Param p2;

    Parameters() : p1(this, "p1name"), p2(this, "p2name") {}
};

} // namespace static_alloc

namespace arround_ctor {

using namespace marto;

struct Parameters;

struct Param : public is_element<Param, in_map<Parameters>> {
    marto::weak_ptr<Param> wparam_;

    template <typename EMS_pointer, typename F, typename... Args>
    static pointer make_shared_extension(F &&f, Args &&...args) {
#ifdef MARTO_DEBUG
        std::cerr << "=> " << marto::type_name<VectorElement>()
                  << "::make_shared_extension<"
                  << marto::type_name<EMS_pointer>() << ">" << std::endl;
#endif
        auto cb =
            std::function<element_smart_pointer(Args...)>(std::forward<F>(f));
        pointer ptr = cb(std::forward<Args>(args)...);

        ptr->wparam_ = weak_ptr<Param>(ptr->shared_from_this());

        return ptr;
    }

    Param() {
        // wparam_ = weak_ptr<Param>(this->shared_from_this());
    }
};

struct Parameters : public is_collection<Parameters, has_map_of<Param>> {};

} // namespace arround_ctor

// set to 0 in order to just test previous static_assert()
#if 1
namespace marto {
class Event;
class EventType;
class Queue;
class QueueState;
class Trajectory;
class TrajectorySeq;
class Config;
class ConfigBis;
}; // namespace marto

namespace marto {

template <typename T,
          typename = decltype(std::declval<const T>().operator<<(std::cout))>
std::ostream &operator<<(std::ostream &out, const T &obj) {
    return obj << out;
}

class Event : public is_element<Event, in_vector<Trajectory>,
                                in_vector<TrajectorySeq>> {

    const int value;

  public:
    Event(const Event &) = delete;
    Event(int val);

  public:
    std::ostream &operator<<(std::ostream &os) const;
};

class QueueState
    : private is_element<QueueState, as_protected<in_vector<Trajectory>>> {
    const int value;
    friend extend_make_shared_class;
    friend struct marto::bits::enable_make_shared_factory<QueueState>;

  public:
    using make_shared_class::pointer;

  protected:
    QueueState(int val);

  public:
    std::ostream &operator<<(std::ostream &os) const;
};

class Trajectory : public is_collection<Trajectory, has_vector_of<Event>,
                                        has_vector_of<QueueState>> {
  public:
    std::ostream &operator<<(std::ostream &os) const;
    Trajectory(){};
};

class TrajectorySeq
    : private is_collection<TrajectorySeq, has_vector_of<Event>> {
    friend Event;

  public:
    std::ostream &operator<<(std::ostream &os) const;
};

Event::Event(int val) : value(val) {
    if (value > 110) {
        throw NotImplementedException(
            (std::string("Exception in Event ctor for val=") +
             std::to_string(value))
                .c_str());
    }
};

QueueState::QueueState(int val) : value(val) {
    if (val > 100) {
        throw NotImplementedException(
            (std::string("Exception in QueueState ctor for val=") +
             std::to_string(val))
                .c_str());
    }
};

std::ostream &Event::operator<<(std::ostream &os) const {
    os << "Event[T: " << this->element_of<Trajectory>::index() << "/"
       << element_of<Trajectory>::vector().size()
       << ", S: " << element_of<TrajectorySeq>::index() << "/"
       << element_of<TrajectorySeq>::vector().size() << "] = " << value;
    return os;
};

std::ostream &QueueState::operator<<(std::ostream &os) const {
    assert(vector().size() == collection().vector_of<QueueState>::size());
    os << "QueueState[" << index() << "/" << vector().size() << "] = " << value;
    return os;
};

std::ostream &Trajectory::operator<<(std::ostream &os) const {
    os << "*** Trajectory:" << std::endl;
    os << "Events:" << std::endl;
    int c = 0;
    for (auto const &e : vector_of<Event>::as_vector()) {
        os << "- " << c++ << ' ' << *e << std::endl;
    }
    os << "QueueState:" << std::endl;
    c = 0;
    for (auto const &e : vector_of<QueueState>::as_vector()) {
        os << c++ << ' ' << *e << std::endl;
    }
    return os;
};

std::ostream &TrajectorySeq::operator<<(std::ostream &os) const {
    os << "*** TrajectorySeq:" << std::endl;
    os << "Events:" << std::endl;
    int c = 0;
    for (auto const &e : as_vector()) {
        os << "- " << c++ << ' ' << *e << std::endl;
    }
    return os;
};

    /* TODO: add tests with n!=0 */

    /******************************************************************/
    /******************************************************************/
    /******************************************************************/

#ifdef TEST_MAPS

class EventType
    : public is_element<EventType, in_map<Config>, in_map<ConfigBis>> {
    const int value;

  public:
    EventType(const Event &) = delete;
    EventType(int val);

  public:
    std::ostream &operator<<(std::ostream &os) const;
};

class Queue : public is_element<Queue, in_map<Config>> {

    const int value;
    // qid must be before config and will really be initialized
    // by config initialisation
    // using mme = collections::MartoMapElement<Config, Queue>;
    // const mme::size_type qid;
    // mme config;

  public:
    Queue(int val);

  public:
    std::ostream &operator<<(std::ostream &os) const;
};

class Config
    : public is_collection<Config, has_map_of<Queue>, has_map_of<EventType>> {
    friend Queue;

  public:
    std::ostream &operator<<(std::ostream &os) const;
    Config() {}
};

class ConfigBis : public is_collection<ConfigBis, has_map_of<EventType>> {
  public:
    std::ostream &operator<<(std::ostream &os) const;
    ConfigBis() {}
};

EventType::EventType(int val) : value(val) {}

Queue::Queue(int val) : value(val) {

    if (val > 100) {
        throw NotImplementedException(
            (std::string("Exception in Queue ctor for val=") +
             std::to_string(val))
                .c_str());
    }
};

std::ostream &Config::operator<<(std::ostream &os) const {
    os << "*** Config:" << std::endl;
    os << "EventTypes:" << std::endl;
    int c = 0;
    for (auto const &e : this->map_of<EventType>::as_vector()) {
        os << "- " << c++ << ' ' << *e << std::endl;
    }
    os << "Queues:" << std::endl;
    c = 0;
    for (auto const &e : map_of<Queue>::as_vector()) {
        os << c++ << ' ' << *e << std::endl;
    }
    return os;
};

std::ostream &ConfigBis::operator<<(std::ostream &os) const {
    os << "*** ConfigBis:" << std::endl;
    os << "EventTypes:" << std::endl;
    int c = 0;
    os << "Events (vector):" << std::endl;
    c = 0;
    for (auto const &e : as_vector()) {
        os << c++ << ' ' << *e << std::endl;
    }
    return os;
};

std::ostream &EventType::operator<<(std::ostream &os) const {
    typedef element_of<Config> config;
    typedef element_of<ConfigBis> configBis;
    os << "EventType[C: " << config::index() << "/" << config::map().size()
       << "|" << config::name() << ", CB: " << configBis::index() << "/"
       << configBis::map().size() << "|" << configBis::name()
       << "] = " << value;
    return os;
};

std::ostream &Queue::operator<<(std::ostream &os) const {
    os << "Queue[T: " << index() << "/" << map().size() << "|"
       << name() //<< ", S: "
       << "] = " << value;
    return os;
};
#endif
} // namespace marto

namespace {

class CollectionBaseTest : public TestingWithCOUT {
  protected:
    CollectionBaseTest() {}
    marto::Event::pointer e1, e2, e3, e11, e12;
    marto::QueueState::pointer qs1, qs2;
    marto::Trajectory *t1;

#ifdef TEST_MAPS
    marto::EventType::pointer et1, et2, et3, et11, et12;
    marto::Queue::pointer q1, q2;
    marto::Config *c1;
#endif
};

TEST_F(CollectionBaseTest, VectorTest) {
    {
        std::cout << "Starting vectors" << std::endl;
        marto::TrajectorySeq s;
        marto::Trajectory t;
        t1 = new marto::Trajectory();
        ASSERT_TRUE(t1);
        ASSERT_THROW(new marto::Event(10), std::logic_error);
        e1 = marto::new_shared<marto::Event>(t, s, 10);
        ASSERT_TRUE(e1);
        e2 = marto::new_shared<marto::Event>(t, s, 11);
        ASSERT_TRUE(e2);
        qs1 = marto::new_shared<marto::QueueState>(t, 21);
        ASSERT_TRUE(qs1);
        qs2 = marto::new_shared<marto::QueueState>(t, 22);
        ASSERT_TRUE(qs2);
        e3 = marto::new_shared<marto::Event>(t, s, 13);
        ASSERT_TRUE(e3);

        // should not compile
        // t.events().push_back(e1);

        e11 = marto::new_shared<marto::Event>(*t1, s, 100);
        ASSERT_TRUE(e11);
        e12 = marto::new_shared<marto::Event>(*t1, s, 101);
        ASSERT_TRUE(e12);

        std::cout << *t1;

        std::cout << t;
        std::cout << s;
    }
    std::cout << "deleteding t1" << std::endl;
    delete (t1);
    std::cout << "t1 deleted" << std::endl;
    std::string expected{R""""(Starting vectors
*** Trajectory:
Events:
- 0 Event[T: 0/2, S: 3/5] = 100
- 1 Event[T: 1/2, S: 4/5] = 101
QueueState:
*** Trajectory:
Events:
- 0 Event[T: 0/3, S: 0/5] = 10
- 1 Event[T: 1/3, S: 1/5] = 11
- 2 Event[T: 2/3, S: 2/5] = 13
QueueState:
0 QueueState[0/2] = 21
1 QueueState[1/2] = 22
*** TrajectorySeq:
Events:
- 0 Event[T: 0/3, S: 0/5] = 10
- 1 Event[T: 1/3, S: 1/5] = 11
- 2 Event[T: 2/3, S: 2/5] = 13
- 3 Event[T: 0/2, S: 3/5] = 100
- 4 Event[T: 1/2, S: 4/5] = 101
deleteding t1
t1 deleted
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}

TEST_F(CollectionBaseTest, VectorCtorExceptionTest) {
    {
        std::cout << "Starting vectors" << std::endl;
        marto::TrajectorySeq s;
        marto::Trajectory t;
        t1 = new marto::Trajectory();
        ASSERT_TRUE(t1);
        e1 = marto::new_shared<marto::Event>(t, s, 10);
        ASSERT_TRUE(e1);
        e2 = marto::new_shared<marto::Event>(t, s, 11);
        ASSERT_TRUE(e2);
        qs1 = marto::new_shared<marto::QueueState>(t, 21);
        ASSERT_TRUE(qs1);
        qs2 = marto::new_shared<marto::QueueState>(t, 22);
        ASSERT_TRUE(qs2);
        e3 = marto::new_shared<marto::Event>(t, s, 13);
        ASSERT_TRUE(e3);

        ASSERT_THROW(marto::new_shared<marto::QueueState>(t, 122),
                     marto::NotImplementedException)
            << "Registrering a queue state that fail to build!";

        ASSERT_THROW(marto::new_shared<marto::Event>(t, s, 122),
                     marto::NotImplementedException)
            << "Registrering a event that fail to build!";

        e11 = marto::new_shared<marto::Event>(*t1, s, 100);
        ASSERT_TRUE(e11);
        e12 = marto::new_shared<marto::Event>(*t1, s, 101);
        ASSERT_TRUE(e12);

        std::cout << *t1;

        std::cout << t;
        std::cout << s;
    }
    std::cout << "deleteding t1" << std::endl;
    delete (t1);
    std::cout << "t1 deleted" << std::endl;
    std::string expected{R""""(Starting vectors
*** Trajectory:
Events:
- 0 Event[T: 0/2, S: 3/5] = 100
- 1 Event[T: 1/2, S: 4/5] = 101
QueueState:
*** Trajectory:
Events:
- 0 Event[T: 0/3, S: 0/5] = 10
- 1 Event[T: 1/3, S: 1/5] = 11
- 2 Event[T: 2/3, S: 2/5] = 13
QueueState:
0 QueueState[0/2] = 21
1 QueueState[1/2] = 22
*** TrajectorySeq:
Events:
- 0 Event[T: 0/3, S: 0/5] = 10
- 1 Event[T: 1/3, S: 1/5] = 11
- 2 Event[T: 2/3, S: 2/5] = 13
- 3 Event[T: 0/2, S: 3/5] = 100
- 4 Event[T: 1/2, S: 4/5] = 101
deleteding t1
t1 deleted
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}

#ifdef TEST_MAPS
TEST_F(CollectionBaseTest, MapTest) {
    {
        std::cout << "Starting maps" << std::endl;
        marto::ConfigBis c2;
        marto::Config c;
        c1 = new marto::Config();
        ASSERT_TRUE(c1);
        ASSERT_THROW(new marto::Event(10), std::logic_error);
        et1 =
            marto::new_shared<marto::EventType>("et1..a", c, "et1..b", c2, 10);
        ASSERT_TRUE(et1);

        et2 = marto::new_shared<marto::EventType>("et2", c, c2, 11);
        ASSERT_TRUE(et2);
        q1 = marto::new_shared<marto::Queue>("q1", c, 21);
        ASSERT_TRUE(q1);
        ASSERT_THROW(marto::new_shared<marto::Queue>("q1", c, 22),
                     marto::ExistingName)
            << "Registrering a queue with an existant name!";
        q2 = marto::new_shared<marto::Queue>("q2", c, 22);
        ASSERT_TRUE(q2);
        et3 = marto::new_shared<marto::EventType>("et1.5x2", c, c2, 13);
        ASSERT_TRUE(et3);

        // should not compile
        // t.events().push_back(e1);

        et11 = marto::new_shared<marto::EventType>("et11", *c1, c2, 100);
        ASSERT_TRUE(et11);
        et12 = marto::new_shared<marto::EventType>("et12", *c1, c2, 101);
        ASSERT_TRUE(et12);
        ASSERT_EQ(c.map_of<marto::EventType>::size(), 3);
        ASSERT_EQ(c1->map_of<marto::EventType>::size(), 2);

        auto etNames = c.map_of<marto::EventType>::names();
        size_t i = 0;
        for (auto name : etNames) {
            std::cout << i++ << ": " << name << " idx: "
                      << c.map_of<marto::EventType>::at(name)
                             ->element_of<typeof c>::index()
                      << std::endl;
        }
        ASSERT_EQ(etNames[0], "et1..a");
        ASSERT_EQ(etNames[1], "et1.5x2");
        ASSERT_EQ(etNames[2], "et2");

        std::cout << *c1;

        std::cout << c;
        std::cout << c2;
    }
    std::cout << "deleteding c1" << std::endl;
    delete (c1);
    std::cout << "c1 deleted" << std::endl;
    std::string expected{R""""(Starting maps
0: et1..a idx: 0
1: et1.5x2 idx: 2
2: et2 idx: 1
*** Config:
EventTypes:
- 0 EventType[C: 0/2|et11, CB: 3/5|et11] = 100
- 1 EventType[C: 1/2|et12, CB: 4/5|et12] = 101
Queues:
*** Config:
EventTypes:
- 0 EventType[C: 0/3|et1..a, CB: 0/5|et1..b] = 10
- 1 EventType[C: 1/3|et2, CB: 1/5|et2] = 11
- 2 EventType[C: 2/3|et1.5x2, CB: 2/5|et1.5x2] = 13
Queues:
0 Queue[T: 0/2|q1] = 21
1 Queue[T: 1/2|q2] = 22
*** ConfigBis:
EventTypes:
Events (vector):
0 EventType[C: 0/3|et1..a, CB: 0/5|et1..b] = 10
1 EventType[C: 1/3|et2, CB: 1/5|et2] = 11
2 EventType[C: 2/3|et1.5x2, CB: 2/5|et1.5x2] = 13
3 EventType[C: 0/2|et11, CB: 3/5|et11] = 100
4 EventType[C: 1/2|et12, CB: 4/5|et12] = 101
deleteding c1
c1 deleted
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}

TEST_F(CollectionBaseTest, MapCtorExceptionTest) {
    {
        ASSERT_TRUE(marto::collections::internal::check_is_map<
                    marto::ConfigBis &>::value);
        std::cout << "Starting maps" << std::endl;
        marto::ConfigBis c2;
        marto::Config c;
        c1 = new marto::Config();
        ASSERT_TRUE(c1);
        et1 = marto::new_shared<marto::EventType>("et1", c, c2, 10);
        ASSERT_TRUE(et1);
        et2 = marto::new_shared<marto::EventType>("et2", c, c2, 11);
        ASSERT_TRUE(et2);
        et3 = marto::new_shared<marto::EventType>("et1.5x2", c, c2, 13);
        ASSERT_TRUE(et3);

        et11 = marto::new_shared<marto::EventType>("et11", *c1, c2, 100);
        ASSERT_TRUE(et11);
        et12 = marto::new_shared<marto::EventType>("et12", *c1, c2, 101);
        ASSERT_TRUE(et12);
        ASSERT_THROW(marto::new_shared<marto::EventType>("et2", *c1, c2, 101),
                     marto::ExistingName)
            << "Registrering a queue with an existant name!";

        using eventTypes = decltype(c)::map_of<marto::EventType>;
        ASSERT_EQ(c.eventTypes::size(), 3);
        ASSERT_EQ(c1->map_of<marto::EventType>::size(), 2);

        ASSERT_THROW(marto::new_shared<marto::Queue>("q1", c, 122),
                     marto::NotImplementedException)
            << "Registrering a queue that fail to build!";

        auto etNames = c.eventTypes::names();
        size_t i = 0;
        for (auto name : etNames) {
            std::cout << i++ << ": " << name
                      << " idx: " << c.eventTypes::at_element(name)->index()
                      << std::endl;
        }
        ASSERT_EQ(etNames[0], "et1");
        ASSERT_EQ(etNames[1], "et1.5x2");
        ASSERT_EQ(etNames[2], "et2");

        std::cout << *c1;

        std::cout << c;
        std::cout << c2;
    }
    std::cout << "deleteding c1" << std::endl;
    delete (c1);
    std::cout << "c1 deleted" << std::endl;
    std::string expected{R""""(Starting maps
0: et1 idx: 0
1: et1.5x2 idx: 2
2: et2 idx: 1
*** Config:
EventTypes:
- 0 EventType[C: 0/2|et11, CB: 3/5|et11] = 100
- 1 EventType[C: 1/2|et12, CB: 4/5|et12] = 101
Queues:
*** Config:
EventTypes:
- 0 EventType[C: 0/3|et1, CB: 0/5|et1] = 10
- 1 EventType[C: 1/3|et2, CB: 1/5|et2] = 11
- 2 EventType[C: 2/3|et1.5x2, CB: 2/5|et1.5x2] = 13
Queues:
*** ConfigBis:
EventTypes:
Events (vector):
0 EventType[C: 0/3|et1, CB: 0/5|et1] = 10
1 EventType[C: 1/3|et2, CB: 1/5|et2] = 11
2 EventType[C: 2/3|et1.5x2, CB: 2/5|et1.5x2] = 13
3 EventType[C: 0/2|et11, CB: 3/5|et11] = 100
4 EventType[C: 1/2|et12, CB: 4/5|et12] = 101
deleteding c1
c1 deleted
)""""};
    std::string actual{get_buffer()};
    EXPECT_EQ(expected, actual);
}
#endif

TEST_F(CollectionBaseTest, MakeSharedInheriredTest) {
    {
        using namespace inherit;
        void(0); /* for correct auto indentation... */
        {
            auto sim = marto::new_shared<::inherit::Simulation>();
            auto tt = marto::new_shared<TransitionTest>("TT", *sim);
            static_assert(
                std::is_same_v<decltype(tt), std::shared_ptr<TransitionTest>>);
            std::cout << marto::type_name<decltype(tt)>() << std::endl;
            // new TransitionTest();
        }
    }
    {
        std::string expected{R""""(std::shared_ptr<inherit::TransitionTest>
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }
    {
        using namespace inherit2;
        void(0); /* for correct auto indentation... */
        {
            auto sim = marto::new_shared<::inherit2::Simulation>();
            auto tt = marto::new_shared<TransitionTest>("TT", *sim);
            static_assert(
                std::is_same_v<decltype(tt), std::shared_ptr<TransitionTest>>);
            std::cout << marto::type_name<decltype(tt)>() << std::endl;
            // new TransitionTest();
        }
    }
    {
        std::string expected{R""""(std::shared_ptr<inherit2::TransitionTest>
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }
}

TEST_F(CollectionBaseTest, MakeSharedStaticAllocTest) {
    {
        using namespace static_alloc;
        void(0); /* for correct auto indentation... */
        {
            auto params = new_shared<Parameters>();
            std::cout << marto::type_name<decltype(params)>() << std::endl;
            std::cout << params->map_of<Param>::size();
            for (auto const &p : params->map_of<Param>::names()) {
                std::cout << ", " << p;
            }
            std::cout << std::endl;
            // new TransitionTest();
        }
    }
    {
        std::string expected{R""""(std::shared_ptr<static_alloc::Parameters>
2, p1name, p2name
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }
}

TEST_F(CollectionBaseTest, MakeSharedArroundCTor) {
    {
        using namespace arround_ctor;
        void(0); /* for correct auto indentation... */
        {
            auto params = new_shared<Parameters>();
            marto::new_shared<Param>("p1name", *params);
            marto::new_shared<Param>("p2name", *params);
            std::cout << marto::type_name<decltype(params)>() << std::endl;
            std::cout << params->map_of<Param>::size();
            for (auto const &p : params->map_of<Param>::names()) {
                std::cout << ", " << p;
            }
            std::cout << std::endl;

            for (auto const &p : params->map_of<Param>::as_vector()) {
                shared_ptr<Param> sp(p->wparam_);
                EXPECT_EQ(sp.get(), p.get());
            }
            // new TransitionTest();
        }
    }
    {
        std::string expected{R""""(std::shared_ptr<arround_ctor::Parameters>
2, p1name, p2name
)""""};
        std::string actual{get_buffer()};
        EXPECT_EQ(expected, actual);
    }
}

} // namespace

#endif
